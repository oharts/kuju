require 'test_helper'

class SsessionsControllerTest < ActionController::TestCase
  setup do
    @ssession = ssessions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ssessions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ssession" do
    assert_difference('Ssession.count') do
      post :create, ssession: { date: @ssession.date, reservation_id: @ssession.reservation_id, time: @ssession.time }
    end

    assert_redirected_to ssession_path(assigns(:ssession))
  end

  test "should show ssession" do
    get :show, id: @ssession
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ssession
    assert_response :success
  end

  test "should update ssession" do
    patch :update, id: @ssession, ssession: { date: @ssession.date, reservation_id: @ssession.reservation_id, time: @ssession.time }
    assert_redirected_to ssession_path(assigns(:ssession))
  end

  test "should destroy ssession" do
    assert_difference('Ssession.count', -1) do
      delete :destroy, id: @ssession
    end

    assert_redirected_to ssessions_path
  end
end
