require 'test_helper'

class GroupSessionsControllerTest < ActionController::TestCase
  setup do
    @group_session = group_sessions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:group_sessions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create group_session" do
    assert_difference('GroupSession.count') do
      post :create, group_session: { capacity: @group_session.capacity, description: @group_session.description, expert_profile_id: @group_session.expert_profile_id, price: @group_session.price, title: @group_session.title }
    end

    assert_redirected_to group_session_path(assigns(:group_session))
  end

  test "should show group_session" do
    get :show, id: @group_session
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @group_session
    assert_response :success
  end

  test "should update group_session" do
    patch :update, id: @group_session, group_session: { capacity: @group_session.capacity, description: @group_session.description, expert_profile_id: @group_session.expert_profile_id, price: @group_session.price, title: @group_session.title }
    assert_redirected_to group_session_path(assigns(:group_session))
  end

  test "should destroy group_session" do
    assert_difference('GroupSession.count', -1) do
      delete :destroy, id: @group_session
    end

    assert_redirected_to group_sessions_path
  end
end
