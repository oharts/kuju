require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post :create, profile: { about: @profile.about, add1: @profile.add1, add2: @profile.add2, ages_suitable: @profile.ages_suitable, availability: @profile.availability, avatar: @profile.avatar, city: @profile.city, credentials: @profile.credentials, display_name: @profile.display_name, experience: @profile.experience, expert_type: @profile.expert_type, first_name_text: @profile.first_name_text, foo: @profile.foo, last_name_text: @profile.last_name_text, price: @profile.price, session_length: @profile.session_length, skill_levels: @profile.skill_levels, state: @profile.state, studio_facility: @profile.studio_facility, travel: @profile.travel, zip: @profile.zip }
    end

    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should show profile" do
    get :show, id: @profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profile
    assert_response :success
  end

  test "should update profile" do
    patch :update, id: @profile, profile: { about: @profile.about, add1: @profile.add1, add2: @profile.add2, ages_suitable: @profile.ages_suitable, availability: @profile.availability, avatar: @profile.avatar, city: @profile.city, credentials: @profile.credentials, display_name: @profile.display_name, experience: @profile.experience, expert_type: @profile.expert_type, first_name_text: @profile.first_name_text, foo: @profile.foo, last_name_text: @profile.last_name_text, price: @profile.price, session_length: @profile.session_length, skill_levels: @profile.skill_levels, state: @profile.state, studio_facility: @profile.studio_facility, travel: @profile.travel, zip: @profile.zip }
    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete :destroy, id: @profile
    end

    assert_redirected_to profiles_path
  end
end
