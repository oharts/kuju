require 'test_helper'

class ExpertProfilesControllerTest < ActionController::TestCase
  setup do
    @expert_profile = expert_profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:expert_profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create expert_profile" do
    assert_difference('ExpertProfile.count') do
      post :create, expert_profile: { about: @expert_profile.about, add1: @expert_profile.add1, add2: @expert_profile.add2, ages_suitable: @expert_profile.ages_suitable, availability: @expert_profile.availability, avatar: @expert_profile.avatar, city: @expert_profile.city, credentials: @expert_profile.credentials, display_name: @expert_profile.display_name, experience: @expert_profile.experience, expert_type: @expert_profile.expert_type, first_name_text: @expert_profile.first_name_text, foo: @expert_profile.foo, last_name_text: @expert_profile.last_name_text, listed: @expert_profile.listed, price: @expert_profile.price, session_length: @expert_profile.session_length, skill_levels: @expert_profile.skill_levels, state: @expert_profile.state, studio_facility: @expert_profile.studio_facility, travel: @expert_profile.travel, user_id: @expert_profile.user_id, zip: @expert_profile.zip }
    end

    assert_redirected_to expert_profile_path(assigns(:expert_profile))
  end

  test "should show expert_profile" do
    get :show, id: @expert_profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @expert_profile
    assert_response :success
  end

  test "should update expert_profile" do
    patch :update, id: @expert_profile, expert_profile: { about: @expert_profile.about, add1: @expert_profile.add1, add2: @expert_profile.add2, ages_suitable: @expert_profile.ages_suitable, availability: @expert_profile.availability, avatar: @expert_profile.avatar, city: @expert_profile.city, credentials: @expert_profile.credentials, display_name: @expert_profile.display_name, experience: @expert_profile.experience, expert_type: @expert_profile.expert_type, first_name_text: @expert_profile.first_name_text, foo: @expert_profile.foo, last_name_text: @expert_profile.last_name_text, listed: @expert_profile.listed, price: @expert_profile.price, session_length: @expert_profile.session_length, skill_levels: @expert_profile.skill_levels, state: @expert_profile.state, studio_facility: @expert_profile.studio_facility, travel: @expert_profile.travel, user_id: @expert_profile.user_id, zip: @expert_profile.zip }
    assert_redirected_to expert_profile_path(assigns(:expert_profile))
  end

  test "should destroy expert_profile" do
    assert_difference('ExpertProfile.count', -1) do
      delete :destroy, id: @expert_profile
    end

    assert_redirected_to expert_profiles_path
  end
end
