class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
before_action :authenticate_user!
  # GET /accounts
  # GET /accounts.json
  def index
    if current_user.type=="Buyer" && current_user.admin_check !="true"
            @future=0
            @past=0
            @canceled=0
          current_user.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.date>=Date.today.to_s
            @future=1
          end
          end
          end
          current_user.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.date<Date.today.to_s
            @past=1
          end
          end
          end
          current_user.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.canceled=="true"
            @canceled=1
          end
          end
          end
         render  "buyer_reservation" 
  elsif current_user.type=="Expert" && current_user.admin_check !="true"
           @futureex=0
            @pastex=0
            @canceledex=0
            if current_user.present?
          current_user.expert_profile.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.date>=Date.today.to_s
            @futureex=1
          end
          end
          end
          current_user.expert_profile.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.date<Date.today.to_s
            @pastex=1
          end
          end
          end
          current_user.expert_profile.reservations.each do |reservation| 
          reservation.ssessions.each do |ssession|
          if ssession.canceled=="true"
            @canceledex=1
          end
          end
          end
        end
       render  "expert_reservation" 
 elsif current_user.admin_check=="true"
        @exp=User.experts
        @byr=User.buyers
        render  "admin_panel"
  end
   

  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:first_name, :last_name, :email, :uid)
    end
end
