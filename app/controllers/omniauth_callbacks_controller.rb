class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def all
    user = Buyer.from_omniauth(request.env["omniauth.auth"])
    if user.persisted?
      flash.notice = "Signed in!"
      sign_in_and_redirect user, :event => :authentication
    else
      session["devise.user_attributes"] = user.attributes
      redirect_to new_buyer_registration_path
    end
  end
  alias_method :facebook, :all
  alias_method :google_oauth2, :all
end