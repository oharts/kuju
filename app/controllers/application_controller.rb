class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  respond_to :html, :json 
  devise_group :user, contains: [:buyer, :expert] 
 before_filter :update_sanitized_params, if: :devise_controller?
before_filter :set_search

def update_sanitized_params
  devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:born_at, :email_preferences,:profile_id, :provider, :uid, :add1, :add2, :city, :state, :zip, :phone, :npi, :sex, :first_name, :last_name, :email, :current_password, :password, :password_confirmation, :type)}
   devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:born_at, :email_preferences, :profile_id, :add1, :add2, :city, :state, :zip, :phone, :npi, :sex, :first_name, :last_name, :email, :current_password, :password, :password_confirmation, :type)}
end
 def after_sign_up_path_for(resource)
    account_path
  end
  
    def set_search
        @q=ExpertProfile.ransack(params[:q])
    end
    
 protected
  def authenticate_user2!
       if user_signed_in?
      return
    else
    redirect_to root_path, :_method => 'get'
end
  end
    
end
