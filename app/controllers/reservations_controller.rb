class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
before_action :authenticate_user!
  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
   redirect_to accounts_url
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
    @p=ExpertProfile.find(params[:expert])
   
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
     

  Reservation.make_payment(params)
   @reservation = Reservation.new
   @reservation.email=current_user.email
   @reservation.user_id=current_user.id
    @reservation.expert_profile_id=params[:expert]
  @reservation.save
  params[:session].each do |numb|
  @ssession=Ssession.new
     numb.each do |numbe| 
  @ssession.date= numbe["date"]
  @ssession.time= numbe["time"]
  @ssession.reservation_id=@reservation.id
end
 @ssession.save
end
  

 
  
  
    

    respond_to do |format|
      if @reservation.save
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:email, :token, :user_id, :expert_profile_id)
    end
end
