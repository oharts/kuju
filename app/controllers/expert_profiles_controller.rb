class ExpertProfilesController < ApplicationController
  before_action :set_expert_profile, only: [:show, :edit, :update, :destroy]
before_action :authenticate_user2!, only: [ :edit, :update, :destroy]
  # GET /expert_profiles
  # GET /expert_profiles.json
  def index
  if params[:etype]
    
   @expert_profiles=ExpertProfile.where(expert_type: params[:etype])
  else
 @q=ExpertProfile.ransack(params[:q])
   @expert_profiles = @q.result(distinct: true)
 end
  end

  # GET /expert_profiles/1
  # GET /expert_profiles/1.json
  def show
    @group_sessions= @expert_profile.group_sessions
  end

  # GET /expert_profiles/new
  def new
    @expert_profile = ExpertProfile.new
    
  end

  # GET /expert_profiles/1/edit
  def edit
       @expert_profile.attachments.build
       @group_sessions=current_user.expert_profile.group_sessions
        
  end

  # POST /expert_profiles
  # POST /expert_profiles.json
  def create
    @expert_profile = ExpertProfile.new(expert_profile_params)

    respond_to do |format|
      if @expert_profile.save
        if params[:images]
        
        image=params[:images][0]
          @expert_profile.attachments.create(image: image)
     end
    
        if params[:medias]
        
        params[:medias].each { |media|
          @expert_profile.attachments.create(media: media)
        }
      end
        format.html { redirect_to @expert_profile, notice: 'Expert profile was successfully created.' }
        format.json { render :show, status: :created, location: @expert_profile }
      else
        format.html { render :new }
        format.json { render json: @expert_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /expert_profiles/1
  # PATCH/PUT /expert_profiles/1.json
  def update
    respond_to do |format|
      if @expert_profile.update(expert_profile_params)
        if params[:images]
        
        params[:images].each { |image|
          @expert_profile.attachments.create(image: image)
        } 
       
      end
      if params[:medias]
        
        params[:medias].each { |media|
          @expert_profile.attachments.create(media: media)
        }
      
      end
        format.html { redirect_to @expert_profile, notice: 'Expert profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @expert_profile }
      else
        format.html { render :edit }
        format.json { render json: @expert_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /expert_profiles/1
  # DELETE /expert_profiles/1.json
  def destroy
    @expert_profile.destroy
    respond_to do |format|
      format.html { redirect_to expert_profiles_url, notice: 'Expert profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expert_profile
      @expert_profile = ExpertProfile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expert_profile_params
      params.require(:expert_profile).permit(:q, :images, :medias, :add1, :add2, :city, :state, :zip, :avatar, :first_name_text, :last_name_text, :display_name, :expert_type, :foo, :availability, :about, :price, :session_length, :experience, :ages_suitable, :skill_levels, :travel, :studio_facility, :credentials, :listed, :user_id, attachments_attributes: [:id, :image, :media, :expert_profile_id, :image_file_name, :image_content_type, :image_file_size, :image_updated_at,:media_file_name, :media_content_type, :media_file_size, :media_updated_at, :_destroy])
    end
end
