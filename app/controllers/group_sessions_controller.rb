class GroupSessionsController < ApplicationController
  before_action :set_group_session, only: [:show, :edit, :update, :destroy]

  # GET /group_sessions
  # GET /group_sessions.json
  def index
    @group_sessions = GroupSession.all
  end

  # GET /group_sessions/1
  # GET /group_sessions/1.json
  def show
  end

  # GET /group_sessions/new
  def new
    @group_session = GroupSession.new
  end

  # GET /group_sessions/1/edit
  def edit
  end

  # POST /group_sessions
  # POST /group_sessions.json
  def create
    @group_session = GroupSession.new(group_session_params)

    respond_to do |format|
      if @group_session.save
         
        format.html { redirect_to @group_session, notice: 'Group session was successfully created.' }
        format.json { render :show, status: :created, location: @group_session }
      else
        format.html { render :new }
        format.json { render json: @group_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_sessions/1
  # PATCH/PUT /group_sessions/1.json
  def update
    respond_to do |format|
    
      if @group_session.update(group_session_params)
         
        format.html { redirect_to @group_session, notice: 'Group session was successfully updated.' }
        format.json { render :show, status: :ok, location: @group_session }
      else
        format.html { render :edit }
        format.json { render json: @group_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_sessions/1
  # DELETE /group_sessions/1.json
  def destroy
    @group_session.destroy
    respond_to do |format|
      format.html { redirect_to "#{edit_expert_profile_path(@group_session.expert_profile)}#group_sessions", notice: 'Group session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_session
      @group_session = GroupSession.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_session_params
      params.require(:group_session).permit(:title, :price, :capacity, :description, :expert_profile_id).merge(expert_profile_id: current_user.expert_profile_id)
    end
end
