class SsessionsController < ApplicationController
  before_action :set_ssession, only: [:show, :edit, :update, :destroy]
before_action :authenticate_user!
  # GET /ssessions
  # GET /ssessions.json
  def index
    @ssessions = Ssession.all
  end

  # GET /ssessions/1
  # GET /ssessions/1.json
  def show
  end

  # GET /ssessions/new
  def new
    @ssession = Ssession.new
  end

  # GET /ssessions/1/edit
  def edit
  end

  # POST /ssessions
  # POST /ssessions.json
  def create
    @ssession = Ssession.new(ssession_params)
   
    respond_to do |format|
      if @ssession.save
        format.html { redirect_to @ssession, notice: 'Ssession was successfully created.' }
        format.json { render :show, status: :created, location: @ssession }
      else
        format.html { render :new }
        format.json { render json: @ssession.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ssessions/1
  # PATCH/PUT /ssessions/1.json
  def update
    respond_to do |format|
      if @ssession.update(ssession_params)
        format.html { redirect_to accounts_url}
        format.json { render :show, status: :ok, location: @ssession }
      else
        format.html { render :edit }
        format.json { render json: @ssession.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ssessions/1
  # DELETE /ssessions/1.json
  def destroy
    @ssession.destroy
    respond_to do |format|
      format.html { redirect_to ssessions_url, notice: 'Ssession was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ssession
      @ssession = Ssession.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ssession_params
      params.require(:ssession).permit(:reservation_id, :date, :time, :user_id, :expert_profile_id, :canceled)
    end
end
