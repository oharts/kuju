class UsersController < ApplicationController

    before_action :set_user2, only: [:update, :destroy]

 def show
  end


    def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to accounts_url}
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user2
   
      @user = User.find(params[:id])
    
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:admin_check, :born_at, :email_preferences, :profile_id, :add1, :add2, :city, :state, :zip, :phone, :npi, :sex, :first_name, :last_name, :email, :current_password, :password, :password_confirmation, :type)
    end
    
end