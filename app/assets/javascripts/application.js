// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require bxslider
//= require twitter/bootstrap
//= require bootstrap.js.coffee
//= require selectize
//= require_tree .

! function(e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    function i(e) {
        var t = e.length,
            i = st.type(e);
        return "function" === i || st.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === i || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
    }

    function n(e, t, i) {
        if (st.isFunction(t)) return st.grep(e, function(e, n) {
            return !!t.call(e, n, e) !== i
        });
        if (t.nodeType) return st.grep(e, function(e) {
            return e === t !== i
        });
        if ("string" == typeof t) {
            if (ht.test(t)) return st.filter(t, e, i);
            t = st.filter(t, e)
        }
        return st.grep(e, function(e) {
            return st.inArray(e, t) >= 0 !== i
        })
    }

    function s(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function o(e) {
        var t = wt[e] = {};
        return st.each(e.match(bt) || [], function(e, i) {
            t[i] = !0
        }), t
    }

    function r() {
        ft.addEventListener ? (ft.removeEventListener("DOMContentLoaded", a, !1), e.removeEventListener("load", a, !1)) : (ft.detachEvent("onreadystatechange", a), e.detachEvent("onload", a))
    }

    function a() {
        (ft.addEventListener || "load" === event.type || "complete" === ft.readyState) && (r(), st.ready())
    }

    function l(e, t, i) {
        if (void 0 === i && 1 === e.nodeType) {
            var n = "data-" + t.replace(St, "-$1").toLowerCase();
            if (i = e.getAttribute(n), "string" == typeof i) {
                try {
                    i = "true" === i ? !0 : "false" === i ? !1 : "null" === i ? null : +i + "" === i ? +i : _t.test(i) ? st.parseJSON(i) : i
                } catch (s) {}
                st.data(e, t, i)
            } else i = void 0
        }
        return i
    }

    function c(e) {
        var t;
        for (t in e)
            if (("data" !== t || !st.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function d(e, t, i, n) {
        if (st.acceptData(e)) {
            var s, o, r = st.expando,
                a = e.nodeType,
                l = a ? st.cache : e,
                c = a ? e[r] : e[r] && r;
            if (c && l[c] && (n || l[c].data) || void 0 !== i || "string" != typeof t) return c || (c = a ? e[r] = X.pop() || st.guid++ : r), l[c] || (l[c] = a ? {} : {
                toJSON: st.noop
            }), ("object" == typeof t || "function" == typeof t) && (n ? l[c] = st.extend(l[c], t) : l[c].data = st.extend(l[c].data, t)), o = l[c], n || (o.data || (o.data = {}), o = o.data), void 0 !== i && (o[st.camelCase(t)] = i), "string" == typeof t ? (s = o[t], null == s && (s = o[st.camelCase(t)])) : s = o, s
        }
    }

    function u(e, t, i) {
        if (st.acceptData(e)) {
            var n, s, o = e.nodeType,
                r = o ? st.cache : e,
                a = o ? e[st.expando] : st.expando;
            if (r[a]) {
                if (t && (n = i ? r[a] : r[a].data)) {
                    st.isArray(t) ? t = t.concat(st.map(t, st.camelCase)) : t in n ? t = [t] : (t = st.camelCase(t), t = t in n ? [t] : t.split(" ")), s = t.length;
                    for (; s--;) delete n[t[s]];
                    if (i ? !c(n) : !st.isEmptyObject(n)) return
                }(i || (delete r[a].data, c(r[a]))) && (o ? st.cleanData([e], !0) : it.deleteExpando || r != r.window ? delete r[a] : r[a] = null)
            }
        }
    }

    function h() {
        return !0
    }

    function p() {
        return !1
    }

    function f() {
        try {
            return ft.activeElement
        } catch (e) {}
    }

    function m(e) {
        var t = Pt.split("|"),
            i = e.createDocumentFragment();
        if (i.createElement)
            for (; t.length;) i.createElement(t.pop());
        return i
    }

    function g(e, t) {
        var i, n, s = 0,
            o = typeof e.getElementsByTagName !== kt ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== kt ? e.querySelectorAll(t || "*") : void 0;
        if (!o)
            for (o = [], i = e.childNodes || e; null != (n = i[s]); s++) !t || st.nodeName(n, t) ? o.push(n) : st.merge(o, g(n, t));
        return void 0 === t || t && st.nodeName(e, t) ? st.merge([e], o) : o
    }

    function v(e) {
        Ot.test(e.type) && (e.defaultChecked = e.checked)
    }

    function y(e, t) {
        return st.nodeName(e, "table") && st.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function b(e) {
        return e.type = (null !== st.find.attr(e, "type")) + "/" + e.type, e
    }

    function w(e) {
        var t = Gt.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function x(e, t) {
        for (var i, n = 0; null != (i = e[n]); n++) st._data(i, "globalEval", !t || st._data(t[n], "globalEval"))
    }

    function $(e, t) {
        if (1 === t.nodeType && st.hasData(e)) {
            var i, n, s, o = st._data(e),
                r = st._data(t, o),
                a = o.events;
            if (a) {
                delete r.handle, r.events = {};
                for (i in a)
                    for (n = 0, s = a[i].length; s > n; n++) st.event.add(t, i, a[i][n])
            }
            r.data && (r.data = st.extend({}, r.data))
        }
    }

    function k(e, t) {
        var i, n, s;
        if (1 === t.nodeType) {
            if (i = t.nodeName.toLowerCase(), !it.noCloneEvent && t[st.expando]) {
                s = st._data(t);
                for (n in s.events) st.removeEvent(t, n, s.handle);
                t.removeAttribute(st.expando)
            }
            "script" === i && t.text !== e.text ? (b(t).text = e.text, w(t)) : "object" === i ? (t.parentNode && (t.outerHTML = e.outerHTML), it.html5Clone && e.innerHTML && !st.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === i && Ot.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === i ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === i || "textarea" === i) && (t.defaultValue = e.defaultValue)
        }
    }

    function _(t, i) {
        var n, s = st(i.createElement(t)).appendTo(i.body),
            o = e.getDefaultComputedStyle && (n = e.getDefaultComputedStyle(s[0])) ? n.display : st.css(s[0], "display");
        return s.detach(), o
    }

    function S(e) {
        var t = ft,
            i = Zt[e];
        return i || (i = _(e, t), "none" !== i && i || (Kt = (Kt || st("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Kt[0].contentWindow || Kt[0].contentDocument).document, t.write(), t.close(), i = _(e, t), Kt.detach()), Zt[e] = i), i
    }

    function C(e, t) {
        return {
            get: function() {
                var i = e();
                if (null != i) return i ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function T(e, t) {
        if (t in e) return t;
        for (var i = t.charAt(0).toUpperCase() + t.slice(1), n = t, s = hi.length; s--;)
            if (t = hi[s] + i, t in e) return t;
        return n
    }

    function D(e, t) {
        for (var i, n, s, o = [], r = 0, a = e.length; a > r; r++) n = e[r], n.style && (o[r] = st._data(n, "olddisplay"), i = n.style.display, t ? (o[r] || "none" !== i || (n.style.display = ""), "" === n.style.display && Dt(n) && (o[r] = st._data(n, "olddisplay", S(n.nodeName)))) : (s = Dt(n), (i && "none" !== i || !s) && st._data(n, "olddisplay", s ? i : st.css(n, "display"))));
        for (r = 0; a > r; r++) n = e[r], n.style && (t && "none" !== n.style.display && "" !== n.style.display || (n.style.display = t ? o[r] || "" : "none"));
        return e
    }

    function E(e, t, i) {
        var n = li.exec(t);
        return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : t
    }

    function O(e, t, i, n, s) {
        for (var o = i === (n ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > o; o += 2) "margin" === i && (r += st.css(e, i + Tt[o], !0, s)), n ? ("content" === i && (r -= st.css(e, "padding" + Tt[o], !0, s)), "margin" !== i && (r -= st.css(e, "border" + Tt[o] + "Width", !0, s))) : (r += st.css(e, "padding" + Tt[o], !0, s), "padding" !== i && (r += st.css(e, "border" + Tt[o] + "Width", !0, s)));
        return r
    }

    function A(e, t, i) {
        var n = !0,
            s = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = ei(e),
            r = it.boxSizing && "border-box" === st.css(e, "boxSizing", !1, o);
        if (0 >= s || null == s) {
            if (s = ti(e, t, o), (0 > s || null == s) && (s = e.style[t]), ni.test(s)) return s;
            n = r && (it.boxSizingReliable() || s === e.style[t]), s = parseFloat(s) || 0
        }
        return s + O(e, t, i || (r ? "border" : "content"), n, o) + "px"
    }

    function M(e, t, i, n, s) {
        return new M.prototype.init(e, t, i, n, s)
    }

    function I() {
        return setTimeout(function() {
            pi = void 0
        }), pi = st.now()
    }

    function N(e, t) {
        var i, n = {
                height: e
            },
            s = 0;
        for (t = t ? 1 : 0; 4 > s; s += 2 - t) i = Tt[s], n["margin" + i] = n["padding" + i] = e;
        return t && (n.opacity = n.width = e), n
    }

    function j(e, t, i) {
        for (var n, s = (bi[t] || []).concat(bi["*"]), o = 0, r = s.length; r > o; o++)
            if (n = s[o].call(i, t, e)) return n
    }

    function P(e, t, i) {
        var n, s, o, r, a, l, c, d, u = this,
            h = {},
            p = e.style,
            f = e.nodeType && Dt(e),
            m = st._data(e, "fxshow");
        i.queue || (a = st._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function() {
            a.unqueued || l()
        }), a.unqueued++, u.always(function() {
            u.always(function() {
                a.unqueued--, st.queue(e, "fx").length || a.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (i.overflow = [p.overflow, p.overflowX, p.overflowY], c = st.css(e, "display"), d = "none" === c ? st._data(e, "olddisplay") || S(e.nodeName) : c, "inline" === d && "none" === st.css(e, "float") && (it.inlineBlockNeedsLayout && "inline" !== S(e.nodeName) ? p.zoom = 1 : p.display = "inline-block")), i.overflow && (p.overflow = "hidden", it.shrinkWrapBlocks() || u.always(function() {
            p.overflow = i.overflow[0], p.overflowX = i.overflow[1], p.overflowY = i.overflow[2]
        }));
        for (n in t)
            if (s = t[n], mi.exec(s)) {
                if (delete t[n], o = o || "toggle" === s, s === (f ? "hide" : "show")) {
                    if ("show" !== s || !m || void 0 === m[n]) continue;
                    f = !0
                }
                h[n] = m && m[n] || st.style(e, n)
            } else c = void 0;
        if (st.isEmptyObject(h)) "inline" === ("none" === c ? S(e.nodeName) : c) && (p.display = c);
        else {
            m ? "hidden" in m && (f = m.hidden) : m = st._data(e, "fxshow", {}), o && (m.hidden = !f), f ? st(e).show() : u.done(function() {
                st(e).hide()
            }), u.done(function() {
                var t;
                st._removeData(e, "fxshow");
                for (t in h) st.style(e, t, h[t])
            });
            for (n in h) r = j(f ? m[n] : 0, n, u), n in m || (m[n] = r.start, f && (r.end = r.start, r.start = "width" === n || "height" === n ? 1 : 0))
        }
    }

    function F(e, t) {
        var i, n, s, o, r;
        for (i in e)
            if (n = st.camelCase(i), s = t[n], o = e[i], st.isArray(o) && (s = o[1], o = e[i] = o[0]), i !== n && (e[n] = o, delete e[i]), r = st.cssHooks[n], r && "expand" in r) {
                o = r.expand(o), delete e[n];
                for (i in o) i in e || (e[i] = o[i], t[i] = s)
            } else t[n] = s
    }

    function H(e, t, i) {
        var n, s, o = 0,
            r = yi.length,
            a = st.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (s) return !1;
                for (var t = pi || I(), i = Math.max(0, c.startTime + c.duration - t), n = i / c.duration || 0, o = 1 - n, r = 0, l = c.tweens.length; l > r; r++) c.tweens[r].run(o);
                return a.notifyWith(e, [c, o, i]), 1 > o && l ? i : (a.resolveWith(e, [c]), !1)
            },
            c = a.promise({
                elem: e,
                props: st.extend({}, t),
                opts: st.extend(!0, {
                    specialEasing: {}
                }, i),
                originalProperties: t,
                originalOptions: i,
                startTime: pi || I(),
                duration: i.duration,
                tweens: [],
                createTween: function(t, i) {
                    var n = st.Tween(e, c.opts, t, i, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(n), n
                },
                stop: function(t) {
                    var i = 0,
                        n = t ? c.tweens.length : 0;
                    if (s) return this;
                    for (s = !0; n > i; i++) c.tweens[i].run(1);
                    return t ? a.resolveWith(e, [c, t]) : a.rejectWith(e, [c, t]), this
                }
            }),
            d = c.props;
        for (F(d, c.opts.specialEasing); r > o; o++)
            if (n = yi[o].call(c, e, d, c.opts)) return n;
        return st.map(d, j, c), st.isFunction(c.opts.start) && c.opts.start.call(e, c), st.fx.timer(st.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function L(e) {
        return function(t, i) {
            "string" != typeof t && (i = t, t = "*");
            var n, s = 0,
                o = t.toLowerCase().match(bt) || [];
            if (st.isFunction(i))
                for (; n = o[s++];) "+" === n.charAt(0) ? (n = n.slice(1) || "*", (e[n] = e[n] || []).unshift(i)) : (e[n] = e[n] || []).push(i)
        }
    }

    function z(e, t, i, n) {
        function s(a) {
            var l;
            return o[a] = !0, st.each(e[a] || [], function(e, a) {
                var c = a(t, i, n);
                return "string" != typeof c || r || o[c] ? r ? !(l = c) : void 0 : (t.dataTypes.unshift(c), s(c), !1)
            }), l
        }
        var o = {},
            r = e === Ri;
        return s(t.dataTypes[0]) || !o["*"] && s("*")
    }

    function q(e, t) {
        var i, n, s = st.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((s[n] ? e : i || (i = {}))[n] = t[n]);
        return i && st.extend(!0, e, i), e
    }

    function W(e, t, i) {
        for (var n, s, o, r, a = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === s && (s = e.mimeType || t.getResponseHeader("Content-Type"));
        if (s)
            for (r in a)
                if (a[r] && a[r].test(s)) {
                    l.unshift(r);
                    break
                }
        if (l[0] in i) o = l[0];
        else {
            for (r in i) {
                if (!l[0] || e.converters[r + " " + l[0]]) {
                    o = r;
                    break
                }
                n || (n = r)
            }
            o = o || n
        }
        return o ? (o !== l[0] && l.unshift(o), i[o]) : void 0
    }

    function R(e, t, i, n) {
        var s, o, r, a, l, c = {},
            d = e.dataTypes.slice();
        if (d[1])
            for (r in e.converters) c[r.toLowerCase()] = e.converters[r];
        for (o = d.shift(); o;)
            if (e.responseFields[o] && (i[e.responseFields[o]] = t), !l && n && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = o, o = d.shift())
                if ("*" === o) o = l;
                else if ("*" !== l && l !== o) {
            if (r = c[l + " " + o] || c["* " + o], !r)
                for (s in c)
                    if (a = s.split(" "), a[1] === o && (r = c[l + " " + a[0]] || c["* " + a[0]])) {
                        r === !0 ? r = c[s] : c[s] !== !0 && (o = a[0], d.unshift(a[1]));
                        break
                    }
            if (r !== !0)
                if (r && e["throws"]) t = r(t);
                else try {
                    t = r(t)
                } catch (u) {
                    return {
                        state: "parsererror",
                        error: r ? u : "No conversion from " + l + " to " + o
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }

    function B(e, t, i, n) {
        var s;
        if (st.isArray(t)) st.each(t, function(t, s) {
            i || Gi.test(e) ? n(e, s) : B(e + "[" + ("object" == typeof s ? t : "") + "]", s, i, n)
        });
        else if (i || "object" !== st.type(t)) n(e, t);
        else
            for (s in t) B(e + "[" + s + "]", t[s], i, n)
    }

    function U() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {}
    }

    function Y() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }

    function G(e) {
        return st.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }
    var X = [],
        V = X.slice,
        Q = X.concat,
        J = X.push,
        K = X.indexOf,
        Z = {},
        et = Z.toString,
        tt = Z.hasOwnProperty,
        it = {},
        nt = "1.11.1",
        st = function(e, t) {
            return new st.fn.init(e, t)
        },
        ot = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        rt = /^-ms-/,
        at = /-([\da-z])/gi,
        lt = function(e, t) {
            return t.toUpperCase()
        };
    st.fn = st.prototype = {
        jquery: nt,
        constructor: st,
        selector: "",
        length: 0,
        toArray: function() {
            return V.call(this)
        },
        get: function(e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : V.call(this)
        },
        pushStack: function(e) {
            var t = st.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return st.each(this, e, t)
        },
        map: function(e) {
            return this.pushStack(st.map(this, function(t, i) {
                return e.call(t, i, t)
            }))
        },
        slice: function() {
            return this.pushStack(V.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                i = +e + (0 > e ? t : 0);
            return this.pushStack(i >= 0 && t > i ? [this[i]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: J,
        sort: X.sort,
        splice: X.splice
    }, st.extend = st.fn.extend = function() {
        var e, t, i, n, s, o, r = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof r && (c = r, r = arguments[a] || {}, a++), "object" == typeof r || st.isFunction(r) || (r = {}), a === l && (r = this, a--); l > a; a++)
            if (null != (s = arguments[a]))
                for (n in s) e = r[n], i = s[n], r !== i && (c && i && (st.isPlainObject(i) || (t = st.isArray(i))) ? (t ? (t = !1, o = e && st.isArray(e) ? e : []) : o = e && st.isPlainObject(e) ? e : {}, r[n] = st.extend(c, o, i)) : void 0 !== i && (r[n] = i));
        return r
    }, st.extend({
        expando: "jQuery" + (nt + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isFunction: function(e) {
            return "function" === st.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === st.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !st.isArray(e) && e - parseFloat(e) >= 0
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        isPlainObject: function(e) {
            var t;
            if (!e || "object" !== st.type(e) || e.nodeType || st.isWindow(e)) return !1;
            try {
                if (e.constructor && !tt.call(e, "constructor") && !tt.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (i) {
                return !1
            }
            if (it.ownLast)
                for (t in e) return tt.call(e, t);
            for (t in e);
            return void 0 === t || tt.call(e, t)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? Z[et.call(e)] || "object" : typeof e
        },
        globalEval: function(t) {
            t && st.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(rt, "ms-").replace(at, lt)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, n) {
            var s, o = 0,
                r = e.length,
                a = i(e);
            if (n) {
                if (a)
                    for (; r > o && (s = t.apply(e[o], n), s !== !1); o++);
                else
                    for (o in e)
                        if (s = t.apply(e[o], n), s === !1) break
            } else if (a)
                for (; r > o && (s = t.call(e[o], o, e[o]), s !== !1); o++);
            else
                for (o in e)
                    if (s = t.call(e[o], o, e[o]), s === !1) break; return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(ot, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? st.merge(n, "string" == typeof e ? [e] : e) : J.call(n, e)), n
        },
        inArray: function(e, t, i) {
            var n;
            if (t) {
                if (K) return K.call(t, e, i);
                for (n = t.length, i = i ? 0 > i ? Math.max(0, n + i) : i : 0; n > i; i++)
                    if (i in t && t[i] === e) return i
            }
            return -1
        },
        merge: function(e, t) {
            for (var i = +t.length, n = 0, s = e.length; i > n;) e[s++] = t[n++];
            if (i !== i)
                for (; void 0 !== t[n];) e[s++] = t[n++];
            return e.length = s, e
        },
        grep: function(e, t, i) {
            for (var n, s = [], o = 0, r = e.length, a = !i; r > o; o++) n = !t(e[o], o), n !== a && s.push(e[o]);
            return s
        },
        map: function(e, t, n) {
            var s, o = 0,
                r = e.length,
                a = i(e),
                l = [];
            if (a)
                for (; r > o; o++) s = t(e[o], o, n), null != s && l.push(s);
            else
                for (o in e) s = t(e[o], o, n), null != s && l.push(s);
            return Q.apply([], l)
        },
        guid: 1,
        proxy: function(e, t) {
            var i, n, s;
            return "string" == typeof t && (s = e[t], t = e, e = s), st.isFunction(e) ? (i = V.call(arguments, 2), n = function() {
                return e.apply(t || this, i.concat(V.call(arguments)))
            }, n.guid = e.guid = e.guid || st.guid++, n) : void 0
        },
        now: function() {
            return +new Date
        },
        support: it
    }), st.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        Z["[object " + t + "]"] = t.toLowerCase()
    });
    var ct = function(e) {
        function t(e, t, i, n) {
            var s, o, r, a, l, c, u, p, f, m;
            if ((t ? t.ownerDocument || t : z) !== M && A(t), t = t || M, i = i || [], !e || "string" != typeof e) return i;
            if (1 !== (a = t.nodeType) && 9 !== a) return [];
            if (N && !n) {
                if (s = yt.exec(e))
                    if (r = s[1]) {
                        if (9 === a) {
                            if (o = t.getElementById(r), !o || !o.parentNode) return i;
                            if (o.id === r) return i.push(o), i
                        } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(r)) && H(t, o) && o.id === r) return i.push(o), i
                    } else {
                        if (s[2]) return Z.apply(i, t.getElementsByTagName(e)), i;
                        if ((r = s[3]) && x.getElementsByClassName && t.getElementsByClassName) return Z.apply(i, t.getElementsByClassName(r)), i
                    }
                if (x.qsa && (!j || !j.test(e))) {
                    if (p = u = L, f = t, m = 9 === a && e, 1 === a && "object" !== t.nodeName.toLowerCase()) {
                        for (c = S(e), (u = t.getAttribute("id")) ? p = u.replace(wt, "\\$&") : t.setAttribute("id", p), p = "[id='" + p + "'] ", l = c.length; l--;) c[l] = p + h(c[l]);
                        f = bt.test(e) && d(t.parentNode) || t, m = c.join(",")
                    }
                    if (m) try {
                        return Z.apply(i, f.querySelectorAll(m)), i
                    } catch (g) {} finally {
                        u || t.removeAttribute("id")
                    }
                }
            }
            return T(e.replace(lt, "$1"), t, i, n)
        }

        function i() {
            function e(i, n) {
                return t.push(i + " ") > $.cacheLength && delete e[t.shift()], e[i + " "] = n
            }
            var t = [];
            return e
        }

        function n(e) {
            return e[L] = !0, e
        }

        function s(e) {
            var t = M.createElement("div");
            try {
                return !!e(t)
            } catch (i) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function o(e, t) {
            for (var i = e.split("|"), n = e.length; n--;) $.attrHandle[i[n]] = t
        }

        function r(e, t) {
            var i = t && e,
                n = i && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || X) - (~e.sourceIndex || X);
            if (n) return n;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return "input" === i && t.type === e
            }
        }

        function l(e) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && t.type === e
            }
        }

        function c(e) {
            return n(function(t) {
                return t = +t, n(function(i, n) {
                    for (var s, o = e([], i.length, t), r = o.length; r--;) i[s = o[r]] && (i[s] = !(n[s] = i[s]))
                })
            })
        }

        function d(e) {
            return e && typeof e.getElementsByTagName !== G && e
        }

        function u() {}

        function h(e) {
            for (var t = 0, i = e.length, n = ""; i > t; t++) n += e[t].value;
            return n
        }

        function p(e, t, i) {
            var n = t.dir,
                s = i && "parentNode" === n,
                o = W++;
            return t.first ? function(t, i, o) {
                for (; t = t[n];)
                    if (1 === t.nodeType || s) return e(t, i, o)
            } : function(t, i, r) {
                var a, l, c = [q, o];
                if (r) {
                    for (; t = t[n];)
                        if ((1 === t.nodeType || s) && e(t, i, r)) return !0
                } else
                    for (; t = t[n];)
                        if (1 === t.nodeType || s) {
                            if (l = t[L] || (t[L] = {}), (a = l[n]) && a[0] === q && a[1] === o) return c[2] = a[2];
                            if (l[n] = c, c[2] = e(t, i, r)) return !0
                        }
            }
        }

        function f(e) {
            return e.length > 1 ? function(t, i, n) {
                for (var s = e.length; s--;)
                    if (!e[s](t, i, n)) return !1;
                return !0
            } : e[0]
        }

        function m(e, i, n) {
            for (var s = 0, o = i.length; o > s; s++) t(e, i[s], n);
            return n
        }

        function g(e, t, i, n, s) {
            for (var o, r = [], a = 0, l = e.length, c = null != t; l > a; a++)(o = e[a]) && (!i || i(o, n, s)) && (r.push(o), c && t.push(a));
            return r
        }

        function v(e, t, i, s, o, r) {
            return s && !s[L] && (s = v(s)), o && !o[L] && (o = v(o, r)), n(function(n, r, a, l) {
                var c, d, u, h = [],
                    p = [],
                    f = r.length,
                    v = n || m(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !n && t ? v : g(v, h, e, a, l),
                    b = i ? o || (n ? e : f || s) ? [] : r : y;
                if (i && i(y, b, a, l), s)
                    for (c = g(b, p), s(c, [], a, l), d = c.length; d--;)(u = c[d]) && (b[p[d]] = !(y[p[d]] = u));
                if (n) {
                    if (o || e) {
                        if (o) {
                            for (c = [], d = b.length; d--;)(u = b[d]) && c.push(y[d] = u);
                            o(null, b = [], c, l)
                        }
                        for (d = b.length; d--;)(u = b[d]) && (c = o ? tt.call(n, u) : h[d]) > -1 && (n[c] = !(r[c] = u))
                    }
                } else b = g(b === r ? b.splice(f, b.length) : b), o ? o(null, r, b, l) : Z.apply(r, b)
            })
        }

        function y(e) {
            for (var t, i, n, s = e.length, o = $.relative[e[0].type], r = o || $.relative[" "], a = o ? 1 : 0, l = p(function(e) {
                    return e === t
                }, r, !0), c = p(function(e) {
                    return tt.call(t, e) > -1
                }, r, !0), d = [function(e, i, n) {
                    return !o && (n || i !== D) || ((t = i).nodeType ? l(e, i, n) : c(e, i, n))
                }]; s > a; a++)
                if (i = $.relative[e[a].type]) d = [p(f(d), i)];
                else {
                    if (i = $.filter[e[a].type].apply(null, e[a].matches), i[L]) {
                        for (n = ++a; s > n && !$.relative[e[n].type]; n++);
                        return v(a > 1 && f(d), a > 1 && h(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(lt, "$1"), i, n > a && y(e.slice(a, n)), s > n && y(e = e.slice(n)), s > n && h(e))
                    }
                    d.push(i)
                }
            return f(d)
        }

        function b(e, i) {
            var s = i.length > 0,
                o = e.length > 0,
                r = function(n, r, a, l, c) {
                    var d, u, h, p = 0,
                        f = "0",
                        m = n && [],
                        v = [],
                        y = D,
                        b = n || o && $.find.TAG("*", c),
                        w = q += null == y ? 1 : Math.random() || .1,
                        x = b.length;
                    for (c && (D = r !== M && r); f !== x && null != (d = b[f]); f++) {
                        if (o && d) {
                            for (u = 0; h = e[u++];)
                                if (h(d, r, a)) {
                                    l.push(d);
                                    break
                                }
                            c && (q = w)
                        }
                        s && ((d = !h && d) && p--, n && m.push(d))
                    }
                    if (p += f, s && f !== p) {
                        for (u = 0; h = i[u++];) h(m, v, r, a);
                        if (n) {
                            if (p > 0)
                                for (; f--;) m[f] || v[f] || (v[f] = J.call(l));
                            v = g(v)
                        }
                        Z.apply(l, v), c && !n && v.length > 0 && p + i.length > 1 && t.uniqueSort(l)
                    }
                    return c && (q = w, D = y), m
                };
            return s ? n(r) : r
        }
        var w, x, $, k, _, S, C, T, D, E, O, A, M, I, N, j, P, F, H, L = "sizzle" + -new Date,
            z = e.document,
            q = 0,
            W = 0,
            R = i(),
            B = i(),
            U = i(),
            Y = function(e, t) {
                return e === t && (O = !0), 0
            },
            G = "undefined",
            X = 1 << 31,
            V = {}.hasOwnProperty,
            Q = [],
            J = Q.pop,
            K = Q.push,
            Z = Q.push,
            et = Q.slice,
            tt = Q.indexOf || function(e) {
                for (var t = 0, i = this.length; i > t; t++)
                    if (this[t] === e) return t;
                return -1
            },
            it = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            nt = "[\\x20\\t\\r\\n\\f]",
            st = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ot = st.replace("w", "w#"),
            rt = "\\[" + nt + "*(" + st + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ot + "))|)" + nt + "*\\]",
            at = ":(" + st + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + rt + ")*)|.*)\\)|)",
            lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
            ct = new RegExp("^" + nt + "*," + nt + "*"),
            dt = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
            ut = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"),
            ht = new RegExp(at),
            pt = new RegExp("^" + ot + "$"),
            ft = {
                ID: new RegExp("^#(" + st + ")"),
                CLASS: new RegExp("^\\.(" + st + ")"),
                TAG: new RegExp("^(" + st.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + rt),
                PSEUDO: new RegExp("^" + at),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + it + ")$", "i"),
                needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
            },
            mt = /^(?:input|select|textarea|button)$/i,
            gt = /^h\d$/i,
            vt = /^[^{]+\{\s*\[native \w/,
            yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            bt = /[+~]/,
            wt = /'|\\/g,
            xt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"),
            $t = function(e, t, i) {
                var n = "0x" + t - 65536;
                return n !== n || i ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            };
        try {
            Z.apply(Q = et.call(z.childNodes), z.childNodes), Q[z.childNodes.length].nodeType
        } catch (kt) {
            Z = {
                apply: Q.length ? function(e, t) {
                    K.apply(e, et.call(t))
                } : function(e, t) {
                    for (var i = e.length, n = 0; e[i++] = t[n++];);
                    e.length = i - 1
                }
            }
        }
        x = t.support = {}, _ = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        }, A = t.setDocument = function(e) {
            var t, i = e ? e.ownerDocument || e : z,
                n = i.defaultView;
            return i !== M && 9 === i.nodeType && i.documentElement ? (M = i, I = i.documentElement, N = !_(i), n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", function() {
                A()
            }, !1) : n.attachEvent && n.attachEvent("onunload", function() {
                A()
            })), x.attributes = s(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), x.getElementsByTagName = s(function(e) {
                return e.appendChild(i.createComment("")), !e.getElementsByTagName("*").length
            }), x.getElementsByClassName = vt.test(i.getElementsByClassName) && s(function(e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
            }), x.getById = s(function(e) {
                return I.appendChild(e).id = L, !i.getElementsByName || !i.getElementsByName(L).length
            }), x.getById ? ($.find.ID = function(e, t) {
                if (typeof t.getElementById !== G && N) {
                    var i = t.getElementById(e);
                    return i && i.parentNode ? [i] : []
                }
            }, $.filter.ID = function(e) {
                var t = e.replace(xt, $t);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete $.find.ID, $.filter.ID = function(e) {
                var t = e.replace(xt, $t);
                return function(e) {
                    var i = typeof e.getAttributeNode !== G && e.getAttributeNode("id");
                    return i && i.value === t
                }
            }), $.find.TAG = x.getElementsByTagName ? function(e, t) {
                return typeof t.getElementsByTagName !== G ? t.getElementsByTagName(e) : void 0
            } : function(e, t) {
                var i, n = [],
                    s = 0,
                    o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; i = o[s++];) 1 === i.nodeType && n.push(i);
                    return n
                }
                return o
            }, $.find.CLASS = x.getElementsByClassName && function(e, t) {
                return typeof t.getElementsByClassName !== G && N ? t.getElementsByClassName(e) : void 0
            }, P = [], j = [], (x.qsa = vt.test(i.querySelectorAll)) && (s(function(e) {
                e.innerHTML = "<select msallowclip=''><option selected=''></option></select>", e.querySelectorAll("[msallowclip^='']").length && j.push("[*^$]=" + nt + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || j.push("\\[" + nt + "*(?:value|" + it + ")"), e.querySelectorAll(":checked").length || j.push(":checked")
            }), s(function(e) {
                var t = i.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && j.push("name" + nt + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || j.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), j.push(",.*:")
            })), (x.matchesSelector = vt.test(F = I.matches || I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && s(function(e) {
                x.disconnectedMatch = F.call(e, "div"), F.call(e, "[s!='']:x"), P.push("!=", at)
            }), j = j.length && new RegExp(j.join("|")), P = P.length && new RegExp(P.join("|")), t = vt.test(I.compareDocumentPosition), H = t || vt.test(I.contains) ? function(e, t) {
                var i = 9 === e.nodeType ? e.documentElement : e,
                    n = t && t.parentNode;
                return e === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(n)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, Y = t ? function(e, t) {
                if (e === t) return O = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !x.sortDetached && t.compareDocumentPosition(e) === n ? e === i || e.ownerDocument === z && H(z, e) ? -1 : t === i || t.ownerDocument === z && H(z, t) ? 1 : E ? tt.call(E, e) - tt.call(E, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t) return O = !0, 0;
                var n, s = 0,
                    o = e.parentNode,
                    a = t.parentNode,
                    l = [e],
                    c = [t];
                if (!o || !a) return e === i ? -1 : t === i ? 1 : o ? -1 : a ? 1 : E ? tt.call(E, e) - tt.call(E, t) : 0;
                if (o === a) return r(e, t);
                for (n = e; n = n.parentNode;) l.unshift(n);
                for (n = t; n = n.parentNode;) c.unshift(n);
                for (; l[s] === c[s];) s++;
                return s ? r(l[s], c[s]) : l[s] === z ? -1 : c[s] === z ? 1 : 0
            }, i) : M
        }, t.matches = function(e, i) {
            return t(e, null, null, i)
        }, t.matchesSelector = function(e, i) {
            if ((e.ownerDocument || e) !== M && A(e), i = i.replace(ut, "='$1']"), !(!x.matchesSelector || !N || P && P.test(i) || j && j.test(i))) try {
                var n = F.call(e, i);
                if (n || x.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
            } catch (s) {}
            return t(i, M, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return (e.ownerDocument || e) !== M && A(e), H(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== M && A(e);
            var i = $.attrHandle[t.toLowerCase()],
                n = i && V.call($.attrHandle, t.toLowerCase()) ? i(e, t, !N) : void 0;
            return void 0 !== n ? n : x.attributes || !N ? e.getAttribute(t) : (n = e.getAttributeNode(t)) && n.specified ? n.value : null
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, i = [],
                n = 0,
                s = 0;
            if (O = !x.detectDuplicates, E = !x.sortStable && e.slice(0), e.sort(Y), O) {
                for (; t = e[s++];) t === e[s] && (n = i.push(s));
                for (; n--;) e.splice(i[n], 1)
            }
            return E = null, e
        }, k = t.getText = function(e) {
            var t, i = "",
                n = 0,
                s = e.nodeType;
            if (s) {
                if (1 === s || 9 === s || 11 === s) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) i += k(e)
                } else if (3 === s || 4 === s) return e.nodeValue
            } else
                for (; t = e[n++];) i += k(t);
            return i
        }, $ = t.selectors = {
            cacheLength: 50,
            createPseudo: n,
            match: ft,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(xt, $t), e[3] = (e[3] || e[4] || e[5] || "").replace(xt, $t), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t, i = !e[6] && e[2];
                    return ft.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : i && ht.test(i) && (t = S(i, !0)) && (t = i.indexOf(")", i.length - t) - i.length) && (e[0] = e[0].slice(0, t), e[2] = i.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(xt, $t).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = R[e + " "];
                    return t || (t = new RegExp("(^|" + nt + ")" + e + "(" + nt + "|$)")) && R(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== G && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, i, n) {
                    return function(s) {
                        var o = t.attr(s, e);
                        return null == o ? "!=" === i : i ? (o += "", "=" === i ? o === n : "!=" === i ? o !== n : "^=" === i ? n && 0 === o.indexOf(n) : "*=" === i ? n && o.indexOf(n) > -1 : "$=" === i ? n && o.slice(-n.length) === n : "~=" === i ? (" " + o + " ").indexOf(n) > -1 : "|=" === i ? o === n || o.slice(0, n.length + 1) === n + "-" : !1) : !0
                    }
                },
                CHILD: function(e, t, i, n, s) {
                    var o = "nth" !== e.slice(0, 3),
                        r = "last" !== e.slice(-4),
                        a = "of-type" === t;
                    return 1 === n && 0 === s ? function(e) {
                        return !!e.parentNode
                    } : function(t, i, l) {
                        var c, d, u, h, p, f, m = o !== r ? "nextSibling" : "previousSibling",
                            g = t.parentNode,
                            v = a && t.nodeName.toLowerCase(),
                            y = !l && !a;
                        if (g) {
                            if (o) {
                                for (; m;) {
                                    for (u = t; u = u[m];)
                                        if (a ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
                                    f = m = "only" === e && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [r ? g.firstChild : g.lastChild], r && y) {
                                for (d = g[L] || (g[L] = {}), c = d[e] || [], p = c[0] === q && c[1], h = c[0] === q && c[2], u = p && g.childNodes[p]; u = ++p && u && u[m] || (h = p = 0) || f.pop();)
                                    if (1 === u.nodeType && ++h && u === t) {
                                        d[e] = [q, p, h];
                                        break
                                    }
                            } else if (y && (c = (t[L] || (t[L] = {}))[e]) && c[0] === q) h = c[1];
                            else
                                for (;
                                    (u = ++p && u && u[m] || (h = p = 0) || f.pop()) && ((a ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++h || (y && ((u[L] || (u[L] = {}))[e] = [q, h]), u !== t)););
                            return h -= s, h === n || h % n === 0 && h / n >= 0
                        }
                    }
                },
                PSEUDO: function(e, i) {
                    var s, o = $.pseudos[e] || $.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[L] ? o(i) : o.length > 1 ? (s = [e, e, "", i], $.setFilters.hasOwnProperty(e.toLowerCase()) ? n(function(e, t) {
                        for (var n, s = o(e, i), r = s.length; r--;) n = tt.call(e, s[r]), e[n] = !(t[n] = s[r])
                    }) : function(e) {
                        return o(e, 0, s)
                    }) : o
                }
            },
            pseudos: {
                not: n(function(e) {
                    var t = [],
                        i = [],
                        s = C(e.replace(lt, "$1"));
                    return s[L] ? n(function(e, t, i, n) {
                        for (var o, r = s(e, null, n, []), a = e.length; a--;)(o = r[a]) && (e[a] = !(t[a] = o))
                    }) : function(e, n, o) {
                        return t[0] = e, s(t, null, o, i), !i.pop()
                    }
                }),
                has: n(function(e) {
                    return function(i) {
                        return t(e, i).length > 0
                    }
                }),
                contains: n(function(e) {
                    return function(t) {
                        return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                    }
                }),
                lang: n(function(e) {
                    return pt.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(xt, $t).toLowerCase(),
                        function(t) {
                            var i;
                            do
                                if (i = N ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return i = i.toLowerCase(), i === e || 0 === i.indexOf(e + "-");
                            while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var i = e.location && e.location.hash;
                    return i && i.slice(1) === t.id
                },
                root: function(e) {
                    return e === I
                },
                focus: function(e) {
                    return e === M.activeElement && (!M.hasFocus || M.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return e.disabled === !1
                },
                disabled: function(e) {
                    return e.disabled === !0
                },
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6) return !1;
                    return !0
                },
                parent: function(e) {
                    return !$.pseudos.empty(e)
                },
                header: function(e) {
                    return gt.test(e.nodeName)
                },
                input: function(e) {
                    return mt.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: c(function() {
                    return [0]
                }),
                last: c(function(e, t) {
                    return [t - 1]
                }),
                eq: c(function(e, t, i) {
                    return [0 > i ? i + t : i]
                }),
                even: c(function(e, t) {
                    for (var i = 0; t > i; i += 2) e.push(i);
                    return e
                }),
                odd: c(function(e, t) {
                    for (var i = 1; t > i; i += 2) e.push(i);
                    return e
                }),
                lt: c(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; --n >= 0;) e.push(n);
                    return e
                }),
                gt: c(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; ++n < t;) e.push(n);
                    return e
                })
            }
        }, $.pseudos.nth = $.pseudos.eq;
        for (w in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) $.pseudos[w] = a(w);
        for (w in {
                submit: !0,
                reset: !0
            }) $.pseudos[w] = l(w);
        return u.prototype = $.filters = $.pseudos, $.setFilters = new u, S = t.tokenize = function(e, i) {
            var n, s, o, r, a, l, c, d = B[e + " "];
            if (d) return i ? 0 : d.slice(0);
            for (a = e, l = [], c = $.preFilter; a;) {
                (!n || (s = ct.exec(a))) && (s && (a = a.slice(s[0].length) || a), l.push(o = [])), n = !1, (s = dt.exec(a)) && (n = s.shift(), o.push({
                    value: n,
                    type: s[0].replace(lt, " ")
                }), a = a.slice(n.length));
                for (r in $.filter) !(s = ft[r].exec(a)) || c[r] && !(s = c[r](s)) || (n = s.shift(), o.push({
                    value: n,
                    type: r,
                    matches: s
                }), a = a.slice(n.length));
                if (!n) break
            }
            return i ? a.length : a ? t.error(e) : B(e, l).slice(0)
        }, C = t.compile = function(e, t) {
            var i, n = [],
                s = [],
                o = U[e + " "];
            if (!o) {
                for (t || (t = S(e)), i = t.length; i--;) o = y(t[i]), o[L] ? n.push(o) : s.push(o);
                o = U(e, b(s, n)), o.selector = e
            }
            return o
        }, T = t.select = function(e, t, i, n) {
            var s, o, r, a, l, c = "function" == typeof e && e,
                u = !n && S(e = c.selector || e);
            if (i = i || [], 1 === u.length) {
                if (o = u[0] = u[0].slice(0), o.length > 2 && "ID" === (r = o[0]).type && x.getById && 9 === t.nodeType && N && $.relative[o[1].type]) {
                    if (t = ($.find.ID(r.matches[0].replace(xt, $t), t) || [])[0], !t) return i;
                    c && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (s = ft.needsContext.test(e) ? 0 : o.length; s-- && (r = o[s], !$.relative[a = r.type]);)
                    if ((l = $.find[a]) && (n = l(r.matches[0].replace(xt, $t), bt.test(o[0].type) && d(t.parentNode) || t))) {
                        if (o.splice(s, 1), e = n.length && h(o), !e) return Z.apply(i, n), i;
                        break
                    }
            }
            return (c || C(e, u))(n, t, !N, i, bt.test(e) && d(t.parentNode) || t), i
        }, x.sortStable = L.split("").sort(Y).join("") === L, x.detectDuplicates = !!O, A(), x.sortDetached = s(function(e) {
            return 1 & e.compareDocumentPosition(M.createElement("div"))
        }), s(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function(e, t, i) {
            return i ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), x.attributes && s(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || o("value", function(e, t, i) {
            return i || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), s(function(e) {
            return null == e.getAttribute("disabled")
        }) || o(it, function(e, t, i) {
            var n;
            return i ? void 0 : e[t] === !0 ? t.toLowerCase() : (n = e.getAttributeNode(t)) && n.specified ? n.value : null
        }), t
    }(e);
    st.find = ct, st.expr = ct.selectors, st.expr[":"] = st.expr.pseudos, st.unique = ct.uniqueSort, st.text = ct.getText, st.isXMLDoc = ct.isXML, st.contains = ct.contains;
    var dt = st.expr.match.needsContext,
        ut = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        ht = /^.[^:#\[\.,]*$/;
    st.filter = function(e, t, i) {
        var n = t[0];
        return i && (e = ":not(" + e + ")"), 1 === t.length && 1 === n.nodeType ? st.find.matchesSelector(n, e) ? [n] : [] : st.find.matches(e, st.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, st.fn.extend({
        find: function(e) {
            var t, i = [],
                n = this,
                s = n.length;
            if ("string" != typeof e) return this.pushStack(st(e).filter(function() {
                for (t = 0; s > t; t++)
                    if (st.contains(n[t], this)) return !0
            }));
            for (t = 0; s > t; t++) st.find(e, n[t], i);
            return i = this.pushStack(s > 1 ? st.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
        },
        filter: function(e) {
            return this.pushStack(n(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(n(this, e || [], !0))
        },
        is: function(e) {
            return !!n(this, "string" == typeof e && dt.test(e) ? st(e) : e || [], !1).length
        }
    });
    var pt, ft = e.document,
        mt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        gt = st.fn.init = function(e, t) {
            var i, n;
            if (!e) return this;
            if ("string" == typeof e) {
                if (i = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : mt.exec(e), !i || !i[1] && t) return !t || t.jquery ? (t || pt).find(e) : this.constructor(t).find(e);
                if (i[1]) {
                    if (t = t instanceof st ? t[0] : t, st.merge(this, st.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : ft, !0)), ut.test(i[1]) && st.isPlainObject(t))
                        for (i in t) st.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                    return this
                }
                if (n = ft.getElementById(i[2]), n && n.parentNode) {
                    if (n.id !== i[2]) return pt.find(e);
                    this.length = 1, this[0] = n
                }
                return this.context = ft, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : st.isFunction(e) ? "undefined" != typeof pt.ready ? pt.ready(e) : e(st) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), st.makeArray(e, this))
        };
    gt.prototype = st.fn, pt = st(ft);
    var vt = /^(?:parents|prev(?:Until|All))/,
        yt = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    st.extend({
        dir: function(e, t, i) {
            for (var n = [], s = e[t]; s && 9 !== s.nodeType && (void 0 === i || 1 !== s.nodeType || !st(s).is(i));) 1 === s.nodeType && n.push(s), s = s[t];
            return n
        },
        sibling: function(e, t) {
            for (var i = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && i.push(e);
            return i
        }
    }), st.fn.extend({
        has: function(e) {
            var t, i = st(e, this),
                n = i.length;
            return this.filter(function() {
                for (t = 0; n > t; t++)
                    if (st.contains(this, i[t])) return !0
            })
        },
        closest: function(e, t) {
            for (var i, n = 0, s = this.length, o = [], r = dt.test(e) || "string" != typeof e ? st(e, t || this.context) : 0; s > n; n++)
                for (i = this[n]; i && i !== t; i = i.parentNode)
                    if (i.nodeType < 11 && (r ? r.index(i) > -1 : 1 === i.nodeType && st.find.matchesSelector(i, e))) {
                        o.push(i);
                        break
                    }
            return this.pushStack(o.length > 1 ? st.unique(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? st.inArray(this[0], st(e)) : st.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(st.unique(st.merge(this.get(), st(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), st.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return st.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, i) {
            return st.dir(e, "parentNode", i)
        },
        next: function(e) {
            return s(e, "nextSibling")
        },
        prev: function(e) {
            return s(e, "previousSibling")
        },
        nextAll: function(e) {
            return st.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return st.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, i) {
            return st.dir(e, "nextSibling", i)
        },
        prevUntil: function(e, t, i) {
            return st.dir(e, "previousSibling", i)
        },
        siblings: function(e) {
            return st.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return st.sibling(e.firstChild)
        },
        contents: function(e) {
            return st.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : st.merge([], e.childNodes)
        }
    }, function(e, t) {
        st.fn[e] = function(i, n) {
            var s = st.map(this, t, i);
            return "Until" !== e.slice(-5) && (n = i), n && "string" == typeof n && (s = st.filter(n, s)), this.length > 1 && (yt[e] || (s = st.unique(s)), vt.test(e) && (s = s.reverse())), this.pushStack(s)
        }
    });
    var bt = /\S+/g,
        wt = {};
    st.Callbacks = function(e) {
        e = "string" == typeof e ? wt[e] || o(e) : st.extend({}, e);
        var t, i, n, s, r, a, l = [],
            c = !e.once && [],
            d = function(o) {
                for (i = e.memory && o, n = !0, r = a || 0, a = 0, s = l.length, t = !0; l && s > r; r++)
                    if (l[r].apply(o[0], o[1]) === !1 && e.stopOnFalse) {
                        i = !1;
                        break
                    }
                t = !1, l && (c ? c.length && d(c.shift()) : i ? l = [] : u.disable())
            },
            u = {
                add: function() {
                    if (l) {
                        var n = l.length;
                        ! function o(t) {
                            st.each(t, function(t, i) {
                                var n = st.type(i);
                                "function" === n ? e.unique && u.has(i) || l.push(i) : i && i.length && "string" !== n && o(i)
                            })
                        }(arguments), t ? s = l.length : i && (a = n, d(i))
                    }
                    return this
                },
                remove: function() {
                    return l && st.each(arguments, function(e, i) {
                        for (var n;
                            (n = st.inArray(i, l, n)) > -1;) l.splice(n, 1), t && (s >= n && s--, r >= n && r--)
                    }), this
                },
                has: function(e) {
                    return e ? st.inArray(e, l) > -1 : !(!l || !l.length)
                },
                empty: function() {
                    return l = [], s = 0, this
                },
                disable: function() {
                    return l = c = i = void 0, this
                },
                disabled: function() {
                    return !l
                },
                lock: function() {
                    return c = void 0, i || u.disable(), this
                },
                locked: function() {
                    return !c
                },
                fireWith: function(e, i) {
                    return !l || n && !c || (i = i || [], i = [e, i.slice ? i.slice() : i], t ? c.push(i) : d(i)), this
                },
                fire: function() {
                    return u.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!n
                }
            };
        return u
    }, st.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", st.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", st.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", st.Callbacks("memory")]
                ],
                i = "pending",
                n = {
                    state: function() {
                        return i
                    },
                    always: function() {
                        return s.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return st.Deferred(function(i) {
                            st.each(t, function(t, o) {
                                var r = st.isFunction(e[t]) && e[t];
                                s[o[1]](function() {
                                    var e = r && r.apply(this, arguments);
                                    e && st.isFunction(e.promise) ? e.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[o[0] + "With"](this === n ? i.promise() : this, r ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? st.extend(e, n) : n
                    }
                },
                s = {};
            return n.pipe = n.then, st.each(t, function(e, o) {
                var r = o[2],
                    a = o[3];
                n[o[1]] = r.add, a && r.add(function() {
                    i = a
                }, t[1 ^ e][2].disable, t[2][2].lock), s[o[0]] = function() {
                    return s[o[0] + "With"](this === s ? n : this, arguments), this
                }, s[o[0] + "With"] = r.fireWith
            }), n.promise(s), e && e.call(s, s), s
        },
        when: function(e) {
            var t, i, n, s = 0,
                o = V.call(arguments),
                r = o.length,
                a = 1 !== r || e && st.isFunction(e.promise) ? r : 0,
                l = 1 === a ? e : st.Deferred(),
                c = function(e, i, n) {
                    return function(s) {
                        i[e] = this, n[e] = arguments.length > 1 ? V.call(arguments) : s, n === t ? l.notifyWith(i, n) : --a || l.resolveWith(i, n)
                    }
                };
            if (r > 1)
                for (t = new Array(r), i = new Array(r), n = new Array(r); r > s; s++) o[s] && st.isFunction(o[s].promise) ? o[s].promise().done(c(s, n, o)).fail(l.reject).progress(c(s, i, t)) : --a;
            return a || l.resolveWith(n, o), l.promise()
        }
    });
    var xt;
    st.fn.ready = function(e) {
        return st.ready.promise().done(e), this
    }, st.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? st.readyWait++ : st.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--st.readyWait : !st.isReady) {
                if (!ft.body) return setTimeout(st.ready);
                st.isReady = !0, e !== !0 && --st.readyWait > 0 || (xt.resolveWith(ft, [st]), st.fn.triggerHandler && (st(ft).triggerHandler("ready"), st(ft).off("ready")))
            }
        }
    }), st.ready.promise = function(t) {
        if (!xt)
            if (xt = st.Deferred(), "complete" === ft.readyState) setTimeout(st.ready);
            else if (ft.addEventListener) ft.addEventListener("DOMContentLoaded", a, !1), e.addEventListener("load", a, !1);
        else {
            ft.attachEvent("onreadystatechange", a), e.attachEvent("onload", a);
            var i = !1;
            try {
                i = null == e.frameElement && ft.documentElement
            } catch (n) {}
            i && i.doScroll && ! function s() {
                if (!st.isReady) {
                    try {
                        i.doScroll("left")
                    } catch (e) {
                        return setTimeout(s, 50)
                    }
                    r(), st.ready()
                }
            }()
        }
        return xt.promise(t)
    };
    var $t, kt = "undefined";
    for ($t in st(it)) break;
    it.ownLast = "0" !== $t, it.inlineBlockNeedsLayout = !1, st(function() {
            var e, t, i, n;
            i = ft.getElementsByTagName("body")[0], i && i.style && (t = ft.createElement("div"), n = ft.createElement("div"), n.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(t), typeof t.style.zoom !== kt && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", it.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (i.style.zoom = 1)), i.removeChild(n))
        }),
        function() {
            var e = ft.createElement("div");
            if (null == it.deleteExpando) {
                it.deleteExpando = !0;
                try {
                    delete e.test
                } catch (t) {
                    it.deleteExpando = !1
                }
            }
            e = null
        }(), st.acceptData = function(e) {
            var t = st.noData[(e.nodeName + " ").toLowerCase()],
                i = +e.nodeType || 1;
            return 1 !== i && 9 !== i ? !1 : !t || t !== !0 && e.getAttribute("classid") === t
        };
    var _t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        St = /([A-Z])/g;
    st.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(e) {
            return e = e.nodeType ? st.cache[e[st.expando]] : e[st.expando], !!e && !c(e)
        },
        data: function(e, t, i) {
            return d(e, t, i)
        },
        removeData: function(e, t) {
            return u(e, t)
        },
        _data: function(e, t, i) {
            return d(e, t, i, !0)
        },
        _removeData: function(e, t) {
            return u(e, t, !0)
        }
    }), st.fn.extend({
        data: function(e, t) {
            var i, n, s, o = this[0],
                r = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (s = st.data(o), 1 === o.nodeType && !st._data(o, "parsedAttrs"))) {
                    for (i = r.length; i--;) r[i] && (n = r[i].name, 0 === n.indexOf("data-") && (n = st.camelCase(n.slice(5)), l(o, n, s[n])));
                    st._data(o, "parsedAttrs", !0)
                }
                return s
            }
            return "object" == typeof e ? this.each(function() {
                st.data(this, e)
            }) : arguments.length > 1 ? this.each(function() {
                st.data(this, e, t)
            }) : o ? l(o, e, st.data(o, e)) : void 0
        },
        removeData: function(e) {
            return this.each(function() {
                st.removeData(this, e)
            })
        }
    }), st.extend({
        queue: function(e, t, i) {
            var n;
            return e ? (t = (t || "fx") + "queue", n = st._data(e, t), i && (!n || st.isArray(i) ? n = st._data(e, t, st.makeArray(i)) : n.push(i)), n || []) : void 0
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var i = st.queue(e, t),
                n = i.length,
                s = i.shift(),
                o = st._queueHooks(e, t),
                r = function() {
                    st.dequeue(e, t)
                };
            "inprogress" === s && (s = i.shift(), n--), s && ("fx" === t && i.unshift("inprogress"), delete o.stop, s.call(e, r, o)), !n && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var i = t + "queueHooks";
            return st._data(e, i) || st._data(e, i, {
                empty: st.Callbacks("once memory").add(function() {
                    st._removeData(e, t + "queue"), st._removeData(e, i)
                })
            })
        }
    }), st.fn.extend({
        queue: function(e, t) {
            var i = 2;
            return "string" != typeof e && (t = e, e = "fx", i--), arguments.length < i ? st.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var i = st.queue(this, e, t);
                st._queueHooks(this, e), "fx" === e && "inprogress" !== i[0] && st.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                st.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var i, n = 1,
                s = st.Deferred(),
                o = this,
                r = this.length,
                a = function() {
                    --n || s.resolveWith(o, [o])
                };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; r--;) i = st._data(o[r], e + "queueHooks"), i && i.empty && (n++, i.empty.add(a));
            return a(), s.promise(t)
        }
    });
    var Ct = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Tt = ["Top", "Right", "Bottom", "Left"],
        Dt = function(e, t) {
            return e = t || e, "none" === st.css(e, "display") || !st.contains(e.ownerDocument, e)
        },
        Et = st.access = function(e, t, i, n, s, o, r) {
            var a = 0,
                l = e.length,
                c = null == i;
            if ("object" === st.type(i)) {
                s = !0;
                for (a in i) st.access(e, t, a, i[a], !0, o, r)
            } else if (void 0 !== n && (s = !0, st.isFunction(n) || (r = !0), c && (r ? (t.call(e, n), t = null) : (c = t, t = function(e, t, i) {
                    return c.call(st(e), i)
                })), t))
                for (; l > a; a++) t(e[a], i, r ? n : n.call(e[a], a, t(e[a], i)));
            return s ? e : c ? t.call(e) : l ? t(e[0], i) : o
        },
        Ot = /^(?:checkbox|radio)$/i;
    ! function() {
        var e = ft.createElement("input"),
            t = ft.createElement("div"),
            i = ft.createDocumentFragment();
        if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", it.leadingWhitespace = 3 === t.firstChild.nodeType, it.tbody = !t.getElementsByTagName("tbody").length, it.htmlSerialize = !!t.getElementsByTagName("link").length, it.html5Clone = "<:nav></:nav>" !== ft.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, i.appendChild(e), it.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", it.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, i.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", it.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, it.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function() {
                it.noCloneEvent = !1
            }), t.cloneNode(!0).click()), null == it.deleteExpando) {
            it.deleteExpando = !0;
            try {
                delete t.test
            } catch (n) {
                it.deleteExpando = !1
            }
        }
    }(),
    function() {
        var t, i, n = ft.createElement("div");
        for (t in {
                submit: !0,
                change: !0,
                focusin: !0
            }) i = "on" + t, (it[t + "Bubbles"] = i in e) || (n.setAttribute(i, "t"), it[t + "Bubbles"] = n.attributes[i].expando === !1);
        n = null
    }();
    var At = /^(?:input|select|textarea)$/i,
        Mt = /^key/,
        It = /^(?:mouse|pointer|contextmenu)|click/,
        Nt = /^(?:focusinfocus|focusoutblur)$/,
        jt = /^([^.]*)(?:\.(.+)|)$/;
    st.event = {
        global: {},
        add: function(e, t, i, n, s) {
            var o, r, a, l, c, d, u, h, p, f, m, g = st._data(e);
            if (g) {
                for (i.handler && (l = i, i = l.handler, s = l.selector), i.guid || (i.guid = st.guid++), (r = g.events) || (r = g.events = {}), (d = g.handle) || (d = g.handle = function(e) {
                        return typeof st === kt || e && st.event.triggered === e.type ? void 0 : st.event.dispatch.apply(d.elem, arguments)
                    }, d.elem = e), t = (t || "").match(bt) || [""], a = t.length; a--;) o = jt.exec(t[a]) || [], p = m = o[1], f = (o[2] || "").split(".").sort(), p && (c = st.event.special[p] || {}, p = (s ? c.delegateType : c.bindType) || p, c = st.event.special[p] || {}, u = st.extend({
                    type: p,
                    origType: m,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: s,
                    needsContext: s && st.expr.match.needsContext.test(s),
                    namespace: f.join(".")
                }, l), (h = r[p]) || (h = r[p] = [], h.delegateCount = 0, c.setup && c.setup.call(e, n, f, d) !== !1 || (e.addEventListener ? e.addEventListener(p, d, !1) : e.attachEvent && e.attachEvent("on" + p, d))), c.add && (c.add.call(e, u), u.handler.guid || (u.handler.guid = i.guid)), s ? h.splice(h.delegateCount++, 0, u) : h.push(u), st.event.global[p] = !0);
                e = null
            }
        },
        remove: function(e, t, i, n, s) {
            var o, r, a, l, c, d, u, h, p, f, m, g = st.hasData(e) && st._data(e);
            if (g && (d = g.events)) {
                for (t = (t || "").match(bt) || [""], c = t.length; c--;)
                    if (a = jt.exec(t[c]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p) {
                        for (u = st.event.special[p] || {}, p = (n ? u.delegateType : u.bindType) || p, h = d[p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = o = h.length; o--;) r = h[o], !s && m !== r.origType || i && i.guid !== r.guid || a && !a.test(r.namespace) || n && n !== r.selector && ("**" !== n || !r.selector) || (h.splice(o, 1), r.selector && h.delegateCount--, u.remove && u.remove.call(e, r));
                        l && !h.length && (u.teardown && u.teardown.call(e, f, g.handle) !== !1 || st.removeEvent(e, p, g.handle), delete d[p])
                    } else
                        for (p in d) st.event.remove(e, p + t[c], i, n, !0);
                st.isEmptyObject(d) && (delete g.handle, st._removeData(e, "events"))
            }
        },
        trigger: function(t, i, n, s) {
            var o, r, a, l, c, d, u, h = [n || ft],
                p = tt.call(t, "type") ? t.type : t,
                f = tt.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = d = n = n || ft, 3 !== n.nodeType && 8 !== n.nodeType && !Nt.test(p + st.event.triggered) && (p.indexOf(".") >= 0 && (f = p.split("."), p = f.shift(), f.sort()), r = p.indexOf(":") < 0 && "on" + p, t = t[st.expando] ? t : new st.Event(p, "object" == typeof t && t), t.isTrigger = s ? 2 : 3, t.namespace = f.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = n), i = null == i ? [t] : st.makeArray(i, [t]), c = st.event.special[p] || {}, s || !c.trigger || c.trigger.apply(n, i) !== !1)) {
                if (!s && !c.noBubble && !st.isWindow(n)) {
                    for (l = c.delegateType || p, Nt.test(l + p) || (a = a.parentNode); a; a = a.parentNode) h.push(a), d = a;
                    d === (n.ownerDocument || ft) && h.push(d.defaultView || d.parentWindow || e)
                }
                for (u = 0;
                    (a = h[u++]) && !t.isPropagationStopped();) t.type = u > 1 ? l : c.bindType || p, o = (st._data(a, "events") || {})[t.type] && st._data(a, "handle"), o && o.apply(a, i), o = r && a[r], o && o.apply && st.acceptData(a) && (t.result = o.apply(a, i), t.result === !1 && t.preventDefault());
                if (t.type = p, !s && !t.isDefaultPrevented() && (!c._default || c._default.apply(h.pop(), i) === !1) && st.acceptData(n) && r && n[p] && !st.isWindow(n)) {
                    d = n[r], d && (n[r] = null), st.event.triggered = p;
                    try {
                        n[p]()
                    } catch (m) {}
                    st.event.triggered = void 0, d && (n[r] = d)
                }
                return t.result
            }
        },
        dispatch: function(e) {
            e = st.event.fix(e);
            var t, i, n, s, o, r = [],
                a = V.call(arguments),
                l = (st._data(this, "events") || {})[e.type] || [],
                c = st.event.special[e.type] || {};
            if (a[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (r = st.event.handlers.call(this, e, l), t = 0;
                    (s = r[t++]) && !e.isPropagationStopped();)
                    for (e.currentTarget = s.elem, o = 0;
                        (n = s.handlers[o++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(n.namespace)) && (e.handleObj = n, e.data = n.data, i = ((st.event.special[n.origType] || {}).handle || n.handler).apply(s.elem, a), void 0 !== i && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        },
        handlers: function(e, t) {
            var i, n, s, o, r = [],
                a = t.delegateCount,
                l = e.target;
            if (a && l.nodeType && (!e.button || "click" !== e.type))
                for (; l != this; l = l.parentNode || this)
                    if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                        for (s = [], o = 0; a > o; o++) n = t[o], i = n.selector + " ", void 0 === s[i] && (s[i] = n.needsContext ? st(i, this).index(l) >= 0 : st.find(i, this, null, [l]).length), s[i] && s.push(n);
                        s.length && r.push({
                            elem: l,
                            handlers: s
                        })
                    }
            return a < t.length && r.push({
                elem: this,
                handlers: t.slice(a)
            }), r
        },
        fix: function(e) {
            if (e[st.expando]) return e;
            var t, i, n, s = e.type,
                o = e,
                r = this.fixHooks[s];
            for (r || (this.fixHooks[s] = r = It.test(s) ? this.mouseHooks : Mt.test(s) ? this.keyHooks : {}), n = r.props ? this.props.concat(r.props) : this.props, e = new st.Event(o), t = n.length; t--;) i = n[t], e[i] = o[i];
            return e.target || (e.target = o.srcElement || ft), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, r.filter ? r.filter(e, o) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, t) {
                var i, n, s, o = t.button,
                    r = t.fromElement;
                return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || ft, s = n.documentElement, i = n.body, e.pageX = t.clientX + (s && s.scrollLeft || i && i.scrollLeft || 0) - (s && s.clientLeft || i && i.clientLeft || 0), e.pageY = t.clientY + (s && s.scrollTop || i && i.scrollTop || 0) - (s && s.clientTop || i && i.clientTop || 0)), !e.relatedTarget && r && (e.relatedTarget = r === e.target ? t.toElement : r), e.which || void 0 === o || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== f() && this.focus) try {
                        return this.focus(), !1
                    } catch (e) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === f() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return st.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                },
                _default: function(e) {
                    return st.nodeName(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function(e, t, i, n) {
            var s = st.extend(new st.Event, i, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            n ? st.event.trigger(s, null, t) : st.event.dispatch.call(t, s), s.isDefaultPrevented() && i.preventDefault()
        }
    }, st.removeEvent = ft.removeEventListener ? function(e, t, i) {
        e.removeEventListener && e.removeEventListener(t, i, !1)
    } : function(e, t, i) {
        var n = "on" + t;
        e.detachEvent && (typeof e[n] === kt && (e[n] = null), e.detachEvent(n, i))
    }, st.Event = function(e, t) {
        return this instanceof st.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? h : p) : this.type = e, t && st.extend(this, t), this.timeStamp = e && e.timeStamp || st.now(), void(this[st.expando] = !0)) : new st.Event(e, t)
    }, st.Event.prototype = {
        isDefaultPrevented: p,
        isPropagationStopped: p,
        isImmediatePropagationStopped: p,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = h, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = h, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = h, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, st.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, t) {
        st.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var i, n = this,
                    s = e.relatedTarget,
                    o = e.handleObj;
                return (!s || s !== n && !st.contains(n, s)) && (e.type = o.origType, i = o.handler.apply(this, arguments), e.type = t), i
            }
        }
    }), it.submitBubbles || (st.event.special.submit = {
        setup: function() {
            return st.nodeName(this, "form") ? !1 : void st.event.add(this, "click._submit keypress._submit", function(e) {
                var t = e.target,
                    i = st.nodeName(t, "input") || st.nodeName(t, "button") ? t.form : void 0;
                i && !st._data(i, "submitBubbles") && (st.event.add(i, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }), st._data(i, "submitBubbles", !0))
            })
        },
        postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && st.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function() {
            return st.nodeName(this, "form") ? !1 : void st.event.remove(this, "._submit")
        }
    }), it.changeBubbles || (st.event.special.change = {
        setup: function() {
            return At.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (st.event.add(this, "propertychange._change", function(e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), st.event.add(this, "click._change", function(e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), st.event.simulate("change", this, e, !0)
            })), !1) : void st.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                At.test(t.nodeName) && !st._data(t, "changeBubbles") && (st.event.add(t, "change._change", function(e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || st.event.simulate("change", this.parentNode, e, !0)
                }), st._data(t, "changeBubbles", !0))
            })
        },
        handle: function(e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
        },
        teardown: function() {
            return st.event.remove(this, "._change"), !At.test(this.nodeName)
        }
    }), it.focusinBubbles || st.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var i = function(e) {
            st.event.simulate(t, e.target, st.event.fix(e), !0)
        };
        st.event.special[t] = {
            setup: function() {
                var n = this.ownerDocument || this,
                    s = st._data(n, t);
                s || n.addEventListener(e, i, !0), st._data(n, t, (s || 0) + 1)
            },
            teardown: function() {
                var n = this.ownerDocument || this,
                    s = st._data(n, t) - 1;
                s ? st._data(n, t, s) : (n.removeEventListener(e, i, !0), st._removeData(n, t))
            }
        }
    }), st.fn.extend({
        on: function(e, t, i, n, s) {
            var o, r;
            if ("object" == typeof e) {
                "string" != typeof t && (i = i || t, t = void 0);
                for (o in e) this.on(o, t, i, e[o], s);
                return this
            }
            if (null == i && null == n ? (n = t, i = t = void 0) : null == n && ("string" == typeof t ? (n = i, i = void 0) : (n = i, i = t, t = void 0)), n === !1) n = p;
            else if (!n) return this;
            return 1 === s && (r = n, n = function(e) {
                return st().off(e), r.apply(this, arguments)
            }, n.guid = r.guid || (r.guid = st.guid++)), this.each(function() {
                st.event.add(this, e, n, i, t)
            })
        },
        one: function(e, t, i, n) {
            return this.on(e, t, i, n, 1)
        },
        off: function(e, t, i) {
            var n, s;
            if (e && e.preventDefault && e.handleObj) return n = e.handleObj, st(e.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" == typeof e) {
                for (s in e) this.off(s, t, e[s]);
                return this
            }
            return (t === !1 || "function" == typeof t) && (i = t, t = void 0), i === !1 && (i = p), this.each(function() {
                st.event.remove(this, e, i, t)
            })
        },
        trigger: function(e, t) {
            return this.each(function() {
                st.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var i = this[0];
            return i ? st.event.trigger(e, t, i, !0) : void 0
        }
    });
    var Pt = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Ft = / jQuery\d+="(?:null|\d+)"/g,
        Ht = new RegExp("<(?:" + Pt + ")[\\s/>]", "i"),
        Lt = /^\s+/,
        zt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        qt = /<([\w:]+)/,
        Wt = /<tbody/i,
        Rt = /<|&#?\w+;/,
        Bt = /<(?:script|style|link)/i,
        Ut = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Yt = /^$|\/(?:java|ecma)script/i,
        Gt = /^true\/(.*)/,
        Xt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Vt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: it.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        Qt = m(ft),
        Jt = Qt.appendChild(ft.createElement("div"));
    Vt.optgroup = Vt.option, Vt.tbody = Vt.tfoot = Vt.colgroup = Vt.caption = Vt.thead, Vt.th = Vt.td, st.extend({
        clone: function(e, t, i) {
            var n, s, o, r, a, l = st.contains(e.ownerDocument, e);
            if (it.html5Clone || st.isXMLDoc(e) || !Ht.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (Jt.innerHTML = e.outerHTML, Jt.removeChild(o = Jt.firstChild)), !(it.noCloneEvent && it.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || st.isXMLDoc(e)))
                for (n = g(o), a = g(e), r = 0; null != (s = a[r]); ++r) n[r] && k(s, n[r]);
            if (t)
                if (i)
                    for (a = a || g(e), n = n || g(o), r = 0; null != (s = a[r]); r++) $(s, n[r]);
                else $(e, o);
            return n = g(o, "script"), n.length > 0 && x(n, !l && g(e, "script")), n = a = s = null, o
        },
        buildFragment: function(e, t, i, n) {
            for (var s, o, r, a, l, c, d, u = e.length, h = m(t), p = [], f = 0; u > f; f++)
                if (o = e[f], o || 0 === o)
                    if ("object" === st.type(o)) st.merge(p, o.nodeType ? [o] : o);
                    else if (Rt.test(o)) {
                for (a = a || h.appendChild(t.createElement("div")), l = (qt.exec(o) || ["", ""])[1].toLowerCase(), d = Vt[l] || Vt._default, a.innerHTML = d[1] + o.replace(zt, "<$1></$2>") + d[2], s = d[0]; s--;) a = a.lastChild;
                if (!it.leadingWhitespace && Lt.test(o) && p.push(t.createTextNode(Lt.exec(o)[0])), !it.tbody)
                    for (o = "table" !== l || Wt.test(o) ? "<table>" !== d[1] || Wt.test(o) ? 0 : a : a.firstChild, s = o && o.childNodes.length; s--;) st.nodeName(c = o.childNodes[s], "tbody") && !c.childNodes.length && o.removeChild(c);
                for (st.merge(p, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                a = h.lastChild
            } else p.push(t.createTextNode(o));
            for (a && h.removeChild(a), it.appendChecked || st.grep(g(p, "input"), v), f = 0; o = p[f++];)
                if ((!n || -1 === st.inArray(o, n)) && (r = st.contains(o.ownerDocument, o), a = g(h.appendChild(o), "script"), r && x(a), i))
                    for (s = 0; o = a[s++];) Yt.test(o.type || "") && i.push(o);
            return a = null, h
        },
        cleanData: function(e, t) {
            for (var i, n, s, o, r = 0, a = st.expando, l = st.cache, c = it.deleteExpando, d = st.event.special; null != (i = e[r]); r++)
                if ((t || st.acceptData(i)) && (s = i[a], o = s && l[s])) {
                    if (o.events)
                        for (n in o.events) d[n] ? st.event.remove(i, n) : st.removeEvent(i, n, o.handle);
                    l[s] && (delete l[s], c ? delete i[a] : typeof i.removeAttribute !== kt ? i.removeAttribute(a) : i[a] = null, X.push(s))
                }
        }
    }), st.fn.extend({
        text: function(e) {
            return Et(this, function(e) {
                return void 0 === e ? st.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ft).createTextNode(e))
            }, null, e, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.appendChild(e)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            for (var i, n = e ? st.filter(e, this) : this, s = 0; null != (i = n[s]); s++) t || 1 !== i.nodeType || st.cleanData(g(i)), i.parentNode && (t && st.contains(i.ownerDocument, i) && x(g(i, "script")), i.parentNode.removeChild(i));
            return this
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && st.cleanData(g(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && st.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return st.clone(this, e, t)
            })
        },
        html: function(e) {
            return Et(this, function(e) {
                var t = this[0] || {},
                    i = 0,
                    n = this.length;
                if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(Ft, "") : void 0;
                if (!("string" != typeof e || Bt.test(e) || !it.htmlSerialize && Ht.test(e) || !it.leadingWhitespace && Lt.test(e) || Vt[(qt.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(zt, "<$1></$2>");
                    try {
                        for (; n > i; i++) t = this[i] || {}, 1 === t.nodeType && (st.cleanData(g(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (s) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = arguments[0];
            return this.domManip(arguments, function(t) {
                e = this.parentNode, st.cleanData(g(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, t) {
            e = Q.apply([], e);
            var i, n, s, o, r, a, l = 0,
                c = this.length,
                d = this,
                u = c - 1,
                h = e[0],
                p = st.isFunction(h);
            if (p || c > 1 && "string" == typeof h && !it.checkClone && Ut.test(h)) return this.each(function(i) {
                var n = d.eq(i);
                p && (e[0] = h.call(this, i, n.html())), n.domManip(e, t)
            });
            if (c && (a = st.buildFragment(e, this[0].ownerDocument, !1, this), i = a.firstChild, 1 === a.childNodes.length && (a = i), i)) {
                for (o = st.map(g(a, "script"), b), s = o.length; c > l; l++) n = a, l !== u && (n = st.clone(n, !0, !0), s && st.merge(o, g(n, "script"))), t.call(this[l], n, l);
                if (s)
                    for (r = o[o.length - 1].ownerDocument, st.map(o, w), l = 0; s > l; l++) n = o[l], Yt.test(n.type || "") && !st._data(n, "globalEval") && st.contains(r, n) && (n.src ? st._evalUrl && st._evalUrl(n.src) : st.globalEval((n.text || n.textContent || n.innerHTML || "").replace(Xt, "")));
                a = i = null
            }
            return this
        }
    }), st.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        st.fn[e] = function(e) {
            for (var i, n = 0, s = [], o = st(e), r = o.length - 1; r >= n; n++) i = n === r ? this : this.clone(!0), st(o[n])[t](i), J.apply(s, i.get());
            return this.pushStack(s)
        }
    });
    var Kt, Zt = {};
    ! function() {
        var e;
        it.shrinkWrapBlocks = function() {
            if (null != e) return e;
            e = !1;
            var t, i, n;
            return i = ft.getElementsByTagName("body")[0], i && i.style ? (t = ft.createElement("div"), n = ft.createElement("div"), n.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(t), typeof t.style.zoom !== kt && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(ft.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), i.removeChild(n), e) : void 0
        }
    }();
    var ei, ti, ii = /^margin/,
        ni = new RegExp("^(" + Ct + ")(?!px)[a-z%]+$", "i"),
        si = /^(top|right|bottom|left)$/;
    e.getComputedStyle ? (ei = function(e) {
            return e.ownerDocument.defaultView.getComputedStyle(e, null)
        }, ti = function(e, t, i) {
            var n, s, o, r, a = e.style;
            return i = i || ei(e), r = i ? i.getPropertyValue(t) || i[t] : void 0, i && ("" !== r || st.contains(e.ownerDocument, e) || (r = st.style(e, t)), ni.test(r) && ii.test(t) && (n = a.width, s = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = r, r = i.width, a.width = n, a.minWidth = s, a.maxWidth = o)), void 0 === r ? r : r + ""
        }) : ft.documentElement.currentStyle && (ei = function(e) {
            return e.currentStyle
        }, ti = function(e, t, i) {
            var n, s, o, r, a = e.style;
            return i = i || ei(e), r = i ? i[t] : void 0, null == r && a && a[t] && (r = a[t]), ni.test(r) && !si.test(t) && (n = a.left, s = e.runtimeStyle, o = s && s.left, o && (s.left = e.currentStyle.left), a.left = "fontSize" === t ? "1em" : r, r = a.pixelLeft + "px", a.left = n, o && (s.left = o)), void 0 === r ? r : r + "" || "auto"
        }),
        function() {
            function t() {
                var t, i, n, s;
                i = ft.getElementsByTagName("body")[0], i && i.style && (t = ft.createElement("div"), n = ft.createElement("div"), n.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", o = r = !1, l = !0, e.getComputedStyle && (o = "1%" !== (e.getComputedStyle(t, null) || {}).top, r = "4px" === (e.getComputedStyle(t, null) || {
                    width: "4px"
                }).width, s = t.appendChild(ft.createElement("div")), s.style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", s.style.marginRight = s.style.width = "0", t.style.width = "1px", l = !parseFloat((e.getComputedStyle(s, null) || {}).marginRight)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", s = t.getElementsByTagName("td"), s[0].style.cssText = "margin:0;border:0;padding:0;display:none", a = 0 === s[0].offsetHeight, a && (s[0].style.display = "", s[1].style.display = "none", a = 0 === s[0].offsetHeight), i.removeChild(n))
            }
            var i, n, s, o, r, a, l;
            i = ft.createElement("div"), i.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", s = i.getElementsByTagName("a")[0], n = s && s.style, n && (n.cssText = "float:left;opacity:.5", it.opacity = "0.5" === n.opacity, it.cssFloat = !!n.cssFloat, i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", it.clearCloneStyle = "content-box" === i.style.backgroundClip, it.boxSizing = "" === n.boxSizing || "" === n.MozBoxSizing || "" === n.WebkitBoxSizing, st.extend(it, {
                reliableHiddenOffsets: function() {
                    return null == a && t(), a
                },
                boxSizingReliable: function() {
                    return null == r && t(), r
                },
                pixelPosition: function() {
                    return null == o && t(), o
                },
                reliableMarginRight: function() {
                    return null == l && t(), l
                }
            }))
        }(), st.swap = function(e, t, i, n) {
            var s, o, r = {};
            for (o in t) r[o] = e.style[o], e.style[o] = t[o];
            s = i.apply(e, n || []);
            for (o in t) e.style[o] = r[o];
            return s
        };
    var oi = /alpha\([^)]*\)/i,
        ri = /opacity\s*=\s*([^)]*)/,
        ai = /^(none|table(?!-c[ea]).+)/,
        li = new RegExp("^(" + Ct + ")(.*)$", "i"),
        ci = new RegExp("^([+-])=(" + Ct + ")", "i"),
        di = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        ui = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        hi = ["Webkit", "O", "Moz", "ms"];
    st.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var i = ti(e, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": it.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, t, i, n) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var s, o, r, a = st.camelCase(t),
                    l = e.style;
                if (t = st.cssProps[a] || (st.cssProps[a] = T(l, a)), r = st.cssHooks[t] || st.cssHooks[a], void 0 === i) return r && "get" in r && void 0 !== (s = r.get(e, !1, n)) ? s : l[t];
                if (o = typeof i, "string" === o && (s = ci.exec(i)) && (i = (s[1] + 1) * s[2] + parseFloat(st.css(e, t)), o = "number"), null != i && i === i && ("number" !== o || st.cssNumber[a] || (i += "px"), it.clearCloneStyle || "" !== i || 0 !== t.indexOf("background") || (l[t] = "inherit"), !(r && "set" in r && void 0 === (i = r.set(e, i, n))))) try {
                    l[t] = i
                } catch (c) {}
            }
        },
        css: function(e, t, i, n) {
            var s, o, r, a = st.camelCase(t);
            return t = st.cssProps[a] || (st.cssProps[a] = T(e.style, a)), r = st.cssHooks[t] || st.cssHooks[a], r && "get" in r && (o = r.get(e, !0, i)), void 0 === o && (o = ti(e, t, n)), "normal" === o && t in ui && (o = ui[t]), "" === i || i ? (s = parseFloat(o), i === !0 || st.isNumeric(s) ? s || 0 : o) : o
        }
    }), st.each(["height", "width"], function(e, t) {
        st.cssHooks[t] = {
            get: function(e, i, n) {
                return i ? ai.test(st.css(e, "display")) && 0 === e.offsetWidth ? st.swap(e, di, function() {
                    return A(e, t, n)
                }) : A(e, t, n) : void 0
            },
            set: function(e, i, n) {
                var s = n && ei(e);
                return E(e, i, n ? O(e, t, n, it.boxSizing && "border-box" === st.css(e, "boxSizing", !1, s), s) : 0)
            }
        }
    }), it.opacity || (st.cssHooks.opacity = {
        get: function(e, t) {
            return ri.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var i = e.style,
                n = e.currentStyle,
                s = st.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                o = n && n.filter || i.filter || "";
            i.zoom = 1, (t >= 1 || "" === t) && "" === st.trim(o.replace(oi, "")) && i.removeAttribute && (i.removeAttribute("filter"), "" === t || n && !n.filter) || (i.filter = oi.test(o) ? o.replace(oi, s) : o + " " + s)
        }
    }), st.cssHooks.marginRight = C(it.reliableMarginRight, function(e, t) {
        return t ? st.swap(e, {
            display: "inline-block"
        }, ti, [e, "marginRight"]) : void 0
    }), st.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        st.cssHooks[e + t] = {
            expand: function(i) {
                for (var n = 0, s = {}, o = "string" == typeof i ? i.split(" ") : [i]; 4 > n; n++) s[e + Tt[n] + t] = o[n] || o[n - 2] || o[0];
                return s
            }
        }, ii.test(e) || (st.cssHooks[e + t].set = E)
    }), st.fn.extend({
        css: function(e, t) {
            return Et(this, function(e, t, i) {
                var n, s, o = {},
                    r = 0;
                if (st.isArray(t)) {
                    for (n = ei(e), s = t.length; s > r; r++) o[t[r]] = st.css(e, t[r], !1, n);
                    return o
                }
                return void 0 !== i ? st.style(e, t, i) : st.css(e, t)
            }, e, t, arguments.length > 1)
        },
        show: function() {
            return D(this, !0)
        },
        hide: function() {
            return D(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                Dt(this) ? st(this).show() : st(this).hide()
            })
        }
    }), st.Tween = M, M.prototype = {
        constructor: M,
        init: function(e, t, i, n, s, o) {
            this.elem = e, this.prop = i, this.easing = s || "swing", this.options = t, this.start = this.now = this.cur(), this.end = n, this.unit = o || (st.cssNumber[i] ? "" : "px")
        },
        cur: function() {
            var e = M.propHooks[this.prop];
            return e && e.get ? e.get(this) : M.propHooks._default.get(this)
        },
        run: function(e) {
            var t, i = M.propHooks[this.prop];
            return this.pos = t = this.options.duration ? st.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : M.propHooks._default.set(this), this
        }
    }, M.prototype.init.prototype = M.prototype, M.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = st.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                st.fx.step[e.prop] ? st.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[st.cssProps[e.prop]] || st.cssHooks[e.prop]) ? st.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, M.propHooks.scrollTop = M.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, st.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, st.fx = M.prototype.init, st.fx.step = {};
    var pi, fi, mi = /^(?:toggle|show|hide)$/,
        gi = new RegExp("^(?:([+-])=|)(" + Ct + ")([a-z%]*)$", "i"),
        vi = /queueHooks$/,
        yi = [P],
        bi = {
            "*": [function(e, t) {
                var i = this.createTween(e, t),
                    n = i.cur(),
                    s = gi.exec(t),
                    o = s && s[3] || (st.cssNumber[e] ? "" : "px"),
                    r = (st.cssNumber[e] || "px" !== o && +n) && gi.exec(st.css(i.elem, e)),
                    a = 1,
                    l = 20;
                if (r && r[3] !== o) {
                    o = o || r[3], s = s || [], r = +n || 1;
                    do a = a || ".5", r /= a, st.style(i.elem, e, r + o); while (a !== (a = i.cur() / n) && 1 !== a && --l)
                }
                return s && (r = i.start = +r || +n || 0, i.unit = o, i.end = s[1] ? r + (s[1] + 1) * s[2] : +s[2]), i
            }]
        };
    st.Animation = st.extend(H, {
            tweener: function(e, t) {
                st.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                for (var i, n = 0, s = e.length; s > n; n++) i = e[n], bi[i] = bi[i] || [], bi[i].unshift(t)
            },
            prefilter: function(e, t) {
                t ? yi.unshift(e) : yi.push(e)
            }
        }), st.speed = function(e, t, i) {
            var n = e && "object" == typeof e ? st.extend({}, e) : {
                complete: i || !i && t || st.isFunction(e) && e,
                duration: e,
                easing: i && t || t && !st.isFunction(t) && t
            };
            return n.duration = st.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in st.fx.speeds ? st.fx.speeds[n.duration] : st.fx.speeds._default, (null == n.queue || n.queue === !0) && (n.queue = "fx"), n.old = n.complete, n.complete = function() {
                st.isFunction(n.old) && n.old.call(this), n.queue && st.dequeue(this, n.queue)
            }, n
        }, st.fn.extend({
            fadeTo: function(e, t, i, n) {
                return this.filter(Dt).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, i, n)
            },
            animate: function(e, t, i, n) {
                var s = st.isEmptyObject(e),
                    o = st.speed(t, i, n),
                    r = function() {
                        var t = H(this, st.extend({}, e), o);
                        (s || st._data(this, "finish")) && t.stop(!0)
                    };
                return r.finish = r, s || o.queue === !1 ? this.each(r) : this.queue(o.queue, r)
            },
            stop: function(e, t, i) {
                var n = function(e) {
                    var t = e.stop;
                    delete e.stop, t(i)
                };
                return "string" != typeof e && (i = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        s = null != e && e + "queueHooks",
                        o = st.timers,
                        r = st._data(this);
                    if (s) r[s] && r[s].stop && n(r[s]);
                    else
                        for (s in r) r[s] && r[s].stop && vi.test(s) && n(r[s]);
                    for (s = o.length; s--;) o[s].elem !== this || null != e && o[s].queue !== e || (o[s].anim.stop(i), t = !1, o.splice(s, 1));
                    (t || !i) && st.dequeue(this, e)
                })
            },
            finish: function(e) {
                return e !== !1 && (e = e || "fx"), this.each(function() {
                    var t, i = st._data(this),
                        n = i[e + "queue"],
                        s = i[e + "queueHooks"],
                        o = st.timers,
                        r = n ? n.length : 0;
                    for (i.finish = !0, st.queue(this, e, []), s && s.stop && s.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                    for (t = 0; r > t; t++) n[t] && n[t].finish && n[t].finish.call(this);
                    delete i.finish
                })
            }
        }), st.each(["toggle", "show", "hide"], function(e, t) {
            var i = st.fn[t];
            st.fn[t] = function(e, n, s) {
                return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(N(t, !0), e, n, s)
            }
        }), st.each({
            slideDown: N("show"),
            slideUp: N("hide"),
            slideToggle: N("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            st.fn[e] = function(e, i, n) {
                return this.animate(t, e, i, n)
            }
        }), st.timers = [], st.fx.tick = function() {
            var e, t = st.timers,
                i = 0;
            for (pi = st.now(); i < t.length; i++) e = t[i], e() || t[i] !== e || t.splice(i--, 1);
            t.length || st.fx.stop(), pi = void 0
        }, st.fx.timer = function(e) {
            st.timers.push(e), e() ? st.fx.start() : st.timers.pop()
        }, st.fx.interval = 13, st.fx.start = function() {
            fi || (fi = setInterval(st.fx.tick, st.fx.interval))
        }, st.fx.stop = function() {
            clearInterval(fi), fi = null
        }, st.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, st.fn.delay = function(e, t) {
            return e = st.fx ? st.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, i) {
                var n = setTimeout(t, e);
                i.stop = function() {
                    clearTimeout(n)
                }
            })
        },
        function() {
            var e, t, i, n, s;
            t = ft.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = t.getElementsByTagName("a")[0], i = ft.createElement("select"), s = i.appendChild(ft.createElement("option")), e = t.getElementsByTagName("input")[0], n.style.cssText = "top:1px", it.getSetAttribute = "t" !== t.className, it.style = /top/.test(n.getAttribute("style")), it.hrefNormalized = "/a" === n.getAttribute("href"), it.checkOn = !!e.value, it.optSelected = s.selected, it.enctype = !!ft.createElement("form").enctype, i.disabled = !0, it.optDisabled = !s.disabled, e = ft.createElement("input"), e.setAttribute("value", ""), it.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), it.radioValue = "t" === e.value
        }();
    var wi = /\r/g;
    st.fn.extend({
        val: function(e) {
            var t, i, n, s = this[0]; {
                if (arguments.length) return n = st.isFunction(e), this.each(function(i) {
                    var s;
                    1 === this.nodeType && (s = n ? e.call(this, i, st(this).val()) : e, null == s ? s = "" : "number" == typeof s ? s += "" : st.isArray(s) && (s = st.map(s, function(e) {
                        return null == e ? "" : e + ""
                    })), t = st.valHooks[this.type] || st.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, s, "value") || (this.value = s))
                });
                if (s) return t = st.valHooks[s.type] || st.valHooks[s.nodeName.toLowerCase()], t && "get" in t && void 0 !== (i = t.get(s, "value")) ? i : (i = s.value, "string" == typeof i ? i.replace(wi, "") : null == i ? "" : i)
            }
        }
    }), st.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = st.find.attr(e, "value");
                    return null != t ? t : st.trim(st.text(e))
                }
            },
            select: {
                get: function(e) {
                    for (var t, i, n = e.options, s = e.selectedIndex, o = "select-one" === e.type || 0 > s, r = o ? null : [], a = o ? s + 1 : n.length, l = 0 > s ? a : o ? s : 0; a > l; l++)
                        if (i = n[l], !(!i.selected && l !== s || (it.optDisabled ? i.disabled : null !== i.getAttribute("disabled")) || i.parentNode.disabled && st.nodeName(i.parentNode, "optgroup"))) {
                            if (t = st(i).val(), o) return t;
                            r.push(t)
                        }
                    return r
                },
                set: function(e, t) {
                    for (var i, n, s = e.options, o = st.makeArray(t), r = s.length; r--;)
                        if (n = s[r], st.inArray(st.valHooks.option.get(n), o) >= 0) try {
                            n.selected = i = !0
                        } catch (a) {
                            n.scrollHeight
                        } else n.selected = !1;
                    return i || (e.selectedIndex = -1), s
                }
            }
        }
    }), st.each(["radio", "checkbox"], function() {
        st.valHooks[this] = {
            set: function(e, t) {
                return st.isArray(t) ? e.checked = st.inArray(st(e).val(), t) >= 0 : void 0
            }
        }, it.checkOn || (st.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var xi, $i, ki = st.expr.attrHandle,
        _i = /^(?:checked|selected)$/i,
        Si = it.getSetAttribute,
        Ci = it.input;
    st.fn.extend({
        attr: function(e, t) {
            return Et(this, st.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                st.removeAttr(this, e)
            })
        }
    }), st.extend({
        attr: function(e, t, i) {
            var n, s, o = e.nodeType;
            if (e && 3 !== o && 8 !== o && 2 !== o) return typeof e.getAttribute === kt ? st.prop(e, t, i) : (1 === o && st.isXMLDoc(e) || (t = t.toLowerCase(), n = st.attrHooks[t] || (st.expr.match.bool.test(t) ? $i : xi)), void 0 === i ? n && "get" in n && null !== (s = n.get(e, t)) ? s : (s = st.find.attr(e, t), null == s ? void 0 : s) : null !== i ? n && "set" in n && void 0 !== (s = n.set(e, i, t)) ? s : (e.setAttribute(t, i + ""), i) : void st.removeAttr(e, t))
        },
        removeAttr: function(e, t) {
            var i, n, s = 0,
                o = t && t.match(bt);
            if (o && 1 === e.nodeType)
                for (; i = o[s++];) n = st.propFix[i] || i, st.expr.match.bool.test(i) ? Ci && Si || !_i.test(i) ? e[n] = !1 : e[st.camelCase("default-" + i)] = e[n] = !1 : st.attr(e, i, ""), e.removeAttribute(Si ? i : n)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!it.radioValue && "radio" === t && st.nodeName(e, "input")) {
                        var i = e.value;
                        return e.setAttribute("type", t), i && (e.value = i), t
                    }
                }
            }
        }
    }), $i = {
        set: function(e, t, i) {
            return t === !1 ? st.removeAttr(e, i) : Ci && Si || !_i.test(i) ? e.setAttribute(!Si && st.propFix[i] || i, i) : e[st.camelCase("default-" + i)] = e[i] = !0, i
        }
    }, st.each(st.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var i = ki[t] || st.find.attr;
        ki[t] = Ci && Si || !_i.test(t) ? function(e, t, n) {
            var s, o;
            return n || (o = ki[t], ki[t] = s, s = null != i(e, t, n) ? t.toLowerCase() : null, ki[t] = o), s
        } : function(e, t, i) {
            return i ? void 0 : e[st.camelCase("default-" + t)] ? t.toLowerCase() : null
        }
    }), Ci && Si || (st.attrHooks.value = {
        set: function(e, t, i) {
            return st.nodeName(e, "input") ? void(e.defaultValue = t) : xi && xi.set(e, t, i)
        }
    }), Si || (xi = {
        set: function(e, t, i) {
            var n = e.getAttributeNode(i);
            return n || e.setAttributeNode(n = e.ownerDocument.createAttribute(i)), n.value = t += "", "value" === i || t === e.getAttribute(i) ? t : void 0
        }
    }, ki.id = ki.name = ki.coords = function(e, t, i) {
        var n;
        return i ? void 0 : (n = e.getAttributeNode(t)) && "" !== n.value ? n.value : null
    }, st.valHooks.button = {
        get: function(e, t) {
            var i = e.getAttributeNode(t);
            return i && i.specified ? i.value : void 0
        },
        set: xi.set
    }, st.attrHooks.contenteditable = {
        set: function(e, t, i) {
            xi.set(e, "" === t ? !1 : t, i)
        }
    }, st.each(["width", "height"], function(e, t) {
        st.attrHooks[t] = {
            set: function(e, i) {
                return "" === i ? (e.setAttribute(t, "auto"), i) : void 0
            }
        }
    })), it.style || (st.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || void 0
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    });
    var Ti = /^(?:input|select|textarea|button|object)$/i,
        Di = /^(?:a|area)$/i;
    st.fn.extend({
        prop: function(e, t) {
            return Et(this, st.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = st.propFix[e] || e, this.each(function() {
                try {
                    this[e] = void 0, delete this[e]
                } catch (t) {}
            })
        }
    }), st.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(e, t, i) {
            var n, s, o, r = e.nodeType;
            if (e && 3 !== r && 8 !== r && 2 !== r) return o = 1 !== r || !st.isXMLDoc(e), o && (t = st.propFix[t] || t, s = st.propHooks[t]), void 0 !== i ? s && "set" in s && void 0 !== (n = s.set(e, i, t)) ? n : e[t] = i : s && "get" in s && null !== (n = s.get(e, t)) ? n : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = st.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Ti.test(e.nodeName) || Di.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }), it.hrefNormalized || st.each(["href", "src"], function(e, t) {
        st.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    }), it.optSelected || (st.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    }), st.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        st.propFix[this.toLowerCase()] = this
    }), it.enctype || (st.propFix.enctype = "encoding");
    var Ei = /[\t\r\n\f]/g;
    st.fn.extend({
        addClass: function(e) {
            var t, i, n, s, o, r, a = 0,
                l = this.length,
                c = "string" == typeof e && e;
            if (st.isFunction(e)) return this.each(function(t) {
                st(this).addClass(e.call(this, t, this.className))
            });
            if (c)
                for (t = (e || "").match(bt) || []; l > a; a++)
                    if (i = this[a], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Ei, " ") : " ")) {
                        for (o = 0; s = t[o++];) n.indexOf(" " + s + " ") < 0 && (n += s + " ");
                        r = st.trim(n), i.className !== r && (i.className = r)
                    }
            return this
        },
        removeClass: function(e) {
            var t, i, n, s, o, r, a = 0,
                l = this.length,
                c = 0 === arguments.length || "string" == typeof e && e;
            if (st.isFunction(e)) return this.each(function(t) {
                st(this).removeClass(e.call(this, t, this.className))
            });
            if (c)
                for (t = (e || "").match(bt) || []; l > a; a++)
                    if (i = this[a], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Ei, " ") : "")) {
                        for (o = 0; s = t[o++];)
                            for (; n.indexOf(" " + s + " ") >= 0;) n = n.replace(" " + s + " ", " ");
                        r = e ? st.trim(n) : "", i.className !== r && (i.className = r)
                    }
            return this
        },
        toggleClass: function(e, t) {
            var i = typeof e;
            return "boolean" == typeof t && "string" === i ? t ? this.addClass(e) : this.removeClass(e) : this.each(st.isFunction(e) ? function(i) {
                st(this).toggleClass(e.call(this, i, this.className, t), t)
            } : function() {
                if ("string" === i)
                    for (var t, n = 0, s = st(this), o = e.match(bt) || []; t = o[n++];) s.hasClass(t) ? s.removeClass(t) : s.addClass(t);
                else(i === kt || "boolean" === i) && (this.className && st._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : st._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            for (var t = " " + e + " ", i = 0, n = this.length; n > i; i++)
                if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(Ei, " ").indexOf(t) >= 0) return !0;
            return !1
        }
    }), st.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        st.fn[t] = function(e, i) {
            return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
        }
    }), st.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        },
        bind: function(e, t, i) {
            return this.on(e, null, t, i)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, i, n) {
            return this.on(t, e, i, n)
        },
        undelegate: function(e, t, i) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
        }
    });
    var Oi = st.now(),
        Ai = /\?/,
        Mi = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    st.parseJSON = function(t) {
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t + "");
        var i, n = null,
            s = st.trim(t + "");
        return s && !st.trim(s.replace(Mi, function(e, t, s, o) {
            return i && t && (n = 0), 0 === n ? e : (i = s || t, n += !o - !s, "")
        })) ? Function("return " + s)() : st.error("Invalid JSON: " + t)
    }, st.parseXML = function(t) {
        var i, n;
        if (!t || "string" != typeof t) return null;
        try {
            e.DOMParser ? (n = new DOMParser, i = n.parseFromString(t, "text/xml")) : (i = new ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(t))
        } catch (s) {
            i = void 0
        }
        return i && i.documentElement && !i.getElementsByTagName("parsererror").length || st.error("Invalid XML: " + t), i
    };
    var Ii, Ni, ji = /#.*$/,
        Pi = /([?&])_=[^&]*/,
        Fi = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Hi = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Li = /^(?:GET|HEAD)$/,
        zi = /^\/\//,
        qi = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        Wi = {},
        Ri = {},
        Bi = "*/".concat("*");
    try {
        Ni = location.href
    } catch (Ui) {
        Ni = ft.createElement("a"), Ni.href = "", Ni = Ni.href
    }
    Ii = qi.exec(Ni.toLowerCase()) || [], st.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ni,
            type: "GET",
            isLocal: Hi.test(Ii[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Bi,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": st.parseJSON,
                "text xml": st.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? q(q(e, st.ajaxSettings), t) : q(st.ajaxSettings, e)
        },
        ajaxPrefilter: L(Wi),
        ajaxTransport: L(Ri),
        ajax: function(e, t) {
            function i(e, t, i, n) {
                var s, d, v, y, w, $ = t;
                2 !== b && (b = 2, a && clearTimeout(a), c = void 0, r = n || "", x.readyState = e > 0 ? 4 : 0, s = e >= 200 && 300 > e || 304 === e, i && (y = W(u, x, i)), y = R(u, y, x, s), s ? (u.ifModified && (w = x.getResponseHeader("Last-Modified"), w && (st.lastModified[o] = w), w = x.getResponseHeader("etag"), w && (st.etag[o] = w)), 204 === e || "HEAD" === u.type ? $ = "nocontent" : 304 === e ? $ = "notmodified" : ($ = y.state, d = y.data, v = y.error, s = !v)) : (v = $, (e || !$) && ($ = "error", 0 > e && (e = 0))), x.status = e, x.statusText = (t || $) + "", s ? f.resolveWith(h, [d, $, x]) : f.rejectWith(h, [x, $, v]), x.statusCode(g), g = void 0, l && p.trigger(s ? "ajaxSuccess" : "ajaxError", [x, u, s ? d : v]), m.fireWith(h, [x, $]), l && (p.trigger("ajaxComplete", [x, u]), --st.active || st.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var n, s, o, r, a, l, c, d, u = st.ajaxSetup({}, t),
                h = u.context || u,
                p = u.context && (h.nodeType || h.jquery) ? st(h) : st.event,
                f = st.Deferred(),
                m = st.Callbacks("once memory"),
                g = u.statusCode || {},
                v = {},
                y = {},
                b = 0,
                w = "canceled",
                x = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === b) {
                            if (!d)
                                for (d = {}; t = Fi.exec(r);) d[t[1].toLowerCase()] = t[2];
                            t = d[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === b ? r : null
                    },
                    setRequestHeader: function(e, t) {
                        var i = e.toLowerCase();
                        return b || (e = y[i] = y[i] || e, v[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return b || (u.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > b)
                                for (t in e) g[t] = [g[t], e[t]];
                            else x.always(e[x.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || w;
                        return c && c.abort(t), i(0, t), this
                    }
                };
            if (f.promise(x).complete = m.add, x.success = x.done, x.error = x.fail, u.url = ((e || u.url || Ni) + "").replace(ji, "").replace(zi, Ii[1] + "//"), u.type = t.method || t.type || u.method || u.type, u.dataTypes = st.trim(u.dataType || "*").toLowerCase().match(bt) || [""], null == u.crossDomain && (n = qi.exec(u.url.toLowerCase()), u.crossDomain = !(!n || n[1] === Ii[1] && n[2] === Ii[2] && (n[3] || ("http:" === n[1] ? "80" : "443")) === (Ii[3] || ("http:" === Ii[1] ? "80" : "443")))), u.data && u.processData && "string" != typeof u.data && (u.data = st.param(u.data, u.traditional)), z(Wi, u, t, x), 2 === b) return x;
            l = u.global, l && 0 === st.active++ && st.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), u.hasContent = !Li.test(u.type), o = u.url, u.hasContent || (u.data && (o = u.url += (Ai.test(o) ? "&" : "?") + u.data, delete u.data), u.cache === !1 && (u.url = Pi.test(o) ? o.replace(Pi, "$1_=" + Oi++) : o + (Ai.test(o) ? "&" : "?") + "_=" + Oi++)), u.ifModified && (st.lastModified[o] && x.setRequestHeader("If-Modified-Since", st.lastModified[o]), st.etag[o] && x.setRequestHeader("If-None-Match", st.etag[o])), (u.data && u.hasContent && u.contentType !== !1 || t.contentType) && x.setRequestHeader("Content-Type", u.contentType), x.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + Bi + "; q=0.01" : "") : u.accepts["*"]);
            for (s in u.headers) x.setRequestHeader(s, u.headers[s]);
            if (u.beforeSend && (u.beforeSend.call(h, x, u) === !1 || 2 === b)) return x.abort();
            w = "abort";
            for (s in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) x[s](u[s]);
            if (c = z(Ri, u, t, x)) {
                x.readyState = 1, l && p.trigger("ajaxSend", [x, u]), u.async && u.timeout > 0 && (a = setTimeout(function() {
                    x.abort("timeout")
                }, u.timeout));
                try {
                    b = 1, c.send(v, i)
                } catch ($) {
                    if (!(2 > b)) throw $;
                    i(-1, $)
                }
            } else i(-1, "No Transport");
            return x
        },
        getJSON: function(e, t, i) {
            return st.get(e, t, i, "json")
        },
        getScript: function(e, t) {
            return st.get(e, void 0, t, "script")
        }
    }), st.each(["get", "post"], function(e, t) {
        st[t] = function(e, i, n, s) {
            return st.isFunction(i) && (s = s || n, n = i, i = void 0), st.ajax({
                url: e,
                type: t,
                dataType: s,
                data: i,
                success: n
            })
        }
    }), st.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        st.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), st._evalUrl = function(e) {
        return st.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }, st.fn.extend({
        wrapAll: function(e) {
            if (st.isFunction(e)) return this.each(function(t) {
                st(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = st(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return this.each(st.isFunction(e) ? function(t) {
                st(this).wrapInner(e.call(this, t))
            } : function() {
                var t = st(this),
                    i = t.contents();
                i.length ? i.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = st.isFunction(e);
            return this.each(function(i) {
                st(this).wrapAll(t ? e.call(this, i) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                st.nodeName(this, "body") || st(this).replaceWith(this.childNodes)
            }).end()
        }
    }), st.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !it.reliableHiddenOffsets() && "none" === (e.style && e.style.display || st.css(e, "display"))
    }, st.expr.filters.visible = function(e) {
        return !st.expr.filters.hidden(e)
    };
    var Yi = /%20/g,
        Gi = /\[\]$/,
        Xi = /\r?\n/g,
        Vi = /^(?:submit|button|image|reset|file)$/i,
        Qi = /^(?:input|select|textarea|keygen)/i;
    st.param = function(e, t) {
        var i, n = [],
            s = function(e, t) {
                t = st.isFunction(t) ? t() : null == t ? "" : t, n[n.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (void 0 === t && (t = st.ajaxSettings && st.ajaxSettings.traditional), st.isArray(e) || e.jquery && !st.isPlainObject(e)) st.each(e, function() {
            s(this.name, this.value)
        });
        else
            for (i in e) B(i, e[i], t, s);
        return n.join("&").replace(Yi, "+")
    }, st.fn.extend({
        serialize: function() {
            return st.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = st.prop(this, "elements");
                return e ? st.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !st(this).is(":disabled") && Qi.test(this.nodeName) && !Vi.test(e) && (this.checked || !Ot.test(e))
            }).map(function(e, t) {
                var i = st(this).val();
                return null == i ? null : st.isArray(i) ? st.map(i, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Xi, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: i.replace(Xi, "\r\n")
                }
            }).get()
        }
    }), st.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function() {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && U() || Y()
    } : U;
    var Ji = 0,
        Ki = {},
        Zi = st.ajaxSettings.xhr();
    e.ActiveXObject && st(e).on("unload", function() {
        for (var e in Ki) Ki[e](void 0, !0)
    }), it.cors = !!Zi && "withCredentials" in Zi, Zi = it.ajax = !!Zi, Zi && st.ajaxTransport(function(e) {
        if (!e.crossDomain || it.cors) {
            var t;
            return {
                send: function(i, n) {
                    var s, o = e.xhr(),
                        r = ++Ji;
                    if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (s in e.xhrFields) o[s] = e.xhrFields[s];
                    e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
                    for (s in i) void 0 !== i[s] && o.setRequestHeader(s, i[s] + "");
                    o.send(e.hasContent && e.data || null), t = function(i, s) {
                        var a, l, c;
                        if (t && (s || 4 === o.readyState))
                            if (delete Ki[r], t = void 0, o.onreadystatechange = st.noop, s) 4 !== o.readyState && o.abort();
                            else {
                                c = {}, a = o.status, "string" == typeof o.responseText && (c.text = o.responseText);
                                try {
                                    l = o.statusText
                                } catch (d) {
                                    l = ""
                                }
                                a || !e.isLocal || e.crossDomain ? 1223 === a && (a = 204) : a = c.text ? 200 : 404
                            }
                        c && n(a, l, c, o.getAllResponseHeaders())
                    }, e.async ? 4 === o.readyState ? setTimeout(t) : o.onreadystatechange = Ki[r] = t : t()
                },
                abort: function() {
                    t && t(void 0, !0)
                }
            }
        }
    }), st.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return st.globalEval(e), e
            }
        }
    }), st.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), st.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, i = ft.head || st("head")[0] || ft.documentElement;
            return {
                send: function(n, s) {
                    t = ft.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function(e, i) {
                        (i || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, i || s(200, "success"))
                    }, i.insertBefore(t, i.firstChild)
                },
                abort: function() {
                    t && t.onload(void 0, !0)
                }
            }
        }
    });
    var en = [],
        tn = /(=)\?(?=&|$)|\?\?/;
    st.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = en.pop() || st.expando + "_" + Oi++;
            return this[e] = !0, e
        }
    }), st.ajaxPrefilter("json jsonp", function(t, i, n) {
        var s, o, r, a = t.jsonp !== !1 && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
        return a || "jsonp" === t.dataTypes[0] ? (s = t.jsonpCallback = st.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(tn, "$1" + s) : t.jsonp !== !1 && (t.url += (Ai.test(t.url) ? "&" : "?") + t.jsonp + "=" + s), t.converters["script json"] = function() {
            return r || st.error(s + " was not called"), r[0]
        }, t.dataTypes[0] = "json", o = e[s], e[s] = function() {
            r = arguments
        }, n.always(function() {
            e[s] = o, t[s] && (t.jsonpCallback = i.jsonpCallback, en.push(s)), r && st.isFunction(o) && o(r[0]), r = o = void 0
        }), "script") : void 0
    }), st.parseHTML = function(e, t, i) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && (i = t, t = !1), t = t || ft;
        var n = ut.exec(e),
            s = !i && [];
        return n ? [t.createElement(n[1])] : (n = st.buildFragment([e], t, s), s && s.length && st(s).remove(), st.merge([], n.childNodes))
    };
    var nn = st.fn.load;
    st.fn.load = function(e, t, i) {
        if ("string" != typeof e && nn) return nn.apply(this, arguments);
        var n, s, o, r = this,
            a = e.indexOf(" ");
        return a >= 0 && (n = st.trim(e.slice(a, e.length)), e = e.slice(0, a)), st.isFunction(t) ? (i = t, t = void 0) : t && "object" == typeof t && (o = "POST"), r.length > 0 && st.ajax({
            url: e,
            type: o,
            dataType: "html",
            data: t
        }).done(function(e) {
            s = arguments, r.html(n ? st("<div>").append(st.parseHTML(e)).find(n) : e)
        }).complete(i && function(e, t) {
            r.each(i, s || [e.responseText, t, e])
        }), this
    }, st.expr.filters.animated = function(e) {
        return st.grep(st.timers, function(t) {
            return e === t.elem
        }).length
    };
    var sn = e.document.documentElement;
    st.offset = {
        setOffset: function(e, t, i) {
            var n, s, o, r, a, l, c, d = st.css(e, "position"),
                u = st(e),
                h = {};
            "static" === d && (e.style.position = "relative"), a = u.offset(), o = st.css(e, "top"), l = st.css(e, "left"), c = ("absolute" === d || "fixed" === d) && st.inArray("auto", [o, l]) > -1, c ? (n = u.position(), r = n.top, s = n.left) : (r = parseFloat(o) || 0, s = parseFloat(l) || 0), st.isFunction(t) && (t = t.call(e, i, a)), null != t.top && (h.top = t.top - a.top + r), null != t.left && (h.left = t.left - a.left + s), "using" in t ? t.using.call(e, h) : u.css(h)
        }
    }, st.fn.extend({
        offset: function(e) {
            if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                st.offset.setOffset(this, e, t)
            });
            var t, i, n = {
                    top: 0,
                    left: 0
                },
                s = this[0],
                o = s && s.ownerDocument;
            if (o) return t = o.documentElement, st.contains(t, s) ? (typeof s.getBoundingClientRect !== kt && (n = s.getBoundingClientRect()), i = G(o), {
                top: n.top + (i.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: n.left + (i.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : n
        },
        position: function() {
            if (this[0]) {
                var e, t, i = {
                        top: 0,
                        left: 0
                    },
                    n = this[0];
                return "fixed" === st.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), st.nodeName(e[0], "html") || (i = e.offset()), i.top += st.css(e[0], "borderTopWidth", !0), i.left += st.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - i.top - st.css(n, "marginTop", !0),
                    left: t.left - i.left - st.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || sn; e && !st.nodeName(e, "html") && "static" === st.css(e, "position");) e = e.offsetParent;
                return e || sn
            })
        }
    }), st.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, t) {
        var i = /Y/.test(t);
        st.fn[e] = function(n) {
            return Et(this, function(e, n, s) {
                var o = G(e);
                return void 0 === s ? o ? t in o ? o[t] : o.document.documentElement[n] : e[n] : void(o ? o.scrollTo(i ? st(o).scrollLeft() : s, i ? s : st(o).scrollTop()) : e[n] = s)
            }, e, n, arguments.length, null)
        }
    }), st.each(["top", "left"], function(e, t) {
        st.cssHooks[t] = C(it.pixelPosition, function(e, i) {
            return i ? (i = ti(e, t), ni.test(i) ? st(e).position()[t] + "px" : i) : void 0
        })
    }), st.each({
        Height: "height",
        Width: "width"
    }, function(e, t) {
        st.each({
            padding: "inner" + e,
            content: t,
            "": "outer" + e
        }, function(i, n) {
            st.fn[n] = function(n, s) {
                var o = arguments.length && (i || "boolean" != typeof n),
                    r = i || (n === !0 || s === !0 ? "margin" : "border");
                return Et(this, function(t, i, n) {
                    var s;
                    return st.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (s = t.documentElement, Math.max(t.body["scroll" + e], s["scroll" + e], t.body["offset" + e], s["offset" + e], s["client" + e])) : void 0 === n ? st.css(t, i, r) : st.style(t, i, n, r)
                }, t, o ? n : void 0, o, null)
            }
        })
    }), st.fn.size = function() {
        return this.length
    }, st.fn.andSelf = st.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return st
    });
    var on = e.jQuery,
        rn = e.$;
    return st.noConflict = function(t) {
        return e.$ === st && (e.$ = rn), t && e.jQuery === st && (e.jQuery = on), st
    }, typeof t === kt && (e.jQuery = e.$ = st), st
}),
function(e, t) {
    e.rails !== t && e.error("jquery-ujs has already been loaded!");
    var i, n = e(document);
    e.rails = i = {
        linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with], a[data-disable]",
        buttonClickSelector: "button[data-remote], button[data-confirm]",
        inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
        formSubmitSelector: "form",
        formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not([type])",
        disableSelector: "input[data-disable-with]:enabled, button[data-disable-with]:enabled, textarea[data-disable-with]:enabled, input[data-disable]:enabled, button[data-disable]:enabled, textarea[data-disable]:enabled",
        enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled, input[data-disable]:disabled, button[data-disable]:disabled, textarea[data-disable]:disabled",
        requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])",
        fileInputSelector: "input[type=file]",
        linkDisableSelector: "a[data-disable-with], a[data-disable]",
        buttonDisableSelector: "button[data-remote][data-disable-with], button[data-remote][data-disable]",
        CSRFProtection: function(t) {
            var i = e('meta[name="csrf-token"]').attr("content");
            i && t.setRequestHeader("X-CSRF-Token", i)
        },
        refreshCSRFTokens: function() {
            var t = e("meta[name=csrf-token]").attr("content"),
                i = e("meta[name=csrf-param]").attr("content");
            e('form input[name="' + i + '"]').val(t)
        },
        fire: function(t, i, n) {
            var s = e.Event(i);
            return t.trigger(s, n), s.result !== !1
        },
        confirm: function(e) {
            return confirm(e)
        },
        ajax: function(t) {
            return e.ajax(t)
        },
        href: function(e) {
            return e.attr("href")
        },
        handleRemote: function(n) {
            var s, o, r, a, l, c, d, u;
            if (i.fire(n, "ajax:before")) {
                if (a = n.data("cross-domain"), l = a === t ? null : a, c = n.data("with-credentials") || null, d = n.data("type") || e.ajaxSettings && e.ajaxSettings.dataType, n.is("form")) {
                    s = n.attr("method"), o = n.attr("action"), r = n.serializeArray();
                    var h = n.data("ujs:submit-button");
                    h && (r.push(h), n.data("ujs:submit-button", null))
                } else n.is(i.inputChangeSelector) ? (s = n.data("method"), o = n.data("url"), r = n.serialize(), n.data("params") && (r = r + "&" + n.data("params"))) : n.is(i.buttonClickSelector) ? (s = n.data("method") || "get", o = n.data("url"), r = n.serialize(), n.data("params") && (r = r + "&" + n.data("params"))) : (s = n.data("method"), o = i.href(n), r = n.data("params") || null);
                return u = {
                    type: s || "GET",
                    data: r,
                    dataType: d,
                    beforeSend: function(e, s) {
                        return s.dataType === t && e.setRequestHeader("accept", "*/*;q=0.5, " + s.accepts.script), i.fire(n, "ajax:beforeSend", [e, s]) ? void n.trigger("ajax:send", e) : !1
                    },
                    success: function(e, t, i) {
                        n.trigger("ajax:success", [e, t, i])
                    },
                    complete: function(e, t) {
                        n.trigger("ajax:complete", [e, t])
                    },
                    error: function(e, t, i) {
                        n.trigger("ajax:error", [e, t, i])
                    },
                    crossDomain: l
                }, c && (u.xhrFields = {
                    withCredentials: c
                }), o && (u.url = o), i.ajax(u)
            }
            return !1
        },
        handleMethod: function(n) {
            var s = i.href(n),
                o = n.data("method"),
                r = n.attr("target"),
                a = e("meta[name=csrf-token]").attr("content"),
                l = e("meta[name=csrf-param]").attr("content"),
                c = e('<form method="post" action="' + s + '"></form>'),
                d = '<input name="_method" value="' + o + '" type="hidden" />';
            l !== t && a !== t && (d += '<input name="' + l + '" value="' + a + '" type="hidden" />'), r && c.attr("target", r), c.hide().append(d).appendTo("body"), c.submit()
        },
        formElements: function(t, i) {
            return t.is("form") ? e(t[0].elements).filter(i) : t.find(i)
        },
        disableFormElements: function(t) {
            i.formElements(t, i.disableSelector).each(function() {
                i.disableFormElement(e(this))
            })
        },
        disableFormElement: function(e) {
            var i, n;
            i = e.is("button") ? "html" : "val", n = e.data("disable-with"), e.data("ujs:enable-with", e[i]()), n !== t && e[i](n), e.prop("disabled", !0)
        },
        enableFormElements: function(t) {
            i.formElements(t, i.enableSelector).each(function() {
                i.enableFormElement(e(this))
            })
        },
        enableFormElement: function(e) {
            var t = e.is("button") ? "html" : "val";
            e.data("ujs:enable-with") && e[t](e.data("ujs:enable-with")), e.prop("disabled", !1)
        },
        allowAction: function(e) {
            var t, n = e.data("confirm"),
                s = !1;
            return n ? (i.fire(e, "confirm") && (s = i.confirm(n), t = i.fire(e, "confirm:complete", [s])), s && t) : !0
        },
        blankInputs: function(t, i, n) {
            var s, o, r = e(),
                a = i || "input,textarea",
                l = t.find(a);
            return l.each(function() {
                if (s = e(this), o = s.is("input[type=checkbox],input[type=radio]") ? s.is(":checked") : s.val(), !o == !n) {
                    if (s.is("input[type=radio]") && l.filter('input[type=radio]:checked[name="' + s.attr("name") + '"]').length) return !0;
                    r = r.add(s)
                }
            }), r.length ? r : !1
        },
        nonBlankInputs: function(e, t) {
            return i.blankInputs(e, t, !0)
        },
        stopEverything: function(t) {
            return e(t.target).trigger("ujs:everythingStopped"), t.stopImmediatePropagation(), !1
        },
        disableElement: function(e) {
            var n = e.data("disable-with");
            e.data("ujs:enable-with", e.html()), n !== t && e.html(n), e.bind("click.railsDisable", function(e) {
                return i.stopEverything(e)
            })
        },
        enableElement: function(e) {
            e.data("ujs:enable-with") !== t && (e.html(e.data("ujs:enable-with")), e.removeData("ujs:enable-with")), e.unbind("click.railsDisable")
        }
    }, i.fire(n, "rails:attachBindings") && (e.ajaxPrefilter(function(e, t, n) {
        e.crossDomain || i.CSRFProtection(n)
    }), n.delegate(i.linkDisableSelector, "ajax:complete", function() {
        i.enableElement(e(this))
    }), n.delegate(i.buttonDisableSelector, "ajax:complete", function() {
        i.enableFormElement(e(this))
    }), n.delegate(i.linkClickSelector, "click.rails", function(n) {
        var s = e(this),
            o = s.data("method"),
            r = s.data("params"),
            a = n.metaKey || n.ctrlKey;
        if (!i.allowAction(s)) return i.stopEverything(n);
        if (!a && s.is(i.linkDisableSelector) && i.disableElement(s), s.data("remote") !== t) {
            if (a && (!o || "GET" === o) && !r) return !0;
            var l = i.handleRemote(s);
            return l === !1 ? i.enableElement(s) : l.error(function() {
                i.enableElement(s)
            }), !1
        }
        return s.data("method") ? (i.handleMethod(s), !1) : void 0
    }), n.delegate(i.buttonClickSelector, "click.rails", function(t) {
        var n = e(this);
        if (!i.allowAction(n)) return i.stopEverything(t);
        n.is(i.buttonDisableSelector) && i.disableFormElement(n);
        var s = i.handleRemote(n);
        return s === !1 ? i.enableFormElement(n) : s.error(function() {
            i.enableFormElement(n)
        }), !1
    }), n.delegate(i.inputChangeSelector, "change.rails", function(t) {
        var n = e(this);
        return i.allowAction(n) ? (i.handleRemote(n), !1) : i.stopEverything(t)
    }), n.delegate(i.formSubmitSelector, "submit.rails", function(n) {
        var s, o, r = e(this),
            a = r.data("remote") !== t;
        if (!i.allowAction(r)) return i.stopEverything(n);
        if (r.attr("novalidate") == t && (s = i.blankInputs(r, i.requiredInputSelector), s && i.fire(r, "ajax:aborted:required", [s]))) return i.stopEverything(n);
        if (a) {
            if (o = i.nonBlankInputs(r, i.fileInputSelector)) {
                setTimeout(function() {
                    i.disableFormElements(r)
                }, 13);
                var l = i.fire(r, "ajax:aborted:file", [o]);
                return l || setTimeout(function() {
                    i.enableFormElements(r)
                }, 13), l
            }
            return i.handleRemote(r), !1
        }
        setTimeout(function() {
            i.disableFormElements(r)
        }, 13)
    }), n.delegate(i.formInputClickSelector, "click.rails", function(t) {
        var n = e(this);
        if (!i.allowAction(n)) return i.stopEverything(t);
        var s = n.attr("name"),
            o = s ? {
                name: s,
                value: n.val()
            } : null;
        n.closest("form").data("ujs:submit-button", o)
    }), n.delegate(i.formSubmitSelector, "ajax:send.rails", function(t) {
        this == t.target && i.disableFormElements(e(this))
    }), n.delegate(i.formSubmitSelector, "ajax:complete.rails", function(t) {
        this == t.target && i.enableFormElements(e(this))
    }), e(function() {
        i.refreshCSRFTokens()
    }))
}(jQuery),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function(e) {
    function t(t, n) {
        var s, o, r, a = t.nodeName.toLowerCase();
        return "area" === a ? (s = t.parentNode, o = s.name, t.href && o && "map" === s.nodeName.toLowerCase() ? (r = e("img[usemap=#" + o + "]")[0], !!r && i(r)) : !1) : (/input|select|textarea|button|object/.test(a) ? !t.disabled : "a" === a ? t.href || n : n) && i(t)
    }

    function i(t) {
        return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function() {
            return "hidden" === e.css(this, "visibility")
        }).length
    }
    e.ui = e.ui || {}, e.extend(e.ui, {
        version: "1.11.0",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), e.fn.extend({
        scrollParent: function() {
            var t = this.css("position"),
                i = "absolute" === t,
                n = this.parents().filter(function() {
                    var t = e(this);
                    return i && "static" === t.css("position") ? !1 : /(auto|scroll)/.test(t.css("overflow") + t.css("overflow-y") + t.css("overflow-x"))
                }).eq(0);
            return "fixed" !== t && n.length ? n : e(this[0].ownerDocument || document)
        },
        uniqueId: function() {
            var e = 0;
            return function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++e)
                })
            }
        }(),
        removeUniqueId: function() {
            return this.each(function() {
                /^ui-id-\d+$/.test(this.id) && e(this).removeAttr("id")
            })
        }
    }), e.extend(e.expr[":"], {
        data: e.expr.createPseudo ? e.expr.createPseudo(function(t) {
            return function(i) {
                return !!e.data(i, t)
            }
        }) : function(t, i, n) {
            return !!e.data(t, n[3])
        },
        focusable: function(i) {
            return t(i, !isNaN(e.attr(i, "tabindex")))
        },
        tabbable: function(i) {
            var n = e.attr(i, "tabindex"),
                s = isNaN(n);
            return (s || n >= 0) && t(i, !s)
        }
    }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function(t, i) {
        function n(t, i, n, o) {
            return e.each(s, function() {
                i -= parseFloat(e.css(t, "padding" + this)) || 0, n && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), o && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
            }), i
        }
        var s = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
            o = i.toLowerCase(),
            r = {
                innerWidth: e.fn.innerWidth,
                innerHeight: e.fn.innerHeight,
                outerWidth: e.fn.outerWidth,
                outerHeight: e.fn.outerHeight
            };
        e.fn["inner" + i] = function(t) {
            return void 0 === t ? r["inner" + i].call(this) : this.each(function() {
                e(this).css(o, n(this, t) + "px")
            })
        }, e.fn["outer" + i] = function(t, s) {
            return "number" != typeof t ? r["outer" + i].call(this, t) : this.each(function() {
                e(this).css(o, n(this, t, !0, s) + "px")
            })
        }
    }), e.fn.addBack || (e.fn.addBack = function(e) {
        return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function(t) {
        return function(i) {
            return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this)
        }
    }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.fn.extend({
        focus: function(t) {
            return function(i, n) {
                return "number" == typeof i ? this.each(function() {
                    var t = this;
                    setTimeout(function() {
                        e(t).focus(), n && n.call(t)
                    }, i)
                }) : t.apply(this, arguments)
            }
        }(e.fn.focus),
        disableSelection: function() {
            var e = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
            return function() {
                return this.bind(e + ".ui-disableSelection", function(e) {
                    e.preventDefault()
                })
            }
        }(),
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        },
        zIndex: function(t) {
            if (void 0 !== t) return this.css("zIndex", t);
            if (this.length)
                for (var i, n, s = e(this[0]); s.length && s[0] !== document;) {
                    if (i = s.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (n = parseInt(s.css("zIndex"), 10), !isNaN(n) && 0 !== n)) return n;
                    s = s.parent()
                }
            return 0
        }
    }), e.ui.plugin = {
        add: function(t, i, n) {
            var s, o = e.ui[t].prototype;
            for (s in n) o.plugins[s] = o.plugins[s] || [], o.plugins[s].push([i, n[s]])
        },
        call: function(e, t, i, n) {
            var s, o = e.plugins[t];
            if (o && (n || e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType))
                for (s = 0; s < o.length; s++) e.options[o[s][0]] && o[s][1].apply(e.element, i)
        }
    }
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function(e) {
    var t = 0,
        i = Array.prototype.slice;
    return e.cleanData = function(t) {
        return function(i) {
            for (var n, s = 0; null != (n = i[s]); s++) try {
                e(n).triggerHandler("remove")
            } catch (o) {}
            t(i)
        }
    }(e.cleanData), e.widget = function(t, i, n) {
        var s, o, r, a, l = {},
            c = t.split(".")[0];
        return t = t.split(".")[1], s = c + "-" + t, n || (n = i, i = e.Widget), e.expr[":"][s.toLowerCase()] = function(t) {
            return !!e.data(t, s)
        }, e[c] = e[c] || {}, o = e[c][t], r = e[c][t] = function(e, t) {
            return this._createWidget ? void(arguments.length && this._createWidget(e, t)) : new r(e, t)
        }, e.extend(r, o, {
            version: n.version,
            _proto: e.extend({}, n),
            _childConstructors: []
        }), a = new i, a.options = e.widget.extend({}, a.options), e.each(n, function(t, n) {
            return e.isFunction(n) ? void(l[t] = function() {
                var e = function() {
                        return i.prototype[t].apply(this, arguments)
                    },
                    s = function(e) {
                        return i.prototype[t].apply(this, e)
                    };
                return function() {
                    var t, i = this._super,
                        o = this._superApply;
                    return this._super = e, this._superApply = s, t = n.apply(this, arguments), this._super = i, this._superApply = o, t
                }
            }()) : void(l[t] = n)
        }), r.prototype = e.widget.extend(a, {
            widgetEventPrefix: o ? a.widgetEventPrefix || t : t
        }, l, {
            constructor: r,
            namespace: c,
            widgetName: t,
            widgetFullName: s
        }), o ? (e.each(o._childConstructors, function(t, i) {
            var n = i.prototype;
            e.widget(n.namespace + "." + n.widgetName, r, i._proto)
        }), delete o._childConstructors) : i._childConstructors.push(r), e.widget.bridge(t, r), r
    }, e.widget.extend = function(t) {
        for (var n, s, o = i.call(arguments, 1), r = 0, a = o.length; a > r; r++)
            for (n in o[r]) s = o[r][n], o[r].hasOwnProperty(n) && void 0 !== s && (t[n] = e.isPlainObject(s) ? e.isPlainObject(t[n]) ? e.widget.extend({}, t[n], s) : e.widget.extend({}, s) : s);
        return t
    }, e.widget.bridge = function(t, n) {
        var s = n.prototype.widgetFullName || t;
        e.fn[t] = function(o) {
            var r = "string" == typeof o,
                a = i.call(arguments, 1),
                l = this;
            return o = !r && a.length ? e.widget.extend.apply(null, [o].concat(a)) : o, this.each(r ? function() {
                var i, n = e.data(this, s);
                return "instance" === o ? (l = n, !1) : n ? e.isFunction(n[o]) && "_" !== o.charAt(0) ? (i = n[o].apply(n, a), i !== n && void 0 !== i ? (l = i && i.jquery ? l.pushStack(i.get()) : i, !1) : void 0) : e.error("no such method '" + o + "' for " + t + " widget instance") : e.error("cannot call methods on " + t + " prior to initialization; attempted to call method '" + o + "'")
            } : function() {
                var t = e.data(this, s);
                t ? (t.option(o || {}), t._init && t._init()) : e.data(this, s, new n(o, this))
            }), l
        }
    }, e.Widget = function() {}, e.Widget._childConstructors = [], e.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(i, n) {
            n = e(n || this.defaultElement || this)[0], this.element = e(n), this.uuid = t++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = e.widget.extend({}, this.options, this._getCreateOptions(), i), this.bindings = e(), this.hoverable = e(), this.focusable = e(), n !== this && (e.data(n, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(e) {
                    e.target === n && this.destroy()
                }
            }), this.document = e(n.style ? n.ownerDocument : n.document || n), this.window = e(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: e.noop,
        _getCreateEventData: e.noop,
        _create: e.noop,
        _init: e.noop,
        destroy: function() {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: e.noop,
        widget: function() {
            return this.element
        },
        option: function(t, i) {
            var n, s, o, r = t;
            if (0 === arguments.length) return e.widget.extend({}, this.options);
            if ("string" == typeof t)
                if (r = {}, n = t.split("."), t = n.shift(), n.length) {
                    for (s = r[t] = e.widget.extend({}, this.options[t]), o = 0; o < n.length - 1; o++) s[n[o]] = s[n[o]] || {}, s = s[n[o]];
                    if (t = n.pop(), 1 === arguments.length) return void 0 === s[t] ? null : s[t];
                    s[t] = i
                } else {
                    if (1 === arguments.length) return void 0 === this.options[t] ? null : this.options[t];
                    r[t] = i
                }
            return this._setOptions(r), this
        },
        _setOptions: function(e) {
            var t;
            for (t in e) this._setOption(t, e[t]);
            return this
        },
        _setOption: function(e, t) {
            return this.options[e] = t, "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!t), t && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
        },
        enable: function() {
            return this._setOptions({
                disabled: !1
            })
        },
        disable: function() {
            return this._setOptions({
                disabled: !0
            })
        },
        _on: function(t, i, n) {
            var s, o = this;
            "boolean" != typeof t && (n = i, i = t, t = !1), n ? (i = s = e(i), this.bindings = this.bindings.add(i)) : (n = i, i = this.element, s = this.widget()), e.each(n, function(n, r) {
                function a() {
                    return t || o.options.disabled !== !0 && !e(this).hasClass("ui-state-disabled") ? ("string" == typeof r ? o[r] : r).apply(o, arguments) : void 0
                }
                "string" != typeof r && (a.guid = r.guid = r.guid || a.guid || e.guid++);
                var l = n.match(/^([\w:-]*)\s*(.*)$/),
                    c = l[1] + o.eventNamespace,
                    d = l[2];
                d ? s.delegate(d, c, a) : i.bind(c, a)
            })
        },
        _off: function(e, t) {
            t = (t || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(t).undelegate(t)
        },
        _delay: function(e, t) {
            function i() {
                return ("string" == typeof e ? n[e] : e).apply(n, arguments)
            }
            var n = this;
            return setTimeout(i, t || 0)
        },
        _hoverable: function(t) {
            this.hoverable = this.hoverable.add(t), this._on(t, {
                mouseenter: function(t) {
                    e(t.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(t) {
                    e(t.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(t) {
            this.focusable = this.focusable.add(t), this._on(t, {
                focusin: function(t) {
                    e(t.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(t) {
                    e(t.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(t, i, n) {
            var s, o, r = this.options[t];
            if (n = n || {}, i = e.Event(i), i.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(), i.target = this.element[0], o = i.originalEvent)
                for (s in o) s in i || (i[s] = o[s]);
            return this.element.trigger(i, n), !(e.isFunction(r) && r.apply(this.element[0], [i].concat(n)) === !1 || i.isDefaultPrevented())
        }
    }, e.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(t, i) {
        e.Widget.prototype["_" + t] = function(n, s, o) {
            "string" == typeof s && (s = {
                effect: s
            });
            var r, a = s ? s === !0 || "number" == typeof s ? i : s.effect || i : t;
            s = s || {}, "number" == typeof s && (s = {
                duration: s
            }), r = !e.isEmptyObject(s), s.complete = o, s.delay && n.delay(s.delay), r && e.effects && e.effects.effect[a] ? n[t](s) : a !== t && n[a] ? n[a](s.duration, s.easing, o) : n.queue(function(i) {
                e(this)[t](), o && o.call(n[0]), i()
            })
        }
    }), e.widget
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function(e) {
    return function() {
        function t(e, t, i) {
            return [parseFloat(e[0]) * (p.test(e[0]) ? t / 100 : 1), parseFloat(e[1]) * (p.test(e[1]) ? i / 100 : 1)]
        }

        function i(t, i) {
            return parseInt(e.css(t, i), 10) || 0
        }

        function n(t) {
            var i = t[0];
            return 9 === i.nodeType ? {
                width: t.width(),
                height: t.height(),
                offset: {
                    top: 0,
                    left: 0
                }
            } : e.isWindow(i) ? {
                width: t.width(),
                height: t.height(),
                offset: {
                    top: t.scrollTop(),
                    left: t.scrollLeft()
                }
            } : i.preventDefault ? {
                width: 0,
                height: 0,
                offset: {
                    top: i.pageY,
                    left: i.pageX
                }
            } : {
                width: t.outerWidth(),
                height: t.outerHeight(),
                offset: t.offset()
            }
        }
        e.ui = e.ui || {};
        var s, o, r = Math.max,
            a = Math.abs,
            l = Math.round,
            c = /left|center|right/,
            d = /top|center|bottom/,
            u = /[\+\-]\d+(\.[\d]+)?%?/,
            h = /^\w+/,
            p = /%$/,
            f = e.fn.position;
        e.position = {
                scrollbarWidth: function() {
                    if (void 0 !== s) return s;
                    var t, i, n = e("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                        o = n.children()[0];
                    return e("body").append(n), t = o.offsetWidth, n.css("overflow", "scroll"), i = o.offsetWidth, t === i && (i = n[0].clientWidth), n.remove(), s = t - i
                },
                getScrollInfo: function(t) {
                    var i = t.isWindow || t.isDocument ? "" : t.element.css("overflow-x"),
                        n = t.isWindow || t.isDocument ? "" : t.element.css("overflow-y"),
                        s = "scroll" === i || "auto" === i && t.width < t.element[0].scrollWidth,
                        o = "scroll" === n || "auto" === n && t.height < t.element[0].scrollHeight;
                    return {
                        width: o ? e.position.scrollbarWidth() : 0,
                        height: s ? e.position.scrollbarWidth() : 0
                    }
                },
                getWithinInfo: function(t) {
                    var i = e(t || window),
                        n = e.isWindow(i[0]),
                        s = !!i[0] && 9 === i[0].nodeType;
                    return {
                        element: i,
                        isWindow: n,
                        isDocument: s,
                        offset: i.offset() || {
                            left: 0,
                            top: 0
                        },
                        scrollLeft: i.scrollLeft(),
                        scrollTop: i.scrollTop(),
                        width: n ? i.width() : i.outerWidth(),
                        height: n ? i.height() : i.outerHeight()
                    }
                }
            }, e.fn.position = function(s) {
                if (!s || !s.of) return f.apply(this, arguments);
                s = e.extend({}, s);
                var p, m, g, v, y, b, w = e(s.of),
                    x = e.position.getWithinInfo(s.within),
                    $ = e.position.getScrollInfo(x),
                    k = (s.collision || "flip").split(" "),
                    _ = {};
                return b = n(w), w[0].preventDefault && (s.at = "left top"), m = b.width, g = b.height, v = b.offset, y = e.extend({}, v), e.each(["my", "at"], function() {
                    var e, t, i = (s[this] || "").split(" ");
                    1 === i.length && (i = c.test(i[0]) ? i.concat(["center"]) : d.test(i[0]) ? ["center"].concat(i) : ["center", "center"]), i[0] = c.test(i[0]) ? i[0] : "center", i[1] = d.test(i[1]) ? i[1] : "center", e = u.exec(i[0]), t = u.exec(i[1]), _[this] = [e ? e[0] : 0, t ? t[0] : 0], s[this] = [h.exec(i[0])[0], h.exec(i[1])[0]]
                }), 1 === k.length && (k[1] = k[0]), "right" === s.at[0] ? y.left += m : "center" === s.at[0] && (y.left += m / 2), "bottom" === s.at[1] ? y.top += g : "center" === s.at[1] && (y.top += g / 2), p = t(_.at, m, g), y.left += p[0], y.top += p[1], this.each(function() {
                    var n, c, d = e(this),
                        u = d.outerWidth(),
                        h = d.outerHeight(),
                        f = i(this, "marginLeft"),
                        b = i(this, "marginTop"),
                        S = u + f + i(this, "marginRight") + $.width,
                        C = h + b + i(this, "marginBottom") + $.height,
                        T = e.extend({}, y),
                        D = t(_.my, d.outerWidth(), d.outerHeight());
                    "right" === s.my[0] ? T.left -= u : "center" === s.my[0] && (T.left -= u / 2), "bottom" === s.my[1] ? T.top -= h : "center" === s.my[1] && (T.top -= h / 2), T.left += D[0], T.top += D[1], o || (T.left = l(T.left), T.top = l(T.top)), n = {
                        marginLeft: f,
                        marginTop: b
                    }, e.each(["left", "top"], function(t, i) {
                        e.ui.position[k[t]] && e.ui.position[k[t]][i](T, {
                            targetWidth: m,
                            targetHeight: g,
                            elemWidth: u,
                            elemHeight: h,
                            collisionPosition: n,
                            collisionWidth: S,
                            collisionHeight: C,
                            offset: [p[0] + D[0], p[1] + D[1]],
                            my: s.my,
                            at: s.at,
                            within: x,
                            elem: d
                        })
                    }), s.using && (c = function(e) {
                        var t = v.left - T.left,
                            i = t + m - u,
                            n = v.top - T.top,
                            o = n + g - h,
                            l = {
                                target: {
                                    element: w,
                                    left: v.left,
                                    top: v.top,
                                    width: m,
                                    height: g
                                },
                                element: {
                                    element: d,
                                    left: T.left,
                                    top: T.top,
                                    width: u,
                                    height: h
                                },
                                horizontal: 0 > i ? "left" : t > 0 ? "right" : "center",
                                vertical: 0 > o ? "top" : n > 0 ? "bottom" : "middle"
                            };
                        u > m && a(t + i) < m && (l.horizontal = "center"), h > g && a(n + o) < g && (l.vertical = "middle"), l.important = r(a(t), a(i)) > r(a(n), a(o)) ? "horizontal" : "vertical", s.using.call(this, e, l)
                    }), d.offset(e.extend(T, {
                        using: c
                    }))
                })
            }, e.ui.position = {
                fit: {
                    left: function(e, t) {
                        var i, n = t.within,
                            s = n.isWindow ? n.scrollLeft : n.offset.left,
                            o = n.width,
                            a = e.left - t.collisionPosition.marginLeft,
                            l = s - a,
                            c = a + t.collisionWidth - o - s;
                        t.collisionWidth > o ? l > 0 && 0 >= c ? (i = e.left + l + t.collisionWidth - o - s, e.left += l - i) : e.left = c > 0 && 0 >= l ? s : l > c ? s + o - t.collisionWidth : s : l > 0 ? e.left += l : c > 0 ? e.left -= c : e.left = r(e.left - a, e.left)
                    },
                    top: function(e, t) {
                        var i, n = t.within,
                            s = n.isWindow ? n.scrollTop : n.offset.top,
                            o = t.within.height,
                            a = e.top - t.collisionPosition.marginTop,
                            l = s - a,
                            c = a + t.collisionHeight - o - s;
                        t.collisionHeight > o ? l > 0 && 0 >= c ? (i = e.top + l + t.collisionHeight - o - s, e.top += l - i) : e.top = c > 0 && 0 >= l ? s : l > c ? s + o - t.collisionHeight : s : l > 0 ? e.top += l : c > 0 ? e.top -= c : e.top = r(e.top - a, e.top)
                    }
                },
                flip: {
                    left: function(e, t) {
                        var i, n, s = t.within,
                            o = s.offset.left + s.scrollLeft,
                            r = s.width,
                            l = s.isWindow ? s.scrollLeft : s.offset.left,
                            c = e.left - t.collisionPosition.marginLeft,
                            d = c - l,
                            u = c + t.collisionWidth - r - l,
                            h = "left" === t.my[0] ? -t.elemWidth : "right" === t.my[0] ? t.elemWidth : 0,
                            p = "left" === t.at[0] ? t.targetWidth : "right" === t.at[0] ? -t.targetWidth : 0,
                            f = -2 * t.offset[0];
                        0 > d ? (i = e.left + h + p + f + t.collisionWidth - r - o, (0 > i || i < a(d)) && (e.left += h + p + f)) : u > 0 && (n = e.left - t.collisionPosition.marginLeft + h + p + f - l, (n > 0 || a(n) < u) && (e.left += h + p + f))
                    },
                    top: function(e, t) {
                        var i, n, s = t.within,
                            o = s.offset.top + s.scrollTop,
                            r = s.height,
                            l = s.isWindow ? s.scrollTop : s.offset.top,
                            c = e.top - t.collisionPosition.marginTop,
                            d = c - l,
                            u = c + t.collisionHeight - r - l,
                            h = "top" === t.my[1],
                            p = h ? -t.elemHeight : "bottom" === t.my[1] ? t.elemHeight : 0,
                            f = "top" === t.at[1] ? t.targetHeight : "bottom" === t.at[1] ? -t.targetHeight : 0,
                            m = -2 * t.offset[1];
                        0 > d ? (n = e.top + p + f + m + t.collisionHeight - r - o, e.top + p + f + m > d && (0 > n || n < a(d)) && (e.top += p + f + m)) : u > 0 && (i = e.top - t.collisionPosition.marginTop + p + f + m - l, e.top + p + f + m > u && (i > 0 || a(i) < u) && (e.top += p + f + m))
                    }
                },
                flipfit: {
                    left: function() {
                        e.ui.position.flip.left.apply(this, arguments), e.ui.position.fit.left.apply(this, arguments)
                    },
                    top: function() {
                        e.ui.position.flip.top.apply(this, arguments), e.ui.position.fit.top.apply(this, arguments)
                    }
                }
            },
            function() {
                var t, i, n, s, r, a = document.getElementsByTagName("body")[0],
                    l = document.createElement("div");
                t = document.createElement(a ? "div" : "body"), n = {
                    visibility: "hidden",
                    width: 0,
                    height: 0,
                    border: 0,
                    margin: 0,
                    background: "none"
                }, a && e.extend(n, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
                for (r in n) t.style[r] = n[r];
                t.appendChild(l), i = a || document.documentElement, i.insertBefore(t, i.firstChild), l.style.cssText = "position: absolute; left: 10.7432222px;", s = e(l).offset().left, o = s > 10 && 11 > s, t.innerHTML = "", i.removeChild(t)
            }()
    }(), e.ui.position
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery", "./core", "./widget", "./position"], e) : e(jQuery)
}(function(e) {
    return e.widget("ui.menu", {
        version: "1.11.0",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {
                submenu: "ui-icon-carat-1-e"
            },
            items: "> *",
            menus: "ul",
            position: {
                my: "left-1 top",
                at: "right top"
            },
            role: "menu",
            blur: null,
            focus: null,
            select: null
        },
        _create: function() {
            this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                role: this.options.role,
                tabIndex: 0
            }), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({
                "mousedown .ui-menu-item": function(e) {
                    e.preventDefault()
                },
                "click .ui-menu-item": function(t) {
                    var i = e(t.target);
                    !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(t), t.isPropagationStopped() || (this.mouseHandled = !0), i.has(".ui-menu").length ? this.expand(t) : !this.element.is(":focus") && e(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                },
                "mouseenter .ui-menu-item": function(t) {
                    var i = e(t.currentTarget);
                    i.siblings(".ui-state-active").removeClass("ui-state-active"), this.focus(t, i)
                },
                mouseleave: "collapseAll",
                "mouseleave .ui-menu": "collapseAll",
                focus: function(e, t) {
                    var i = this.active || this.element.find(this.options.items).eq(0);
                    t || this.focus(e, i)
                },
                blur: function(t) {
                    this._delay(function() {
                        e.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(t)
                    })
                },
                keydown: "_keydown"
            }), this.refresh(), this._on(this.document, {
                click: function(e) {
                    this._closeOnDocumentClick(e) && this.collapseAll(e), this.mouseHandled = !1
                }
            })
        },
        _destroy: function() {
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
                var t = e(this);
                t.data("ui-menu-submenu-carat") && t.remove()
            }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
        },
        _keydown: function(t) {
            function i(e) {
                return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            }
            var n, s, o, r, a, l = !0;
            switch (t.keyCode) {
                case e.ui.keyCode.PAGE_UP:
                    this.previousPage(t);
                    break;
                case e.ui.keyCode.PAGE_DOWN:
                    this.nextPage(t);
                    break;
                case e.ui.keyCode.HOME:
                    this._move("first", "first", t);
                    break;
                case e.ui.keyCode.END:
                    this._move("last", "last", t);
                    break;
                case e.ui.keyCode.UP:
                    this.previous(t);
                    break;
                case e.ui.keyCode.DOWN:
                    this.next(t);
                    break;
                case e.ui.keyCode.LEFT:
                    this.collapse(t);
                    break;
                case e.ui.keyCode.RIGHT:
                    this.active && !this.active.is(".ui-state-disabled") && this.expand(t);
                    break;
                case e.ui.keyCode.ENTER:
                case e.ui.keyCode.SPACE:
                    this._activate(t);
                    break;
                case e.ui.keyCode.ESCAPE:
                    this.collapse(t);
                    break;
                default:
                    l = !1, s = this.previousFilter || "", o = String.fromCharCode(t.keyCode), r = !1, clearTimeout(this.filterTimer), o === s ? r = !0 : o = s + o, a = new RegExp("^" + i(o), "i"), n = this.activeMenu.find(this.options.items).filter(function() {
                        return a.test(e(this).text())
                    }), n = r && -1 !== n.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : n, n.length || (o = String.fromCharCode(t.keyCode), a = new RegExp("^" + i(o), "i"), n = this.activeMenu.find(this.options.items).filter(function() {
                        return a.test(e(this).text())
                    })), n.length ? (this.focus(t, n), n.length > 1 ? (this.previousFilter = o, this.filterTimer = this._delay(function() {
                        delete this.previousFilter
                    }, 1e3)) : delete this.previousFilter) : delete this.previousFilter
            }
            l && t.preventDefault()
        },
        _activate: function(e) {
            this.active.is(".ui-state-disabled") || (this.active.is("[aria-haspopup='true']") ? this.expand(e) : this.select(e))
        },
        refresh: function() {
            var t, i, n = this,
                s = this.options.icons.submenu,
                o = this.element.find(this.options.menus);
            this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length), o.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function() {
                var t = e(this),
                    i = t.parent(),
                    n = e("<span>").addClass("ui-menu-icon ui-icon " + s).data("ui-menu-submenu-carat", !0);
                i.attr("aria-haspopup", "true").prepend(n), t.attr("aria-labelledby", i.attr("id"))
            }), t = o.add(this.element), i = t.find(this.options.items), i.not(".ui-menu-item").each(function() {
                var t = e(this);
                n._isDivider(t) && t.addClass("ui-widget-content ui-menu-divider")
            }), i.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({
                tabIndex: -1,
                role: this._itemRole()
            }), i.filter(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !e.contains(this.element[0], this.active[0]) && this.blur()
        },
        _itemRole: function() {
            return {
                menu: "menuitem",
                listbox: "option"
            }[this.options.role]
        },
        _setOption: function(e, t) {
            "icons" === e && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(t.submenu), "disabled" === e && this.element.toggleClass("ui-state-disabled", !!t).attr("aria-disabled", t), this._super(e, t)
        },
        focus: function(e, t) {
            var i, n;
            this.blur(e, e && "focus" === e.type), this._scrollIntoView(t), this.active = t.first(), n = this.active.addClass("ui-state-focus").removeClass("ui-state-active"), this.options.role && this.element.attr("aria-activedescendant", n.attr("id")), this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"), e && "keydown" === e.type ? this._close() : this.timer = this._delay(function() {
                this._close()
            }, this.delay), i = t.children(".ui-menu"), i.length && e && /^mouse/.test(e.type) && this._startOpening(i), this.activeMenu = t.parent(), this._trigger("focus", e, {
                item: t
            })
        },
        _scrollIntoView: function(t) {
            var i, n, s, o, r, a;
            this._hasScroll() && (i = parseFloat(e.css(this.activeMenu[0], "borderTopWidth")) || 0, n = parseFloat(e.css(this.activeMenu[0], "paddingTop")) || 0, s = t.offset().top - this.activeMenu.offset().top - i - n, o = this.activeMenu.scrollTop(), r = this.activeMenu.height(), a = t.outerHeight(), 0 > s ? this.activeMenu.scrollTop(o + s) : s + a > r && this.activeMenu.scrollTop(o + s - r + a))
        },
        blur: function(e, t) {
            t || clearTimeout(this.timer), this.active && (this.active.removeClass("ui-state-focus"), this.active = null, this._trigger("blur", e, {
                item: this.active
            }))
        },
        _startOpening: function(e) {
            clearTimeout(this.timer), "true" === e.attr("aria-hidden") && (this.timer = this._delay(function() {
                this._close(), this._open(e)
            }, this.delay))
        },
        _open: function(t) {
            var i = e.extend({
                of: this.active
            }, this.options.position);
            clearTimeout(this.timer), this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden", "true"), t.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i)
        },
        collapseAll: function(t, i) {
            clearTimeout(this.timer), this.timer = this._delay(function() {
                var n = i ? this.element : e(t && t.target).closest(this.element.find(".ui-menu"));
                n.length || (n = this.element), this._close(n), this.blur(t), this.activeMenu = n
            }, this.delay)
        },
        _close: function(e) {
            e || (e = this.active ? this.active.parent() : this.element), e.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")
        },
        _closeOnDocumentClick: function(t) {
            return !e(t.target).closest(".ui-menu").length
        },
        _isDivider: function(e) {
            return !/[^\-\u2014\u2013\s]/.test(e.text())
        },
        collapse: function(e) {
            var t = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            t && t.length && (this._close(), this.focus(e, t))
        },
        expand: function(e) {
            var t = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
            t && t.length && (this._open(t.parent()), this._delay(function() {
                this.focus(e, t)
            }))
        },
        next: function(e) {
            this._move("next", "first", e)
        },
        previous: function(e) {
            this._move("prev", "last", e)
        },
        isFirstItem: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length
        },
        isLastItem: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length
        },
        _move: function(e, t, i) {
            var n;
            this.active && (n = "first" === e || "last" === e ? this.active["first" === e ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[e + "All"](".ui-menu-item").eq(0)), n && n.length && this.active || (n = this.activeMenu.find(this.options.items)[t]()), this.focus(i, n)
        },
        nextPage: function(t) {
            var i, n, s;
            return this.active ? void(this.isLastItem() || (this._hasScroll() ? (n = this.active.offset().top, s = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
                return i = e(this), i.offset().top - n - s < 0
            }), this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]()))) : void this.next(t)
        },
        previousPage: function(t) {
            var i, n, s;
            return this.active ? void(this.isFirstItem() || (this._hasScroll() ? (n = this.active.offset().top, s = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
                return i = e(this), i.offset().top - n + s > 0
            }), this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items).first()))) : void this.next(t)
        },
        _hasScroll: function() {
            return this.element.outerHeight() < this.element.prop("scrollHeight")
        },
        select: function(t) {
            this.active = this.active || e(t.target).closest(".ui-menu-item");
            var i = {
                item: this.active
            };
            this.active.has(".ui-menu").length || this.collapseAll(t, !0), this._trigger("select", t, i)
        }
    })
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery", "./core", "./widget", "./position", "./menu"], e) : e(jQuery)
}(function(e) {
    return e.widget("ui.autocomplete", {
        version: "1.11.0",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function() {
            var t, i, n, s = this.element[0].nodeName.toLowerCase(),
                o = "textarea" === s,
                r = "input" === s;
            this.isMultiLine = o ? !0 : r ? !1 : this.element.prop("isContentEditable"), this.valueMethod = this.element[o || r ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, {
                keydown: function(s) {
                    if (this.element.prop("readOnly")) return t = !0, n = !0, void(i = !0);
                    t = !1, n = !1, i = !1;
                    var o = e.ui.keyCode;
                    switch (s.keyCode) {
                        case o.PAGE_UP:
                            t = !0, this._move("previousPage", s);
                            break;
                        case o.PAGE_DOWN:
                            t = !0, this._move("nextPage", s);
                            break;
                        case o.UP:
                            t = !0, this._keyEvent("previous", s);
                            break;
                        case o.DOWN:
                            t = !0, this._keyEvent("next", s);
                            break;
                        case o.ENTER:
                            this.menu.active && (t = !0, s.preventDefault(), this.menu.select(s));
                            break;
                        case o.TAB:
                            this.menu.active && this.menu.select(s);
                            break;
                        case o.ESCAPE:
                            this.menu.element.is(":visible") && (this._value(this.term), this.close(s), s.preventDefault());
                            break;
                        default:
                            i = !0, this._searchTimeout(s)
                    }
                },
                keypress: function(n) {
                    if (t) return t = !1, void((!this.isMultiLine || this.menu.element.is(":visible")) && n.preventDefault());
                    if (!i) {
                        var s = e.ui.keyCode;
                        switch (n.keyCode) {
                            case s.PAGE_UP:
                                this._move("previousPage", n);
                                break;
                            case s.PAGE_DOWN:
                                this._move("nextPage", n);
                                break;
                            case s.UP:
                                this._keyEvent("previous", n);
                                break;
                            case s.DOWN:
                                this._keyEvent("next", n)
                        }
                    }
                },
                input: function(e) {
                    return n ? (n = !1, void e.preventDefault()) : void this._searchTimeout(e)
                },
                focus: function() {
                    this.selectedItem = null, this.previous = this._value()
                },
                blur: function(e) {
                    return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching), this.close(e), void this._change(e))
                }
            }), this._initSource(), this.menu = e("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
                role: null
            }).hide().menu("instance"), this._on(this.menu.element, {
                mousedown: function(t) {
                    t.preventDefault(), this.cancelBlur = !0, this._delay(function() {
                        delete this.cancelBlur
                    });
                    var i = this.menu.element[0];
                    e(t.target).closest(".ui-menu-item").length || this._delay(function() {
                        var t = this;
                        this.document.one("mousedown", function(n) {
                            n.target === t.element[0] || n.target === i || e.contains(i, n.target) || t.close()
                        })
                    })
                },
                menufocus: function(t, i) {
                    var n, s;
                    return this.isNewMenu && (this.isNewMenu = !1, t.originalEvent && /^mouse/.test(t.originalEvent.type)) ? (this.menu.blur(), void this.document.one("mousemove", function() {
                        e(t.target).trigger(t.originalEvent)
                    })) : (s = i.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", t, {
                        item: s
                    }) && t.originalEvent && /^key/.test(t.originalEvent.type) && this._value(s.value), n = i.item.attr("aria-label") || s.value, void(n && jQuery.trim(n).length && (this.liveRegion.children().hide(), e("<div>").text(n).appendTo(this.liveRegion))))
                },
                menuselect: function(e, t) {
                    var i = t.item.data("ui-autocomplete-item"),
                        n = this.previous;
                    this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = n, this._delay(function() {
                        this.previous = n, this.selectedItem = i
                    })), !1 !== this._trigger("select", e, {
                        item: i
                    }) && this._value(i.value), this.term = this._value(), this.close(e), this.selectedItem = i
                }
            }), this.liveRegion = e("<span>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body), this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _destroy: function() {
            clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove()
        },
        _setOption: function(e, t) {
            this._super(e, t), "source" === e && this._initSource(), "appendTo" === e && this.menu.element.appendTo(this._appendTo()), "disabled" === e && t && this.xhr && this.xhr.abort()
        },
        _appendTo: function() {
            var t = this.options.appendTo;
            return t && (t = t.jquery || t.nodeType ? e(t) : this.document.find(t).eq(0)), t && t[0] || (t = this.element.closest(".ui-front")), t.length || (t = this.document[0].body), t
        },
        _initSource: function() {
            var t, i, n = this;
            e.isArray(this.options.source) ? (t = this.options.source, this.source = function(i, n) {
                n(e.ui.autocomplete.filter(t, i.term))
            }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function(t, s) {
                n.xhr && n.xhr.abort(), n.xhr = e.ajax({
                    url: i,
                    data: t,
                    dataType: "json",
                    success: function(e) {
                        s(e)
                    },
                    error: function() {
                        s([])
                    }
                })
            }) : this.source = this.options.source
        },
        _searchTimeout: function(e) {
            clearTimeout(this.searching), this.searching = this._delay(function() {
                var t = this.term === this._value(),
                    i = this.menu.element.is(":visible"),
                    n = e.altKey || e.ctrlKey || e.metaKey || e.shiftKey;
                (!t || t && !i && !n) && (this.selectedItem = null, this.search(null, e))
            }, this.options.delay)
        },
        search: function(e, t) {
            return e = null != e ? e : this._value(), this.term = this._value(), e.length < this.options.minLength ? this.close(t) : this._trigger("search", t) !== !1 ? this._search(e) : void 0
        },
        _search: function(e) {
            this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({
                term: e
            }, this._response())
        },
        _response: function() {
            var t = ++this.requestIndex;
            return e.proxy(function(e) {
                t === this.requestIndex && this.__response(e), this.pending--, this.pending || this.element.removeClass("ui-autocomplete-loading")
            }, this)
        },
        __response: function(e) {
            e && (e = this._normalize(e)), this._trigger("response", null, {
                content: e
            }), !this.options.disabled && e && e.length && !this.cancelSearch ? (this._suggest(e), this._trigger("open")) : this._close()
        },
        close: function(e) {
            this.cancelSearch = !0, this._close(e)
        },
        _close: function(e) {
            this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", e))
        },
        _change: function(e) {
            this.previous !== this._value() && this._trigger("change", e, {
                item: this.selectedItem
            })
        },
        _normalize: function(t) {
            return t.length && t[0].label && t[0].value ? t : e.map(t, function(t) {
                return "string" == typeof t ? {
                    label: t,
                    value: t
                } : e.extend({}, t, {
                    label: t.label || t.value,
                    value: t.value || t.label
                })
            })
        },
        _suggest: function(t) {
            var i = this.menu.element.empty();
            this._renderMenu(i, t), this.isNewMenu = !0, this.menu.refresh(), i.show(), this._resizeMenu(), i.position(e.extend({
                of: this.element
            }, this.options.position)), this.options.autoFocus && this.menu.next()
        },
        _resizeMenu: function() {
            var e = this.menu.element;
            e.outerWidth(Math.max(e.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function(t, i) {
            var n = this;
            e.each(i, function(e, i) {
                n._renderItemData(t, i)
            })
        },
        _renderItemData: function(e, t) {
            return this._renderItem(e, t).data("ui-autocomplete-item", t)
        },
        _renderItem: function(t, i) {
            return e("<li>").text(i.label).appendTo(t)
        },
        _move: function(e, t) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(e) || this.menu.isLastItem() && /^next/.test(e) ? (this.isMultiLine || this._value(this.term), void this.menu.blur()) : void this.menu[e](t) : void this.search(null, t)
        },
        widget: function() {
            return this.menu.element
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments)
        },
        _keyEvent: function(e, t) {
            (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(e, t), t.preventDefault())
        }
    }), e.extend(e.ui.autocomplete, {
        escapeRegex: function(e) {
            return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
        },
        filter: function(t, i) {
            var n = new RegExp(e.ui.autocomplete.escapeRegex(i), "i");
            return e.grep(t, function(e) {
                return n.test(e.label || e.value || e)
            })
        }
    }), e.widget("ui.autocomplete", e.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function(e) {
                    return e + (e > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                }
            }
        },
        __response: function(t) {
            var i;
            this._superApply(arguments), this.options.disabled || this.cancelSearch || (i = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults, this.liveRegion.children().hide(), e("<div>").text(i).appendTo(this.liveRegion))
        }
    }), e.ui.autocomplete
}),
function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(window.jQuery)
}(function(e) {
    "use strict";
    var t = 0;
    e.ajaxTransport("iframe", function(i) {
        if (i.async) {
            var n, s, o, r = i.initialIframeSrc || "javascript:false;";
            return {
                send: function(a, l) {
                    n = e('<form style="display:none;"></form>'), n.attr("accept-charset", i.formAcceptCharset), o = /\?/.test(i.url) ? "&" : "?", "DELETE" === i.type ? (i.url = i.url + o + "_method=DELETE", i.type = "POST") : "PUT" === i.type ? (i.url = i.url + o + "_method=PUT", i.type = "POST") : "PATCH" === i.type && (i.url = i.url + o + "_method=PATCH", i.type = "POST"), t += 1, s = e('<iframe src="' + r + '" name="iframe-transport-' + t + '"></iframe>').bind("load", function() {
                        var t, o = e.isArray(i.paramName) ? i.paramName : [i.paramName];
                        s.unbind("load").bind("load", function() {
                            var t;
                            try {
                                if (t = s.contents(), !t.length || !t[0].firstChild) throw new Error
                            } catch (i) {
                                t = void 0
                            }
                            l(200, "success", {
                                iframe: t
                            }), e('<iframe src="' + r + '"></iframe>').appendTo(n), window.setTimeout(function() {
                                n.remove()
                            }, 0)
                        }), n.prop("target", s.prop("name")).prop("action", i.url).prop("method", i.type), i.formData && e.each(i.formData, function(t, i) {
                            e('<input type="hidden"/>').prop("name", i.name).val(i.value).appendTo(n)
                        }), i.fileInput && i.fileInput.length && "POST" === i.type && (t = i.fileInput.clone(), i.fileInput.after(function(e) {
                            return t[e]
                        }), i.paramName && i.fileInput.each(function(t) {
                            e(this).prop("name", o[t] || i.paramName)
                        }), n.append(i.fileInput).prop("enctype", "multipart/form-data").prop("encoding", "multipart/form-data"), i.fileInput.removeAttr("form")), n.submit(), t && t.length && i.fileInput.each(function(i, n) {
                            var s = e(t[i]);
                            e(n).prop("name", s.prop("name")).attr("form", s.attr("form")), s.replaceWith(n)
                        })
                    }), n.append(s).appendTo(document.body)
                },
                abort: function() {
                    s && s.unbind("load").prop("src", r), n && n.remove()
                }
            }
        }
    }), e.ajaxSetup({
        converters: {
            "iframe text": function(t) {
                return t && e(t[0].body).text()
            },
            "iframe json": function(t) {
                return t && e.parseJSON(e(t[0].body).text())
            },
            "iframe html": function(t) {
                return t && e(t[0].body).html()
            },
            "iframe xml": function(t) {
                var i = t && t[0];
                return i && e.isXMLDoc(i) ? i : e.parseXML(i.XMLDocument && i.XMLDocument.xml || e(i.body).html())
            },
            "iframe script": function(t) {
                return t && e.globalEval(e(t[0].body).text())
            }
        }
    })
}),
function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery", "jquery.ui.widget"], e) : e(window.jQuery)
}(function(e) {
    "use strict";
    e.support.fileInput = !(new RegExp("(Android (1\\.[0156]|2\\.[01]))|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)|(w(eb)?OSBrowser)|(webOS)|(Kindle/(1\\.0|2\\.[05]|3\\.0))").test(window.navigator.userAgent) || e('<input type="file">').prop("disabled")), e.support.xhrFileUpload = !(!window.ProgressEvent || !window.FileReader), e.support.xhrFormDataFileUpload = !!window.FormData, e.support.blobSlice = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice), e.widget("blueimp.fileupload", {
        options: {
            dropZone: e(document),
            pasteZone: e(document),
            fileInput: void 0,
            replaceFileInput: !0,
            paramName: void 0,
            singleFileUploads: !0,
            limitMultiFileUploads: void 0,
            limitMultiFileUploadSize: void 0,
            limitMultiFileUploadSizeOverhead: 512,
            sequentialUploads: !1,
            limitConcurrentUploads: void 0,
            forceIframeTransport: !1,
            redirect: void 0,
            redirectParamName: void 0,
            postMessage: void 0,
            multipart: !0,
            maxChunkSize: void 0,
            uploadedBytes: void 0,
            recalculateProgress: !0,
            progressInterval: 100,
            bitrateInterval: 500,
            autoUpload: !0,
            messages: {
                uploadedBytes: "Uploaded bytes exceed file size"
            },
            i18n: function(t, i) {
                return t = this.messages[t] || t.toString(), i && e.each(i, function(e, i) {
                    t = t.replace("{" + e + "}", i)
                }), t
            },
            formData: function(e) {
                return e.serializeArray()
            },
            add: function(t, i) {
                return t.isDefaultPrevented() ? !1 : void((i.autoUpload || i.autoUpload !== !1 && e(this).fileupload("option", "autoUpload")) && i.process().done(function() {
                    i.submit()
                }))
            },
            processData: !1,
            contentType: !1,
            cache: !1
        },
        _specialOptions: ["fileInput", "dropZone", "pasteZone", "multipart", "forceIframeTransport"],
        _blobSlice: e.support.blobSlice && function() {
            var e = this.slice || this.webkitSlice || this.mozSlice;
            return e.apply(this, arguments)
        },
        _BitrateTimer: function() {
            this.timestamp = Date.now ? Date.now() : (new Date).getTime(), this.loaded = 0, this.bitrate = 0, this.getBitrate = function(e, t, i) {
                var n = e - this.timestamp;
                return (!this.bitrate || !i || n > i) && (this.bitrate = (t - this.loaded) * (1e3 / n) * 8, this.loaded = t, this.timestamp = e), this.bitrate
            }
        },
        _isXHRUpload: function(t) {
            return !t.forceIframeTransport && (!t.multipart && e.support.xhrFileUpload || e.support.xhrFormDataFileUpload)
        },
        _getFormData: function(t) {
            var i;
            return "function" === e.type(t.formData) ? t.formData(t.form) : e.isArray(t.formData) ? t.formData : "object" === e.type(t.formData) ? (i = [], e.each(t.formData, function(e, t) {
                i.push({
                    name: e,
                    value: t
                })
            }), i) : []
        },
        _getTotal: function(t) {
            var i = 0;
            return e.each(t, function(e, t) {
                i += t.size || 1
            }), i
        },
        _initProgressObject: function(t) {
            var i = {
                loaded: 0,
                total: 0,
                bitrate: 0
            };
            t._progress ? e.extend(t._progress, i) : t._progress = i
        },
        _initResponseObject: function(e) {
            var t;
            if (e._response)
                for (t in e._response) e._response.hasOwnProperty(t) && delete e._response[t];
            else e._response = {}
        },
        _onProgress: function(t, i) {
            if (t.lengthComputable) {
                var n, s = Date.now ? Date.now() : (new Date).getTime();
                if (i._time && i.progressInterval && s - i._time < i.progressInterval && t.loaded !== t.total) return;
                i._time = s, n = Math.floor(t.loaded / t.total * (i.chunkSize || i._progress.total)) + (i.uploadedBytes || 0), this._progress.loaded += n - i._progress.loaded, this._progress.bitrate = this._bitrateTimer.getBitrate(s, this._progress.loaded, i.bitrateInterval), i._progress.loaded = i.loaded = n, i._progress.bitrate = i.bitrate = i._bitrateTimer.getBitrate(s, n, i.bitrateInterval), this._trigger("progress", e.Event("progress", {
                    delegatedEvent: t
                }), i), this._trigger("progressall", e.Event("progressall", {
                    delegatedEvent: t
                }), this._progress)
            }
        },
        _initProgressListener: function(t) {
            var i = this,
                n = t.xhr ? t.xhr() : e.ajaxSettings.xhr();
            n.upload && (e(n.upload).bind("progress", function(e) {
                var n = e.originalEvent;
                e.lengthComputable = n.lengthComputable, e.loaded = n.loaded, e.total = n.total, i._onProgress(e, t)
            }), t.xhr = function() {
                return n
            })
        },
        _isInstanceOf: function(e, t) {
            return Object.prototype.toString.call(t) === "[object " + e + "]"
        },
        _initXHRData: function(t) {
            var i, n = this,
                s = t.files[0],
                o = t.multipart || !e.support.xhrFileUpload,
                r = "array" === e.type(t.paramName) ? t.paramName[0] : t.paramName;
            t.headers = e.extend({}, t.headers), t.contentRange && (t.headers["Content-Range"] = t.contentRange), o && !t.blob && this._isInstanceOf("File", s) || (t.headers["Content-Disposition"] = 'attachment; filename="' + encodeURI(s.name) + '"'), o ? e.support.xhrFormDataFileUpload && (t.postMessage ? (i = this._getFormData(t), t.blob ? i.push({
                name: r,
                value: t.blob
            }) : e.each(t.files, function(n, s) {
                i.push({
                    name: "array" === e.type(t.paramName) && t.paramName[n] || r,
                    value: s
                })
            })) : (n._isInstanceOf("FormData", t.formData) ? i = t.formData : (i = new FormData, e.each(this._getFormData(t), function(e, t) {
                i.append(t.name, t.value)
            })), t.blob ? i.append(r, t.blob, s.name) : e.each(t.files, function(s, o) {
                (n._isInstanceOf("File", o) || n._isInstanceOf("Blob", o)) && i.append("array" === e.type(t.paramName) && t.paramName[s] || r, o, o.uploadName || o.name)
            })), t.data = i) : (t.contentType = s.type || "application/octet-stream", t.data = t.blob || s), t.blob = null
        },
        _initIframeSettings: function(t) {
            var i = e("<a></a>").prop("href", t.url).prop("host");
            t.dataType = "iframe " + (t.dataType || ""), t.formData = this._getFormData(t), t.redirect && i && i !== location.host && t.formData.push({
                name: t.redirectParamName || "redirect",
                value: t.redirect
            })
        },
        _initDataSettings: function(e) {
            this._isXHRUpload(e) ? (this._chunkedUpload(e, !0) || (e.data || this._initXHRData(e), this._initProgressListener(e)), e.postMessage && (e.dataType = "postmessage " + (e.dataType || ""))) : this._initIframeSettings(e)
        },
        _getParamName: function(t) {
            var i = e(t.fileInput),
                n = t.paramName;
            return n ? e.isArray(n) || (n = [n]) : (n = [], i.each(function() {
                for (var t = e(this), i = t.prop("name") || "files[]", s = (t.prop("files") || [1]).length; s;) n.push(i), s -= 1
            }), n.length || (n = [i.prop("name") || "files[]"])), n
        },
        _initFormSettings: function(t) {
            t.form && t.form.length || (t.form = e(t.fileInput.prop("form")), t.form.length || (t.form = e(this.options.fileInput.prop("form")))), t.paramName = this._getParamName(t), t.url || (t.url = t.form.prop("action") || location.href), t.type = (t.type || "string" === e.type(t.form.prop("method")) && t.form.prop("method") || "").toUpperCase(), "POST" !== t.type && "PUT" !== t.type && "PATCH" !== t.type && (t.type = "POST"), t.formAcceptCharset || (t.formAcceptCharset = t.form.attr("accept-charset"))
        },
        _getAJAXSettings: function(t) {
            var i = e.extend({}, this.options, t);
            return this._initFormSettings(i), this._initDataSettings(i), i
        },
        _getDeferredState: function(e) {
            return e.state ? e.state() : e.isResolved() ? "resolved" : e.isRejected() ? "rejected" : "pending"
        },
        _enhancePromise: function(e) {
            return e.success = e.done, e.error = e.fail, e.complete = e.always, e
        },
        _getXHRPromise: function(t, i, n) {
            var s = e.Deferred(),
                o = s.promise();
            return i = i || this.options.context || o, t === !0 ? s.resolveWith(i, n) : t === !1 && s.rejectWith(i, n), o.abort = s.promise, this._enhancePromise(o)
        },
        _addConvenienceMethods: function(t, i) {
            var n = this,
                s = function(t) {
                    return e.Deferred().resolveWith(n, t).promise()
                };
            i.process = function(t, o) {
                return (t || o) && (i._processQueue = this._processQueue = (this._processQueue || s([this])).pipe(function() {
                    return i.errorThrown ? e.Deferred().rejectWith(n, [i]).promise() : s(arguments)
                }).pipe(t, o)), this._processQueue || s([this])
            }, i.submit = function() {
                return "pending" !== this.state() && (i.jqXHR = this.jqXHR = n._trigger("submit", e.Event("submit", {
                    delegatedEvent: t
                }), this) !== !1 && n._onSend(t, this)), this.jqXHR || n._getXHRPromise()
            }, i.abort = function() {
                return this.jqXHR ? this.jqXHR.abort() : (this.errorThrown = "abort", n._trigger("fail", null, this), n._getXHRPromise(!1))
            }, i.state = function() {
                return this.jqXHR ? n._getDeferredState(this.jqXHR) : this._processQueue ? n._getDeferredState(this._processQueue) : void 0
            }, i.processing = function() {
                return !this.jqXHR && this._processQueue && "pending" === n._getDeferredState(this._processQueue)
            }, i.progress = function() {
                return this._progress
            }, i.response = function() {
                return this._response
            }
        },
        _getUploadedBytes: function(e) {
            var t = e.getResponseHeader("Range"),
                i = t && t.split("-"),
                n = i && i.length > 1 && parseInt(i[1], 10);
            return n && n + 1
        },
        _chunkedUpload: function(t, i) {
            t.uploadedBytes = t.uploadedBytes || 0;
            var n, s, o = this,
                r = t.files[0],
                a = r.size,
                l = t.uploadedBytes,
                c = t.maxChunkSize || a,
                d = this._blobSlice,
                u = e.Deferred(),
                h = u.promise();
            return this._isXHRUpload(t) && d && (l || a > c) && !t.data ? i ? !0 : l >= a ? (r.error = t.i18n("uploadedBytes"), this._getXHRPromise(!1, t.context, [null, "error", r.error])) : (s = function() {
                var i = e.extend({}, t),
                    h = i._progress.loaded;
                i.blob = d.call(r, l, l + c, r.type), i.chunkSize = i.blob.size, i.contentRange = "bytes " + l + "-" + (l + i.chunkSize - 1) + "/" + a, o._initXHRData(i), o._initProgressListener(i), n = (o._trigger("chunksend", null, i) !== !1 && e.ajax(i) || o._getXHRPromise(!1, i.context)).done(function(n, r, c) {
                    l = o._getUploadedBytes(c) || l + i.chunkSize, h + i.chunkSize - i._progress.loaded && o._onProgress(e.Event("progress", {
                        lengthComputable: !0,
                        loaded: l - i.uploadedBytes,
                        total: l - i.uploadedBytes
                    }), i), t.uploadedBytes = i.uploadedBytes = l, i.result = n, i.textStatus = r, i.jqXHR = c, o._trigger("chunkdone", null, i), o._trigger("chunkalways", null, i), a > l ? s() : u.resolveWith(i.context, [n, r, c])
                }).fail(function(e, t, n) {
                    i.jqXHR = e, i.textStatus = t, i.errorThrown = n, o._trigger("chunkfail", null, i), o._trigger("chunkalways", null, i), u.rejectWith(i.context, [e, t, n])
                })
            }, this._enhancePromise(h), h.abort = function() {
                return n.abort()
            }, s(), h) : !1
        },
        _beforeSend: function(e, t) {
            0 === this._active && (this._trigger("start"), this._bitrateTimer = new this._BitrateTimer, this._progress.loaded = this._progress.total = 0, this._progress.bitrate = 0), this._initResponseObject(t), this._initProgressObject(t), t._progress.loaded = t.loaded = t.uploadedBytes || 0, t._progress.total = t.total = this._getTotal(t.files) || 1, t._progress.bitrate = t.bitrate = 0, this._active += 1, this._progress.loaded += t.loaded, this._progress.total += t.total
        },
        _onDone: function(t, i, n, s) {
            var o = s._progress.total,
                r = s._response;
            s._progress.loaded < o && this._onProgress(e.Event("progress", {
                lengthComputable: !0,
                loaded: o,
                total: o
            }), s), r.result = s.result = t, r.textStatus = s.textStatus = i, r.jqXHR = s.jqXHR = n, this._trigger("done", null, s)
        },
        _onFail: function(e, t, i, n) {
            var s = n._response;
            n.recalculateProgress && (this._progress.loaded -= n._progress.loaded, this._progress.total -= n._progress.total), s.jqXHR = n.jqXHR = e, s.textStatus = n.textStatus = t, s.errorThrown = n.errorThrown = i, this._trigger("fail", null, n)
        },
        _onAlways: function(e, t, i, n) {
            this._trigger("always", null, n)
        },
        _onSend: function(t, i) {
            i.submit || this._addConvenienceMethods(t, i);
            var n, s, o, r, a = this,
                l = a._getAJAXSettings(i),
                c = function() {
                    return a._sending += 1, l._bitrateTimer = new a._BitrateTimer, n = n || ((s || a._trigger("send", e.Event("send", {
                        delegatedEvent: t
                    }), l) === !1) && a._getXHRPromise(!1, l.context, s) || a._chunkedUpload(l) || e.ajax(l)).done(function(e, t, i) {
                        a._onDone(e, t, i, l)
                    }).fail(function(e, t, i) {
                        a._onFail(e, t, i, l)
                    }).always(function(e, t, i) {
                        if (a._onAlways(e, t, i, l), a._sending -= 1, a._active -= 1, l.limitConcurrentUploads && l.limitConcurrentUploads > a._sending)
                            for (var n = a._slots.shift(); n;) {
                                if ("pending" === a._getDeferredState(n)) {
                                    n.resolve();
                                    break
                                }
                                n = a._slots.shift()
                            }
                        0 === a._active && a._trigger("stop")
                    })
                };
            return this._beforeSend(t, l), this.options.sequentialUploads || this.options.limitConcurrentUploads && this.options.limitConcurrentUploads <= this._sending ? (this.options.limitConcurrentUploads > 1 ? (o = e.Deferred(), this._slots.push(o), r = o.pipe(c)) : (this._sequence = this._sequence.pipe(c, c), r = this._sequence), r.abort = function() {
                return s = [void 0, "abort", "abort"], n ? n.abort() : (o && o.rejectWith(l.context, s), c())
            }, this._enhancePromise(r)) : c()
        },
        _onAdd: function(t, i) {
            var n, s, o, r, a = this,
                l = !0,
                c = e.extend({}, this.options, i),
                d = i.files,
                u = d.length,
                h = c.limitMultiFileUploads,
                p = c.limitMultiFileUploadSize,
                f = c.limitMultiFileUploadSizeOverhead,
                m = 0,
                g = this._getParamName(c),
                v = 0;
            if (!p || u && void 0 !== d[0].size || (p = void 0), (c.singleFileUploads || h || p) && this._isXHRUpload(c))
                if (c.singleFileUploads || p || !h)
                    if (!c.singleFileUploads && p)
                        for (o = [], n = [], r = 0; u > r; r += 1) m += d[r].size + f, (r + 1 === u || m + d[r + 1].size + f > p || h && r + 1 - v >= h) && (o.push(d.slice(v, r + 1)), s = g.slice(v, r + 1), s.length || (s = g), n.push(s), v = r + 1, m = 0);
                    else n = g;
            else
                for (o = [], n = [], r = 0; u > r; r += h) o.push(d.slice(r, r + h)), s = g.slice(r, r + h), s.length || (s = g), n.push(s);
            else o = [d], n = [g];
            return i.originalFiles = d, e.each(o || d, function(s, r) {
                var c = e.extend({}, i);
                return c.files = o ? r : [r], c.paramName = n[s], a._initResponseObject(c), a._initProgressObject(c), a._addConvenienceMethods(t, c), l = a._trigger("add", e.Event("add", {
                    delegatedEvent: t
                }), c)
            }), l
        },
        _replaceFileInput: function(t) {
            var i = t.fileInput,
                n = i.clone(!0);
            t.fileInputClone = n, e("<form></form>").append(n)[0].reset(), i.after(n).detach(), e.cleanData(i.unbind("remove")), this.options.fileInput = this.options.fileInput.map(function(e, t) {
                return t === i[0] ? n[0] : t
            }), i[0] === this.element[0] && (this.element = n)
        },
        _handleFileTreeEntry: function(t, i) {
            var n, s = this,
                o = e.Deferred(),
                r = function(e) {
                    e && !e.entry && (e.entry = t), o.resolve([e])
                },
                a = function(e) {
                    s._handleFileTreeEntries(e, i + t.name + "/").done(function(e) {
                        o.resolve(e)
                    }).fail(r)
                },
                l = function() {
                    n.readEntries(function(e) {
                        e.length ? (c = c.concat(e), l()) : a(c)
                    }, r)
                },
                c = [];
            return i = i || "", t.isFile ? t._file ? (t._file.relativePath = i, o.resolve(t._file)) : t.file(function(e) {
                e.relativePath = i, o.resolve(e)
            }, r) : t.isDirectory ? (n = t.createReader(), l()) : o.resolve([]), o.promise()
        },
        _handleFileTreeEntries: function(t, i) {
            var n = this;
            return e.when.apply(e, e.map(t, function(e) {
                return n._handleFileTreeEntry(e, i)
            })).pipe(function() {
                return Array.prototype.concat.apply([], arguments)
            })
        },
        _getDroppedFiles: function(t) {
            t = t || {};
            var i = t.items;
            return i && i.length && (i[0].webkitGetAsEntry || i[0].getAsEntry) ? this._handleFileTreeEntries(e.map(i, function(e) {
                var t;
                return e.webkitGetAsEntry ? (t = e.webkitGetAsEntry(), t && (t._file = e.getAsFile()), t) : e.getAsEntry()
            })) : e.Deferred().resolve(e.makeArray(t.files)).promise()
        },
        _getSingleFileInputFiles: function(t) {
            t = e(t);
            var i, n, s = t.prop("webkitEntries") || t.prop("entries");
            if (s && s.length) return this._handleFileTreeEntries(s);
            if (i = e.makeArray(t.prop("files")), i.length) void 0 === i[0].name && i[0].fileName && e.each(i, function(e, t) {
                t.name = t.fileName, t.size = t.fileSize
            });
            else {
                if (n = t.prop("value"), !n) return e.Deferred().resolve([]).promise();
                i = [{
                    name: n.replace(/^.*\\/, "")
                }]
            }
            return e.Deferred().resolve(i).promise()
        },
        _getFileInputFiles: function(t) {
            return t instanceof e && 1 !== t.length ? e.when.apply(e, e.map(t, this._getSingleFileInputFiles)).pipe(function() {
                return Array.prototype.concat.apply([], arguments)
            }) : this._getSingleFileInputFiles(t)
        },
        _onChange: function(t) {
            var i = this,
                n = {
                    fileInput: e(t.target),
                    form: e(t.target.form)
                };
            this._getFileInputFiles(n.fileInput).always(function(s) {
                n.files = s, i.options.replaceFileInput && i._replaceFileInput(n), i._trigger("change", e.Event("change", {
                    delegatedEvent: t
                }), n) !== !1 && i._onAdd(t, n)
            })
        },
        _onPaste: function(t) {
            var i = t.originalEvent && t.originalEvent.clipboardData && t.originalEvent.clipboardData.items,
                n = {
                    files: []
                };
            i && i.length && (e.each(i, function(e, t) {
                var i = t.getAsFile && t.getAsFile();
                i && n.files.push(i)
            }), this._trigger("paste", e.Event("paste", {
                delegatedEvent: t
            }), n) !== !1 && this._onAdd(t, n))
        },
        _onDrop: function(t) {
            t.dataTransfer = t.originalEvent && t.originalEvent.dataTransfer;
            var i = this,
                n = t.dataTransfer,
                s = {};
            n && n.files && n.files.length && (t.preventDefault(), this._getDroppedFiles(n).always(function(n) {
                s.files = n, i._trigger("drop", e.Event("drop", {
                    delegatedEvent: t
                }), s) !== !1 && i._onAdd(t, s)
            }))
        },
        _onDragOver: function(t) {
            t.dataTransfer = t.originalEvent && t.originalEvent.dataTransfer;
            var i = t.dataTransfer;
            i && -1 !== e.inArray("Files", i.types) && this._trigger("dragover", e.Event("dragover", {
                delegatedEvent: t
            })) !== !1 && (t.preventDefault(), i.dropEffect = "copy")
        },
        _initEventHandlers: function() {
            this._isXHRUpload(this.options) && (this._on(this.options.dropZone, {
                dragover: this._onDragOver,
                drop: this._onDrop
            }), this._on(this.options.pasteZone, {
                paste: this._onPaste
            })), e.support.fileInput && this._on(this.options.fileInput, {
                change: this._onChange
            })
        },
        _destroyEventHandlers: function() {
            this._off(this.options.dropZone, "dragover drop"), this._off(this.options.pasteZone, "paste"), this._off(this.options.fileInput, "change")
        },
        _setOption: function(t, i) {
            var n = -1 !== e.inArray(t, this._specialOptions);
            n && this._destroyEventHandlers(), this._super(t, i), n && (this._initSpecialOptions(), this._initEventHandlers())
        },
        _initSpecialOptions: function() {
            var t = this.options;
            void 0 === t.fileInput ? t.fileInput = this.element.is('input[type="file"]') ? this.element : this.element.find('input[type="file"]') : t.fileInput instanceof e || (t.fileInput = e(t.fileInput)), t.dropZone instanceof e || (t.dropZone = e(t.dropZone)), t.pasteZone instanceof e || (t.pasteZone = e(t.pasteZone))
        },
        _getRegExp: function(e) {
            var t = e.split("/"),
                i = t.pop();
            return t.shift(), new RegExp(t.join("/"), i)
        },
        _isRegExpOption: function(t, i) {
            return "url" !== t && "string" === e.type(i) && /^\/.*\/[igm]{0,3}$/.test(i)
        },
        _initDataAttributes: function() {
            var t = this,
                i = this.options,
                n = e(this.element[0].cloneNode(!1));
            e.each(n.data(), function(e, s) {
                var o = "data-" + e.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
                n.attr(o) && (t._isRegExpOption(e, s) && (s = t._getRegExp(s)), i[e] = s)
            })
        },
        _create: function() {
            this._initDataAttributes(), this._initSpecialOptions(), this._slots = [], this._sequence = this._getXHRPromise(!0), this._sending = this._active = 0, this._initProgressObject(this), this._initEventHandlers()
        },
        active: function() {
            return this._active
        },
        progress: function() {
            return this._progress
        },
        add: function(t) {
            var i = this;
            t && !this.options.disabled && (t.fileInput && !t.files ? this._getFileInputFiles(t.fileInput).always(function(e) {
                t.files = e, i._onAdd(null, t)
            }) : (t.files = e.makeArray(t.files), this._onAdd(null, t)))
        },
        send: function(t) {
            if (t && !this.options.disabled) {
                if (t.fileInput && !t.files) {
                    var i, n, s = this,
                        o = e.Deferred(),
                        r = o.promise();
                    return r.abort = function() {
                        return n = !0, i ? i.abort() : (o.reject(null, "abort", "abort"), r)
                    }, this._getFileInputFiles(t.fileInput).always(function(e) {
                        if (!n) {
                            if (!e.length) return void o.reject();
                            t.files = e, i = s._onSend(null, t), i.then(function(e, t, i) {
                                o.resolve(e, t, i)
                            }, function(e, t, i) {
                                o.reject(e, t, i)
                            })
                        }
                    }), this._enhancePromise(r)
                }
                if (t.files = e.makeArray(t.files), t.files.length) return this._onSend(null, t)
            }
            return this._getXHRPromise(!1, t && t.context)
        }
    })
}), window.gon || (window.gon = {}), jQuery(document).on('page:change', function() {
    var e = new Date,
        t = new Date(e.getFullYear(), e.getMonth(), e.getDate(), 0, 0, 0, 0);
    $(".datepicker").each(function() {
        $(this).datepicker({
            format: "mm/dd/yyyy",
            onRender: function(e) {
                return e.valueOf() < t.valueOf() ? "disabled" : ""
            }
        })
    }), $(".timepickers").each(function() {
        $(this).timepicker({
            minuteStep: 5
        })
    }), $("[data-toggle=tooltip]").each(function() {
        $(this).tooltip()
    }), $(".ratings").each(function() {
        $(this).rating({
            filled: "fa fa-star filled",
            empty: "fa fa-star-o",
            start: 1
        })
    }), $("#lightGallery").lightGallery({
        onSlideBefore: function() {
            $(".lightGallery-slide iframe").each(function() {
                this.contentWindow && (this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*"), this.contentWindow.postMessage('{"method":"pause","value":"void"}', "*"))
            })
        }
    }), $(".selectpicker.default").selectpicker()
});
var Kuju = {
    sticky_relocate: function() {
        var e = $(window).scrollTop(),
            t = $(".sticky").attr("data-top");
        e > t ? $(".sticky").addClass("sticked") : t > e && $(".sticky").removeClass("sticked")
    },
    fit_images: function(e, t) {
        $(e).each(function() {
            var e = $(this),
                i = e.attr("src"),
                n = new Image;
            n.src = i, n.addEventListener("load", function() {
                var i = "tall";
                this.width / this.height > t && (i = "wide"), e.addClass(i)
            })
        })
    },
    build_url: function(e) {
        if (void 0 == e && (e = {}), url = -1 === document.location.href.indexOf("?") ? document.location.href + "?" : /.+?\?/.exec(document.location.href), url_params = /\?(.*)/.exec(document.location.href), null != url_params)
            for (var t = url_params[1].split("&"), i = 0; i < t.length; i++) {
                var n = t[i].split("=");
                n.length > 1 && void 0 == e[n[0]] && (e[n[0]] = n[1])
            }
        var s, o = [];
        for (s in e) parseInt(s) + 1 > 0 || !e[s] || "page" == s || o.push(s + "=" + e[s]);
        return url + o.join("&")
    },
    weekdays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
};
if (function() {
        jQuery(document).on('page:change', function() {
            var e, t, i, n, s;
            return $(".reserv-page").length > 0 ? (n = /[^\?]+$/.exec(document.location.href)[0].replace(/&/, "=").split("="), e = n.indexOf("accept"), t = n.indexOf("decline"), i = n.indexOf("info"), s = n.indexOf("review"), setTimeout(function() {
                return -1 !== e && $("a.reservation-approve[rid=" + n[e + 1] + "]").click(), -1 !== t && $("a.cancel[rid=" + n[t + 1] + "]").click(), -1 !== i && $(".show-info[rid=" + n[i + 1] + "]").click(), -1 !== s ? $(".review[rid=" + n[s + 1] + "]").click() : void 0
            }, 500)) : void 0
        })
    }.call(this), function() {
        jQuery(document).on('page:change', function() {
            return $(".modal").each(function() {
                return $(this).on("show.bs.modal", function() {
                    var e;
                    return e = $(this), setTimeout(function() {
                        return e.css("visibility", "visible")
                    }, 150)
                }), $(this).on("hidden.bs.modal", function() {
                    return $(this).css("visibility", "hidden")
                })
            })
        })
    }.call(this), function() {}.call(this), function() {}.call(this), function() {}.call(this), function() {
        jQuery(document).on('page:change', function() {
            return $(".send").click(function() {
                var e, t;
                return t = $("#contact-modal form"), e = {
                    name: t.find(".name").val(),
                    email: t.find(".email").val(),
                    message: t.find(".message").val()
                }, "" !== e.email && "" !== e.message ? ($.ajax({
                    type: "POST",
                    url: t.attr("action") + ".json",
                    data: e,
                    success: function(e) {
                        return e.success ? ($("#contact-modal").modal("hide"), $("#contact-modal form")[0].reset(), swal("", e.notice)) : swal("Error :(", "Please, try again.", "error")
                    }
                }), !1) : void 0
            })
        })
    }.call(this), function() {
        var e;
        jQuery(document).on('page:change', function() {
            return $(".with-tooltip").each(function() {
                return $(this).tooltip({
                    delay: {
                        show: 100,
                        hide: 100
                    }
                })
            }), $(".actions .cancel").each(function() {
                return $(this).click(function() {
                    return $("#r-cancel .cancel-btn").attr("rid", $(this).attr("rid")), $("#r-cancel .price").html("$" + $(this).attr("price")), $("#r-cancel #cancel-read").attr("checked", !1), $("#r-cancel .cancel-btn").attr("disabled", !0), $("#r-cancel .fa").hide(), $("#r-cancel .fa-square-o").show(), $("#cancelation-reason").val(""), $("#r-cancel").modal("show"), !1
                })
            }), $("#cancel-read").change(function() {
                return this.checked ? $(".cancel-btn").attr("disabled", !1) : $(".cancel-btn").attr("disabled", !0)
            }), $("#r-cancel .cancel-btn").click(function() {
                var e, t, i, n, s;
                if (e = confirm("Are you sure? This cannot be undone."), e === !0) {
                    if ($(".cancel-btn").attr("disabled", !0), n = $(this).attr("path"), i = $(this).attr("rid"), s = $("#cancelation-reason").val(), s.length < 50) return alert("Please enter longer cancelation reason."), $("#cancelation-reason").addClass("with-error"), $(".cancel-btn").attr("disabled", !1), !1;
                    $("#cancelation-reason").removeClass("with-error"), t = n + "/" + i, $(".preloader").show(), $.ajax({
                        type: "POST",
                        url: t,
                        data: {
                            message: s
                        },
                        success: function(e) {
                            return $(".preloader").hide(), $("#r-cancel").modal("hide"), $(".cancel-btn").attr("disabled", !1), e.errors ? swal("", e.errors.join("\n"), "error") : (swal("The session canceled.", "", "success"), $(".r-entry-" + i).slideToggle())
                        },
                        error: function() {
                            return $(".preloader").hide(), swal("Unrecognized error :(", "", "error")
                        }
                    })
                }
            }), $(".reservation-approve").each(function() {
                return $(this).click(function() {
                    var e, t;
                    return e = $(this), e.hasClass("in-process") ? void 0 : (e.addClass("in-process"), t = $(this).attr("rid"), $.ajax({
                        type: "POST",
                        url: "/reservations/" + t + "/approve",
                        success: function(i) {
                            return i.errors ? swal("", i.errors.join("\n"), "error") : (swal("Approved successfully!", "", "success"), e.hide(), $(".r-entry-" + t + " .waiting").addClass("approved"), $(".r-entry-" + t + " .waiting").removeClass("waiting"), $(".actions[rid=" + t + "] .gray-label").hide())
                        },
                        error: function() {
                            return swal("Unrecognized error :(", "", "error")
                        },
                        complete: function() {
                            return e.removeClass("in-process")
                        }
                    }))
                })
            }), $(".actions .review").each(function() {
                return $(this).click(function() {
                    return $(".review-btn").attr("rid", $(this).attr("rid")), $("#add_review .modal-title").html("WRITE REVIEW"), $("#add_review #comment").val(""), $("#add_review #points").val(""), $("#add_review .rating-symbol").parent().remove(), $("#add_review #points").rating({
                        filled: "fa fa-star filled",
                        empty: "fa fa-star-o",
                        start: 1
                    }), $("#add_review").modal("show"), !1
                })
            }), $(".review-btn").click(function() {
                var e, t, i;
                return $(".preloader-r").show(), e = $(this).attr("disabled", !0), t = {
                    title: $("#add_review #title").val(),
                    point: $("#add_review #points").val(),
                    comment: $("#add_review #comment").val(),
                    reservation_id: $(".review-btn").attr("rid")
                }, i = $(this).attr("path"), $.ajax({
                    type: "POST",
                    url: i,
                    data: {
                        review: t
                    },
                    success: function(i) {
                        return e.attr("disabled", !1), $(".preloader-r").hide(), i.errors ? (swal("", i.errors.join("\n"), "error"), i.close ? $("#add_review").modal("hide") : void 0) : ($("#add_review").modal("hide"), swal("Thanks for reviewing!", "", "success"), $(".actions .review[rid=" + t.reservation_id + "]").hide(), $(".review-delete[rid=" + t.reservation_id + "]").show(), $(".review-edit[rid=" + t.reservation_id + "]").show())
                    },
                    error: function() {
                        return swal("Unrecognized error :(", "", "error"), e.attr("disabled", !1), $(".preloader-r").hide()
                    }
                })
            }), $(".actions .review-edit").each(function() {
                return $(this).click(function() {
                    var e, t, i;
                    return t = $(this).attr("rid"), e = $(".review-btn"), i = e.attr("path"), e.attr("disabled", !0), e.attr("rid", t), $("#add_review .modal-title").html("EDIT REVIEW"), $("#add_review").modal("show"), $.ajax({
                        type: "GET",
                        url: i + "/" + t,
                        success: function(e) {
                            return e.errors ? (swal("", e.errors.join("\n"), "error"), e.close ? $("#add_review").modal("hide") : void 0) : ($("#add_review #comment").val(e.comment), $("#add_review #points").val(e.points), $("#add_review .rating-symbol").parent().remove(), $("#add_review #points").rating({
                                filled: "fa fa-star filled",
                                empty: "fa fa-star-o",
                                start: 1
                            }))
                        },
                        error: function() {
                            return swal("Unrecognized error :(", "", "error")
                        },
                        complete: function() {
                            return e.attr("disabled", !1)
                        }
                    }), !1
                })
            }), $(".actions .review-delete").each(function() {
                return $(this).click(function() {
                    var e, t, i;
                    return t = $(this).attr("rid"), i = $(".review-btn").attr("path"), e = confirm("Are you sure?"), e && $.ajax({
                        type: "DELETE",
                        url: i + "/" + t,
                        success: function(e) {
                            return e.errors ? swal("", e.errors.join("\n"), "error") : ($(".review-delete[rid=" + t + "]").hide(), $(".review-edit[rid=" + t + "]").hide(), $(".review[rid=" + t + "]").show())
                        },
                        errors: function() {
                            return swal("Unrecognized error :(", "", "error")
                        }
                    }), !1
                })
            }), $(".show-info").each(function() {
                return $(this).click(function() {
                    var t;
                    return $("#r-info").modal("show"), t = $(this).attr("rid"), $.ajax({
                        type: "GET",
                        url: "/reservations/" + t,
                        success: function(t) {
                            return $("#r-info .ready").html(t), e(), $("#r-info .preloader").hide(), $("#r-info .ready").show(), $("#r-info .accept").click(function() {
                                return $(".reservation-approve[rid=" + $(this).attr("rid") + "]").click(), $("#r-info").modal("hide")
                            }), $("#r-info .decline").click(function() {
                                return $(".reschedule-session[rid=" + $(this).attr("rid") + "]").click(), $("#r-info").modal("hide")
                            })
                        },
                        errors: function() {
                            return $("#r-info").modal("hide")
                        }
                    })
                })
            }), $("#r-info").on("hidden.bs.modal", function() {
                return $("#r-info .preloader").show(), $("#r-info .ready").hide()
            }), $(".sync-with-google").each(function() {
                var e;
                return e = $(this), e.click(function() {
                    var t;
                    return t = "syncing/push_appointment_to_google/" + e.attr("rid"), $.ajax({
                        method: "PUT",
                        url: t,
                        success: function(e) {
                            return e.errors ? swal("", e.errors.join("\n"), "error") : swal("Successfully synced!", "", "success")
                        },
                        error: function() {
                            return swal("Server error happened:(", "Please, try again later", "error")
                        }
                    })
                })
            }), $(".reschedule-session").click(function() {
                return $("#r-reschedule .reschedule-submit").attr("rid", $(this).attr("rid")), $("#reschedule-date").datepicker("setValue", $(this).attr("date")), $("#reschedule-time").val($(this).attr("time")), $("#reschedule-time").timepicker("updateFromElementVal"), $("#reschedule-message").val(""), $("#r-reschedule").modal("show")
            }), $(".reschedule-submit").click(function() {
                var e, t, i;
                return $("#r-reschedule .preloader").show(), e = $("#reschedule-date").val(), i = $("#reschedule-time").val(), t = $(this).attr("path").replace(":id", $(this).attr("rid")), $.ajax({
                    type: "POST",
                    url: t,
                    data: {
                        date: e,
                        time: i
                    },
                    success: function(e) {
                        return e.errors ? swal("", e.errors.join("\n"), "error") : (swal("Session rescheduled and waiting for approval.", "", "success"), $(".r-entry-" + e.id + " .date").html(e.infodate), $(".r-entry-" + e.id + " .time").html(e.infotime), $(".reschedule-session[rid=" + e.id + "]").attr("date", e.date), $(".reschedule-session[rid=" + e.id + "]").attr("time", e.time), $("#r-reschedule").modal("hide"), $("#r-reschedule .preloader, .reservation-approve").hide())
                    },
                    error: function() {
                        return swal("Server error, please try again.", "", "error"), $("#r-reschedule .preloader").hide()
                    }
                })
            }), $(".send-message").click(function() {
                var e;
                return e = $(this).attr("rid"), $("#send-message .recipient-name").html($(".r-entry-" + e + " .user-title").html()), $("#send-message .send").attr("rid", e), $("#send-message").modal("show")
            }), $("#send-message .send").click(function() {
                var e, t, i, n;
                return e = $(this), n = e.attr("rid"), i = e.attr("path"), t = $("#send-message .message-text").val(), $("#send-message .preloader").show(), e.attr("disabled", !0), $.ajax({
                    type: "POST",
                    url: i,
                    data: {
                        text: t,
                        reservation_id: n
                    },
                    success: function(e) {
                        return e.errors ? swal("", e.errors.join("\n"), "error") : (swal("Message sent.", "", "success"), $("#send-message").modal("hide"), $("#send-message .message-text").val(""))
                    },
                    error: function() {
                        return swal("Server error", "Try again later", "error")
                    },
                    complete: function() {
                        return e.attr("disabled", !1), $("#send-message .preloader").hide()
                    }
                })
            })
        }), e = function() {
            var e, t, i, n;
            return (t = document.getElementById("map_canvas")) ? (n = new google.maps.LatLng(t.getAttribute("lat"), t.getAttribute("long")), i = {
                center: n,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }, e = new google.maps.Map(t, i), google.maps.event.addListener(e, "click", function() {
                return window.open(this.mapUrl)
            }), setTimeout(function() {
                return google.maps.event.trigger(e, "resize"), window.marker = new google.maps.Marker({
                    position: n,
                    map: e,
                    title: t.getAttribute("marker_title"),
                    cursor: "default"
                })
            }, 100)) : void 0
        }
    }.call(this), function() {
        var e, t, i, n, s;
        jQuery(document).on('page:change', function() {
            var i, o, r, a, l, c;
            return $(".expert-profile.display").length > 0 && (e(), $("#map_canvas").length > 0 && (o = document.getElementById("map_canvas"), o && (l = new google.maps.LatLng(o.getAttribute("lat"), o.getAttribute("long")), r = {
                center: l,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: !0
            }, i = new google.maps.Map(o, r), a = new google.maps.Marker({
                position: l,
                map: i
            }))), $(".rating").each(function() {
                return $(this).rating({
                    filled: "fa fa-star filled",
                    empty: "fa fa-star-o"
                })
            }), $(".amedia, .avatar").click(function() {
                return $(".media a:first").click()
            }), $(".next-week").click(function() {
                return s.offset++, t(s.offset), n(s.offset, "next")
            }), $(".prev-week").click(function() {
                return 0 !== s.offset && s.offset--, t(s.offset), n(s.offset, "prev")
            }), $("#booknow").click(function() {
                var e, t, i;
                return i = $(".reserving").attr("reservation_path"), e = $("#sessions-count :selected").val(), t = i + "", gon.current_user ? window.location.href = t : (document.cookie = "redirect_location=" + t + "; path=/", $("#sign-up-modal").modal("show"))
            }), Kuju.fit_images(".media-entry img", 1.45), c = /\?session=([\d]+)/.exec(document.location.href), c && 2 === c.length) ? $("[data-id=" + c[1] + "]").click() : void 0
        }), s = {
            offset: 0
        }, t = function(e) {
            return e > 0 ? $(".prev-week").addClass("inverse") : $(".prev-week").removeClass("inverse")
        }, e = function() {
            var t;
            return t = $(".reserving").attr("reservation_path"), $(".r-on").each(function() {
                return $(this).unbind().click(function() {
                    var e, i;
                    return i = $(this).attr("sid"), e = t + "?type=group&sid=" + i, gon.current_user ? window.location.href = e : (document.cookie = "redirect_location=" + e + "; path=/", $("#sign-up-modal").modal("show"))
                }), $(".gsessions_list .title span, .gsessions_list .loc span").each(function() {
                    var e;
                    return e = $(this), e.popover({
                        delay: {
                            hide: 700
                        },
                        html: !0,
                        placement: "right",
                        template: '<div class="popover" role="tooltip"><div class="popover-content"></div><span class="arrow"></span></div>',
                        trigger: "hover click",
                        animation: !1,
                        container: e
                    })
                }), $('[data-trigger="session-info"]').each(function() {
                    return $(this).unbind().click(function() {
                        return $("#r-info").modal("show"), t = "/trainer_sessions/" + this.dataset.id, $.ajax({
                            method: "GET",
                            url: t,
                            success: function(t) {
                                return $("#r-info .ready").html(t), fbAsyncInit(), i(), $("#r-info .preloader").hide(), $("#r-info .ready").show(), e()
                            },
                            error: function() {
                                return $("#r-info").modal("hide"), alert("Error.")
                            }
                        })
                    })
                })
            })
        }, n = function(t, i) {
            var n;
            return n = $("#ags #sess-list").attr("get-url"), $.ajax({
                method: "get",
                url: n,
                data: {
                    offset: t,
                    direction: i
                },
                success: function(t) {
                    return $("#ags #sess-list").html(t.html), t.first_page ? $(".prev-week").hide() : $(".prev-week").show(), t.last_page ? $(".next-week").hide() : $(".next-week").show(), s.offset = t.offset, e()
                }
            })
        }, i = function() {
            var e, t, i, n, s;
            return (e = document.getElementById("map_canvas_session")) ? (s = new google.maps.LatLng(e.getAttribute("lat"), e.getAttribute("long")), t = {
                center: s,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }, i = new google.maps.Map(e, t), google.maps.event.addListener(i, "click", function() {
                return window.open(this.mapUrl)
            }), google.maps.event.addListenerOnce(i, "idle", function() {
                return google.maps.event.trigger(i, "resize"), i.panTo(s), setTimeout(function() {
                    return $(e).animate({
                        opacity: 1
                    }, 150)
                }, 100)
            }), n = new google.maps.Marker({
                position: s,
                map: i,
                title: e.getAttribute("marker_title"),
                cursor: "default"
            })) : void 0
        }
    }.call(this), function() {
        var e, t;
        jQuery(document).on('page:change', function() {
            var i;
            return $(".edit_expert_profile").length > 0 && (window.gon || (window.gon = {}), window.gon.expert_type = [], $.each(gon.expert_types_list, function(e, t) {
                return window.gon.expert_type.push({
                    val: t
                })
            }), window.gon.availability = [{
                val: "Sunday"
            }, {
                val: "Monday"
            }, {
                val: "Tuesday"
            }, {
                val: "Wednesday"
            }, {
                val: "Thursday"
            }, {
                val: "Friday"
            }, {
                val: "Saturday"
            }], window.gon.checkbox = [{
                val: "YES"
            }, {
                val: "NO"
            }], window.gon.skill_levels = [{
                val: "ALL LEVELS"
            }, {
                val: "BEGINNER"
            }, {
                val: "INTERMEDIATE"
            }, {
                val: "EXPERT"
            }]), $("[role=tab]").length > 0 && (window.location.hash && setTimeout(function() {
                return $("a[href=" + window.location.hash + "]").click()
            }, 0), $("[role=tab]").each(function() {
                return $(this).click(function() {
                    return $(this).tab("show"), !1
                })
            })), $("[role=ctab]").each(function() {
                return $(this).click(function() {
                    return $("a[href=" + $(this).attr("href") + "]").click(), !1
                })
            }), $("#expert_profile_avatar").length > 0 && ($(".epa").click(function() {
                return $("#expert_profile_avatar").click(), !1
            }), $("#expert_profile_avatar").change(function() {
                var e, t, i, n;
                return i = $(".avatar-img"), e = $("form.edit_expert_profile"), t = new FormData(e[0]), this.files && this.files[0] && (i.hide(), t.append("expert_profile[avatar]", this.files[0]), t.append("expert_profile[about]", $("#expert_profile_about").val()), n = new FileReader, n.onload = function(e) {
                    return i[0].src = e.target.result
                }, n.readAsDataURL(this.files[0]), i.fadeIn()), $.ajax({
                    type: "POST",
                    url: e.attr("action"),
                    processData: !1,
                    contentType: !1,
                    data: t
                })
            }), $(".save-profile").click(function() {
                var e, t;
                return e = $("form.edit_expert_profile"), t = new FormData(e[0]), t.append("expert_profile[about]", $("#expert_profile_about").val()), $.ajax({
                    type: "POST",
                    url: e.attr("action") + ".json?validate=false",
                    processData: !1,
                    contentType: !1,
                    data: t,
                    success: function(e) {
                        return e.errors ? swal("", e.errors.join("\n"), "error") : swal("Successfully saved", "", "success")
                    }
                }), !1
            }), $(".selectize").each(function() {
                var e, t;
                t = $(this).attr("kind"), e = -1 !== ["expert_type", "checkbox", "skill_levels"].indexOf(t) ? 1 : 100, $(this).selectize({
                    delimiter: ", ",
                    persist: !1,
                    valueField: "val",
                    labelField: "val",
                    maxItems: e,
                    options: gon[t],
                    onOptionAdd: function(e, t) {
                        return console.log(e), console.log(t)
                    }
                })
            }), $(".location-show").focus(function() {
                return $("#location").modal("show")
            }), $("#location").on("hidden.bs.modal", function() {
                var e, t;
                return e = $("#expert_profile_city").val(), t = $("#expert_profile_state").val(), e ? $(".location-show").val(e + ", " + t) : void 0
            }), $(".upload_media").click(function() {
                return $("#add_media").modal("show"), !1
            }), $("button.list").click(function() {
                var e, t, i;
                return $(".your-profile-listed, .your-profile-unlisted").toggle(), i = $(this).hasClass("not-listed"), $("#expert_profile_listed").val(i ? "t" : "f"), e = $("form.edit_expert_profile"), t = new FormData(e[0]), t.append("expert_profile[about]", $("#expert_profile_about").val()), $.ajax({
                    type: "POST",
                    url: e.attr("action") + ".json",
                    processData: !1,
                    contentType: !1,
                    data: t,
                    success: function(e) {
                        return e.errors ? (swal("Profile not listed", e.errors.join("\n"), "error"), $(".your-profile-listed").hide(),  $("#expert_profile_listed").val("f"), $(".your-profile-unlisted").show()) : i ? swal("Your profile is now public!", "", "success") : void 0
                    }
                })
            }), i = /[^\?]+$/.exec(document.location.href)[0].split("="), -1 !== i.indexOf("auto_list_profile") && 0 === $(".alert-first").length && $("button.list.not-listed").trigger("click"), Kuju.fit_images(".media-entry img", 1.45)), $("#new_media").length > 0 ? (e(), $("#media_kind_image").click(function() {
                return $(".media .video").hide(), $(".media .image").show()
            }), $("#media_kind_video").click(function() {
                return $(".media .image").hide(), $(".media .video").show()
            }), $(".image-upload-btn").click(function() {
                return $("#media_asset").click(), $("#add_media .gs-new-preloader").show(), !1
            }), $("#media_asset").change(function(e) {
                var i;
                return "" !== $(this).val() ? (i = new FormData(document.getElementById("new_media")), $.each(e.target.files, function(e, t) {
                    return i.append("images[]", t)
                }), i.append("kind", "image"), $.ajax({
                    type: "POST",
                    url: $("#new_media").attr("action"),
                    async: !0,
                    processData: !1,
                    contentType: !1,
                    data: i,
                    success: function() {
                        return t(), $("#add_media").modal("hide"), $("#add_media .gs-new-preloader").hide()
                    },
                    error: function() {
                        return $("#add_media .gs-new-preloader").hide()
                    }
                })) : void $("#add_media .gs-new-preloader").hide()
            }), $(".add-medias-submit").click(function() {
                var e;
                return e = $("#media_video_url").val(), -1 !== e.indexOf("youtu") || -1 !== e.indexOf("vimeo") ? $.ajax({
                    type: "POST",
                    url: $("#new_media").attr("action"),
                    data: {
                        kind: "video",
                        video_url: $("#media_video_url").val()
                    },
                    success: function(e) {
                        var i;
                        return e.errors ? (i = "<li>" + e.errors.join("</li><li>") + "</li>", $("#new_media .errors_list ul").html(i), $("#new_media .errors_list").show()) : (t(), $("#new_media .errors_list").hide(), $("#add_media").modal("hide")), $("#add_media .gs-new-preloader").hide()
                    },
                    error: function() {
                        return $("#add_media .gs-new-preloader").hide()
                    }
                }) : swal("", "Unsupported video url. Please use YouTube or Vimeo services.", "error")
            })) : void 0
        }), e = function() {
            return $(".media-remove").each(function() {
                return $(this).unbind().click(function() {
                    var e, i, n;
                    return e = $(this).attr("mid"), n = $("#media").attr("get-url"), i = confirm("Are you sure?"), i && $.ajax({
                        type: "DELETE",
                        url: n + "/" + e,
                        success: function(e) {
                            return e.errors ? alert(e.errors) : t()
                        }
                    }), !1
                })
            })
        }, t = function() {
            return $.ajax({
                type: "GET",
                url: $("#media").attr("get-url"),
                success: function(t) {
                    return $("#media").html(t), Kuju.fit_images(".media-entry img", 1.45), e(), $("#lightGallery").lightGallery()
                }
            })
        }
    }.call(this), function() {
        jQuery(document).on('page:change', function() {
            var e, t;
            if (0 !== $("#expert_profile_availability").length) return $("#expert_profile_availability + .selectize-control").click(function() {
                var t, i, n;
                return n = $(this).find(".selectize-input .active").html(), n ? (t = e(), i = t[n], i ? ($("#time_range .from-time").timepicker("setTime", i.from), $("#time_range .to-time").timepicker("setTime", i.to)) : ($("#time_range .from-time").timepicker("setTime", "9:00 AM"), $("#time_range .to-time").timepicker("setTime", "9:00 PM")), $("#time_range .submit").attr("data-day", n), $("#time_range .day-name").html(n.toUpperCase()), $("#time_range").modal("show")) : void 0
            }), $("#time_range .submit").click(function() {
                var e, i;
                return e = $(this).attr("data-day"), i = {}, i[e] = {
                    from: $("#time_range .from-time").val(),
                    to: $("#time_range .to-time").val()
                }, t(i), $("#time_range").modal("hide")
            }), e = function() {
                var e;
                return e = $("#advanced_data").val(), e && "null" !== e ? JSON.parse(e) : {}
            }, t = function(t) {
                var i, n;
                return i = e(), n = $.extend(i, t), $("#advanced_data").val(JSON.stringify(n))
            }
        })
    }.call(this), jQuery(document).on('page:change', function() {
        gon && gon.enable_facebook_api && (window.fbAsyncInit = function() {
            FB.init({
                appId: "",
                xfbml: !0,
                version: "v2.1"
            })
        }, function(e, t, i) {
            var n, s = e.getElementsByTagName(t)[0];
            e.getElementById(i) || (n = e.createElement(t), n.id = i, n.src = "//connect.facebook.net/en_US/sdk.js", s.parentNode.insertBefore(n, s))
        }(document, "script", "facebook-jssdk"))
    }), function() {
        var e, t, i;
        jQuery(document).on('page:change', function() {
            return $("#create_gs").length > 0 ? e() : void 0
        }), e = function() {
            return $(".t-line h4").each(function() {
                return $(this).unbind().click(function() {
                    return $(this).parent().next().slideToggle({
                        easing: "linear"
                    }), $(this).find("i").each(function() {
                        return $(this).toggleClass("hide")
                    })
                })
            }), $("#create_gs").unbind().click(function() {
                return $("#group_session_add").modal("show"), !1
            }), $(".gs-new-submit").unbind().click(function() {
                var e;
                return $(this).attr("disabled", !0), e = $("#new_group_session"), $(".gs-new-preloader").show(), $.ajax({
                    type: "POST",
                    url: e.attr("action"),
                    data: e.serialize(),
                    success: function(e) {
                        var i;
                        return e.errors ? (i = "<li>" + e.errors.join("</li><li>") + "</li>", $(".gs .errors_list ul").html(i), $(".gs .errors_list").show()) : ($("gs .errors_list").hide(), t("new_group", e), $("#group_session_add").modal("hide"), $("#new_group_session")[0].reset()), $(".gs-new-preloader").hide(), $(".gs-new-submit").attr("disabled", !1)
                    }
                })
            }), $(".edit-class").unbind().click(function() {
                var e, t;
                return $("#group_session_edit .gs-new-preloader").show(), e = $("#group_session_edit").attr("get-url"), t = $(this).attr("gsid"), $("#group_session_edit").modal("show"), $.ajax({
                    type: "get",
                    url: e + "/" + t + "/edit",
                    success: function(e) {
                        return $("#group_session_edit .modal-body").html(e), $("#group_session_edit .gs-new-preloader").hide()
                    }
                })
            }), $(".gs-edit-submit").unbind().click(function() {
                var e;
                return e = $(".edit_group_session"), $(".gs-new-preloader").show(), $.ajax({
                    type: "PATCH",
                    url: e.attr("action"),
                    data: e.serialize(),
                    success: function(e) {
                        var i;
                        return e.errors ? (i = "<li>" + e.errors.join("</li><li>") + "</li>", $("#group_session_edit .errors_list ul").html(i), $("#group_session_edit .errors_list").show()) : ($("#group_session_edit .errors_list").hide(), t("edited_group", e), $("#group_session_edit").modal("hide"), $("#group_session_edit .modal-body").html("")), $(".gs-new-preloader").hide()
                    }
                })
            }), $("#session_date").datepicker({
                format: "mm/dd/yyyy"
            }).on("changeDate", function() {
                var e, t;
                return t = new Date($("#session_date").val()), e = Kuju.weekdays[t.getDay()], $(".dayname").html(e + "s"), $(".repeat_options").show()
            }), $("#session_start").timepicker({
                minuteStep: 5
            }), $("#session_end").timepicker({
                minuteStep: 5
            }), $(".add-session-to-group").unbind().click(function() {
                var e;
                return $("#session_repeat_session").selectpicker("val", "0"), $(".repeat_options input:checked").attr("checked", !1), $("#new_session_modal").modal("show"), e = $(this).attr("gsid"), $("#session_group_session_id").val(e), $.ajax({
                    type: "GET",
                    url: "/trainer_sessions/last",
                    data: {
                        group_session_id: e
                    },
                    success: function(e) {
                        var t, i, n, s;
                        for (s = ["add1", "add2", "location_name", "instructor", "city", "state", "zip"], i = 0, n = s.length; n > i; i++) t = s[i], $("#session_" + t).val(e[t]);
                        return $(".class-name").html(e.group_session_name)
                    }
                })
            }), $("#session_date").change(function() {
                return console.log($("#session_date").val())
            }), $(".add-session-submit").unbind().click(function() {
                var e;
                return "" === $("#session_date").val() ? alert("You have to select session date.") : ($(".gs-new-preloader").show(), e = $("#new_session"), $.ajax({
                    type: "POST",
                    url: e.attr("action"),
                    data: e.serialize(),
                    success: function(e) {
                        var i;
                        return e.errors ? (i = "<li>" + e.errors.join("</li><li>") + "</li>", $("#new_session_modal .errors_list ul").html(i), $("#new_session_modal .errors_list").show()) : (t("added_session", e), $("#new_session_modal").modal("hide"), $("#session_date").val(""), $("#new_session_modal .errors_list ul").html(""), $("#new_session_modal .errors_list").hide()), $(".gs-new-preloader").hide()
                    }
                }))
            }), $(".session-remove").each(function() {
                return $(this).unbind().click(function() {
                    var e, i;
                    return i = $(this).attr("sid"), e = confirm("Are you sure?"), e && $.ajax({
                        type: "DELETE",
                        url: "/trainer_sessions/" + i,
                        success: function(e) {
                            return e.errors ? alert(e.errors) : t("deleted_session", e)
                        }
                    }), !1
                })
            }), $(".group-sessions-remove").each(function() {
                return $(this).unbind().click(function() {
                    var e, i;
                    return e = $(this).attr("gsid"), i = confirm("Are you sure?"), i && $.ajax({
                        type: "DELETE",
                        url: "/group_sessions/" + e,
                        success: function(e) {
                            return e.errors ? alert(e.errors) : t("deleted_group", e)
                        }
                    }), !1
                })
            }), $(".show-history").each(function() {
                return $(".show-history").unbind().click(function() {
                    var t;
                    if ($(this).hasClass("next-page")) i.page++;
                    else if ($(this).hasClass("prev-page")) {
                        if (!(i.page > 2)) return !1;
                        i.page--
                    }
                    return $("#group_sessions_history .modal-body.wait").show(), $("#group_sessions_history .modal-body.ready").hide(), $("#group_sessions_history").modal("show"), t = $("#group_sessions_history").attr("get-url").replace(":id", $(this).attr("gsid")), $.ajax({
                        type: "GET",
                        url: t + "?page=" + i.page,
                        success: function(t) {
                            return $("#group_sessions_history .modal-body.ready").html(t), $("#group_sessions_history .modal-body.wait").hide(), $("#group_sessions_history .modal-body.ready").show(), e()
                        }
                    })
                })
            })
        }, i = {
            page: 1
        }, t = function(t, i) {
            var n;
            switch (t) {
                case "new_group":
                    $("#group_sessions_list .gs-list > div:first-child").length > 0 ? $("#group_sessions_list .gs-list > div:first-child").before(i.html) : $("#group_sessions_list .gs-list").html(i.html);
                    break;
                case "edited_group":
                    $("#group_sessions_list .gs-box[gsid=" + i.id + "]").replaceWith(i.html);
                    break;
                case "added_session":
                    $("#group_sessions_list .gs-box[gsid=" + i.gsid + "] tbody:last").append(i.html);
                    break;
                case "deleted_session":
                    $("#group_sessions_list .sess-entry[sid=" + i.id + "]").slideToggle();
                    break;
                case "deleted_group":
                    $("#group_sessions_list .gs-box[gsid=" + i.id + "]").slideToggle();
                    break;
                default:
                    n = $("#group_sessions_list"), $.ajax({
                        type: "get",
                        url: n.attr("get-url"),
                        success: function(t) {
                            return n.html(t), e()
                        },
                        error: function() {
                            return location.reload()
                        }
                    })
            }
            return e()
        }
    }.call(this), function() {
        jQuery(document).on('page:change', function() {
            return setTimeout(function() {
                return $("i.frame_source").each(function() {
                    var e;
                    return e = document.createElement("iframe"), e.src = $(this).attr("src"), e.width = $(this).attr("width"), e.height = $(this).attr("height"), $(this).parent().append(e)
                })
            }, 500)
        })
    }.call(this), function() {}.call(this), function() {
        var e;
        jQuery(document).on('page:change', function() {
            return $(".nice-checkbox").each(function() {
                var t;
                return t = $(this).find("[type=checkbox]"), e(this, t.is(":checked")), $(this).click(function() {
                    var i;
                    return t = $(this).find("[type=checkbox]"), i = !t.is(":checked"), t.trigger("click"), e(this, i)
                })
            })
        }), e = function(e, t) {
            var i, n;
            return i = $(e).find(".fa-check-square-o"), n = $(e).find(".fa-square-o"), t ? (n.hide(), i.show()) : (i.hide(), n.show())
        }
    }.call(this), function() {
        var e, t;
        jQuery(document).ready(function(i) {
            return i("#packages-list").length > 0 ? (t(), e(), i(".add-package").click(function() {
                return i("#add_package_modal .modal-title").html("CREATE NEW PACKAGE"), i("#add_package_modal .add-package-submit").html("CREATE"), i("#new_package").removeAttr("edit-action"), i("#package_price, #package_sessions, #package_description").val(""), i("#add_package_modal").modal("show")
            })) : void 0
        }), t = function() {
            return $(".add-package-submit").click(function() {
                var t, i, n, s, o;
                return s = $("#package_price").val(), o = $("#package_sessions").val(), i = $("#package_description").val(), "" === s || "" === o || "" === i ? swal("", "Please, fill all fields.", "error") : ($("#new_package").attr("edit-action") ? (t = $("#new_package").attr("edit-action") + ".json", n = "PATCH") : (t = $("#new_package").attr("action") + ".json", n = "POST"), $.ajax({
                    type: n,
                    url: t,
                    data: {
                        price: s,
                        sessions: o,
                        description: i
                    },
                    success: function(t) {
                        return $("#add_package_modal .gs-new-preloader").hide(), $("#packages-list").html(t.html), $("#add_package_modal").modal("hide"), e()
                    },
                    error: function() {
                        return $("#add_package_modal .gs-new-preloader").hide(), swal("", "Some error happened.", "error")
                    }
                }))
            })
        }, e = function() {
            return $(".edit-package").unbind().click(function() {
                var e, t;
                return e = $(this).data("id"), t = $(this).closest("tr"), $("#package_price").val(t.find(".p-price").html()), $("#package_sessions").val(t.find(".sessions").html()), $("#package_description").val(t.find(".description").html()), $("#add_package_modal .modal-title").html("EDIT PACKAGE"), $("#add_package_modal .add-package-submit").html("SAVE"), $("#new_package").attr("edit-action", $("#new_package").attr("action") + ("/" + e)), $("#add_package_modal").modal("show")
            }), $(".destroy-package").unbind().click(function() {
                var t, i;
                return t = confirm("Are you sure?"), t ? (i = $(this).data("id"), $.ajax({
                    type: "DELETE",
                    url: $("#new_package").attr("action") + ("/" + i),
                    success: function(t) {
                        return $("#packages-list").html(t.html), e()
                    },
                    error: function() {
                        return swal("", "Some error happened.", "error")
                    }
                })) : void 0
            })
        }
    }.call(this), function() {
        var e, t;
        jQuery(document).on('page:change', function() {
            return $(".show-login").click(function() {
                return t("#sign-in-modal")
            }), $(".show-sign-up").click(function() {
                return t("#sign-up-modal")
            }), $(".show-email-signup").click(function() {
                return t("#sign-up-modal-email")
            }), $(".show-forgot-pass").click(function() {
                return t("#forgot-pass-modal")
            }), $("#apply-now-link").click(function() {
                return "" === $("#apply-now-modal #email").val() ? alert("Please fill in your email") : $.ajax({
                    type: "POST",
                    url: "apply",
                    data: {
                        name: $("#apply-now-modal #name").val(),
                        email: $("#apply-now-modal #email").val(),
                        message: $("#apply-now-modal #message").val()
                    },
                    success: function() {
                        return $("#apply-now-modal").modal("hide"), swal("Thanks for applying!", "One of our team members will get in touch with you shortly.", "success")
                    }
                })
            }), $(".send-reset-pass").click(function() {
                var e;
                return e = $("#forgot-pass-modal form .email").val(), -1 === e.indexOf("@") ? !1 : ($.ajax({
                    type: "POST",
                    url: $("#forgot-pass-modal form").attr("action") + ".json",
                    data: {
                        user: {
                            email: e
                        }
                    },
                    success: function(e) {
                        return e.errors ? swal("", e.errors.join("\n"), "error") : ($("#forgot-pass-modal").modal("hide"), swal("", "You will receive an email with instructions on how to reset your password in a few minutes.", "success"))
                    }
                }), !1)
            }), $("#sign-up-modal-email form").submit(function() {
                return e("#sign-up-modal-email form"), !1
            }), $("#sign-in-modal form").submit(function() {
                return e("#sign-in-modal form"), !1
            }), $("#new-pass-modal form").submit(function() {
                return e("#new-pass-modal form"), !1
            })
        }), e = function(e) {
            var t;
            return t = $(e).serializeObject(), $.ajax({
                type: "POST",
                url: $(e).attr("action") + ".json",
                async: !0,
                data: t,
                success: function(e) {
                    $("#sign-up-modal-email").modal("hide")
                    return  window.location.href = 'https://kuju-straho.c9users.io/accounts'
    
                 },
                error: function() {
                    return swal("", "Email or password is incorrect", "error")
                }
            })
        }, t = function(e) {
            return $(".modal.in").each(function() {
                return $(this).modal("hide")
            }), $(e).modal("show")
        }, $(function() {
            var e, t;
            return $("ul.bxslider").bxSlider({
                mode: "fade",
                auto: !0,
                speed: 1e3,
                pause: 3e3,
                controls: !1,
                pager: !1,
                width: 940
            }), window.slider = $("ul.bxslider2").bxSlider({
                mode: "fade",
                auto: !0,
                speed: 500,
                pause: 3e3,
                controls: !1,
                pager: !1,
                adaptiveHeight: !0,
                easing: "ease-out"
            }), e = ["Yoga Trainer", "Boxing coach", "Fitness coach", "Fitness expert", "Baseball coach", "MMA expert", "Nutritionist", "Dietician", "Personal trainer", "Health expert", "Life coach"], t = ["New York, NY", "Queens, NY", "Astoria, NY", "Brooklyn, NY", "Bronx, NY", "Staten Island, NY", "Long Island City, NY", "Harlem, NY", "Manhattan, NY"], $("#search").autocomplete({
                source: e
            }).autocomplete("widget").addClass("ssearch"), $("#where").autocomplete({
                source: t
            }).autocomplete("widget").addClass("swhere"), $.extend($.ui.autocomplete.prototype, {
                _renderItem: function(e, t) {
                    var i, n, s, o;
                    return o = this.element.val(), n = new RegExp(o, "ig"), s = '<span class="h-style">$&</span>', i = t.label.replace(n, s), $("<li></li>").data("item.autocomplete", t).append($("<a></a>").html(i)).appendTo(e)
                }
            })
        })
    }.call(this), function() {}.call(this), function() {
        var e, t, i, n, s, o, r, a, l, c;
        jQuery(document).on('page:change', function() {
            return $("#stripe_key").length > 0 && (Stripe.setPublishableKey($("#stripe_key").val()), n.setupForm()), ".container.reservation".length > 0 ? (e(), $(".show_add_card_form").click(function() {
                return "add" === $(this).attr("state") ? ($(this).html("CANCEL"), $(this).attr("state", "CANCEL"), $(".add_card_form").removeClass("hide"), $(".add_card_form input").each(function() {
                    return $(this).val("")
                }), $(".add_card_form select").each(function() {
                    var e;
                    return e = $(this).find("option:first").val(), $(this).selectpicker("val", e)
                })) : ($(this).html("ADD NEW CARD"), $(this).attr("state", "add"), $(".add_card_form").addClass("hide"), $(".add_card_form input").each(function() {
                    return $(this).val("")
                }))
            }), $(".edit-bill-info-link").click(function() {
                return $(".billing-info").addClass("hide"), $(".edit-bill-info-form").removeClass("hide")
            }), $("#terms").click(function() {
                return $("#submit-order").attr("disabled") ? ($("#submit-order").attr("disabled", !1), $(this).addClass("fa-check-square-o"), $(this).removeClass("fa-square-o")) : ($("#submit-order").attr("disabled", !0), $(this).addClass("fa-square-o"), $(this).removeClass("fa-check-square-o"))
            }), $(".check").click(function() {
                return $(this).hasClass("fa-square-o") ? ($(this).removeClass("fa-square-o"), $(this).next().attr("checked", "checked")) : ($(this).addClass("fa-square-o"), $(this).next().attr("checked", !1))
            }), $(".add-sess").click(function() {
                return $(this).before('<div class="csession w-100 vc" style="display:none"> <div class="col-sname relative"> <span>Session <span class="sess-num"></span></span> <span class="remove-session link">remove</span> </div> <div class="col-sdate"> <input type="text" class="sdate datepicker-r" name="date" value="Pick Date"> </div> <div class="col-stime"> <input type="text" class="stime" disabled name="time" value="Pick Time"> <i class="fa fa-angle-down text-light ricon"></i> </div> </div>'), c(), $(this).prev().slideToggle("slow"), e(), a()
            }), $(".apply-promo").click(function() {
                return t()
            })) : void 0
        }), o = {
            value: 0,
            kind: 1
        }, r = function() {
            var e;
            return o.value > 0 ? 1 === o.kind ? e = o.value : 2 === o.kind && (e = o.value * gon.cost_per_session / 100) : e = 0, e
        }, t = function() {
            var e;
            return e = $("#promo-code-value").val(), e && "" !== e ? ($(".promo-preloader").show(), $.ajax({
                url: "/reservations/promo-code-check",
                method: "POST",
                data: {
                    code: e
                },
                success: function(e) {
                    return o = {
                        value: e.value,
                        kind: e.kind
                    }, a(), swal("Your code was applied!", null, "success"), $("#promo-code").modal("hide"), $("#promo_code_value").val(e.code), $(".promo-discount .val").html("$" + r()), $(".promo-discount").removeClass("hide"), $(".have-a-promo").hide()
                },
                error: function() {
                    return swal("Invalid promo code", null, "error")
                },
                complete: function() {
                    return $(".promo-preloader").hide()
                }
            })) : void 0
        }, c = function() {
            var e;
            return e = 1, $(".csession").each(function() {
                return $(this).find(".sess-num").html(e), $(this).find(".sdate").attr("name", "session[" + e + "][date]"), $(this).find(".stime").attr("name", "session[" + e + "][time]"), e++
            })
        }, a = function() {
            var e, t;
            return $(".sessions-count").html($(".csession").length), t = $(".csession").length, 1 > t && (t = 1), e = t * gon.cost_per_session, $('input[name="medo"]').attr('value', e),  e -= r(), 0 > e && (e = 0), $("#total-price").html("$" + e)
        }, e = function() {
            var e, t;
            return $(".ricon").each(function() {
                return $(this).unbind().click(function() {
                    return $(this).prev().click()
                })
            }), t = new Date, e = new Date(t.getFullYear(), t.getMonth(), t.getDate(), 0, 0, 0, 0), $(".datepicker-r").each(function() {
                var t;
                return t = $(this).datepicker({
                    format: "mm/dd/yyyy",
                    onRender: function(t) {
                        return t.valueOf() < e.valueOf() ? "disabled" : gon.expert_availability && !gon.expert_availability[t.getDay()] ? "disabled" : ""
                    }
                }).on("changeDate", function(e) {
                    var i, n;
                    return e.date.valueOf() > t.date.valueOf() && (i = new Date(e.date), i.setDate(i.getDate() + 1), t.setValue(i)), n = $(this).parent().parent().find(".stime"), n.attr("disabled", !1), n.attr("data-from", !1), n.timepicker("setTime", n.val())
                }).data("datepicker")
            }), $(".remove-session").unbind().click(function() {
                return $(this).parent().parent().remove(), a(), c()
            }), $("#saved-card").change(function() {
                return $("#used_card_id").val("")
            }), $(".stime").each(function() {
                return $(this).timepicker({
                    minuteStep: 5
                }).on("changeTime.timepicker", l)
            })
        }, gon.timepicker = {}, l = function(e) {
            var t, n, s, o, r, a, l, c;
            return l = $(e.target), c = i(e.target), a = gon.timepicker[c], "updating" === a ? void(gon.timepicker[c] = "done") : (gon.timepicker[c] = "updating", n = new Date(l.parent().parent().find(".sdate").val()), (r = gon.expert_time_availability[n.getDay()]) ? (t = new Date("01 01 2017 " + e.time.value), o = new Date("01 01 2017 " + r.from), s = new Date("01 01 2017 " + r.to), o > t ? l.timepicker("setTime", r.from) : t > s ? l.timepicker("setTime", r.to) : gon.timepicker[c] = "done") : void 0)
        }, i = function(e) {
            var t;
            return (t = $(e).attr("id")) ? t : (t = Math.floor(1e15 * Math.random()), $(e).attr("id", t), t)
        }, s = function() {
            var e, t;
            return $("#r-success").on("hidden.bs.modal", function() {
                return document.location.href = $("#r-success a.btn-success").attr("href")
            }), e = $("#bookform"), t = new FormData(e[0]), $.ajax({
                type: "POST",
                url: e.attr("action") + ".json",
                async: !0,
                processData: !1,
                contentType: !1,
                data: t,
                success: function(e) {
                    return e.errors ? (swal("", e.errors.join("\n"), "error"), $("#submit-order").attr("disabled", !1), e.card_id ? $("#used_card_id").val(e.card_id) : void 0) : (e.calendar_links && ($("#r-success #add-to-calendar-links").html(e.calendar_links), $("#r-success .calendar-sync").show()), $("#r-success").modal("show"))
                },
                error: function() {
                    return swal("", "Server error has happened. Please use contact form to report your problem.", "error"), $("#submit-order").attr("disabled", !1)
                }
            })
        }, n = {
            setupForm: function() {
                return $("#submit-order").click(function() {
                    var e, t, i, s;
                    return $(this).attr("disabled", !0), t = !1, $(".custom-sessions .sdate").each(function() {
                        return "pick date" === $(this).val().toLowerCase() ? (t = "You must enter the dates for sessions.", $(this).addClass("with-error")) : $(this).removeClass("with-error")
                    }), $(".csession").length < 1 && 0 === $("#group_session_id").length && 0 === $("#package_id").length && (t = "Please add at least one session."), $("#card_number").val().length > 0 ? $("input[name^=card]").each(function() {
                        return "" === $(this).val() ? (t = "Please fill out " + $(this).prev().html().toLowerCase() + " field", $(this).addClass("with-error")) : $(this).removeClass("with-error")
                    }) : $("#saved-card option:selected").length > 0 && (i = $("#saved-card option:selected").attr("expired"), s = new Date(i), e = new Date, e > s && (t = "Your card was expired")), 0 === $("#card_number").val().length && 0 === $("#saved-card option").length && (t = "Please enter card data"), t ? (swal("", t, "error"), $("#submit-order").attr("disabled", !1)) : (n.error_showed = !1, n.processCard()), !1
                })
            },
            processCard: function() {
                var e;
                return n.needs_to_update_token() ? (e = {
                    name: $("#name").val(),
                    number: $("#card_number").val(),
                    cvc: $("#card_cvc").val(),
             
                    expMonth: $("#card_expiration_date_month").val(),
                    expYear: $("#card_expiration_date_year").val()
                }, $("#used_card_number").val(e.number), $("#used_card_id").val(""), Stripe.createToken(e, n.handleStripeResponse)) : s()
            },
            handleStripeResponse: function(e, t) {
                var i, o;
                return 200 === e ? (n.error_showed = !1, o = !1, i = !1, $("#stripe_card_token").val(t.id), s()) : (swal("Card error: ", t.error.message, "error"), $("#submit-order").attr("disabled", !1))
            },
            needs_to_update_token: function() {
                var e, t, i;
                return e = $("#card_number").val(), t = $("#stripe_card_token").val(), i = $("#used_card_number").val(), "" !== e && ("" === t || "" !== i && e !== i)
            }
        }
    }.call(this), function() {}.call(this), function() {
        var e;
        jQuery(document).on('page:change', function() {
            var t, i;
            return $(".result-entry").length > 0 && Kuju.fit_images(".avatar img", .875), $("select.multiselect").length > 0 && ($("select.multiselect").multipleSelect({
                placeholder: "NO PREFERENCES",
                width: "162px",
                selectAll: !1,
                onClose: e
            }), i = /[\?&]days=([^&]+)/.exec(document.location.href), i.length > 1) ? (t = i[1].split(","), $("select.multiselect").multipleSelect("setSelects", t), console.log(t)) : void 0
        }), e = function() {
            var e;
            return e = $("select.multiselect").multipleSelect("getSelects", "value"), document.location.href = Kuju.build_url({
                days: e
            })
        }
    }.call(this), function() {}.call(this), function() {
        jQuery(document).on('page:change', function() {
            var e;
            return $(".google-calendars-list").length > 0 ? (e = $(".google-calendars-list").attr("path"), $.ajax({
                type: "GET",
                url: e,
                success: function(e) {
                    var t, i, n, s, o;
                    if (e.authorized) {
                        for (i = "", o = e.items, n = 0, s = o.length; s > n; n++) t = o[n], i += '<option value="' + t.calendar_id + '"', t.selected && (i += ' selected="selected"'), i += ">" + t.calendar_title + "</option>";
                        return $(".google-calendars-list select").html(i), $(".google-calendars-list .selectpicker").selectpicker("refresh"), $(".google-calendars-list .preloader").hide(), $(".google-calendars-list .calendar-list").show()
                    }
                    return alert("Error happened, check console."), console.log(e)
                }
            })) : void 0
        })
    }.call(this), function() {}.call(this), function() {
        var e;
        jQuery(document).on('page:change', function() {
            var t;
            return $(".datepicker-u").each(function() {
                return $(this).datepicker({
                    format: "mm/dd/yyyy"
                })
            }), $("#user_avatar").change(function() {
                var e, t, i, n;
                return i = $(".avatar_photo img"), e = $("form.edit_user"), t = new FormData(e[0]), this.files && this.files[0] && (i.hide(), t.append("user[avatar]", this.files[0]), n = new FileReader, n.onload = function(e) {
                    return i[0].src = e.target.result
                }, n.readAsDataURL(this.files[0]), i.fadeIn()), $.ajax({
                    type: "POST",
                    url: e.attr("action"),
                    processData: !1,
                    contentType: !1,
                    data: t
                })
            }), $(".update-payment").length > 0 && ($(".update-payment").click(function() {
                return $("#update-payment-modal").modal("show")
            }), $(".add-new-card").each(function() {
                return $(this).click(function() {
                    return $(".add-new-card").toggleClass("hide"), $("#update-payment-modal .form").slideToggle(), t.add_new_card = !t.add_new_card
                })
            }), $(".remove-card").click(function() {
                var e, t;
                return e = confirm("Do you really want to delete selected account?"), e ? (t = $("#saved-cards option:selected").val(), $.ajax({
                    type: "DELETE",
                    url: "/cards/" + t,
                    success: function(e) {
                        return e.errors[0] ? alert(e.errors.join("\n")) : ($("#saved-cards option:selected").remove(), $("#saved-cards").selectpicker("refresh"), $("#payment-data").html($("#saved-cards option:selected").html() || "not set."), alert("Deleted."))
                    }
                })) : void 0
            }), $(".payment-method-save").click(function() {
                var i, n, s, o, r, a, l;
                if (t.add_new_card) {
                    for ($(".payment-method-save").attr("disabled", !0), l = ["name", "number", "cvc"], r = 0, a = l.length; a > r; r++) s = l[r], o = "#update-payment-modal #card_" + s, "" === $(o).val() ? $(o).addClass("with-error") : $(o).removeClass("with-error");
                    return $("#update-payment-modal .with-error").length > 0 ? (alert("Please check your data."), $(".payment-method-save").attr("disabled", !1)) : ($(".preloader").show(), n = {
                        name: $("#card_name").val(),
                        number: $("#card_number").val(),
                        cvc: $("#card_cvc").val(),
                        expMonth: $("#card_expiration_date_month").val(),
                        expYear: $("#card_expiration_date_year").val()
                    }, Stripe.createToken(n, e))
                }
                return i = $("#saved-cards option:selected").val(), $.ajax({
                    type: "PATCH",
                    url: "/cards/" + i,
                    data: {
                        card: {
                            active: !0
                        }
                    },
                    success: function(e) {
                        return e.errors[0] ? alert(e.errors.join("\n")) : ($("#payment-data").html($("#saved-cards option:selected").html()), $("#update-payment-modal").modal("hide"))
                    }
                })
            })), $(".resend-verify-email").length > 0 && $(".resend-verify-email").click(function() {
                return $.ajax({
                    type: "POST",
                    url: "/accounts/resend_verification_email",
                    success: function(e) {
                        return swal("Done!", "Verification email was sent to: " + e.email, "success")
                    },
                    error: function() {
                        return swal("Error", "Unknown error, please try again later.", "error")
                    }
                })
            }), $(".save-payout").click(function() {
                var e, t;
                return e = $(this), t = e.hasClass("paypal") ? {
                    type: "paypal",
                    email: $(".paypal-email").val()
                } : {
                    type: "check",
                    name: $(".check-name").val()
                }, $.ajax({
                    type: "POST",
                    url: "/accounts/update-payout-method",
                    data: t,
                    success: function(e) {
                        return $("span.method").html(e.text), $(".modal").modal("hide"), swal("Payout settings updated.", null, "success")
                    },
                    error: function() {
                        return swal("Server error", "Please try again later", "error")
                    }
                })
            }), t = {
                add_new_card: !$("#saved-cards").length
            }
        }), e = function(e, t) {
            return 200 === e ? $.ajax({
                type: "POST",
                url: "/cards",
                data: {
                    card: {
                        number: $("#card_number").val(),
                        name: $("#card_name").val(),
                        date_year: $("#card_expiration_date_year").val(),
                        date_month: $("#card_expiration_date_month").val(),
                        stripe_card_token: t.id,
                        active: !0
                    }
                },
                success: function(e) {
                    var t;
                    return e.errors[0] ? alert(e.errors.join("\n")) : (t = $("#card_number").val(), $("#payment-data").html("XXXX-XXXX-XXXX-" + t.substring(12, 16))), $(".preloader").hide(), $(".payment-method-save").attr("disabled", !1), $("#update-payment-modal").modal("hide"), window.location.reload()
                },
                error: function() {
                    return $(".payment-method-save").attr("disabled", !1), alert("Unknown error, try again please."), $(".preloader").hide()
                }
            }) : (alert("Card error: " + t.error.message), $(".payment-method-save").attr("disabled", !1), $(".preloader").hide())
        }
    }.call(this), ! function(e) {
        var t = function(t, n) {
            if (this.element = e(t), this.format = i.parseFormat(n.format || this.element.data("date-format") || "mm/dd/yyyy"), this.picker = e(i.template).appendTo("body").on({
                    click: e.proxy(this.click, this)
                }), this.isInput = this.element.is("input"), this.component = this.element.is(".date") ? this.element.find(".add-on") : !1, this.isInput ? this.element.on({
                    focus: e.proxy(this.show, this),
                    keyup: e.proxy(this.update, this)
                }) : this.component ? this.component.on("click", e.proxy(this.show, this)) : this.element.on("click", e.proxy(this.show, this)), this.minViewMode = n.minViewMode || this.element.data("date-minviewmode") || 0, "string" == typeof this.minViewMode) switch (this.minViewMode) {
                case "months":
                    this.minViewMode = 1;
                    break;
                case "years":
                    this.minViewMode = 2;
                    break;
                default:
                    this.minViewMode = 0
            }
            if (this.viewMode = n.viewMode || this.element.data("date-viewmode") || 0, "string" == typeof this.viewMode) switch (this.viewMode) {
                case "months":
                    this.viewMode = 1;
                    break;
                case "years":
                    this.viewMode = 2;
                    break;
                default:
                    this.viewMode = 0
            }
            this.startViewMode = this.viewMode, this.weekStart = n.weekStart || this.element.data("date-weekstart") || 0, this.weekEnd = 0 === this.weekStart ? 6 : this.weekStart - 1, this.onRender = n.onRender, this.fillDow(), this.fillMonths(), this.update(), this.showMode()
        };
        t.prototype = {
            constructor: t,
            show: function(t) {
                this.picker.show(), this.height = this.component ? this.component.outerHeight() : this.element.outerHeight(), this.place(), e(window).on("resize", e.proxy(this.place, this)), t && (t.stopPropagation(), t.preventDefault()), !this.isInput;
                var i = this;
                e(document).on("mousedown", function(t) {
                    0 == e(t.target).closest(".datepicker").length && i.hide()
                }), this.element.trigger({
                    type: "show",
                    date: this.date
                })
            },
            hide: function() {
                this.picker.hide(), e(window).off("resize", this.place), this.viewMode = this.startViewMode, this.showMode(), this.isInput || e(document).off("mousedown", this.hide), this.element.trigger({
                    type: "hide",
                    date: this.date
                })
            },
            set: function() {
                var e = i.formatDate(this.date, this.format);
                this.isInput ? this.element.prop("value", e) : (this.component && this.element.find("input").prop("value", e), this.element.data("date", e))
            },
            setValue: function(e) {
                this.date = "string" == typeof e ? i.parseDate(e, this.format) : new Date(e), this.set(), this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0), this.fill()
            },
            place: function() {
                var e = this.component ? this.component.offset() : this.element.offset();
                this.picker.css({
                    top: e.top + this.height,
                    left: e.left
                })
            },
            update: function(e) {
                this.date = i.parseDate("string" == typeof e ? e : this.isInput ? this.element.prop("value") : this.element.data("date"), this.format), this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0), this.fill()
            },
            fillDow: function() {
                for (var e = this.weekStart, t = "<tr>"; e < this.weekStart + 7;) t += '<th class="dow">' + i.dates.daysMin[e++ % 7] + "</th>";
                t += "</tr>", this.picker.find(".datepicker-days thead").append(t)
            },
            fillMonths: function() {
                for (var e = "", t = 0; 12 > t;) e += '<span class="month">' + i.dates.monthsShort[t++] + "</span>";
                this.picker.find(".datepicker-months td").append(e)
            },
            fill: function() {
                var e = new Date(this.viewDate),
                    t = e.getFullYear(),
                    n = e.getMonth(),
                    s = this.date.valueOf();
                this.picker.find(".datepicker-days th:eq(1)").text(i.dates.months[n] + " " + t);
                var o = new Date(t, n - 1, 28, 0, 0, 0, 0),
                    r = i.getDaysInMonth(o.getFullYear(), o.getMonth());
                o.setDate(r), o.setDate(r - (o.getDay() - this.weekStart + 7) % 7);
                var a = new Date(o);
                a.setDate(a.getDate() + 42), a = a.valueOf();
                for (var l, c, d, u = []; o.valueOf() < a;) o.getDay() === this.weekStart && u.push("<tr>"), l = this.onRender(o), c = o.getFullYear(), d = o.getMonth(), n > d && c === t || t > c ? l += " old" : (d > n && c === t || c > t) && (l += " new"), o.valueOf() === s && (l += " active"), u.push('<td class="day ' + l + '">' + o.getDate() + "</td>"), o.getDay() === this.weekEnd && u.push("</tr>"), o.setDate(o.getDate() + 1);
                this.picker.find(".datepicker-days tbody").empty().append(u.join(""));
                var h = this.date.getFullYear(),
                    p = this.picker.find(".datepicker-months").find("th:eq(1)").text(t).end().find("span").removeClass("active");
                h === t && p.eq(this.date.getMonth()).addClass("active"), u = "", t = 10 * parseInt(t / 10, 10);
                var f = this.picker.find(".datepicker-years").find("th:eq(1)").text(t + "-" + (t + 9)).end().find("td");
                t -= 1;
                for (var m = -1; 11 > m; m++) u += '<span class="year' + (-1 === m || 10 === m ? " old" : "") + (h === t ? " active" : "") + '">' + t + "</span>", t += 1;
                f.html(u)
            },
            click: function(t) {
                t.stopPropagation(), t.preventDefault();
                var n = e(t.target).closest("span, td, th");
                if (1 === n.length) switch (n[0].nodeName.toLowerCase()) {
                    case "th":
                        switch (n[0].className) {
                            case "switch":
                                this.showMode(1);
                                break;
                            case "prev":
                            case "next":
                                this.viewDate["set" + i.modes[this.viewMode].navFnc].call(this.viewDate, this.viewDate["get" + i.modes[this.viewMode].navFnc].call(this.viewDate) + i.modes[this.viewMode].navStep * ("prev" === n[0].className ? -1 : 1)), this.fill(), this.set()
                        }
                        break;
                    case "span":
                        if (n.is(".month")) {
                            var s = n.parent().find("span").index(n);
                            this.viewDate.setMonth(s)
                        } else {
                            var o = parseInt(n.text(), 10) || 0;
                            this.viewDate.setFullYear(o)
                        }
                        0 !== this.viewMode && (this.date = new Date(this.viewDate), this.element.trigger({
                            type: "changeDate",
                            date: this.date,
                            viewMode: i.modes[this.viewMode].clsName
                        })), this.showMode(-1), this.fill(), this.set();
                        break;
                    case "td":
                        if (n.is(".day") && !n.is(".disabled")) {
                            var r = parseInt(n.text(), 10) || 1,
                                s = this.viewDate.getMonth();
                            n.is(".old") ? s -= 1 : n.is(".new") && (s += 1);
                            var o = this.viewDate.getFullYear();
                            this.date = new Date(o, s, r, 0, 0, 0, 0), this.viewDate = new Date(o, s, Math.min(28, r), 0, 0, 0, 0), this.fill(), this.set(), this.element.trigger({
                                type: "changeDate",
                                date: this.date,
                                viewMode: i.modes[this.viewMode].clsName
                            })
                        }
                }
            },
            mousedown: function(e) {
                e.stopPropagation(), e.preventDefault()
            },
            showMode: function(e) {
                e && (this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + e))), this.picker.find(">div").hide().filter(".datepicker-" + i.modes[this.viewMode].clsName).show()
            }
        }, e.fn.datepicker = function(i, n) {
            return this.each(function() {
                var s = e(this),
                    o = s.data("datepicker"),
                    r = "object" == typeof i && i;
                o || s.data("datepicker", o = new t(this, e.extend({}, e.fn.datepicker.defaults, r))), "string" == typeof i && o[i](n)
            })
        }, e.fn.datepicker.defaults = {
            onRender: function() {
                return ""
            }
        }, e.fn.datepicker.Constructor = t;
        var i = {
            modes: [{
                clsName: "days",
                navFnc: "Month",
                navStep: 1
            }, {
                clsName: "months",
                navFnc: "FullYear",
                navStep: 1
            }, {
                clsName: "years",
                navFnc: "FullYear",
                navStep: 10
            }],
            dates: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            },
            isLeapYear: function(e) {
                return e % 4 === 0 && e % 100 !== 0 || e % 400 === 0
            },
            getDaysInMonth: function(e, t) {
                return [31, i.isLeapYear(e) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][t]
            },
            parseFormat: function(e) {
                var t = e.match(/[.\/\-\s].*?/),
                    i = e.split(/\W+/);
                if (!t || !i || 0 === i.length) throw new Error("Invalid date format.");
                return {
                    separator: t,
                    parts: i
                }
            },
            parseDate: function(e, t) {
                var i, n = e.split(t.separator),
                    e = new Date;
                if (e.setHours(0), e.setMinutes(0), e.setSeconds(0), e.setMilliseconds(0), n.length === t.parts.length) {
                    for (var s = e.getFullYear(), o = e.getDate(), r = e.getMonth(), a = 0, l = t.parts.length; l > a; a++) switch (i = parseInt(n[a], 10) || 1, t.parts[a]) {
                        case "dd":
                        case "d":
                            o = i, e.setDate(i);
                            break;
                        case "mm":
                        case "m":
                            r = i - 1, e.setMonth(i - 1);
                            break;
                        case "yy":
                            s = 2e3 + i, e.setFullYear(2e3 + i);
                            break;
                        case "yyyy":
                            s = i, e.setFullYear(i)
                    }
                    e = new Date(s, r, o, 0, 0, 0)
                }
                return e
            },
            formatDate: function(e, t) {
                var i = {
                    d: e.getDate(),
                    m: e.getMonth() + 1,
                    yy: e.getFullYear().toString().substring(2),
                    yyyy: e.getFullYear()
                };
                i.dd = (i.d < 10 ? "0" : "") + i.d, i.mm = (i.m < 10 ? "0" : "") + i.m;
                for (var e = [], n = 0, s = t.parts.length; s > n; n++) e.push(i[t.parts[n]]);
                return e.join(t.separator)
            },
            headTemplate: '<thead><tr><th class="prev">&lsaquo;</th><th colspan="5" class="switch"></th><th class="next">&rsaquo;</th></tr></thead>',
            contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
        };
        i.template = '<div class="datepicker dropdown-menu"><div class="datepicker-days"><table class=" table-condensed">' + i.headTemplate + '<tbody></tbody></table></div><div class="datepicker-months"><table class="table-condensed">' + i.headTemplate + i.contTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + i.headTemplate + i.contTemplate + "</table></div></div>"
    }(window.jQuery), function(e, t) {
        "use strict";
        var i = 5;
        e.fn.rating = function(n) {
            this.each(function() {
                var s = e(this),
                    o = e.extend({}, s.data(), n);
                o.start = parseInt(o.start, 10) || t, o.stop = parseInt(o.stop, 10) || o.start + i || t, o.step = parseInt(o.step, 10) || t, o = e.extend({}, e.fn.rating.defaults, o);
                for (var r = function(e) {
                        var t = h.children();
                        t.removeClass(o.filled).addClass(o.empty), t.eq(e).prevAll(".rating-symbol").addBack().removeClass(o.empty).addClass(o.filled)
                    }, a = function(e) {
                        return o.start + e * o.step
                    }, l = function(e) {
                        return Math.max(Math.ceil((e - o.start) / o.step), 0)
                    }, c = function(e) {
                        var t = o.step > 0 ? o.start : o.stop,
                            i = o.step > 0 ? o.stop - 1 : o.start + 1;
                        return e >= t && i >= e && (o.start + e) % o.step === 0
                    }, d = function(e) {
                        var t = parseInt(e, 10);
                        c(t) && r(l(t))
                    }, u = function(e) {
                        return function() {
                            s.prop("disabled") || s.prop("readonly") || e.call(this)
                        }
                    }, h = e("<div></div>").insertBefore(s), p = 0; p < l(o.stop); p++) h.append('<div class="rating-symbol ' + o.empty + '"></div>');
                d(s.val()), s.on("change", function() {
                    d(e(this).val())
                }), h.on("click", ".rating-symbol", u(function() {
                    s.val(a(e(this).index())).change()
                })).on("mouseenter", ".rating-symbol", u(function() {
                    r(e(this).index())
                })).on("mouseleave", ".rating-symbol", u(function() {
                    r(l(parseInt(s.val(), 10)))
                }))
            })
        }, e.fn.rating.defaults = {
            filled: "glyphicon glyphicon-star",
            empty: "glyphicon glyphicon-star-empty",
            start: 0,
            stop: i,
            step: 1
        }, e(function() {
            e("input.rating").rating()
        })
    }(jQuery), function(e) {
        "use strict";

        function t(e, t) {
            return e.toUpperCase().indexOf(t.toUpperCase()) > -1
        }

        function i(t) {
            var i = [{
                re: /[\xC0-\xC6]/g,
                ch: "A"
            }, {
                re: /[\xE0-\xE6]/g,
                ch: "a"
            }, {
                re: /[\xC8-\xCB]/g,
                ch: "E"
            }, {
                re: /[\xE8-\xEB]/g,
                ch: "e"
            }, {
                re: /[\xCC-\xCF]/g,
                ch: "I"
            }, {
                re: /[\xEC-\xEF]/g,
                ch: "i"
            }, {
                re: /[\xD2-\xD6]/g,
                ch: "O"
            }, {
                re: /[\xF2-\xF6]/g,
                ch: "o"
            }, {
                re: /[\xD9-\xDC]/g,
                ch: "U"
            }, {
                re: /[\xF9-\xFC]/g,
                ch: "u"
            }, {
                re: /[\xC7-\xE7]/g,
                ch: "c"
            }, {
                re: /[\xD1]/g,
                ch: "N"
            }, {
                re: /[\xF1]/g,
                ch: "n"
            }];
            return e.each(i, function() {
                t = t.replace(this.re, this.ch)
            }), t
        }

        function n(t, i) {
            var n = arguments,
                o = t,
                t = n[0],
                i = n[1];
            [].shift.apply(n), "undefined" == typeof t && (t = o);
            var r, a = this.each(function() {
                var o = e(this);
                if (o.is("select")) {
                    var a = o.data("selectpicker"),
                        l = "object" == typeof t && t;
                    if (a) {
                        if (l)
                            for (var c in l) l.hasOwnProperty(c) && (a.options[c] = l[c])
                    } else {
                        var d = e.extend({}, s.DEFAULTS, e.fn.selectpicker.defaults || {}, o.data(), l);
                        o.data("selectpicker", a = new s(this, d, i))
                    }
                    "string" == typeof t && (r = a[t] instanceof Function ? a[t].apply(a, n) : a.options[t])
                }
            });
            return "undefined" != typeof r ? r : a
        }
        e.expr[":"].icontains = function(i, n, s) {
            return t(e(i).text(), s[3])
        }, e.expr[":"].aicontains = function(i, n, s) {
            return t(e(i).data("normalizedText") || e(i).text(), s[3])
        };
        var s = function(t, i, n) {
            n && (n.stopPropagation(), n.preventDefault()), this.$element = e(t), this.$newElement = null, this.$button = null, this.$menu = null, this.$lis = null, this.options = i, null === this.options.title && (this.options.title = this.$element.attr("title")), this.val = s.prototype.val, this.render = s.prototype.render, this.refresh = s.prototype.refresh, this.setStyle = s.prototype.setStyle, this.selectAll = s.prototype.selectAll, this.deselectAll = s.prototype.deselectAll, this.destroy = s.prototype.remove, this.remove = s.prototype.remove, this.show = s.prototype.show, this.hide = s.prototype.hide, this.init()
        };
        s.VERSION = "1.6.2", s.DEFAULTS = {
            noneSelectedText: "Nothing selected",
            noneResultsText: "No results match",
            countSelectedText: function(e) {
                return 1 == e ? "{0} item selected" : "{0} items selected"
            },
            maxOptionsText: function(e, t) {
                var i = [];
                return i[0] = 1 == e ? "Limit reached ({n} item max)" : "Limit reached ({n} items max)", i[1] = 1 == t ? "Group limit reached ({n} item max)" : "Group limit reached ({n} items max)", i
            },
            selectAllText: "Select All",
            deselectAllText: "Deselect All",
            multipleSeparator: ", ",
            style: "btn-default",
            size: "auto",
            title: null,
            selectedTextFormat: "values",
            width: !1,
            container: !1,
            hideDisabled: !1,
            showSubtext: !1,
            showIcon: !0,
            showContent: !0,
            dropupAuto: !0,
            header: !1,
            liveSearch: !1,
            actionsBox: !1,
            iconBase: "glyphicon",
            tickIcon: "glyphicon-ok",
            maxOptions: !1,
            mobile: !1,
            selectOnTab: !1,
            dropdownAlignRight: !1,
            searchAccentInsensitive: !1
        }, s.prototype = {
            constructor: s,
            init: function() {
                var t = this,
                    i = this.$element.attr("id");
                this.$element.hide(), this.multiple = this.$element.prop("multiple"), this.autofocus = this.$element.prop("autofocus"), this.$newElement = this.createView(), this.$element.after(this.$newElement), this.$menu = this.$newElement.find("> .dropdown-menu"), this.$button = this.$newElement.find("> button"), this.$searchbox = this.$newElement.find("input"), this.options.dropdownAlignRight && this.$menu.addClass("dropdown-menu-right"), "undefined" != typeof i && (this.$button.attr("data-id", i), e('label[for="' + i + '"]').click(function(e) {
                    e.preventDefault(), t.$button.focus()
                })), this.checkDisabled(), this.clickListener(), this.options.liveSearch && this.liveSearchListener(), this.render(), this.liHeight(), this.setStyle(), this.setWidth(), this.options.container && this.selectPosition(), this.$menu.data("this", this), this.$newElement.data("this", this), this.options.mobile && this.mobile()
            },
            createDropdown: function() {
                var t = this.multiple ? " show-tick" : "",
                    i = this.$element.parent().hasClass("input-group") ? " input-group-btn" : "",
                    n = this.autofocus ? " autofocus" : "",
                    s = this.$element.parents().hasClass("form-group-lg") ? " btn-lg" : this.$element.parents().hasClass("form-group-sm") ? " btn-sm" : "",
                    o = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + "</div>" : "",
                    r = this.options.liveSearch ? '<div class="bs-searchbox"><input type="text" class="input-block-level form-control" autocomplete="off" /></div>' : "",
                    a = this.options.actionsBox ? '<div class="bs-actionsbox"><div class="btn-group btn-block"><button class="actions-btn bs-select-all btn btn-sm btn-default">' + this.options.selectAllText + '</button><button class="actions-btn bs-deselect-all btn btn-sm btn-default">' + this.options.deselectAllText + "</button></div></div>" : "",
                    l = '<div class="btn-group bootstrap-select' + t + i + '"><button type="button" class="btn dropdown-toggle selectpicker' + s + '" data-toggle="dropdown"' + n + '><span class="filter-option pull-left"></span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open">' + o + r + a + '<ul class="dropdown-menu inner selectpicker" role="menu"></ul></div></div>';
                return e(l)
            },
            createView: function() {
                var e = this.createDropdown(),
                    t = this.createLi();
                return e.find("ul").append(t), e
            },
            reloadLi: function() {
                this.destroyLi();
                var e = this.createLi();
                this.$menu.find("ul").append(e)
            },
            destroyLi: function() {
                this.$menu.find("li").remove()
            },
            createLi: function() {
                var t = this,
                    n = [],
                    s = 0,
                    o = function(e, t, i) {
                        return "<li" + ("undefined" != typeof i ? ' class="' + i + '"' : "") + ("undefined" != typeof t | null === t ? ' data-original-index="' + t + '"' : "") + ">" + e + "</li>"
                    },
                    r = function(n, s, o, r) {
                        var a = i(e.trim(e("<div/>").html(n).text()).replace(/\s\s+/g, " "));
                        return '<a tabindex="0"' + ("undefined" != typeof s ? ' class="' + s + '"' : "") + ("undefined" != typeof o ? ' style="' + o + '"' : "") + ("undefined" != typeof r ? 'data-optgroup="' + r + '"' : "") + ' data-normalized-text="' + a + '">' + n + '<span class="' + t.options.iconBase + " " + t.options.tickIcon + ' icon-ok check-mark"></span></a>'
                    };
                return this.$element.find("option").each(function() {
                    var i = e(this),
                        a = i.attr("class") || "",
                        l = i.attr("style"),
                        c = i.data("content") ? i.data("content") : i.html(),
                        d = "undefined" != typeof i.data("subtext") ? '<small class="muted text-muted">' + i.data("subtext") + "</small>" : "",
                        u = "undefined" != typeof i.data("icon") ? '<span class="' + t.options.iconBase + " " + i.data("icon") + '"></span> ' : "",
                        h = i.is(":disabled") || i.parent().is(":disabled"),
                        p = i[0].index;
                    if ("" !== u && h && (u = "<span>" + u + "</span>"), i.data("content") || (c = u + '<span class="text">' + c + d + "</span>"), !t.options.hideDisabled || !h)
                        if (i.parent().is("optgroup") && i.data("divider") !== !0) {
                            if (0 === i.index()) {
                                s += 1;
                                var f = i.parent().attr("label"),
                                    m = "undefined" != typeof i.parent().data("subtext") ? '<small class="muted text-muted">' + i.parent().data("subtext") + "</small>" : "",
                                    g = i.parent().data("icon") ? '<span class="' + t.options.iconBase + " " + i.parent().data("icon") + '"></span> ' : "";
                                f = g + '<span class="text">' + f + m + "</span>", 0 !== p && n.length > 0 && n.push(o("", null, "divider")), n.push(o(f, null, "dropdown-header"))
                            }
                            n.push(o(r(c, "opt " + a, l, s), p))
                        } else n.push(i.data("divider") === !0 ? o("", p, "divider") : i.data("hidden") === !0 ? o(r(c, a, l), p, "hide is-hidden") : o(r(c, a, l), p))
                }), this.multiple || 0 !== this.$element.find("option:selected").length || this.options.title || this.$element.find("option").eq(0).prop("selected", !0).attr("selected", "selected"), e(n.join(""))
            },
            findLis: function() {
                return null == this.$lis && (this.$lis = this.$menu.find("li")), this.$lis
            },
            render: function(t) {
                var i = this;
                t !== !1 && this.$element.find("option").each(function(t) {
                    i.setDisabled(t, e(this).is(":disabled") || e(this).parent().is(":disabled")), i.setSelected(t, e(this).is(":selected"))
                }), this.tabIndex();
                var n = this.options.hideDisabled ? ":not([disabled])" : "",
                    s = this.$element.find("option:selected" + n).map(function() {
                        var t, n = e(this),
                            s = n.data("icon") && i.options.showIcon ? '<i class="' + i.options.iconBase + " " + n.data("icon") + '"></i> ' : "";
                        return t = i.options.showSubtext && n.attr("data-subtext") && !i.multiple ? ' <small class="muted text-muted">' + n.data("subtext") + "</small>" : "", n.data("content") && i.options.showContent ? n.data("content") : "undefined" != typeof n.attr("title") ? n.attr("title") : s + n.html() + t
                    }).toArray(),
                    o = this.multiple ? s.join(this.options.multipleSeparator) : s[0];
                if (this.multiple && this.options.selectedTextFormat.indexOf("count") > -1) {
                    var r = this.options.selectedTextFormat.split(">");
                    if (r.length > 1 && s.length > r[1] || 1 == r.length && s.length >= 2) {
                        n = this.options.hideDisabled ? ", [disabled]" : "";
                        var a = this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]' + n).length,
                            l = "function" == typeof this.options.countSelectedText ? this.options.countSelectedText(s.length, a) : this.options.countSelectedText;
                        o = l.replace("{0}", s.length.toString()).replace("{1}", a.toString())
                    }
                }
                this.options.title = this.$element.attr("title"), "static" == this.options.selectedTextFormat && (o = this.options.title), o || (o = "undefined" != typeof this.options.title ? this.options.title : this.options.noneSelectedText), this.$button.attr("title", e.trim(e("<div/>").html(o).text()).replace(/\s\s+/g, " ")), this.$newElement.find(".filter-option").html(o)
            },
            setStyle: function(e, t) {
                this.$element.attr("class") && this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|validate\[.*\]/gi, ""));
                var i = e ? e : this.options.style;
                "add" == t ? this.$button.addClass(i) : "remove" == t ? this.$button.removeClass(i) : (this.$button.removeClass(this.options.style), this.$button.addClass(i))
            },
            liHeight: function() {
                if (this.options.size !== !1) {
                    var e = this.$menu.parent().clone().find("> .dropdown-toggle").prop("autofocus", !1).end().appendTo("body"),
                        t = e.addClass("open").find("> .dropdown-menu"),
                        i = t.find("li").not(".divider").not(".dropdown-header").filter(":visible").children("a").outerHeight(),
                        n = this.options.header ? t.find(".popover-title").outerHeight() : 0,
                        s = this.options.liveSearch ? t.find(".bs-searchbox").outerHeight() : 0,
                        o = this.options.actionsBox ? t.find(".bs-actionsbox").outerHeight() : 0;
                    e.remove(), this.$newElement.data("liHeight", i).data("headerHeight", n).data("searchHeight", s).data("actionsHeight", o)
                }
            },
            setSize: function() {
                this.findLis();
                var t, i, n, s = this,
                    o = this.$menu,
                    r = o.find(".inner"),
                    a = this.$newElement.outerHeight(),
                    l = this.$newElement.data("liHeight"),
                    c = this.$newElement.data("headerHeight"),
                    d = this.$newElement.data("searchHeight"),
                    u = this.$newElement.data("actionsHeight"),
                    h = this.$lis.filter(".divider").outerHeight(!0),
                    p = parseInt(o.css("padding-top")) + parseInt(o.css("padding-bottom")) + parseInt(o.css("border-top-width")) + parseInt(o.css("border-bottom-width")),
                    f = this.options.hideDisabled ? ", .disabled" : "",
                    m = e(window),
                    g = p + parseInt(o.css("margin-top")) + parseInt(o.css("margin-bottom")) + 2,
                    v = function() {
                        i = s.$newElement.offset().top - m.scrollTop(), n = m.height() - i - a
                    };
                if (v(), this.options.header && o.css("padding-top", 0), "auto" == this.options.size) {
                    var y = function() {
                        var e, a = s.$lis.not(".hide");
                        v(), t = n - g, s.options.dropupAuto && s.$newElement.toggleClass("dropup", i > n && t - g < o.height()), s.$newElement.hasClass("dropup") && (t = i - g), e = a.length + a.filter(".dropdown-header").length > 3 ? 3 * l + g - 2 : 0, o.css({
                            "max-height": t + "px",
                            overflow: "hidden",
                            "min-height": e + c + d + u + "px"
                        }), r.css({
                            "max-height": t - c - d - u - p + "px",
                            "overflow-y": "auto",
                            "min-height": Math.max(e - p, 0) + "px"
                        })
                    };
                    y(), this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize", y), e(window).off("resize.getSize").on("resize.getSize", y), e(window).off("scroll.getSize").on("scroll.getSize", y)
                } else if (this.options.size && "auto" != this.options.size && o.find("li" + f).length > this.options.size) {
                    var b = this.$lis.not(".divider" + f).find(" > *").slice(0, this.options.size).last().parent().index(),
                        w = this.$lis.slice(0, b + 1).filter(".divider").length;
                    t = l * this.options.size + w * h + p, s.options.dropupAuto && this.$newElement.toggleClass("dropup", i > n && t < o.height()), o.css({
                        "max-height": t + c + d + u + "px",
                        overflow: "hidden"
                    }), r.css({
                        "max-height": t - p + "px",
                        "overflow-y": "auto"
                    })
                }
            },
            setWidth: function() {
                if ("auto" == this.options.width) {
                    this.$menu.css("min-width", "0");
                    var e = this.$newElement.clone().appendTo("body"),
                        t = e.find("> .dropdown-menu").css("width"),
                        i = e.css("width", "auto").find("> button").css("width");
                    e.remove(), this.$newElement.css("width", Math.max(parseInt(t), parseInt(i)) + "px")
                } else "fit" == this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", "").addClass("fit-width")) : this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", this.options.width)) : (this.$menu.css("min-width", ""), this.$newElement.css("width", ""));
                this.$newElement.hasClass("fit-width") && "fit" !== this.options.width && this.$newElement.removeClass("fit-width")
            },
            selectPosition: function() {
                var t, i, n = this,
                    s = "<div />",
                    o = e(s),
                    r = function(e) {
                        o.addClass(e.attr("class").replace(/form-control/gi, "")).toggleClass("dropup", e.hasClass("dropup")), t = e.offset(), i = e.hasClass("dropup") ? 0 : e[0].offsetHeight, o.css({
                            top: t.top + i,
                            left: t.left,
                            width: e[0].offsetWidth,
                            position: "absolute"
                        })
                    };
                this.$newElement.on("click", function() {
                    n.isDisabled() || (r(e(this)), o.appendTo(n.options.container), o.toggleClass("open", !e(this).hasClass("open")), o.append(n.$menu))
                }), e(window).resize(function() {
                    r(n.$newElement)
                }), e(window).on("scroll", function() {
                    r(n.$newElement)
                }), e("html").on("click", function(t) {
                    e(t.target).closest(n.$newElement).length < 1 && o.removeClass("open")
                })
            },
            setSelected: function(e, t) {
                this.findLis(), this.$lis.filter('[data-original-index="' + e + '"]').toggleClass("selected", t)
            },
            setDisabled: function(e, t) {
                this.findLis(), t ? this.$lis.filter('[data-original-index="' + e + '"]').addClass("disabled").find("a").attr("href", "#").attr("tabindex", -1) : this.$lis.filter('[data-original-index="' + e + '"]').removeClass("disabled").find("a").removeAttr("href").attr("tabindex", 0)
            },
            isDisabled: function() {
                return this.$element.is(":disabled")
            },
            checkDisabled: function() {
                var e = this;
                this.isDisabled() ? this.$button.addClass("disabled").attr("tabindex", -1) : (this.$button.hasClass("disabled") && this.$button.removeClass("disabled"), -1 == this.$button.attr("tabindex") && (this.$element.data("tabindex") || this.$button.removeAttr("tabindex"))), this.$button.click(function() {
                    return !e.isDisabled()
                })
            },
            tabIndex: function() {
                this.$element.is("[tabindex]") && (this.$element.data("tabindex", this.$element.attr("tabindex")), this.$button.attr("tabindex", this.$element.data("tabindex")))
            },
            clickListener: function() {
                var t = this;
                this.$newElement.on("touchstart.dropdown", ".dropdown-menu", function(e) {
                    e.stopPropagation()
                }), this.$newElement.on("click", function() {
                    t.setSize(), t.options.liveSearch || t.multiple || setTimeout(function() {
                        t.$menu.find(".selected a").focus()
                    }, 10)
                }), this.$menu.on("click", "li a", function(i) {
                    var n = e(this),
                        s = n.parent().data("originalIndex"),
                        o = t.$element.val(),
                        r = t.$element.prop("selectedIndex");
                    if (t.multiple && i.stopPropagation(), i.preventDefault(), !t.isDisabled() && !n.parent().hasClass("disabled")) {
                        var a = t.$element.find("option"),
                            l = a.eq(s),
                            c = l.prop("selected"),
                            d = l.parent("optgroup"),
                            u = t.options.maxOptions,
                            h = d.data("maxOptions") || !1;
                        if (t.multiple) {
                            if (l.prop("selected", !c), t.setSelected(s, !c), n.blur(), u !== !1 || h !== !1) {
                                var p = u < a.filter(":selected").length,
                                    f = h < d.find("option:selected").length;
                                if (u && p || h && f)
                                    if (u && 1 == u) a.prop("selected", !1), l.prop("selected", !0), t.$menu.find(".selected").removeClass("selected"), t.setSelected(s, !0);
                                    else if (h && 1 == h) {
                                    d.find("option:selected").prop("selected", !1), l.prop("selected", !0);
                                    var m = n.data("optgroup");
                                    t.$menu.find(".selected").has('a[data-optgroup="' + m + '"]').removeClass("selected"), t.setSelected(s, !0)
                                } else {
                                    var g = "function" == typeof t.options.maxOptionsText ? t.options.maxOptionsText(u, h) : t.options.maxOptionsText,
                                        v = g[0].replace("{n}", u),
                                        y = g[1].replace("{n}", h),
                                        b = e('<div class="notify"></div>');
                                    g[2] && (v = v.replace("{var}", g[2][u > 1 ? 0 : 1]), y = y.replace("{var}", g[2][h > 1 ? 0 : 1])), l.prop("selected", !1), t.$menu.append(b), u && p && (b.append(e("<div>" + v + "</div>")), t.$element.trigger("maxReached.bs.select")), h && f && (b.append(e("<div>" + y + "</div>")), t.$element.trigger("maxReachedGrp.bs.select")), setTimeout(function() {
                                        t.setSelected(s, !1)
                                    }, 10), b.delay(750).fadeOut(300, function() {
                                        e(this).remove()
                                    })
                                }
                            }
                        } else a.prop("selected", !1), l.prop("selected", !0), t.$menu.find(".selected").removeClass("selected"), t.setSelected(s, !0);
                        t.multiple ? t.options.liveSearch && t.$searchbox.focus() : t.$button.focus(), (o != t.$element.val() && t.multiple || r != t.$element.prop("selectedIndex") && !t.multiple) && t.$element.change()
                    }
                }), this.$menu.on("click", "li.disabled a, .popover-title, .popover-title :not(.close)", function(e) {
                    e.target == this && (e.preventDefault(), e.stopPropagation(), t.options.liveSearch ? t.$searchbox.focus() : t.$button.focus())
                }), this.$menu.on("click", "li.divider, li.dropdown-header", function(e) {
                    e.preventDefault(), e.stopPropagation(), t.options.liveSearch ? t.$searchbox.focus() : t.$button.focus()
                }), this.$menu.on("click", ".popover-title .close", function() {
                    t.$button.focus()
                }), this.$searchbox.on("click", function(e) {
                    e.stopPropagation()
                }), this.$menu.on("click", ".actions-btn", function(i) {
                    t.options.liveSearch ? t.$searchbox.focus() : t.$button.focus(), i.preventDefault(), i.stopPropagation(), e(this).is(".bs-select-all") ? t.selectAll() : t.deselectAll(), t.$element.change()
                }), this.$element.change(function() {
                    t.render(!1)
                })
            },
            liveSearchListener: function() {
                var t = this,
                    n = e('<li class="no-results"></li>');
                this.$newElement.on("click.dropdown.data-api", function() {
                    t.$menu.find(".active").removeClass("active"), t.$searchbox.val() && (t.$searchbox.val(""), t.$lis.not(".is-hidden").removeClass("hide"), n.parent().length && n.remove()), t.multiple || t.$menu.find(".selected").addClass("active"), setTimeout(function() {
                        t.$searchbox.focus()
                    }, 10)
                }), this.$searchbox.on("input propertychange", function() {
                    t.$searchbox.val() ? (t.options.searchAccentInsensitive ? t.$lis.not(".is-hidden").removeClass("hide").find("a").not(":aicontains(" + i(t.$searchbox.val()) + ")").parent().addClass("hide") : t.$lis.not(".is-hidden").removeClass("hide").find("a").not(":icontains(" + t.$searchbox.val() + ")").parent().addClass("hide"), t.$menu.find("li").filter(":visible:not(.no-results)").length ? n.parent().length && n.remove() : (n.parent().length && n.remove(), n.html(t.options.noneResultsText + ' "' + t.$searchbox.val() + '"').show(), t.$menu.find("li").last().after(n))) : (t.$lis.not(".is-hidden").removeClass("hide"), n.parent().length && n.remove()), t.$menu.find("li.active").removeClass("active"), t.$menu.find("li").filter(":visible:not(.divider)").eq(0).addClass("active").find("a").focus(), e(this).focus()
                }), this.$menu.on("mouseenter", "a", function(i) {
                    t.$menu.find(".active").removeClass("active"), e(i.currentTarget).parent().not(".disabled").addClass("active")
                }), this.$menu.on("mouseleave", "a", function() {
                    t.$menu.find(".active").removeClass("active")
                })
            },
            val: function(e) {
                return "undefined" != typeof e ? (this.$element.val(e), this.render(), this.$element) : this.$element.val()
            },
            selectAll: function() {
                this.findLis(), this.$lis.not(".divider").not(".disabled").not(".selected").filter(":visible").find("a").click()
            },
            deselectAll: function() {
                this.findLis(), this.$lis.not(".divider").not(".disabled").filter(".selected").filter(":visible").find("a").click()
            },
            keydown: function(t) {
                var n, s, o, r, a, l, c, d, u, h, p, f, m = {
                    32: " ",
                    48: "0",
                    49: "1",
                    50: "2",
                    51: "3",
                    52: "4",
                    53: "5",
                    54: "6",
                    55: "7",
                    56: "8",
                    57: "9",
                    59: ";",
                    65: "a",
                    66: "b",
                    67: "c",
                    68: "d",
                    69: "e",
                    70: "f",
                    71: "g",
                    72: "h",
                    73: "i",
                    74: "j",
                    75: "k",
                    76: "l",
                    77: "m",
                    78: "n",
                    79: "o",
                    80: "p",
                    81: "q",
                    82: "r",
                    83: "s",
                    84: "t",
                    85: "u",
                    86: "v",
                    87: "w",
                    88: "x",
                    89: "y",
                    90: "z",
                    96: "0",
                    97: "1",
                    98: "2",
                    99: "3",
                    100: "4",
                    101: "5",
                    102: "6",
                    103: "7",
                    104: "8",
                    105: "9"
                };
                if (n = e(this), o = n.parent(), n.is("input") && (o = n.parent().parent()), h = o.data("this"), h.options.liveSearch && (o = n.parent().parent()), h.options.container && (o = h.$menu), s = e("[role=menu] li a", o), f = h.$menu.parent().hasClass("open"), !f && /([0-9]|[A-z])/.test(String.fromCharCode(t.keyCode)) && (h.options.container ? h.$newElement.trigger("click") : (h.setSize(), h.$menu.parent().addClass("open"), f = !0), h.$searchbox.focus()), h.options.liveSearch && (/(^9$|27)/.test(t.keyCode.toString(10)) && f && 0 === h.$menu.find(".active").length && (t.preventDefault(), h.$menu.parent().removeClass("open"), h.$button.focus()), s = e("[role=menu] li:not(.divider):not(.dropdown-header):visible", o), n.val() || /(38|40)/.test(t.keyCode.toString(10)) || 0 === s.filter(".active").length && (s = h.$newElement.find("li").filter(h.options.searchAccentInsensitive ? ":aicontains(" + i(m[t.keyCode]) + ")" : ":icontains(" + m[t.keyCode] + ")"))), s.length) {
                    if (/(38|40)/.test(t.keyCode.toString(10))) r = s.index(s.filter(":focus")), l = s.parent(":not(.disabled):visible").first().index(), c = s.parent(":not(.disabled):visible").last().index(), a = s.eq(r).parent().nextAll(":not(.disabled):visible").eq(0).index(), d = s.eq(r).parent().prevAll(":not(.disabled):visible").eq(0).index(), u = s.eq(a).parent().prevAll(":not(.disabled):visible").eq(0).index(), h.options.liveSearch && (s.each(function(t) {
                        e(this).is(":not(.disabled)") && e(this).data("index", t)
                    }), r = s.index(s.filter(".active")), l = s.filter(":not(.disabled):visible").first().data("index"), c = s.filter(":not(.disabled):visible").last().data("index"), a = s.eq(r).nextAll(":not(.disabled):visible").eq(0).data("index"), d = s.eq(r).prevAll(":not(.disabled):visible").eq(0).data("index"), u = s.eq(a).prevAll(":not(.disabled):visible").eq(0).data("index")), p = n.data("prevIndex"), 38 == t.keyCode && (h.options.liveSearch && (r -= 1), r != u && r > d && (r = d), l > r && (r = l), r == p && (r = c)), 40 == t.keyCode && (h.options.liveSearch && (r += 1), -1 == r && (r = 0), r != u && a > r && (r = a), r > c && (r = c), r == p && (r = l)), n.data("prevIndex", r), h.options.liveSearch ? (t.preventDefault(), n.is(".dropdown-toggle") || (s.removeClass("active"), s.eq(r).addClass("active").find("a").focus(), n.focus())) : s.eq(r).focus();
                    else if (!n.is("input")) {
                        var g, v, y = [];
                        s.each(function() {
                            e(this).parent().is(":not(.disabled)") && e.trim(e(this).text().toLowerCase()).substring(0, 1) == m[t.keyCode] && y.push(e(this).parent().index())
                        }), g = e(document).data("keycount"), g++, e(document).data("keycount", g), v = e.trim(e(":focus").text().toLowerCase()).substring(0, 1), v != m[t.keyCode] ? (g = 1, e(document).data("keycount", g)) : g >= y.length && (e(document).data("keycount", 0), g > y.length && (g = 1)), s.eq(y[g - 1]).focus()
                    }(/(13|32)/.test(t.keyCode.toString(10)) || /(^9$)/.test(t.keyCode.toString(10)) && h.options.selectOnTab) && f && (/(32)/.test(t.keyCode.toString(10)) || t.preventDefault(), h.options.liveSearch ? /(32)/.test(t.keyCode.toString(10)) || (h.$menu.find(".active a").click(), n.focus()) : e(":focus").click(), e(document).data("keycount", 0)), (/(^9$|27)/.test(t.keyCode.toString(10)) && f && (h.multiple || h.options.liveSearch) || /(27)/.test(t.keyCode.toString(10)) && !f) && (h.$menu.parent().removeClass("open"), h.$button.focus())
                }
            },
            mobile: function() {
                this.$element.addClass("mobile-device").appendTo(this.$newElement), this.options.container && this.$menu.hide()
            },
            refresh: function() {
                this.$lis = null, this.reloadLi(), this.render(), this.setWidth(), this.setStyle(), this.checkDisabled(), this.liHeight()
            },
            update: function() {
                this.reloadLi(), this.setWidth(), this.setStyle(), this.checkDisabled(), this.liHeight()
            },
            hide: function() {
                this.$newElement.hide()
            },
            show: function() {
                this.$newElement.show()
            },
            remove: function() {
                this.$newElement.remove(), this.$element.remove()
            }
        };
        var o = e.fn.selectpicker;
        e.fn.selectpicker = n, e.fn.selectpicker.Constructor = s, e.fn.selectpicker.noConflict = function() {
            return e.fn.selectpicker = o, this
        }, e(document).data("keycount", 0).on("keydown", ".bootstrap-select [data-toggle=dropdown], .bootstrap-select [role=menu], .bs-searchbox input", s.prototype.keydown).on("focusin.modal", ".bootstrap-select [data-toggle=dropdown], .bootstrap-select [role=menu], .bs-searchbox input", function(e) {
            e.stopPropagation()
        }), e(window).on("load.bs.select.data-api", function() {
            e(".selectpicker").each(function() {
                var t = e(this);
                n.call(t, t.data())
            })
        })
    }(jQuery), function(e, t, i) {
        "use strict";
        var n = function(t, i) {
            this.widget = "", this.$element = e(t), this.defaultTime = i.defaultTime, this.disableFocus = i.disableFocus, this.disableMousewheel = i.disableMousewheel, this.isOpen = i.isOpen, this.minuteStep = i.minuteStep, this.modalBackdrop = i.modalBackdrop, this.orientation = i.orientation, this.secondStep = i.secondStep, this.showInputs = i.showInputs, this.showMeridian = i.showMeridian, this.showSeconds = i.showSeconds, this.template = i.template, this.appendWidgetTo = i.appendWidgetTo, this.showWidgetOnAddonClick = i.showWidgetOnAddonClick, this._init()
        };
        n.prototype = {
            constructor: n,
            _init: function() {
                var t = this;
                this.showWidgetOnAddonClick && (this.$element.parent().hasClass("input-append") || this.$element.parent().hasClass("input-prepend")) ? (this.$element.parent(".input-append, .input-prepend").find(".add-on").on({
                    "click.timepicker": e.proxy(this.showWidget, this)
                }), this.$element.on({
                    "focus.timepicker": e.proxy(this.highlightUnit, this),
                    "click.timepicker": e.proxy(this.highlightUnit, this),
                    "keydown.timepicker": e.proxy(this.elementKeydown, this),
                    "blur.timepicker": e.proxy(this.blurElement, this),
                    "mousewheel.timepicker DOMMouseScroll.timepicker": e.proxy(this.mousewheel, this)
                })) : this.$element.on(this.template ? {
                    "focus.timepicker": e.proxy(this.showWidget, this),
                    "click.timepicker": e.proxy(this.showWidget, this),
                    "blur.timepicker": e.proxy(this.blurElement, this),
                    "mousewheel.timepicker DOMMouseScroll.timepicker": e.proxy(this.mousewheel, this)
                } : {
                    "focus.timepicker": e.proxy(this.highlightUnit, this),
                    "click.timepicker": e.proxy(this.highlightUnit, this),
                    "keydown.timepicker": e.proxy(this.elementKeydown, this),
                    "blur.timepicker": e.proxy(this.blurElement, this),
                    "mousewheel.timepicker DOMMouseScroll.timepicker": e.proxy(this.mousewheel, this)
                }), this.$widget = this.template !== !1 ? e(this.getTemplate()).on("click", e.proxy(this.widgetClick, this)) : !1, this.showInputs && this.$widget !== !1 && this.$widget.find("input").each(function() {
                    e(this).on({
                        "click.timepicker": function() {
                            e(this).select()
                        },
                        "keydown.timepicker": e.proxy(t.widgetKeydown, t),
                        "keyup.timepicker": e.proxy(t.widgetKeyup, t)
                    })
                }), this.setDefaultTime(this.defaultTime)
            },
            blurElement: function() {
                this.highlightedUnit = null, this.updateFromElementVal()
            },
            clear: function() {
                this.hour = "", this.minute = "", this.second = "", this.meridian = "", this.$element.val("")
            },
            decrementHour: function() {
                if (this.showMeridian)
                    if (1 === this.hour) this.hour = 12;
                    else {
                        if (12 === this.hour) return this.hour--, this.toggleMeridian();
                        if (0 === this.hour) return this.hour = 11, this.toggleMeridian();
                        this.hour--
                    }
                else this.hour <= 0 ? this.hour = 23 : this.hour--
            },
            decrementMinute: function(e) {
                var t;
                t = e ? this.minute - e : this.minute - this.minuteStep, 0 > t ? (this.decrementHour(), this.minute = t + 60) : this.minute = t
            },
            decrementSecond: function() {
                var e = this.second - this.secondStep;
                0 > e ? (this.decrementMinute(!0), this.second = e + 60) : this.second = e
            },
            elementKeydown: function(e) {
                switch (e.keyCode) {
                    case 9:
                    case 27:
                        this.updateFromElementVal();
                        break;
                    case 37:
                        e.preventDefault(), this.highlightPrevUnit();
                        break;
                    case 38:
                        switch (e.preventDefault(), this.highlightedUnit) {
                            case "hour":
                                this.incrementHour(), this.highlightHour();
                                break;
                            case "minute":
                                this.incrementMinute(), this.highlightMinute();
                                break;
                            case "second":
                                this.incrementSecond(), this.highlightSecond();
                                break;
                            case "meridian":
                                this.toggleMeridian(), this.highlightMeridian()
                        }
                        this.update();
                        break;
                    case 39:
                        e.preventDefault(), this.highlightNextUnit();
                        break;
                    case 40:
                        switch (e.preventDefault(), this.highlightedUnit) {
                            case "hour":
                                this.decrementHour(), this.highlightHour();
                                break;
                            case "minute":
                                this.decrementMinute(), this.highlightMinute();
                                break;
                            case "second":
                                this.decrementSecond(), this.highlightSecond();
                                break;
                            case "meridian":
                                this.toggleMeridian(), this.highlightMeridian()
                        }
                        this.update()
                }
            },
            getCursorPosition: function() {
                var e = this.$element.get(0);
                if ("selectionStart" in e) return e.selectionStart;
                if (i.selection) {
                    e.focus();
                    var t = i.selection.createRange(),
                        n = i.selection.createRange().text.length;
                    return t.moveStart("character", -e.value.length), t.text.length - n
                }
            },
            getTemplate: function() {
                var e, t, i, n, s, o;
                switch (this.showInputs ? (t = '<input type="text" class="bootstrap-timepicker-hour" maxlength="2"/>', i = '<input type="text" class="bootstrap-timepicker-minute" maxlength="2"/>', n = '<input type="text" class="bootstrap-timepicker-second" maxlength="2"/>', s = '<input type="text" class="bootstrap-timepicker-meridian" maxlength="2"/>') : (t = '<span class="bootstrap-timepicker-hour"></span>', i = '<span class="bootstrap-timepicker-minute"></span>', n = '<span class="bootstrap-timepicker-second"></span>', s = '<span class="bootstrap-timepicker-meridian"></span>'), o = '<table><tr><td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="fa fa-chevron-up"></i></a></td>' : "") + "</tr><tr><td>" + t + '</td> <td class="separator">:</td><td>' + i + "</td> " + (this.showSeconds ? '<td class="separator">:</td><td>' + n + "</td>" : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td>' + s + "</td>" : "") + '</tr><tr><td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td>' + (this.showSeconds ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td>' : "") + (this.showMeridian ? '<td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="fa fa-chevron-down"></i></a></td>' : "") + "</tr></table>", this.template) {
                    case "modal":
                        e = '<div class="bootstrap-timepicker-widget modal hide fade in" data-backdrop="' + (this.modalBackdrop ? "true" : "false") + '"><div class="modal-header"><a href="#" class="close" data-dismiss="modal">\xd7</a><h3>Pick a Time</h3></div><div class="modal-content">' + o + '</div><div class="modal-footer"><a href="#" class="btn btn-primary" data-dismiss="modal">OK</a></div></div>';
                        break;
                    case "dropdown":
                        e = '<div class="bootstrap-timepicker-widget dropdown-menu">' + o + "</div>"
                }
                return e
            },
            getTime: function() {
                return "" === this.hour ? "" : this.hour + ":" + (1 === this.minute.toString().length ? "0" + this.minute : this.minute) + (this.showSeconds ? ":" + (1 === this.second.toString().length ? "0" + this.second : this.second) : "") + (this.showMeridian ? " " + this.meridian : "")
            },
            hideWidget: function() {
                this.isOpen !== !1 && (this.$element.trigger({
                    type: "hide.timepicker",
                    time: {
                        value: this.getTime(),
                        hours: this.hour,
                        minutes: this.minute,
                        seconds: this.second,
                        meridian: this.meridian
                    }
                }), "modal" === this.template && this.$widget.modal ? this.$widget.modal("hide") : this.$widget.removeClass("open"), e(i).off("mousedown.timepicker, touchend.timepicker"), this.isOpen = !1, this.$widget.detach())
            },
            highlightUnit: function() {
                this.position = this.getCursorPosition(), this.position >= 0 && this.position <= 2 ? this.highlightHour() : this.position >= 3 && this.position <= 5 ? this.highlightMinute() : this.position >= 6 && this.position <= 8 ? this.showSeconds ? this.highlightSecond() : this.highlightMeridian() : this.position >= 9 && this.position <= 11 && this.highlightMeridian()
            },
            highlightNextUnit: function() {
                switch (this.highlightedUnit) {
                    case "hour":
                        this.highlightMinute();
                        break;
                    case "minute":
                        this.showSeconds ? this.highlightSecond() : this.showMeridian ? this.highlightMeridian() : this.highlightHour();
                        break;
                    case "second":
                        this.showMeridian ? this.highlightMeridian() : this.highlightHour();
                        break;
                    case "meridian":
                        this.highlightHour()
                }
            },
            highlightPrevUnit: function() {
                switch (this.highlightedUnit) {
                    case "hour":
                        this.showMeridian ? this.highlightMeridian() : this.showSeconds ? this.highlightSecond() : this.highlightMinute();
                        break;
                    case "minute":
                        this.highlightHour();
                        break;
                    case "second":
                        this.highlightMinute();
                        break;
                    case "meridian":
                        this.showSeconds ? this.highlightSecond() : this.highlightMinute()
                }
            },
            highlightHour: function() {
                var e = this.$element.get(0),
                    t = this;
                this.highlightedUnit = "hour", e.setSelectionRange && setTimeout(function() {
                    t.hour < 10 ? e.setSelectionRange(0, 1) : e.setSelectionRange(0, 2)
                }, 0)
            },
            highlightMinute: function() {
                var e = this.$element.get(0),
                    t = this;
                this.highlightedUnit = "minute", e.setSelectionRange && setTimeout(function() {
                    t.hour < 10 ? e.setSelectionRange(2, 4) : e.setSelectionRange(3, 5)
                }, 0)
            },
            highlightSecond: function() {
                var e = this.$element.get(0),
                    t = this;
                this.highlightedUnit = "second", e.setSelectionRange && setTimeout(function() {
                    t.hour < 10 ? e.setSelectionRange(5, 7) : e.setSelectionRange(6, 8)
                }, 0)
            },
            highlightMeridian: function() {
                var e = this.$element.get(0),
                    t = this;
                this.highlightedUnit = "meridian", e.setSelectionRange && (this.showSeconds ? setTimeout(function() {
                    t.hour < 10 ? e.setSelectionRange(8, 10) : e.setSelectionRange(9, 11)
                }, 0) : setTimeout(function() {
                    t.hour < 10 ? e.setSelectionRange(5, 7) : e.setSelectionRange(6, 8)
                }, 0))
            },
            incrementHour: function() {
                if (this.showMeridian) {
                    if (11 === this.hour) return this.hour++, this.toggleMeridian();
                    12 === this.hour && (this.hour = 0)
                }
                return 23 === this.hour ? void(this.hour = 0) : void this.hour++
            },
            incrementMinute: function(e) {
                var t;
                t = e ? this.minute + e : this.minute + this.minuteStep - this.minute % this.minuteStep, t > 59 ? (this.incrementHour(), this.minute = t - 60) : this.minute = t
            },
            incrementSecond: function() {
                var e = this.second + this.secondStep - this.second % this.secondStep;
                e > 59 ? (this.incrementMinute(!0), this.second = e - 60) : this.second = e
            },
            mousewheel: function(t) {
                if (!this.disableMousewheel) {
                    t.preventDefault(), t.stopPropagation();
                    var i = t.originalEvent.wheelDelta || -t.originalEvent.detail,
                        n = null;
                    switch ("mousewheel" === t.type ? n = -1 * t.originalEvent.wheelDelta : "DOMMouseScroll" === t.type && (n = 40 * t.originalEvent.detail), n && (t.preventDefault(), e(this).scrollTop(n + e(this).scrollTop())), this.highlightedUnit) {
                        case "minute":
                            i > 0 ? this.incrementMinute() : this.decrementMinute(), this.highlightMinute();
                            break;
                        case "second":
                            i > 0 ? this.incrementSecond() : this.decrementSecond(), this.highlightSecond();
                            break;
                        case "meridian":
                            this.toggleMeridian(), this.highlightMeridian();
                            break;
                        default:
                            i > 0 ? this.incrementHour() : this.decrementHour(), this.highlightHour()
                    }
                    return !1
                }
            },
            place: function() {
                if (!this.isInline) {
                    var i = this.$widget.outerWidth(),
                        n = this.$widget.outerHeight(),
                        s = 10,
                        o = e(t).width(),
                        r = e(t).height(),
                        a = e(t).scrollTop(),
                        l = parseInt(this.$element.parents().filter(function() {}).first().css("z-index"), 10) + 10,
                        c = this.component ? this.component.parent().offset() : this.$element.offset(),
                        d = this.component ? this.component.outerHeight(!0) : this.$element.outerHeight(!1),
                        u = this.component ? this.component.outerWidth(!0) : this.$element.outerWidth(!1),
                        h = c.left,
                        p = c.top;
                    this.$widget.removeClass("timepicker-orient-top timepicker-orient-bottom timepicker-orient-right timepicker-orient-left"), "auto" !== this.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.orientation.x), "right" === this.orientation.x && (h -= i - u)) : (this.$widget.addClass("timepicker-orient-left"), c.left < 0 ? h -= c.left - s : c.left + i > o && (h = o - i - s));
                    var f, m, g = this.orientation.y;
                    "auto" === g && (f = -a + c.top - n, m = a + r - (c.top + d + n), g = Math.max(f, m) === m ? "top" : "bottom"), this.$widget.addClass("timepicker-orient-" + g), "top" === g ? p += d : p -= n + parseInt(this.$widget.css("padding-top"), 10), this.$widget.css({
                        top: p,
                        left: h,
                        zIndex: l
                    })
                }
            },
            remove: function() {
                e("document").off(".timepicker"), this.$widget && this.$widget.remove(), delete this.$element.data().timepicker
            },
            setDefaultTime: function(e) {
                if (this.$element.val()) this.updateFromElementVal();
                else if ("current" === e) {
                    var t = new Date,
                        i = t.getHours(),
                        n = t.getMinutes(),
                        s = t.getSeconds(),
                        o = "AM";
                    0 !== s && (s = Math.ceil(t.getSeconds() / this.secondStep) * this.secondStep, 60 === s && (n += 1, s = 0)), 0 !== n && (n = Math.ceil(t.getMinutes() / this.minuteStep) * this.minuteStep, 60 === n && (i += 1, n = 0)), this.showMeridian && (0 === i ? i = 12 : i >= 12 ? (i > 12 && (i -= 12), o = "PM") : o = "AM"), this.hour = i, this.minute = n, this.second = s, this.meridian = o, this.update()
                } else e === !1 ? (this.hour = 0, this.minute = 0, this.second = 0, this.meridian = "AM") : this.setTime(e)
            },
            setTime: function(e, t) {
                if (!e) return void this.clear();
                var i, n, s, o, r;
                "object" == typeof e && e.getMonth ? (n = e.getHours(), s = e.getMinutes(), o = e.getSeconds(), this.showMeridian && (r = "AM", n > 12 && (r = "PM", n %= 12), 12 === n && (r = "PM"))) : (r = null !== e.match(/p/i) ? "PM" : "AM", e = e.replace(/[^0-9\:]/g, ""), i = e.split(":"), n = i[0] ? i[0].toString() : i.toString(), s = i[1] ? i[1].toString() : "", o = i[2] ? i[2].toString() : "", n.length > 4 && (o = n.substr(4, 2)), n.length > 2 && (s = n.substr(2, 2), n = n.substr(0, 2)), s.length > 2 && (o = s.substr(2, 2), s = s.substr(0, 2)), o.length > 2 && (o = o.substr(2, 2)), n = parseInt(n, 10), s = parseInt(s, 10), o = parseInt(o, 10), isNaN(n) && (n = 0), isNaN(s) && (s = 0), isNaN(o) && (o = 0), this.showMeridian ? 1 > n ? n = 1 : n > 12 && (n = 12) : (n >= 24 ? n = 23 : 0 > n && (n = 0), 13 > n && "PM" === r && (n += 12)), 0 > s ? s = 0 : s >= 60 && (s = 59), this.showSeconds && (isNaN(o) ? o = 0 : 0 > o ? o = 0 : o >= 60 && (o = 59))), this.hour = n, this.minute = s, this.second = o, this.meridian = r, this.update(t)
            },
            showWidget: function() {
                if (!this.isOpen && !this.$element.is(":disabled")) {
                    this.$widget.appendTo(this.appendWidgetTo);
                    var t = this;
                    e(i).on("mousedown.timepicker, touchend.timepicker", function(e) {
                        t.$element.parent().find(e.target).length || t.$widget.is(e.target) || t.$widget.find(e.target).length || t.hideWidget()
                    }), this.$element.trigger({
                        type: "show.timepicker",
                        time: {
                            value: this.getTime(),
                            hours: this.hour,
                            minutes: this.minute,
                            seconds: this.second,
                            meridian: this.meridian
                        }
                    }), this.place(), this.disableFocus && this.$element.blur(), "" === this.hour && (this.defaultTime ? this.setDefaultTime(this.defaultTime) : this.setTime("0:0:0")), "modal" === this.template && this.$widget.modal ? this.$widget.modal("show").on("hidden", e.proxy(this.hideWidget, this)) : this.isOpen === !1 && this.$widget.addClass("open"), this.isOpen = !0
                }
            },
            toggleMeridian: function() {
                this.meridian = "AM" === this.meridian ? "PM" : "AM"
            },
            update: function(e) {
                this.updateElement(), e || this.updateWidget(), this.$element.trigger({
                    type: "changeTime.timepicker",
                    time: {
                        value: this.getTime(),
                        hours: this.hour,
                        minutes: this.minute,
                        seconds: this.second,
                        meridian: this.meridian
                    }
                })
            },
            updateElement: function() {
                this.$element.val(this.getTime()).change()
            },
            updateFromElementVal: function() {
                this.setTime(this.$element.val())
            },
            updateWidget: function() {
                if (this.$widget !== !1) {
                    var e = this.hour,
                        t = 1 === this.minute.toString().length ? "0" + this.minute : this.minute,
                        i = 1 === this.second.toString().length ? "0" + this.second : this.second;
                    this.showInputs ? (this.$widget.find("input.bootstrap-timepicker-hour").val(e), this.$widget.find("input.bootstrap-timepicker-minute").val(t), this.showSeconds && this.$widget.find("input.bootstrap-timepicker-second").val(i), this.showMeridian && this.$widget.find("input.bootstrap-timepicker-meridian").val(this.meridian)) : (this.$widget.find("span.bootstrap-timepicker-hour").text(e), this.$widget.find("span.bootstrap-timepicker-minute").text(t), this.showSeconds && this.$widget.find("span.bootstrap-timepicker-second").text(i), this.showMeridian && this.$widget.find("span.bootstrap-timepicker-meridian").text(this.meridian))
                }
            },
            updateFromWidgetInputs: function() {
                if (this.$widget !== !1) {
                    var e = this.$widget.find("input.bootstrap-timepicker-hour").val() + ":" + this.$widget.find("input.bootstrap-timepicker-minute").val() + (this.showSeconds ? ":" + this.$widget.find("input.bootstrap-timepicker-second").val() : "") + (this.showMeridian ? this.$widget.find("input.bootstrap-timepicker-meridian").val() : "");
                    this.setTime(e, !0)
                }
            },
            widgetClick: function(t) {
                t.stopPropagation(), t.preventDefault();
                var i = e(t.target),
                    n = i.closest("a").data("action");
                n && this[n](), this.update(), i.is("input") && i.get(0).setSelectionRange(0, 2)
            },
            widgetKeydown: function(t) {
                var i = e(t.target),
                    n = i.attr("class").replace("bootstrap-timepicker-", "");
                switch (t.keyCode) {
                    case 9:
                        if (this.showMeridian && "meridian" === n || this.showSeconds && "second" === n || !this.showMeridian && !this.showSeconds && "minute" === n) return this.hideWidget();
                        break;
                    case 27:
                        this.hideWidget();
                        break;
                    case 38:
                        switch (t.preventDefault(), n) {
                            case "hour":
                                this.incrementHour();
                                break;
                            case "minute":
                                this.incrementMinute();
                                break;
                            case "second":
                                this.incrementSecond();
                                break;
                            case "meridian":
                                this.toggleMeridian()
                        }
                        this.setTime(this.getTime()), i.get(0).setSelectionRange(0, 2);
                        break;
                    case 40:
                        switch (t.preventDefault(), n) {
                            case "hour":
                                this.decrementHour();
                                break;
                            case "minute":
                                this.decrementMinute();
                                break;
                            case "second":
                                this.decrementSecond();
                                break;
                            case "meridian":
                                this.toggleMeridian()
                        }
                        this.setTime(this.getTime()), i.get(0).setSelectionRange(0, 2)
                }
            },
            widgetKeyup: function(e) {
                (65 === e.keyCode || 77 === e.keyCode || 80 === e.keyCode || 46 === e.keyCode || 8 === e.keyCode || e.keyCode >= 46 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105) && this.updateFromWidgetInputs()
            }
        }, e.fn.timepicker = function(t) {
            var i = Array.apply(null, arguments);
            return i.shift(), this.each(function() {
                var s = e(this),
                    o = s.data("timepicker"),
                    r = "object" == typeof t && t;
                o || s.data("timepicker", o = new n(this, e.extend({}, e.fn.timepicker.defaults, r, e(this).data()))), "string" == typeof t && o[t].apply(o, i)
            })
        }, e.fn.timepicker.defaults = {
            defaultTime: "current",
            disableFocus: !1,
            disableMousewheel: !1,
            isOpen: !1,
            minuteStep: 15,
            modalBackdrop: !1,
            orientation: {
                x: "auto",
                y: "auto"
            },
            secondStep: 15,
            showSeconds: !1,
            showInputs: !0,
            showMeridian: !0,
            template: "dropdown",
            appendWidgetTo: "body",
            showWidgetOnAddonClick: !0
        }, e.fn.timepicker.Constructor = n
    }(jQuery, window, document), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var i = e(this),
                s = i.data("bs.alert");
            s || i.data("bs.alert", s = new n(this)), "string" == typeof t && s[t].call(i)
        })
    }
    var i = '[data-dismiss="alert"]',
        n = function(t) {
            e(t).on("click", i, this.close)
        };
    n.VERSION = "3.2.0", n.prototype.close = function(t) {
        function i() {
            o.detach().trigger("closed.bs.alert").remove()
        }
        var n = e(this),
            s = n.attr("data-target");
        s || (s = n.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, ""));
        var o = e(s);
        t && t.preventDefault(), o.length || (o = n.hasClass("alert") ? n : n.parent()), o.trigger(t = e.Event("close.bs.alert")), t.isDefaultPrevented() || (o.removeClass("in"), e.support.transition && o.hasClass("fade") ? o.one("bsTransitionEnd", i).emulateTransitionEnd(150) : i())
    };
    var s = e.fn.alert;
    e.fn.alert = t, e.fn.alert.Constructor = n, e.fn.alert.noConflict = function() {
        return e.fn.alert = s, this
    }, e(document).on("click.bs.alert.data-api", i, n.prototype.close)
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.button"),
                o = "object" == typeof t && t;
            s || n.data("bs.button", s = new i(this, o)), "toggle" == t ? s.toggle() : t && s.setState(t)
        })
    }
    var i = function(t, n) {
        this.$element = e(t), this.options = e.extend({}, i.DEFAULTS, n), this.isLoading = !1
    };
    i.VERSION = "3.2.0", i.DEFAULTS = {
        loadingText: "loading..."
    }, i.prototype.setState = function(t) {
        var i = "disabled",
            n = this.$element,
            s = n.is("input") ? "val" : "html",
            o = n.data();
        t += "Text", null == o.resetText && n.data("resetText", n[s]()), n[s](null == o[t] ? this.options[t] : o[t]), setTimeout(e.proxy(function() {
            "loadingText" == t ? (this.isLoading = !0, n.addClass(i).attr(i, i)) : this.isLoading && (this.isLoading = !1, n.removeClass(i).removeAttr(i))
        }, this), 0)
    }, i.prototype.toggle = function() {
        var e = !0,
            t = this.$element.closest('[data-toggle="buttons"]');
        if (t.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") && (i.prop("checked") && this.$element.hasClass("active") ? e = !1 : t.find(".active").removeClass("active")), e && i.prop("checked", !this.$element.hasClass("active")).trigger("change")
        }
        e && this.$element.toggleClass("active")
    };
    var n = e.fn.button;
    e.fn.button = t, e.fn.button.Constructor = i, e.fn.button.noConflict = function() {
        return e.fn.button = n, this
    }, e(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(i) {
        var n = e(i.target);
        n.hasClass("btn") || (n = n.closest(".btn")), t.call(n, "toggle"), i.preventDefault()
    })
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        t && 3 === t.which || (e(s).remove(), e(o).each(function() {
            var n = i(e(this)),
                s = {
                    relatedTarget: this
                };
            n.hasClass("open") && (n.trigger(t = e.Event("hide.bs.dropdown", s)), t.isDefaultPrevented() || n.removeClass("open").trigger("hidden.bs.dropdown", s))
        }))
    }

    function i(t) {
        var i = t.attr("data-target");
        i || (i = t.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i && e(i);
        return n && n.length ? n : t.parent()
    }

    function n(t) {
        return this.each(function() {
            var i = e(this),
                n = i.data("bs.dropdown");
            n || i.data("bs.dropdown", n = new r(this)), "string" == typeof t && n[t].call(i)
        })
    }
    var s = ".dropdown-backdrop",
        o = '[data-toggle="dropdown"]',
        r = function(t) {
            e(t).on("click.bs.dropdown", this.toggle)
        };
    r.VERSION = "3.2.0", r.prototype.toggle = function(n) {
        var s = e(this);
        if (!s.is(".disabled, :disabled")) {
            var o = i(s),
                r = o.hasClass("open");
            if (t(), !r) {
                "ontouchstart" in document.documentElement && !o.closest(".navbar-nav").length && e('<div class="dropdown-backdrop"/>').insertAfter(e(this)).on("click", t);
                var a = {
                    relatedTarget: this
                };
                if (o.trigger(n = e.Event("show.bs.dropdown", a)), n.isDefaultPrevented()) return;
                s.trigger("focus"), o.toggleClass("open").trigger("shown.bs.dropdown", a)
            }
            return !1
        }
    }, r.prototype.keydown = function(t) {
        if (/(38|40|27)/.test(t.keyCode)) {
            var n = e(this);
            if (t.preventDefault(), t.stopPropagation(), !n.is(".disabled, :disabled")) {
                var s = i(n),
                    r = s.hasClass("open");
                if (!r || r && 27 == t.keyCode) return 27 == t.which && s.find(o).trigger("focus"), n.trigger("click");
                var a = " li:not(.divider):visible a",
                    l = s.find('[role="menu"]' + a + ', [role="listbox"]' + a);
                if (l.length) {
                    var c = l.index(l.filter(":focus"));
                    38 == t.keyCode && c > 0 && c--, 40 == t.keyCode && c < l.length - 1 && c++, ~c || (c = 0), l.eq(c).trigger("focus")
                }
            }
        }
    };
    var a = e.fn.dropdown;
    e.fn.dropdown = n, e.fn.dropdown.Constructor = r, e.fn.dropdown.noConflict = function() {
        return e.fn.dropdown = a, this
    }, e(document).on("click.bs.dropdown.data-api", t).on("click.bs.dropdown.data-api", ".dropdown form", function(e) {
        e.stopPropagation()
    }).on("click.bs.dropdown.data-api", o, r.prototype.toggle).on("keydown.bs.dropdown.data-api", o + ', [role="menu"], [role="listbox"]', r.prototype.keydown)
}(jQuery), + function(e) {
    "use strict";

    function t(t, n) {
        return this.each(function() {
            var s = e(this),
                o = s.data("bs.modal"),
                r = e.extend({}, i.DEFAULTS, s.data(), "object" == typeof t && t);
            o || s.data("bs.modal", o = new i(this, r)), "string" == typeof t ? o[t](n) : r.show && o.show(n)
        })
    }
    var i = function(t, i) {
        this.options = i, this.$body = e(document.body), this.$element = e(t), this.$backdrop = this.isShown = null, this.scrollbarWidth = 0, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, e.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.2.0", i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function(e) {
        return this.isShown ? this.hide() : this.show(e)
    }, i.prototype.show = function(t) {
        var i = this,
            n = e.Event("show.bs.modal", {
                relatedTarget: t
            });
        this.$element.trigger(n), this.isShown || n.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.$body.addClass("modal-open"), this.setScrollbar(), this.escape(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', e.proxy(this.hide, this)), this.backdrop(function() {
            var n = e.support.transition && i.$element.hasClass("fade");
            i.$element.parent().length || i.$element.appendTo(i.$body), i.$element.show().scrollTop(0), n && i.$element[0].offsetWidth, i.$element.addClass("in").attr("aria-hidden", !1), i.enforceFocus();
            var s = e.Event("shown.bs.modal", {
                relatedTarget: t
            });
            n ? i.$element.find(".modal-dialog").one("bsTransitionEnd", function() {
                i.$element.trigger("focus").trigger(s)
            }).emulateTransitionEnd(300) : i.$element.trigger("focus").trigger(s)
        }))
    }, i.prototype.hide = function(t) {
        t && t.preventDefault(), t = e.Event("hide.bs.modal"), this.$element.trigger(t), this.isShown && !t.isDefaultPrevented() && (this.isShown = !1, this.$body.removeClass("modal-open"), this.resetScrollbar(), this.escape(), e(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.bs.modal"), e.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", e.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal())
    }, i.prototype.enforceFocus = function() {
        e(document).off("focusin.bs.modal").on("focusin.bs.modal", e.proxy(function(e) {
            this.$element[0] === e.target || this.$element.has(e.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.bs.modal", e.proxy(function(e) {
            27 == e.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keyup.dismiss.bs.modal")
    }, i.prototype.hideModal = function() {
        var e = this;
        this.$element.hide(), this.backdrop(function() {
            e.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function(t) {
        var i = this,
            n = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var s = e.support.transition && n;
            if (this.$backdrop = e('<div class="modal-backdrop ' + n + '" />').appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", e.proxy(function(e) {
                    e.target === e.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this))
                }, this)), s && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !t) return;
            s ? this.$backdrop.one("bsTransitionEnd", t).emulateTransitionEnd(150) : t()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var o = function() {
                i.removeBackdrop(), t && t()
            };
            e.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", o).emulateTransitionEnd(150) : o()
        } else t && t()
    }, i.prototype.checkScrollbar = function() {
        document.body.clientWidth >= window.innerWidth || (this.scrollbarWidth = this.scrollbarWidth || this.measureScrollbar())
    }, i.prototype.setScrollbar = function() {
        var e = parseInt(this.$body.css("padding-right") || 0, 10);
        this.scrollbarWidth && this.$body.css("padding-right", e + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", "")
    }, i.prototype.measureScrollbar = function() {
        var e = document.createElement("div");
        e.className = "modal-scrollbar-measure", this.$body.append(e);
        var t = e.offsetWidth - e.clientWidth;
        return this.$body[0].removeChild(e), t
    };
    var n = e.fn.modal;
    e.fn.modal = t, e.fn.modal.Constructor = i, e.fn.modal.noConflict = function() {
        return e.fn.modal = n, this
    }, e(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(i) {
        var n = e(this),
            s = n.attr("href"),
            o = e(n.attr("data-target") || s && s.replace(/.*(?=#[^\s]+$)/, "")),
            r = o.data("bs.modal") ? "toggle" : e.extend({
                remote: !/#/.test(s) && s
            }, o.data(), n.data());
        n.is("a") && i.preventDefault(), o.one("show.bs.modal", function(e) {
            e.isDefaultPrevented() || o.one("hidden.bs.modal", function() {
                n.is(":visible") && n.trigger("focus")
            })
        }), t.call(o, r, this)
    })
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.tooltip"),
                o = "object" == typeof t && t;
            (s || "destroy" != t) && (s || n.data("bs.tooltip", s = new i(this, o)), "string" == typeof t && s[t]())
        })
    }
    var i = function(e, t) {
        this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, this.init("tooltip", e, t)
    };
    i.VERSION = "3.2.0", i.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, i.prototype.init = function(t, i, n) {
        this.enabled = !0, this.type = t, this.$element = e(i), this.options = this.getOptions(n), this.$viewport = this.options.viewport && e(this.options.viewport.selector || this.options.viewport);
        for (var s = this.options.trigger.split(" "), o = s.length; o--;) {
            var r = s[o];
            if ("click" == r) this.$element.on("click." + this.type, this.options.selector, e.proxy(this.toggle, this));
            else if ("manual" != r) {
                var a = "hover" == r ? "mouseenter" : "focusin",
                    l = "hover" == r ? "mouseleave" : "focusout";
                this.$element.on(a + "." + this.type, this.options.selector, e.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, e.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = e.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.getOptions = function(t) {
        return t = e.extend({}, this.getDefaults(), this.$element.data(), t), t.delay && "number" == typeof t.delay && (t.delay = {
            show: t.delay,
            hide: t.delay
        }), t
    }, i.prototype.getDelegateOptions = function() {
        var t = {},
            i = this.getDefaults();
        return this._options && e.each(this._options, function(e, n) {
            i[e] != n && (t[e] = n)
        }), t
    }, i.prototype.enter = function(t) {
        var i = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, i)), clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function() {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show()
    }, i.prototype.leave = function(t) {
        var i = t instanceof this.constructor ? t : e(t.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, i)), clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function() {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide()
    }, i.prototype.show = function() {
        var t = e.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(t);
            var i = e.contains(document.documentElement, this.$element[0]);
            if (t.isDefaultPrevented() || !i) return;
            var n = this,
                s = this.tip(),
                o = this.getUID(this.type);
            this.setContent(), s.attr("id", o), this.$element.attr("aria-describedby", o), this.options.animation && s.addClass("fade");
            var r = "function" == typeof this.options.placement ? this.options.placement.call(this, s[0], this.$element[0]) : this.options.placement,
                a = /\s?auto?\s?/i,
                l = a.test(r);
            l && (r = r.replace(a, "") || "top"), s.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(r).data("bs." + this.type, this), this.options.container ? s.appendTo(this.options.container) : s.insertAfter(this.$element);
            var c = this.getPosition(),
                d = s[0].offsetWidth,
                u = s[0].offsetHeight;
            if (l) {
                var h = r,
                    p = this.$element.parent(),
                    f = this.getPosition(p);
                r = "bottom" == r && c.top + c.height + u - f.scroll > f.height ? "top" : "top" == r && c.top - f.scroll - u < 0 ? "bottom" : "right" == r && c.right + d > f.width ? "left" : "left" == r && c.left - d < f.left ? "right" : r, s.removeClass(h).addClass(r)
            }
            var m = this.getCalculatedOffset(r, c, d, u);
            this.applyPlacement(m, r);
            var g = function() {
                n.$element.trigger("shown.bs." + n.type), n.hoverState = null
            };
            e.support.transition && this.$tip.hasClass("fade") ? s.one("bsTransitionEnd", g).emulateTransitionEnd(150) : g()
        }
    }, i.prototype.applyPlacement = function(t, i) {
        var n = this.tip(),
            s = n[0].offsetWidth,
            o = n[0].offsetHeight,
            r = parseInt(n.css("margin-top"), 10),
            a = parseInt(n.css("margin-left"), 10);
        isNaN(r) && (r = 0), isNaN(a) && (a = 0), t.top = t.top + r, t.left = t.left + a, e.offset.setOffset(n[0], e.extend({
            using: function(e) {
                n.css({
                    top: Math.round(e.top),
                    left: Math.round(e.left)
                })
            }
        }, t), 0), n.addClass("in");
        var l = n[0].offsetWidth,
            c = n[0].offsetHeight;
        "top" == i && c != o && (t.top = t.top + o - c);
        var d = this.getViewportAdjustedDelta(i, t, l, c);
        d.left ? t.left += d.left : t.top += d.top;
        var u = d.left ? 2 * d.left - s + l : 2 * d.top - o + c,
            h = d.left ? "left" : "top",
            p = d.left ? "offsetWidth" : "offsetHeight";
        n.offset(t), this.replaceArrow(u, n[0][p], h)
    }, i.prototype.replaceArrow = function(e, t, i) {
        this.arrow().css(i, e ? 50 * (1 - e / t) + "%" : "")
    }, i.prototype.setContent = function() {
        var e = this.tip(),
            t = this.getTitle();
        e.find(".tooltip-inner")[this.options.html ? "html" : "text"](t), e.removeClass("fade in top bottom left right")
    }, i.prototype.hide = function() {
        function t() {
            "in" != i.hoverState && n.detach(), i.$element.trigger("hidden.bs." + i.type)
        }
        var i = this,
            n = this.tip(),
            s = e.Event("hide.bs." + this.type);
        return this.$element.removeAttr("aria-describedby"), this.$element.trigger(s), s.isDefaultPrevented() ? void 0 : (n.removeClass("in"), e.support.transition && this.$tip.hasClass("fade") ? n.one("bsTransitionEnd", t).emulateTransitionEnd(150) : t(), this.hoverState = null, this)
    }, i.prototype.fixTitle = function() {
        var e = this.$element;
        (e.attr("title") || "string" != typeof e.attr("data-original-title")) && e.attr("data-original-title", e.attr("title") || "").attr("title", "")
    }, i.prototype.hasContent = function() {
        return this.getTitle()
    }, i.prototype.getPosition = function(t) {
        t = t || this.$element;
        var i = t[0],
            n = "BODY" == i.tagName;
        return e.extend({}, "function" == typeof i.getBoundingClientRect ? i.getBoundingClientRect() : null, {
            scroll: n ? document.documentElement.scrollTop || document.body.scrollTop : t.scrollTop(),
            width: n ? e(window).width() : t.outerWidth(),
            height: n ? e(window).height() : t.outerHeight()
        }, n ? {
            top: 0,
            left: 0
        } : t.offset())
    }, i.prototype.getCalculatedOffset = function(e, t, i, n) {
        return "bottom" == e ? {
            top: t.top + t.height,
            left: t.left + t.width / 2 - i / 2
        } : "top" == e ? {
            top: t.top - n,
            left: t.left + t.width / 2 - i / 2
        } : "left" == e ? {
            top: t.top + t.height / 2 - n / 2,
            left: t.left - i
        } : {
            top: t.top + t.height / 2 - n / 2,
            left: t.left + t.width
        }
    }, i.prototype.getViewportAdjustedDelta = function(e, t, i, n) {
        var s = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return s;
        var o = this.options.viewport && this.options.viewport.padding || 0,
            r = this.getPosition(this.$viewport);
        if (/right|left/.test(e)) {
            var a = t.top - o - r.scroll,
                l = t.top + o - r.scroll + n;
            a < r.top ? s.top = r.top - a : l > r.top + r.height && (s.top = r.top + r.height - l)
        } else {
            var c = t.left - o,
                d = t.left + o + i;
            c < r.left ? s.left = r.left - c : d > r.width && (s.left = r.left + r.width - d)
        }
        return s
    }, i.prototype.getTitle = function() {
        var e, t = this.$element,
            i = this.options;
        return e = t.attr("data-original-title") || ("function" == typeof i.title ? i.title.call(t[0]) : i.title)
    }, i.prototype.getUID = function(e) {
        do e += ~~(1e6 * Math.random()); while (document.getElementById(e));
        return e
    }, i.prototype.tip = function() {
        return this.$tip = this.$tip || e(this.options.template)
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, i.prototype.validate = function() {
        this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
    }, i.prototype.enable = function() {
        this.enabled = !0
    }, i.prototype.disable = function() {
        this.enabled = !1
    }, i.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, i.prototype.toggle = function(t) {
        var i = this;
        t && (i = e(t.currentTarget).data("bs." + this.type), i || (i = new this.constructor(t.currentTarget, this.getDelegateOptions()), e(t.currentTarget).data("bs." + this.type, i))), i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, i.prototype.destroy = function() {
        clearTimeout(this.timeout), this.hide().$element.off("." + this.type).removeData("bs." + this.type)
    };
    var n = e.fn.tooltip;
    e.fn.tooltip = t, e.fn.tooltip.Constructor = i, e.fn.tooltip.noConflict = function() {
        return e.fn.tooltip = n, this
    }
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.popover"),
                o = "object" == typeof t && t;
            (s || "destroy" != t) && (s || n.data("bs.popover", s = new i(this, o)), "string" == typeof t && s[t]())
        })
    }
    var i = function(e, t) {
        this.init("popover", e, t)
    };
    if (!e.fn.tooltip) throw new Error("Popover requires tooltip.js");
    i.VERSION = "3.2.0", i.DEFAULTS = e.extend({}, e.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), i.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype), i.prototype.constructor = i, i.prototype.getDefaults = function() {
        return i.DEFAULTS
    }, i.prototype.setContent = function() {
        var e = this.tip(),
            t = this.getTitle(),
            i = this.getContent();
        e.find(".popover-title")[this.options.html ? "html" : "text"](t), e.find(".popover-content").empty()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), e.removeClass("fade top bottom left right in"), e.find(".popover-title").html() || e.find(".popover-title").hide()
    }, i.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, i.prototype.getContent = function() {
        var e = this.$element,
            t = this.options;
        return e.attr("data-content") || ("function" == typeof t.content ? t.content.call(e[0]) : t.content)
    }, i.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    }, i.prototype.tip = function() {
        return this.$tip || (this.$tip = e(this.options.template)), this.$tip
    };
    var n = e.fn.popover;
    e.fn.popover = t, e.fn.popover.Constructor = i, e.fn.popover.noConflict = function() {
        return e.fn.popover = n, this
    }
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.tab");
            s || n.data("bs.tab", s = new i(this)), "string" == typeof t && s[t]()
        })
    }
    var i = function(t) {
        this.element = e(t)
    };
    i.VERSION = "3.2.0", i.prototype.show = function() {
        var t = this.element,
            i = t.closest("ul:not(.dropdown-menu)"),
            n = t.data("target");
        if (n || (n = t.attr("href"), n = n && n.replace(/.*(?=#[^\s]*$)/, "")), !t.parent("li").hasClass("active")) {
            var s = i.find(".active:last a")[0],
                o = e.Event("show.bs.tab", {
                    relatedTarget: s
                });
            if (t.trigger(o), !o.isDefaultPrevented()) {
                var r = e(n);
                this.activate(t.closest("li"), i), this.activate(r, r.parent(), function() {
                    t.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: s
                    })
                })
            }
        }
    }, i.prototype.activate = function(t, i, n) {
        function s() {
            o.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"), t.addClass("active"), r ? (t[0].offsetWidth, t.addClass("in")) : t.removeClass("fade"), t.parent(".dropdown-menu") && t.closest("li.dropdown").addClass("active"), n && n()
        }
        var o = i.find("> .active"),
            r = n && e.support.transition && o.hasClass("fade");
        r ? o.one("bsTransitionEnd", s).emulateTransitionEnd(150) : s(), o.removeClass("in")
    };
    var n = e.fn.tab;
    e.fn.tab = t, e.fn.tab.Constructor = i, e.fn.tab.noConflict = function() {
        return e.fn.tab = n, this
    }, e(document).on("click.bs.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"]', function(i) {
        i.preventDefault(), t.call(e(this), "show")
    })
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.affix"),
                o = "object" == typeof t && t;
            s || n.data("bs.affix", s = new i(this, o)), "string" == typeof t && s[t]()
        })
    }
    var i = function(t, n) {
        this.options = e.extend({}, i.DEFAULTS, n), this.$target = e(this.options.target).on("scroll.bs.affix.data-api", e.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", e.proxy(this.checkPositionWithEventLoop, this)), this.$element = e(t), this.affixed = this.unpin = this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.2.0", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var e = this.$target.scrollTop(),
            t = this.$element.offset();
        return this.pinnedOffset = t.top - e
    }, i.prototype.checkPositionWithEventLoop = function() {
        setTimeout(e.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var t = e(document).height(),
                n = this.$target.scrollTop(),
                s = this.$element.offset(),
                o = this.options.offset,
                r = o.top,
                a = o.bottom;
            "object" != typeof o && (a = r = o), "function" == typeof r && (r = o.top(this.$element)), "function" == typeof a && (a = o.bottom(this.$element));
            var l = null != this.unpin && n + this.unpin <= s.top ? !1 : null != a && s.top + this.$element.height() >= t - a ? "bottom" : null != r && r >= n ? "top" : !1;
            if (this.affixed !== l) {
                null != this.unpin && this.$element.css("top", "");
                var c = "affix" + (l ? "-" + l : ""),
                    d = e.Event(c + ".bs.affix");
                this.$element.trigger(d), d.isDefaultPrevented() || (this.affixed = l, this.unpin = "bottom" == l ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(c).trigger(e.Event(c.replace("affix", "affixed"))), "bottom" == l && this.$element.offset({
                    top: t - this.$element.height() - a
                }))
            }
        }
    };
    var n = e.fn.affix;
    e.fn.affix = t, e.fn.affix.Constructor = i, e.fn.affix.noConflict = function() {
        return e.fn.affix = n, this
    }, e(window).on("load", function() {
        e('[data-spy="affix"]').each(function() {
            var i = e(this),
                n = i.data();
            n.offset = n.offset || {}, n.offsetBottom && (n.offset.bottom = n.offsetBottom), n.offsetTop && (n.offset.top = n.offsetTop), t.call(i, n)
        })
    })
}(jQuery), + function(e) {
    "use strict";

    function t(t) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.collapse"),
                o = e.extend({}, i.DEFAULTS, n.data(), "object" == typeof t && t);
            !s && o.toggle && "show" == t && (t = !t), s || n.data("bs.collapse", s = new i(this, o)), "string" == typeof t && s[t]()
        })
    }
    var i = function(t, n) {
        this.$element = e(t), this.options = e.extend({}, i.DEFAULTS, n), this.transitioning = null, this.options.parent && (this.$parent = e(this.options.parent)), this.options.toggle && this.toggle()
    };
    i.VERSION = "3.2.0", i.DEFAULTS = {
        toggle: !0
    }, i.prototype.dimension = function() {
        var e = this.$element.hasClass("width");
        return e ? "width" : "height"
    }, i.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var i = e.Event("show.bs.collapse");
            if (this.$element.trigger(i), !i.isDefaultPrevented()) {
                var n = this.$parent && this.$parent.find("> .panel > .in");
                if (n && n.length) {
                    var s = n.data("bs.collapse");
                    if (s && s.transitioning) return;
                    t.call(n, "hide"), s || n.data("bs.collapse", null)
                }
                var o = this.dimension();
                this.$element.removeClass("collapse").addClass("collapsing")[o](0), this.transitioning = 1;
                var r = function() {
                    this.$element.removeClass("collapsing").addClass("collapse in")[o](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                };
                if (!e.support.transition) return r.call(this);
                var a = e.camelCase(["scroll", o].join("-"));
                this.$element.one("bsTransitionEnd", e.proxy(r, this)).emulateTransitionEnd(350)[o](this.$element[0][a])
            }
        }
    }, i.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var t = e.Event("hide.bs.collapse");
            if (this.$element.trigger(t), !t.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"), this.transitioning = 1;
                var n = function() {
                    this.transitioning = 0, this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")
                };
                return e.support.transition ? void this.$element[i](0).one("bsTransitionEnd", e.proxy(n, this)).emulateTransitionEnd(350) : n.call(this)
            }
        }
    }, i.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    };
    var n = e.fn.collapse;
    e.fn.collapse = t, e.fn.collapse.Constructor = i, e.fn.collapse.noConflict = function() {
        return e.fn.collapse = n, this
    }, e(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(i) {
        var n, s = e(this),
            o = s.attr("data-target") || i.preventDefault() || (n = s.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""),
            r = e(o),
            a = r.data("bs.collapse"),
            l = a ? "toggle" : s.data(),
            c = s.attr("data-parent"),
            d = c && e(c);
        a && a.transitioning || (d && d.find('[data-toggle="collapse"][data-parent="' + c + '"]').not(s).addClass("collapsed"), s[r.hasClass("in") ? "addClass" : "removeClass"]("collapsed")), t.call(r, l)
    })
}(jQuery), + function(e) {
    "use strict";

    function t() {
        var e = document.createElement("bootstrap"),
            t = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var i in t)
            if (void 0 !== e.style[i]) return {
                end: t[i]
            };
        return !1
    }
    e.fn.emulateTransitionEnd = function(t) {
        var i = !1,
            n = this;
        e(this).one("bsTransitionEnd", function() {
            i = !0
        });
        var s = function() {
            i || e(n).trigger(e.support.transition.end)
        };
        return setTimeout(s, t), this
    }, e(function() {
        e.support.transition = t(), e.support.transition && (e.event.special.bsTransitionEnd = {
            bindType: e.support.transition.end,
            delegateType: e.support.transition.end,
            handle: function(t) {
                return e(t.target).is(this) ? t.handleObj.handler.apply(this, arguments) : void 0
            }
        })
    })
}(jQuery), Date.parseFunctions = {
        count: 0
    }, Date.parseRegexes = [], Date.formatFunctions = {
        count: 0
    }, Date.prototype.dateFormat = function(e) {
        if ("unixtime" == e) return parseInt(this.getTime() / 1e3);
        null == Date.formatFunctions[e] && Date.createNewFormat(e);
        var t = Date.formatFunctions[e];
        return this[t]()
    }, Date.createNewFormat = function(format) {
        var funcName = "format" + Date.formatFunctions.count++;
        Date.formatFunctions[format] = funcName;
        for (var code = "Date.prototype." + funcName + " = function() {return ", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, code += "'" + String.escape(ch) + "' + ") : code += Date.getFormatCode(ch) : special = !0;
        eval(code.substring(0, code.length - 3) + ";}")
    }, Date.getFormatCode = function(e) {
        switch (e) {
            case "d":
                return "String.leftPad(this.getDate(), 2, '0') + ";
            case "D":
                return "Date.dayNames[this.getDay()].substring(0, 3) + ";
            case "j":
                return "this.getDate() + ";
            case "l":
                return "Date.dayNames[this.getDay()] + ";
            case "S":
                return "this.getSuffix() + ";
            case "w":
                return "this.getDay() + ";
            case "z":
                return "this.getDayOfYear() + ";
            case "W":
                return "this.getWeekOfYear() + ";
            case "F":
                return "Date.monthNames[this.getMonth()] + ";
            case "m":
                return "String.leftPad(this.getMonth() + 1, 2, '0') + ";
            case "M":
                return "Date.monthNames[this.getMonth()].substring(0, 3) + ";
            case "n":
                return "(this.getMonth() + 1) + ";
            case "t":
                return "this.getDaysInMonth() + ";
            case "L":
                return "(this.isLeapYear() ? 1 : 0) + ";
            case "Y":
                return "this.getFullYear() + ";
            case "y":
                return "('' + this.getFullYear()).substring(2, 4) + ";
            case "a":
                return "(this.getHours() < 12 ? 'am' : 'pm') + ";
            case "A":
                return "(this.getHours() < 12 ? 'AM' : 'PM') + ";
            case "g":
                return "((this.getHours() %12) ? this.getHours() % 12 : 12) + ";
            case "G":
                return "this.getHours() + ";
            case "h":
                return "String.leftPad((this.getHours() %12) ? this.getHours() % 12 : 12, 2, '0') + ";
            case "H":
                return "String.leftPad(this.getHours(), 2, '0') + ";
            case "i":
                return "String.leftPad(this.getMinutes(), 2, '0') + ";
            case "s":
                return "String.leftPad(this.getSeconds(), 2, '0') + ";
            case "O":
                return "this.getGMTOffset() + ";
            case "T":
                return "this.getTimezone() + ";
            case "Z":
                return "(this.getTimezoneOffset() * -60) + ";
            default:
                return "'" + String.escape(e) + "' + "
        }
    }, Date.parseDate = function(e, t) {
        if ("unixtime" == t) return new Date(isNaN(parseInt(e)) ? 0 : 1e3 * parseInt(e));
        null == Date.parseFunctions[t] && Date.createParser(t);
        var i = Date.parseFunctions[t];
        return Date[i](e)
    }, Date.createParser = function(format) {
        var funcName = "parse" + Date.parseFunctions.count++,
            regexNum = Date.parseRegexes.length,
            currentGroup = 1;
        Date.parseFunctions[format] = funcName;
        for (var code = "Date." + funcName + " = function(input) {\nvar y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, z = -1;\nvar d = new Date();\ny = d.getFullYear();\nm = d.getMonth();\nd = d.getDate();\nvar results = input.match(Date.parseRegexes[" + regexNum + "]);\nif (results && results.length > 0) {", regex = "", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, regex += String.escape(ch)) : (obj = Date.formatCodeToRegex(ch, currentGroup), currentGroup += obj.g, regex += obj.s, obj.g && obj.c && (code += obj.c)) : special = !0;
        code += "if (y > 0 && z > 0){\nvar doyDate = new Date(y,0);\ndoyDate.setDate(z);\nm = doyDate.getMonth();\nd = doyDate.getDate();\n}", code += "if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0)\n{return new Date(y, m, d, h, i, s);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0)\n{return new Date(y, m, d, h, i);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0)\n{return new Date(y, m, d, h);}\nelse if (y > 0 && m >= 0 && d > 0)\n{return new Date(y, m, d);}\nelse if (y > 0 && m >= 0)\n{return new Date(y, m);}\nelse if (y > 0)\n{return new Date(y);}\n}return null;}", Date.parseRegexes[regexNum] = new RegExp("^" + regex + "$"), eval(code)
    }, Date.formatCodeToRegex = function(e, t) {
        switch (e) {
            case "D":
                return {
                    g: 0,
                    c: null,
                    s: "(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)"
                };
            case "j":
            case "d":
                return {
                    g: 1,
                    c: "d = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,2})"
                };
            case "l":
                return {
                    g: 0,
                    c: null,
                    s: "(?:" + Date.dayNames.join("|") + ")"
                };
            case "S":
                return {
                    g: 0,
                    c: null,
                    s: "(?:st|nd|rd|th)"
                };
            case "w":
                return {
                    g: 0,
                    c: null,
                    s: "\\d"
                };
            case "z":
                return {
                    g: 1,
                    c: "z = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,3})"
                };
            case "W":
                return {
                    g: 0,
                    c: null,
                    s: "(?:\\d{2})"
                };
            case "F":
                return {
                    g: 1,
                    c: "m = parseInt(Date.monthNumbers[results[" + t + "].substring(0, 3)], 10);\n",
                    s: "(" + Date.monthNames.join("|") + ")"
                };
            case "M":
                return {
                    g: 1,
                    c: "m = parseInt(Date.monthNumbers[results[" + t + "]], 10);\n",
                    s: "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"
                };
            case "n":
            case "m":
                return {
                    g: 1,
                    c: "m = parseInt(results[" + t + "], 10) - 1;\n",
                    s: "(\\d{1,2})"
                };
            case "t":
                return {
                    g: 0,
                    c: null,
                    s: "\\d{1,2}"
                };
            case "L":
                return {
                    g: 0,
                    c: null,
                    s: "(?:1|0)"
                };
            case "Y":
                return {
                    g: 1,
                    c: "y = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{4})"
                };
            case "y":
                return {
                    g: 1,
                    c: "var ty = parseInt(results[" + t + "], 10);\ny = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n",
                    s: "(\\d{1,2})"
                };
            case "a":
                return {
                    g: 1,
                    c: "if (results[" + t + "] == 'am') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
                    s: "(am|pm)"
                };
            case "A":
                return {
                    g: 1,
                    c: "if (results[" + t + "] == 'AM') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
                    s: "(AM|PM)"
                };
            case "g":
            case "G":
            case "h":
            case "H":
                return {
                    g: 1,
                    c: "h = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,2})"
                };
            case "i":
                return {
                    g: 1,
                    c: "i = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{2})"
                };
            case "s":
                return {
                    g: 1,
                    c: "s = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{2})"
                };
            case "O":
                return {
                    g: 0,
                    c: null,
                    s: "[+-]\\d{4}"
                };
            case "T":
                return {
                    g: 0,
                    c: null,
                    s: "[A-Z]{3}"
                };
            case "Z":
                return {
                    g: 0,
                    c: null,
                    s: "[+-]\\d{1,5}"
                };
            default:
                return {
                    g: 0,
                    c: null,
                    s: String.escape(e)
                }
        }
    }, Date.prototype.getTimezone = function() {
        return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3")
    }, Date.prototype.getGMTOffset = function() {
        return (this.getTimezoneOffset() > 0 ? "-" : "+") + String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset()) / 60), 2, "0") + String.leftPad(Math.abs(this.getTimezoneOffset()) % 60, 2, "0")
    }, Date.prototype.getDayOfYear = function() {
        var e = 0;
        Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
        for (var t = 0; t < this.getMonth(); ++t) e += Date.daysInMonth[t];
        return e + this.getDate()
    }, Date.prototype.getWeekOfYear = function() {
        var e = this.getDayOfYear() + (4 - this.getDay()),
            t = new Date(this.getFullYear(), 0, 1),
            i = 7 - t.getDay() + 4;
        return String.leftPad(Math.ceil((e - i) / 7) + 1, 2, "0")
    }, Date.prototype.isLeapYear = function() {
        var e = this.getFullYear();
        return 0 == (3 & e) && (e % 100 || e % 400 == 0 && e)
    }, Date.prototype.getFirstDayOfMonth = function() {
        var e = (this.getDay() - (this.getDate() - 1)) % 7;
        return 0 > e ? e + 7 : e
    }, Date.prototype.getLastDayOfMonth = function() {
        var e = (this.getDay() + (Date.daysInMonth[this.getMonth()] - this.getDate())) % 7;
        return 0 > e ? e + 7 : e
    }, Date.prototype.getDaysInMonth = function() {
        return Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28, Date.daysInMonth[this.getMonth()]
    }, Date.prototype.getSuffix = function() {
        switch (this.getDate()) {
            case 1:
            case 21:
            case 31:
                return "st";
            case 2:
            case 22:
                return "nd";
            case 3:
            case 23:
                return "rd";
            default:
                return "th"
        }
    }, String.escape = function(e) {
        return e.replace(/('|\\)/g, "\\$1")
    }, String.leftPad = function(e, t, i) {
        var n = new String(e);
        for (null == i && (i = " "); n.length < t;) n = i + n;
        return n
    }, Date.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], Date.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], Date.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], Date.y2kYear = 50, Date.monthNumbers = {
        Jan: 0,
        Feb: 1,
        Mar: 2,
        Apr: 3,
        May: 4,
        Jun: 5,
        Jul: 6,
        Aug: 7,
        Sep: 8,
        Oct: 9,
        Nov: 10,
        Dec: 11
    }, Date.patterns = {
        ISO8601LongPattern: "Y-m-d H:i:s",
        ISO8601ShortPattern: "Y-m-d",
        ShortDatePattern: "n/j/Y",
        LongDatePattern: "l, F d, Y",
        FullDateTimePattern: "l, F d, Y g:i:s A",
        MonthDayPattern: "F d",
        ShortTimePattern: "g:i A",
        LongTimePattern: "g:i:s A",
        SortableDateTimePattern: "Y-m-d\\TH:i:s",
        UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
        YearMonthPattern: "F, Y"
    },
    function(e, t, i) {
        e.fn.backstretch = function(n, s) {
            return (n === i || 0 === n.length) && e.error("No images were supplied for Backstretch"), 0 === e(t).scrollTop() && t.scrollTo(0, 0), this.each(function() {
                var t = e(this),
                    i = t.data("backstretch");
                if (i) {
                    if ("string" == typeof n && "function" == typeof i[n]) return void i[n](s);
                    s = e.extend(i.options, s), i.destroy(!0)
                }
                i = new o(this, n, s), t.data("backstretch", i)
            })
        }, e.backstretch = function(t, i) {
            return e("body").backstretch(t, i).data("backstretch")
        }, e.expr[":"].backstretch = function(t) {
            return e(t).data("backstretch") !== i
        }, e.fn.backstretch.defaults = {
            centeredX: !0,
            centeredY: !0,
            duration: 5e3,
            fade: 0
        };
        var n = {
                left: 0,
                top: 0,
                overflow: "hidden",
                margin: 0,
                padding: 0,
                height: "100%",
                width: "100%",
                zIndex: -999999
            },
            s = {
                position: "absolute",
                display: "none",
                margin: 0,
                padding: 0,
                border: "none",
                width: "auto",
                height: "auto",
                maxHeight: "none",
                maxWidth: "none",
                zIndex: -999999
            },
            o = function(i, s, o) {
                this.options = e.extend({}, e.fn.backstretch.defaults, o || {}), this.images = e.isArray(s) ? s : [s], e.each(this.images, function() {
                    e("<img />")[0].src = this
                }), this.isBody = i === document.body, this.$container = e(i), this.$root = this.isBody ? e(r ? t : document) : this.$container, i = this.$container.children(".backstretch").first(), this.$wrap = i.length ? i : e('<div class="backstretch"></div>').css(n).appendTo(this.$container), this.isBody || (i = this.$container.css("position"), s = this.$container.css("zIndex"), this.$container.css({
                    position: "static" === i ? "relative" : i,
                    zIndex: "auto" === s ? 0 : s,
                    background: "none"
                }), this.$wrap.css({
                    zIndex: -999998
                })), this.$wrap.css({
                    position: this.isBody && r ? "fixed" : "absolute"
                }), this.index = 0, this.show(this.index), e(t).on("resize.backstretch", e.proxy(this.resize, this)).on("orientationchange.backstretch", e.proxy(function() {
                    this.isBody && 0 === t.pageYOffset && (t.scrollTo(0, 1), this.resize())
                }, this))
            };
        o.prototype = {
            resize: function() {
                try {
                    var e, i = {
                            left: 0,
                            top: 0
                        },
                        n = this.isBody ? this.$root.width() : this.$root.innerWidth(),
                        s = n,
                        o = this.isBody ? t.innerHeight ? t.innerHeight : this.$root.height() : this.$root.innerHeight(),
                        r = s / this.$img.data("ratio");
                    r >= o ? (e = (r - o) / 2, this.options.centeredY && (i.top = "-" + e + "px")) : (r = o, s = r * this.$img.data("ratio"), e = (s - n) / 2, this.options.centeredX && (i.left = "-" + e + "px")), this.$wrap.css({
                        width: n,
                        height: o
                    }).find("img:not(.deleteable)").css({
                        width: s,
                        height: r
                    }).css(i)
                } catch (a) {}
                return this
            },
            show: function(t) {
                if (!(Math.abs(t) > this.images.length - 1)) {
                    var i = this,
                        n = i.$wrap.find("img").addClass("deleteable"),
                        o = {
                            relatedTarget: i.$container[0]
                        };
                    return i.$container.trigger(e.Event("backstretch.before", o), [i, t]), this.index = t, clearInterval(i.interval), i.$img = e("<img />").css(s).bind("load", function(s) {
                        var r = this.width || e(s.target).width();
                        s = this.height || e(s.target).height(), e(this).data("ratio", r / s), e(this).fadeIn(i.options.speed || i.options.fade, function() {
                            n.remove(), i.paused || i.cycle(), e(["after", "show"]).each(function() {
                                i.$container.trigger(e.Event("backstretch." + this, o), [i, t])
                            })
                        }), i.resize()
                    }).appendTo(i.$wrap), i.$img.attr("src", i.images[t]), i
                }
            },
            next: function() {
                return this.show(this.index < this.images.length - 1 ? this.index + 1 : 0)
            },
            prev: function() {
                return this.show(0 === this.index ? this.images.length - 1 : this.index - 1)
            },
            pause: function() {
                return this.paused = !0, this
            },
            resume: function() {
                return this.paused = !1, this.next(), this
            },
            cycle: function() {
                return 1 < this.images.length && (clearInterval(this.interval), this.interval = setInterval(e.proxy(function() {
                    this.paused || this.next()
                }, this), this.options.duration)), this
            },
            destroy: function(i) {
                e(t).off("resize.backstretch orientationchange.backstretch"), clearInterval(this.interval), i || this.$wrap.remove(), this.$container.removeData("backstretch")
            }
        };
        var r, a = navigator.userAgent,
            l = navigator.platform,
            c = a.match(/AppleWebKit\/([0-9]+)/),
            c = !!c && c[1],
            d = a.match(/Fennec\/([0-9]+)/),
            d = !!d && d[1],
            u = a.match(/Opera Mobi\/([0-9]+)/),
            h = !!u && u[1],
            p = a.match(/MSIE ([0-9]+)/),
            p = !!p && p[1];
        r = !((-1 < l.indexOf("iPhone") || -1 < l.indexOf("iPad") || -1 < l.indexOf("iPod")) && c && 534 > c || t.operamini && "[object OperaMini]" === {}.toString.call(t.operamini) || u && 7458 > h || -1 < a.indexOf("Android") && c && 533 > c || d && 6 > d || "palmGetResource" in t && c && 534 > c || -1 < a.indexOf("MeeGo") && -1 < a.indexOf("NokiaBrowser/8.5.0") || p && 6 >= p)
    }(jQuery, window), ! function(e) {
        var t = {},
            n = {
                mode: "horizontal",
                slideSelector: "",
                infiniteLoop: !0,
                hideControlOnEnd: !1,
                speed: 500,
                easing: null,
                slideMargin: 0,
                startSlide: 0,
                randomStart: !1,
                captions: !1,
                ticker: !1,
                tickerHover: !1,
                adaptiveHeight: !1,
                adaptiveHeightSpeed: 500,
                video: !1,
                useCSS: !0,
                preloadImages: "visible",
                responsive: !0,
                slideZIndex: 50,
                touchEnabled: !0,
                swipeThreshold: 50,
                oneToOneTouch: !0,
                preventDefaultSwipeX: !0,
                preventDefaultSwipeY: !1,
                pager: !0,
                pagerType: "full",
                pagerShortSeparator: " / ",
                pagerSelector: null,
                buildPager: null,
                pagerCustom: null,
                controls: !0,
                nextText: "Next",
                prevText: "Prev",
                nextSelector: null,
                prevSelector: null,
                autoControls: !1,
                startText: "Start",
                stopText: "Stop",
                autoControlsCombine: !1,
                autoControlsSelector: null,
                auto: !1,
                pause: 4e3,
                autoStart: !0,
                autoDirection: "next",
                autoHover: !1,
                autoDelay: 0,
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 0,
                slideWidth: 0,
                onSliderLoad: function() {},
                onSlideBefore: function() {},
                onSlideAfter: function() {},
                onSlideNext: function() {},
                onSlidePrev: function() {},
                onSliderResize: function() {}
            };
        e.fn.bxSlider = function(s) {
            if (0 == this.length) return this;
            if (this.length > 1) return this.each(function() {
                e(this).bxSlider(s)
            }), this;
            var o = {},
                r = this;
            t.el = this;
            var a = e(window).width(),
                l = e(window).height(),
                c = function() {
                    o.settings = e.extend({}, n, s), o.settings.slideWidth = parseInt(o.settings.slideWidth), o.children = r.children(o.settings.slideSelector), o.children.length < o.settings.minSlides && (o.settings.minSlides = o.children.length), o.children.length < o.settings.maxSlides && (o.settings.maxSlides = o.children.length), o.settings.randomStart && (o.settings.startSlide = Math.floor(Math.random() * o.children.length)), o.active = {
                        index: o.settings.startSlide
                    }, o.carousel = o.settings.minSlides > 1 || o.settings.maxSlides > 1, o.carousel && (o.settings.preloadImages = "all"), o.minThreshold = o.settings.minSlides * o.settings.slideWidth + (o.settings.minSlides - 1) * o.settings.slideMargin, o.maxThreshold = o.settings.maxSlides * o.settings.slideWidth + (o.settings.maxSlides - 1) * o.settings.slideMargin, o.working = !1, o.controls = {}, o.interval = null, o.animProp = "vertical" == o.settings.mode ? "top" : "left", o.usingCSS = o.settings.useCSS && "fade" != o.settings.mode && function() {
                        var e = document.createElement("div"),
                            t = ["WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                        for (var i in t)
                            if (void 0 !== e.style[t[i]]) return o.cssPrefix = t[i].replace("Perspective", "").toLowerCase(), o.animProp = "-" + o.cssPrefix + "-transform", !0;
                        return !1
                    }(), "vertical" == o.settings.mode && (o.settings.maxSlides = o.settings.minSlides), r.data("origStyle", r.attr("style")), r.children(o.settings.slideSelector).each(function() {
                        e(this).data("origStyle", e(this).attr("style"))
                    }), d()
                },
                d = function() {
                    r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'), o.viewport = r.parent(), o.loader = e('<div class="bx-loading" />'), o.viewport.prepend(o.loader), r.css({
                        width: "horizontal" == o.settings.mode ? 100 * o.children.length + 215 + "%" : "auto",
                        position: "relative"
                    }), o.usingCSS && o.settings.easing ? r.css("-" + o.cssPrefix + "-transition-timing-function", o.settings.easing) : o.settings.easing || (o.settings.easing = "swing"), g(), o.viewport.css({
                        width: "100%",
                        overflow: "hidden",
                        position: "relative"
                    }), o.viewport.parent().css({
                        maxWidth: f()
                    }), o.settings.pager || o.viewport.parent().css({
                        margin: "0 auto 0px"
                    }), o.children.css({
                        "float": "horizontal" == o.settings.mode ? "left" : "none",
                        listStyle: "none",
                        position: "relative"
                    }), o.children.css("width", m()), "horizontal" == o.settings.mode && o.settings.slideMargin > 0 && o.children.css("marginRight", o.settings.slideMargin), "vertical" == o.settings.mode && o.settings.slideMargin > 0 && o.children.css("marginBottom", o.settings.slideMargin), "fade" == o.settings.mode && (o.children.css({
                        position: "absolute",
                        zIndex: 0,
                        display: "none"
                    }), o.children.eq(o.settings.startSlide).css({
                        zIndex: o.settings.slideZIndex,
                        display: "block"
                    })), o.controls.el = e('<div class="bx-controls" />'), o.settings.captions && S(), o.active.last = o.settings.startSlide == v() - 1, o.settings.video && r.fitVids();
                    var t = o.children.eq(o.settings.startSlide);
                    "all" == o.settings.preloadImages && (t = o.children), o.settings.ticker ? o.settings.pager = !1 : (o.settings.pager && $(), o.settings.controls && k(), o.settings.auto && o.settings.autoControls && _(), (o.settings.controls || o.settings.autoControls || o.settings.pager) && o.viewport.after(o.controls.el)), u(t, h)
                },
                u = function(t, i) {
                    var n = t.find("img, iframe").length;
                    if (0 == n) return void i();
                    var s = 0;
                    t.find("img, iframe").each(function() {
                        e(this).one("load", function() {
                            ++s == n && i()
                        }).each(function() {
                            this.complete && e(this).load()
                        })
                    })
                },
                h = function() {
                    if (o.settings.infiniteLoop && "fade" != o.settings.mode && !o.settings.ticker) {
                        var t = "vertical" == o.settings.mode ? o.settings.minSlides : o.settings.maxSlides,
                            i = o.children.slice(0, t).clone().addClass("bx-clone"),
                            n = o.children.slice(-t).clone().addClass("bx-clone");
                        r.append(i).prepend(n)
                    }
                    o.loader.remove(), b(), "vertical" == o.settings.mode && (o.settings.adaptiveHeight = !0), o.viewport.height(p()), r.redrawSlider(), o.settings.onSliderLoad(o.active.index), o.initialized = !0, o.settings.responsive && e(window).bind("resize", W), o.settings.auto && o.settings.autoStart && j(), o.settings.ticker && P(), o.settings.pager && A(o.settings.startSlide), o.settings.controls && N(), o.settings.touchEnabled && !o.settings.ticker && H()
                },
                p = function() {
                    var t = 0,
                        n = e();
                    if ("vertical" == o.settings.mode || o.settings.adaptiveHeight)
                        if (o.carousel) {
                            var s = 1 == o.settings.moveSlides ? o.active.index : o.active.index * y();
                            for (n = o.children.eq(s), i = 1; i <= o.settings.maxSlides - 1; i++) n = n.add(s + i >= o.children.length ? o.children.eq(i - 1) : o.children.eq(s + i))
                        } else n = o.children.eq(o.active.index);
                    else n = o.children;
                    return "vertical" == o.settings.mode ? (n.each(function() {
                        t += e(this).outerHeight()
                    }), o.settings.slideMargin > 0 && (t += o.settings.slideMargin * (o.settings.minSlides - 1))) : t = Math.max.apply(Math, n.map(function() {
                        return e(this).outerHeight(!1)
                    }).get()), t
                },
                f = function() {
                    var e = "100%";
                    return o.settings.slideWidth > 0 && (e = "horizontal" == o.settings.mode ? o.settings.maxSlides * o.settings.slideWidth + (o.settings.maxSlides - 1) * o.settings.slideMargin : o.settings.slideWidth), e
                },
                m = function() {
                    var e = o.settings.slideWidth,
                        t = o.viewport.width();
                    return 0 == o.settings.slideWidth || o.settings.slideWidth > t && !o.carousel || "vertical" == o.settings.mode ? e = t : o.settings.maxSlides > 1 && "horizontal" == o.settings.mode && (t > o.maxThreshold || t < o.minThreshold && (e = (t - o.settings.slideMargin * (o.settings.minSlides - 1)) / o.settings.minSlides)), e
                },
                g = function() {
                    var e = 1;
                    if ("horizontal" == o.settings.mode && o.settings.slideWidth > 0)
                        if (o.viewport.width() < o.minThreshold) e = o.settings.minSlides;
                        else if (o.viewport.width() > o.maxThreshold) e = o.settings.maxSlides;
                    else {
                        var t = o.children.first().width();
                        e = Math.floor(o.viewport.width() / t)
                    } else "vertical" == o.settings.mode && (e = o.settings.minSlides);
                    return e
                },
                v = function() {
                    var e = 0;
                    if (o.settings.moveSlides > 0)
                        if (o.settings.infiniteLoop) e = o.children.length / y();
                        else
                            for (var t = 0, i = 0; t < o.children.length;) ++e, t = i + g(), i += o.settings.moveSlides <= g() ? o.settings.moveSlides : g();
                    else e = Math.ceil(o.children.length / g());
                    return e
                },
                y = function() {
                    return o.settings.moveSlides > 0 && o.settings.moveSlides <= g() ? o.settings.moveSlides : g()
                },
                b = function() {
                    if (o.children.length > o.settings.maxSlides && o.active.last && !o.settings.infiniteLoop) {
                        if ("horizontal" == o.settings.mode) {
                            var e = o.children.last(),
                                t = e.position();
                            w(-(t.left - (o.viewport.width() - e.width())), "reset", 0)
                        } else if ("vertical" == o.settings.mode) {
                            var i = o.children.length - o.settings.minSlides,
                                t = o.children.eq(i).position();
                            w(-t.top, "reset", 0)
                        }
                    } else {
                        var t = o.children.eq(o.active.index * y()).position();
                        o.active.index == v() - 1 && (o.active.last = !0), void 0 != t && ("horizontal" == o.settings.mode ? w(-t.left, "reset", 0) : "vertical" == o.settings.mode && w(-t.top, "reset", 0))
                    }
                },
                w = function(e, t, i, n) {
                    if (o.usingCSS) {
                        var s = "vertical" == o.settings.mode ? "translate3d(0, " + e + "px, 0)" : "translate3d(" + e + "px, 0, 0)";
                        r.css("-" + o.cssPrefix + "-transition-duration", i / 1e3 + "s"), "slide" == t ? (r.css(o.animProp, s), r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
                            r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), M()
                        })) : "reset" == t ? r.css(o.animProp, s) : "ticker" == t && (r.css("-" + o.cssPrefix + "-transition-timing-function", "linear"), r.css(o.animProp, s), r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
                            r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"), w(n.resetValue, "reset", 0), F()
                        }))
                    } else {
                        var a = {};
                        a[o.animProp] = e, "slide" == t ? r.animate(a, i, o.settings.easing, function() {
                            M()
                        }) : "reset" == t ? r.css(o.animProp, e) : "ticker" == t && r.animate(a, speed, "linear", function() {
                            w(n.resetValue, "reset", 0), F()
                        })
                    }
                },
                x = function() {
                    for (var t = "", i = v(), n = 0; i > n; n++) {
                        var s = "";
                        o.settings.buildPager && e.isFunction(o.settings.buildPager) ? (s = o.settings.buildPager(n), o.pagerEl.addClass("bx-custom-pager")) : (s = n + 1, o.pagerEl.addClass("bx-default-pager")), t += '<div class="bx-pager-item"><a href="" data-slide-index="' + n + '" class="bx-pager-link">' + s + "</a></div>"
                    }
                    o.pagerEl.html(t)
                },
                $ = function() {
                    o.settings.pagerCustom ? o.pagerEl = e(o.settings.pagerCustom) : (o.pagerEl = e('<div class="bx-pager" />'), o.settings.pagerSelector ? e(o.settings.pagerSelector).html(o.pagerEl) : o.controls.el.addClass("bx-has-pager").append(o.pagerEl), x()), o.pagerEl.on("click", "a", O)
                },
                k = function() {
                    o.controls.next = e('<a class="bx-next" href="">' + o.settings.nextText + "</a>"), o.controls.prev = e('<a class="bx-prev" href="">' + o.settings.prevText + "</a>"), o.controls.next.bind("click", C), o.controls.prev.bind("click", T), o.settings.nextSelector && e(o.settings.nextSelector).append(o.controls.next), o.settings.prevSelector && e(o.settings.prevSelector).append(o.controls.prev), o.settings.nextSelector || o.settings.prevSelector || (o.controls.directionEl = e('<div class="bx-controls-direction" />'), o.controls.directionEl.append(o.controls.prev).append(o.controls.next), o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))
                },
                _ = function() {
                    o.controls.start = e('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + o.settings.startText + "</a></div>"), o.controls.stop = e('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + o.settings.stopText + "</a></div>"), o.controls.autoEl = e('<div class="bx-controls-auto" />'), o.controls.autoEl.on("click", ".bx-start", D), o.controls.autoEl.on("click", ".bx-stop", E), o.settings.autoControlsCombine ? o.controls.autoEl.append(o.controls.start) : o.controls.autoEl.append(o.controls.start).append(o.controls.stop), o.settings.autoControlsSelector ? e(o.settings.autoControlsSelector).html(o.controls.autoEl) : o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl), I(o.settings.autoStart ? "stop" : "start")
                },
                S = function() {
                    o.children.each(function() {
                        var t = e(this).find("img:first").attr("title");
                        void 0 != t && ("" + t).length && e(this).append('<div class="bx-caption"><span>' + t + "</span></div>")
                    })
                },
                C = function(e) {
                    o.settings.auto && r.stopAuto(), r.goToNextSlide(), e.preventDefault()
                },
                T = function(e) {
                    o.settings.auto && r.stopAuto(), r.goToPrevSlide(), e.preventDefault()
                },
                D = function(e) {
                    r.startAuto(), e.preventDefault()
                },
                E = function(e) {
                    r.stopAuto(), e.preventDefault()
                },
                O = function(t) {
                    o.settings.auto && r.stopAuto();
                    var i = e(t.currentTarget),
                        n = parseInt(i.attr("data-slide-index"));
                    n != o.active.index && r.goToSlide(n), t.preventDefault()
                },
                A = function(t) {
                    var i = o.children.length;
                    return "short" == o.settings.pagerType ? (o.settings.maxSlides > 1 && (i = Math.ceil(o.children.length / o.settings.maxSlides)), void o.pagerEl.html(t + 1 + o.settings.pagerShortSeparator + i)) : (o.pagerEl.find("a").removeClass("active"), void o.pagerEl.each(function(i, n) {
                        e(n).find("a").eq(t).addClass("active")
                    }))
                },
                M = function() {
                    if (o.settings.infiniteLoop) {
                        var e = "";
                        0 == o.active.index ? e = o.children.eq(0).position() : o.active.index == v() - 1 && o.carousel ? e = o.children.eq((v() - 1) * y()).position() : o.active.index == o.children.length - 1 && (e = o.children.eq(o.children.length - 1).position()), e && ("horizontal" == o.settings.mode ? w(-e.left, "reset", 0) : "vertical" == o.settings.mode && w(-e.top, "reset", 0))
                    }
                    o.working = !1, o.settings.onSlideAfter(o.children.eq(o.active.index), o.oldIndex, o.active.index)
                },
                I = function(e) {
                    o.settings.autoControlsCombine ? o.controls.autoEl.html(o.controls[e]) : (o.controls.autoEl.find("a").removeClass("active"), o.controls.autoEl.find("a:not(.bx-" + e + ")").addClass("active"))
                },
                N = function() {
                    1 == v() ? (o.controls.prev.addClass("disabled"), o.controls.next.addClass("disabled")) : !o.settings.infiniteLoop && o.settings.hideControlOnEnd && (0 == o.active.index ? (o.controls.prev.addClass("disabled"), o.controls.next.removeClass("disabled")) : o.active.index == v() - 1 ? (o.controls.next.addClass("disabled"), o.controls.prev.removeClass("disabled")) : (o.controls.prev.removeClass("disabled"), o.controls.next.removeClass("disabled")))
                },
                j = function() {
                    o.settings.autoDelay > 0 ? setTimeout(r.startAuto, o.settings.autoDelay) : r.startAuto(), o.settings.autoHover && r.hover(function() {
                        o.interval && (r.stopAuto(!0), o.autoPaused = !0)
                    }, function() {
                        o.autoPaused && (r.startAuto(!0), o.autoPaused = null)
                    })
                },
                P = function() {
                    var t = 0;
                    if ("next" == o.settings.autoDirection) r.append(o.children.clone().addClass("bx-clone"));
                    else {
                        r.prepend(o.children.clone().addClass("bx-clone"));
                        var i = o.children.first().position();
                        t = "horizontal" == o.settings.mode ? -i.left : -i.top
                    }
                    w(t, "reset", 0), o.settings.pager = !1, o.settings.controls = !1, o.settings.autoControls = !1, o.settings.tickerHover && !o.usingCSS && o.viewport.hover(function() {
                        r.stop()
                    }, function() {
                        var t = 0;
                        o.children.each(function() {
                            t += "horizontal" == o.settings.mode ? e(this).outerWidth(!0) : e(this).outerHeight(!0)
                        });
                        var i = o.settings.speed / t,
                            n = "horizontal" == o.settings.mode ? "left" : "top",
                            s = i * (t - Math.abs(parseInt(r.css(n))));
                        F(s)
                    }), F()
                },
                F = function(e) {
                    speed = e ? e : o.settings.speed;
                    var t = {
                            left: 0,
                            top: 0
                        },
                        i = {
                            left: 0,
                            top: 0
                        };
                    "next" == o.settings.autoDirection ? t = r.find(".bx-clone").first().position() : i = o.children.first().position();
                    var n = "horizontal" == o.settings.mode ? -t.left : -t.top,
                        s = "horizontal" == o.settings.mode ? -i.left : -i.top,
                        a = {
                            resetValue: s
                        };
                    w(n, "ticker", speed, a)
                },
                H = function() {
                    o.touch = {
                        start: {
                            x: 0,
                            y: 0
                        },
                        end: {
                            x: 0,
                            y: 0
                        }
                    }, o.viewport.bind("touchstart", L)
                },
                L = function(e) {
                    if (o.working) e.preventDefault();
                    else {
                        o.touch.originalPos = r.position();
                        var t = e.originalEvent;
                        o.touch.start.x = t.changedTouches[0].pageX, o.touch.start.y = t.changedTouches[0].pageY, o.viewport.bind("touchmove", z), o.viewport.bind("touchend", q)
                    }
                },
                z = function(e) {
                    var t = e.originalEvent,
                        i = Math.abs(t.changedTouches[0].pageX - o.touch.start.x),
                        n = Math.abs(t.changedTouches[0].pageY - o.touch.start.y);
                    if (3 * i > n && o.settings.preventDefaultSwipeX ? e.preventDefault() : 3 * n > i && o.settings.preventDefaultSwipeY && e.preventDefault(), "fade" != o.settings.mode && o.settings.oneToOneTouch) {
                        var s = 0;
                        if ("horizontal" == o.settings.mode) {
                            var r = t.changedTouches[0].pageX - o.touch.start.x;
                            s = o.touch.originalPos.left + r
                        } else {
                            var r = t.changedTouches[0].pageY - o.touch.start.y;
                            s = o.touch.originalPos.top + r
                        }
                        w(s, "reset", 0)
                    }
                },
                q = function(e) {
                    o.viewport.unbind("touchmove", z);
                    var t = e.originalEvent,
                        i = 0;
                    if (o.touch.end.x = t.changedTouches[0].pageX, o.touch.end.y = t.changedTouches[0].pageY, "fade" == o.settings.mode) {
                        var n = Math.abs(o.touch.start.x - o.touch.end.x);
                        n >= o.settings.swipeThreshold && (o.touch.start.x > o.touch.end.x ? r.goToNextSlide() : r.goToPrevSlide(), r.stopAuto())
                    } else {
                        var n = 0;
                        "horizontal" == o.settings.mode ? (n = o.touch.end.x - o.touch.start.x, i = o.touch.originalPos.left) : (n = o.touch.end.y - o.touch.start.y, i = o.touch.originalPos.top), !o.settings.infiniteLoop && (0 == o.active.index && n > 0 || o.active.last && 0 > n) ? w(i, "reset", 200) : Math.abs(n) >= o.settings.swipeThreshold ? (0 > n ? r.goToNextSlide() : r.goToPrevSlide(), r.stopAuto()) : w(i, "reset", 200)
                    }
                    o.viewport.unbind("touchend", q)
                },
                W = function() {
                    var t = e(window).width(),
                        i = e(window).height();
                    (a != t || l != i) && (a = t, l = i, r.redrawSlider(), o.settings.onSliderResize.call(r, o.active.index))
                };
            return r.goToSlide = function(t, i) {
                if (!o.working && o.active.index != t)
                    if (o.working = !0, o.oldIndex = o.active.index, o.active.index = 0 > t ? v() - 1 : t >= v() ? 0 : t, o.settings.onSlideBefore(o.children.eq(o.active.index), o.oldIndex, o.active.index), "next" == i ? o.settings.onSlideNext(o.children.eq(o.active.index), o.oldIndex, o.active.index) : "prev" == i && o.settings.onSlidePrev(o.children.eq(o.active.index), o.oldIndex, o.active.index), o.active.last = o.active.index >= v() - 1, o.settings.pager && A(o.active.index), o.settings.controls && N(), "fade" == o.settings.mode) o.settings.adaptiveHeight && o.viewport.height() != p() && o.viewport.animate({
                        height: p()
                    }, o.settings.adaptiveHeightSpeed), o.children.filter(":visible").fadeOut(o.settings.speed).css({
                        zIndex: 0
                    }), o.children.eq(o.active.index).css("zIndex", o.settings.slideZIndex + 1).fadeIn(o.settings.speed, function() {
                        e(this).css("zIndex", o.settings.slideZIndex), M()
                    });
                    else {
                        o.settings.adaptiveHeight && o.viewport.height() != p() && o.viewport.animate({
                            height: p()
                        }, o.settings.adaptiveHeightSpeed);
                        var n = 0,
                            s = {
                                left: 0,
                                top: 0
                            };
                        if (!o.settings.infiniteLoop && o.carousel && o.active.last)
                            if ("horizontal" == o.settings.mode) {
                                var a = o.children.eq(o.children.length - 1);
                                s = a.position(), n = o.viewport.width() - a.outerWidth()
                            } else {
                                var l = o.children.length - o.settings.minSlides;
                                s = o.children.eq(l).position()
                            }
                        else if (o.carousel && o.active.last && "prev" == i) {
                            var c = 1 == o.settings.moveSlides ? o.settings.maxSlides - y() : (v() - 1) * y() - (o.children.length - o.settings.maxSlides),
                                a = r.children(".bx-clone").eq(c);
                            s = a.position()
                        } else if ("next" == i && 0 == o.active.index) s = r.find("> .bx-clone").eq(o.settings.maxSlides).position(), o.active.last = !1;
                        else if (t >= 0) {
                            var d = t * y();
                            s = o.children.eq(d).position()
                        }
                        if ("undefined" != typeof s) {
                            var u = "horizontal" == o.settings.mode ? -(s.left - n) : -s.top;
                            w(u, "slide", o.settings.speed)
                        }
                    }
            }, r.goToNextSlide = function() {
                if (o.settings.infiniteLoop || !o.active.last) {
                    var e = parseInt(o.active.index) + 1;
                    r.goToSlide(e, "next")
                }
            }, r.goToPrevSlide = function() {
                if (o.settings.infiniteLoop || 0 != o.active.index) {
                    var e = parseInt(o.active.index) - 1;
                    r.goToSlide(e, "prev")
                }
            }, r.startAuto = function(e) {
                o.interval || (o.interval = setInterval(function() {
                    "next" == o.settings.autoDirection ? r.goToNextSlide() : r.goToPrevSlide()
                }, o.settings.pause), o.settings.autoControls && 1 != e && I("stop"))
            }, r.stopAuto = function(e) {
                o.interval && (clearInterval(o.interval), o.interval = null, o.settings.autoControls && 1 != e && I("start"))
            }, r.getCurrentSlide = function() {
                return o.active.index
            }, r.getCurrentSlideElement = function() {
                return o.children.eq(o.active.index)
            }, r.getSlideCount = function() {
                return o.children.length
            }, r.redrawSlider = function() {
                o.children.add(r.find(".bx-clone")).outerWidth(m()), o.viewport.css("height", p()), o.settings.ticker || b(), o.active.last && (o.active.index = v() - 1), o.active.index >= v() && (o.active.last = !0), o.settings.pager && !o.settings.pagerCustom && (x(), A(o.active.index))
            }, r.destroySlider = function() {
                o.initialized && (o.initialized = !1, e(".bx-clone", this).remove(), o.children.each(function() {
                    void 0 != e(this).data("origStyle") ? e(this).attr("style", e(this).data("origStyle")) : e(this).removeAttr("style")
                }), void 0 != e(this).data("origStyle") ? this.attr("style", e(this).data("origStyle")) : e(this).removeAttr("style"), e(this).unwrap().unwrap(), o.controls.el && o.controls.el.remove(), o.controls.next && o.controls.next.remove(), o.controls.prev && o.controls.prev.remove(), o.pagerEl && o.settings.controls && o.pagerEl.remove(), e(".bx-caption", this).remove(), o.controls.autoEl && o.controls.autoEl.remove(), clearInterval(o.interval), o.settings.responsive && e(window).unbind("resize", W))
            }, r.reloadSlider = function(e) {
                void 0 != e && (s = e), r.destroySlider(), c()
            }, c(), this
        }
    }(jQuery),
    function(e) {
        "use strict";
        var t = {
            i18n: {
                ru: {
                    months: ["\u042f\u043d\u0432\u0430\u0440\u044c", "\u0424\u0435\u0432\u0440\u0430\u043b\u044c", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440\u0435\u043b\u044c", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433\u0443\u0441\u0442", "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c", "\u041e\u043a\u0442\u044f\u0431\u0440\u044c", "\u041d\u043e\u044f\u0431\u0440\u044c", "\u0414\u0435\u043a\u0430\u0431\u0440\u044c"],
                    dayOfWeek: ["\u0412\u0441\u043a", "\u041f\u043d", "\u0412\u0442", "\u0421\u0440", "\u0427\u0442", "\u041f\u0442", "\u0421\u0431"]
                },
                en: {
                    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    dayOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
                },
                de: {
                    months: ["Januar", "Februar", "M\xe4rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                    dayOfWeek: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"]
                },
                nl: {
                    months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
                    dayOfWeek: ["zo", "ma", "di", "wo", "do", "vr", "za"]
                },
                tr: {
                    months: ["Ocak", "\u015eubat", "Mart", "Nisan", "May\u0131s", "Haziran", "Temmuz", "A\u011fustos", "Eyl\xfcl", "Ekim", "Kas\u0131m", "Aral\u0131k"],
                    dayOfWeek: ["Paz", "Pts", "Sal", "\xc7ar", "Per", "Cum", "Cts"]
                },
                fr: {
                    months: ["Janvier", "F\xe9vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao\xfbt", "Septembre", "Octobre", "Novembre", "D\xe9cembre"],
                    dayOfWeek: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]
                },
                es: {
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    dayOfWeek: ["Dom", "Lun", "Mar", "Mi\xe9", "Jue", "Vie", "S\xe1b"]
                },
                th: {
                    months: ["\u0e21\u0e01\u0e23\u0e32\u0e04\u0e21", "\u0e01\u0e38\u0e21\u0e20\u0e32\u0e1e\u0e31\u0e19\u0e18\u0e4c", "\u0e21\u0e35\u0e19\u0e32\u0e04\u0e21", "\u0e40\u0e21\u0e29\u0e32\u0e22\u0e19", "\u0e1e\u0e24\u0e29\u0e20\u0e32\u0e04\u0e21", "\u0e21\u0e34\u0e16\u0e38\u0e19\u0e32\u0e22\u0e19", "\u0e01\u0e23\u0e01\u0e0e\u0e32\u0e04\u0e21", "\u0e2a\u0e34\u0e07\u0e2b\u0e32\u0e04\u0e21", "\u0e01\u0e31\u0e19\u0e22\u0e32\u0e22\u0e19", "\u0e15\u0e38\u0e25\u0e32\u0e04\u0e21", "\u0e1e\u0e24\u0e28\u0e08\u0e34\u0e01\u0e32\u0e22\u0e19", "\u0e18\u0e31\u0e19\u0e27\u0e32\u0e04\u0e21"],
                    dayOfWeek: ["\u0e2d\u0e32.", "\u0e08.", "\u0e2d.", "\u0e1e.", "\u0e1e\u0e24.", "\u0e28.", "\u0e2a."]
                },
                pl: {
                    months: ["stycze\u0144", "luty", "marzec", "kwiecie\u0144", "maj", "czerwiec", "lipiec", "sierpie\u0144", "wrzesie\u0144", "pa\u017adziernik", "listopad", "grudzie\u0144"],
                    dayOfWeek: ["nd", "pn", "wt", "\u015br", "cz", "pt", "sb"]
                },
                pt: {
                    months: ["Janeiro", "Fevereiro", "Mar\xe7o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    dayOfWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"]
                },
                ch: {
                    months: ["\u4e00\u6708", "\u4e8c\u6708", "\u4e09\u6708", "\u56db\u6708", "\u4e94\u6708", "\u516d\u6708", "\u4e03\u6708", "\u516b\u6708", "\u4e5d\u6708", "\u5341\u6708", "\u5341\u4e00\u6708", "\u5341\u4e8c\u6708"],
                    dayOfWeek: ["\u65e5", "\u4e00", "\u4e8c", "\u4e09", "\u56db", "\u4e94", "\u516d"]
                },
                se: {
                    months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
                    dayOfWeek: ["S\xf6n", "M\xe5n", "Tis", "Ons", "Tor", "Fre", "L\xf6r"]
                },
                kr: {
                    months: ["1\uc6d4", "2\uc6d4", "3\uc6d4", "4\uc6d4", "5\uc6d4", "6\uc6d4", "7\uc6d4", "8\uc6d4", "9\uc6d4", "10\uc6d4", "11\uc6d4", "12\uc6d4"],
                    dayOfWeek: ["\uc77c", "\uc6d4", "\ud654", "\uc218", "\ubaa9", "\uae08", "\ud1a0"]
                },
                it: {
                    months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
                    dayOfWeek: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"]
                },
                da: {
                    months: ["January", "Februar", "Marts", "April", "Maj", "Juni", "July", "August", "September", "Oktober", "November", "December"],
                    dayOfWeek: ["S\xf8n", "Man", "Tir", "ons", "Tor", "Fre", "l\xf8r"]
                },
                ja: {
                    months: ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                    dayOfWeek: ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"]
                },
                vi: {
                    months: ["Th\xe1ng 1", "Th\xe1ng 2", "Th\xe1ng 3", "Th\xe1ng 4", "Th\xe1ng 5", "Th\xe1ng 6", "Th\xe1ng 7", "Th\xe1ng 8", "Th\xe1ng 9", "Th\xe1ng 10", "Th\xe1ng 11", "Th\xe1ng 12"],
                    dayOfWeek: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"]
                }
            },
            value: "",
            lang: "en",
            format: "Y/m/d H:i",
            formatTime: "H:i",
            formatDate: "Y/m/d",
            startDate: !1,
            step: 60,
            closeOnDateSelect: !1,
            closeOnWithoutClick: !0,
            timepicker: !0,
            datepicker: !0,
            minDate: !1,
            maxDate: !1,
            minTime: !1,
            maxTime: !1,
            allowTimes: [],
            opened: !1,
            initTime: !0,
            inline: !1,
            onSelectDate: function() {},
            onSelectTime: function() {},
            onChangeMonth: function() {},
            onChangeDateTime: function() {},
            onShow: function() {},
            onClose: function() {},
            onGenerate: function() {},
            withoutCopyright: !0,
            inverseButton: !1,
            hours12: !1,
            next: "xdsoft_next",
            prev: "xdsoft_prev",
            dayOfWeekStart: 0,
            timeHeightInTimePicker: 25,
            timepickerScrollbar: !0,
            todayButton: !0,
            defaultSelect: !0,
            scrollMonth: !0,
            scrollTime: !0,
            scrollInput: !0,
            lazyInit: !1,
            mask: !1,
            validateOnBlur: !0,
            allowBlank: !0,
            yearStart: 1950,
            yearEnd: 2050,
            style: "",
            id: "",
            roundTime: "round",
            className: "",
            weekends: [],
            yearOffset: 0
        };
        Array.prototype.indexOf || (Array.prototype.indexOf = function(e, t) {
            for (var i = t || 0, n = this.length; n > i; i++)
                if (this[i] === e) return i;
            return -1
        }), e.fn.xdsoftScroller = function(t) {
            return this.each(function() {
                var i = e(this);
                if (!e(this).hasClass("xdsoft_scroller_box")) {
                    var n = function(e) {
                            var t = {
                                x: 0,
                                y: 0
                            };
                            if ("touchstart" == e.type || "touchmove" == e.type || "touchend" == e.type || "touchcancel" == e.type) {
                                var i = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                                t.x = i.pageX, t.y = i.pageY
                            } else("mousedown" == e.type || "mouseup" == e.type || "mousemove" == e.type || "mouseover" == e.type || "mouseout" == e.type || "mouseenter" == e.type || "mouseleave" == e.type) && (t.x = e.pageX, t.y = e.pageY);
                            return t
                        },
                        s = 0,
                        o = i.children().eq(0),
                        r = i[0].clientHeight,
                        a = o[0].offsetHeight,
                        l = e('<div class="xdsoft_scrollbar"></div>'),
                        c = e('<div class="xdsoft_scroller"></div>'),
                        d = 100,
                        u = !1;
                    l.append(c), i.addClass("xdsoft_scroller_box").append(l), c.on("mousedown.xdsoft_scroller", function(n) {
                        r || i.trigger("resize_scroll.xdsoft_scroller", [t]);
                        var o = n.pageY,
                            a = parseInt(c.css("margin-top")),
                            u = l[0].offsetHeight;
                        e(document.body).addClass("xdsoft_noselect"), e([document.body, window]).on("mouseup.xdsoft_scroller", function h() {
                            e([document.body, window]).off("mouseup.xdsoft_scroller", h).off("mousemove.xdsoft_scroller", s).removeClass("xdsoft_noselect")
                        }), e(document.body).on("mousemove.xdsoft_scroller", s = function(e) {
                            var t = e.pageY - o + a;
                            0 > t && (t = 0), t + c[0].offsetHeight > u && (t = u - c[0].offsetHeight), i.trigger("scroll_element.xdsoft_scroller", [d ? t / d : 0])
                        })
                    }), i.on("scroll_element.xdsoft_scroller", function(e, t) {
                        r || i.trigger("resize_scroll.xdsoft_scroller", [t, !0]), t = t > 1 ? 1 : 0 > t || isNaN(t) ? 0 : t, c.css("margin-top", d * t), o.css("marginTop", -parseInt((a - r) * t))
                    }).on("resize_scroll.xdsoft_scroller", function(e, t, n) {
                        r = i[0].clientHeight, a = o[0].offsetHeight;
                        var s = r / a,
                            u = s * l[0].offsetHeight;
                        s > 1 ? c.hide() : (c.show(), c.css("height", parseInt(u > 10 ? u : 10)), d = l[0].offsetHeight - c[0].offsetHeight, n !== !0 && i.trigger("scroll_element.xdsoft_scroller", [t ? t : Math.abs(parseInt(o.css("marginTop"))) / (a - r)]))
                    }), i.mousewheel && i.mousewheel(function(e, t) {
                        var n = Math.abs(parseInt(o.css("marginTop")));
                        return i.trigger("scroll_element.xdsoft_scroller", [(n - 20 * t) / (a - r)]), e.stopPropagation(), !1
                    }), i.on("touchstart", function(e) {
                        u = n(e)
                    }), i.on("touchmove", function(e) {
                        if (u) {
                            var t = n(e),
                                s = Math.abs(parseInt(o.css("marginTop")));
                            i.trigger("scroll_element.xdsoft_scroller", [(s - (t.y - u.y)) / (a - r)]), e.stopPropagation(), e.preventDefault()
                        }
                    }), i.on("touchend touchcancel", function() {
                        u = !1
                    })
                }
                i.trigger("resize_scroll.xdsoft_scroller", [t])
            })
        }, e.fn.datetimepicker = function(i) {
            var n = 48,
                s = 57,
                o = 96,
                r = 105,
                a = 17,
                l = 46,
                c = 13,
                d = 27,
                u = 8,
                h = 37,
                p = 38,
                f = 39,
                m = 40,
                g = 9,
                v = 116,
                y = 65,
                b = 67,
                w = 86,
                x = 90,
                $ = 89,
                k = !1,
                _ = e.isPlainObject(i) || !i ? e.extend(!0, {}, t, i) : e.extend({}, t),
                S = 0,
                C = function(e) {
                    e.on("open.xdsoft focusin.xdsoft mousedown.xdsoft", function t() {
                        e.is(":disabled") || e.is(":hidden") || !e.is(":visible") || e.data("xdsoft_datetimepicker") || (clearTimeout(S), S = setTimeout(function() {
                            e.data("xdsoft_datetimepicker") || T(e), e.off("open.xdsoft focusin.xdsoft mousedown.xdsoft", t).trigger("open.xdsoft")
                        }, 100))
                    })
                },
                T = function(t) {
                    function i() {
                        var e = _.value ? _.value : t && t.val && t.val() ? t.val() : "";
                        return e && F.isValidDate(e = Date.parseDate(e, _.format)) ? S.data("changed", !0) : e = "", e || _.startDate === !1 || (e = F.strToDateTime(_.startDate)), e ? e : 0
                    }
                    var S = e("<div " + (_.id ? 'id="' + _.id + '"' : "") + " " + (_.style ? 'style="' + _.style + '"' : "") + ' class="xdsoft_datetimepicker xdsoft_noselect ' + _.className + '"></div>'),
                        C = e('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
                        T = e('<div class="xdsoft_datepicker active"></div>'),
                        D = e('<div class="xdsoft_mounthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button><div class="xdsoft_label xdsoft_month"><span></span></div><div class="xdsoft_label xdsoft_year"><span></span></div><button type="button" class="xdsoft_next"></button></div>'),
                        E = e('<div class="xdsoft_calendar"></div>'),
                        O = e('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
                        A = O.find(".xdsoft_time_box").eq(0),
                        M = e('<div class="xdsoft_time_variant"></div>'),
                        I = e('<div class="xdsoft_scrollbar"></div>'),
                        N = (e('<div class="xdsoft_scroller"></div>'), e('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>')),
                        j = e('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>');
                    D.find(".xdsoft_month span").after(N), D.find(".xdsoft_year span").after(j), D.find(".xdsoft_month,.xdsoft_year").on("mousedown.xdsoft", function(t) {
                        D.find(".xdsoft_select").hide();
                        var i = e(this).find(".xdsoft_select").eq(0),
                            n = 0,
                            s = 0;
                        F.currentTime && (n = F.currentTime[e(this).hasClass("xdsoft_month") ? "getMonth" : "getFullYear"]()), i.show();
                        for (var o = i.find("div.xdsoft_option"), r = 0; r < o.length && o.eq(r).data("value") != n; r++) s += o[0].offsetHeight;
                        return i.xdsoftScroller(s / (i.children()[0].offsetHeight - i[0].clientHeight)), t.stopPropagation(), !1
                    }), D.find(".xdsoft_select").xdsoftScroller().on("mousedown.xdsoft", function(e) {
                        e.stopPropagation(), e.preventDefault()
                    }).on("mousedown.xdsoft", ".xdsoft_option", function() {
                        F && F.currentTime && F.currentTime[e(this).parent().parent().hasClass("xdsoft_monthselect") ? "setMonth" : "setFullYear"](e(this).data("value")), e(this).parent().parent().hide(), S.trigger("xchange.xdsoft"), _.onChangeMonth && _.onChangeMonth.call && _.onChangeMonth.call(S, F.currentTime, S.data("input"))
                    }), S.setOptions = function(i) {
                        if (_ = e.extend(!0, {}, _, i), i.allowTimes && e.isArray(i.allowTimes) && i.allowTimes.length && (_.allowTimes = e.extend(!0, [], i.allowTimes)), i.weekends && e.isArray(i.weekends) && i.weekends.length && (_.weekends = e.extend(!0, [], i.weekends)), !_.open && !_.opened || _.inline || t.trigger("open.xdsoft"), _.inline && (S.addClass("xdsoft_inline"), t.after(S).hide(), S.trigger("afterOpen.xdsoft")), _.inverseButton && (_.next = "xdsoft_prev", _.prev = "xdsoft_next"), _.datepicker ? T.addClass("active") : T.removeClass("active"), _.timepicker ? O.addClass("active") : O.removeClass("active"), _.value && (t && t.val && t.val(_.value), F.setCurrentTime(_.value)), _.dayOfWeekStart = isNaN(_.dayOfWeekStart) || parseInt(_.dayOfWeekStart) < 0 || parseInt(_.dayOfWeekStart) > 6 ? 0 : parseInt(_.dayOfWeekStart), _.timepickerScrollbar || I.hide(), _.minDate && /^-(.*)$/.test(_.minDate) && (_.minDate = F.strToDateTime(_.minDate).dateFormat(_.formatDate)), _.maxDate && /^\+(.*)$/.test(_.maxDate) && (_.maxDate = F.strToDateTime(_.maxDate).dateFormat(_.formatDate)), D.find(".xdsoft_today_button").css("visibility", _.todayButton ? "visible" : "hidden"), _.mask) {
                            var C = function(e) {
                                    try {
                                        if (document.selection && document.selection.createRange) {
                                            var t = document.selection.createRange();
                                            return t.getBookmark().charCodeAt(2) - 2
                                        }
                                        if (e.setSelectionRange) return e.selectionStart
                                    } catch (i) {
                                        return 0
                                    }
                                },
                                E = function(e, t) {
                                    var e = "string" == typeof e || e instanceof String ? document.getElementById(e) : e;
                                    if (!e) return !1;
                                    if (e.createTextRange) {
                                        var i = e.createTextRange();
                                        return i.collapse(!0), i.moveEnd(t), i.moveStart(t), i.select(), !0
                                    }
                                    return e.setSelectionRange ? (e.setSelectionRange(t, t), !0) : !1
                                },
                                A = function(e, t) {
                                    var i = e.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, "\\$1").replace(/_/g, "{digit+}").replace(/([0-9]{1})/g, "{digit$1}").replace(/\{digit([0-9]{1})\}/g, "[0-$1_]{1}").replace(/\{digit[\+]\}/g, "[0-9_]{1}");
                                    return RegExp(i).test(t)
                                };
                            switch (t.off("keydown.xdsoft"), !0) {
                                case _.mask === !0:
                                    _.mask = _.format.replace(/Y/g, "9999").replace(/F/g, "9999").replace(/m/g, "19").replace(/d/g, "39").replace(/H/g, "29").replace(/i/g, "59").replace(/s/g, "59");
                                case "string" == e.type(_.mask):
                                    A(_.mask, t.val()) || t.val(_.mask.replace(/[0-9]/g, "_")), t.on("keydown.xdsoft", function(i) {
                                        var S = this.value,
                                            T = i.which;
                                        switch (!0) {
                                            case T >= n && s >= T || T >= o && r >= T || T == u || T == l:
                                                var D = C(this),
                                                    O = T != u && T != l ? String.fromCharCode(T >= o && r >= T ? T - n : T) : "_";
                                                for (T != u && T != l || !D || (D--, O = "_");
                                                    /[^0-9_]/.test(_.mask.substr(D, 1)) && D < _.mask.length && D > 0;) D += T == u || T == l ? -1 : 1;
                                                if (S = S.substr(0, D) + O + S.substr(D + 1), "" == e.trim(S)) S = _.mask.replace(/[0-9]/g, "_");
                                                else if (D == _.mask.length) break;
                                                for (D += T == u || T == l ? 0 : 1;
                                                    /[^0-9_]/.test(_.mask.substr(D, 1)) && D < _.mask.length && D > 0;) D += T == u || T == l ? -1 : 1;
                                                A(_.mask, S) ? (this.value = S, E(this, D)) : "" == e.trim(S) ? this.value = _.mask.replace(/[0-9]/g, "_") : t.trigger("error_input.xdsoft");
                                                break;
                                            case !!~[y, b, w, x, $].indexOf(T) && k:
                                            case !!~[d, p, m, h, f, v, a, g, c].indexOf(T):
                                                return !0
                                        }
                                        return i.preventDefault(), !1
                                    })
                            }
                        }
                        _.validateOnBlur && t.off("blur.xdsoft").on("blur.xdsoft", function() {
                            _.allowBlank && !e.trim(e(this).val()).length ? (e(this).val(null), S.data("xdsoft_datetime").empty()) : Date.parseDate(e(this).val(), _.format) ? S.data("xdsoft_datetime").setCurrentTime(e(this).val()) : (e(this).val(F.now().dateFormat(_.format)), S.data("xdsoft_datetime").setCurrentTime(e(this).val())), S.trigger("changedatetime.xdsoft")
                        }), _.dayOfWeekStartPrev = 0 == _.dayOfWeekStart ? 6 : _.dayOfWeekStart - 1, S.trigger("xchange.xdsoft")
                    }, S.data("options", _).on("mousedown.xdsoft", function(e) {
                        return e.stopPropagation(), e.preventDefault(), j.hide(), N.hide(), !1
                    });
                    var P = O.find(".xdsoft_time_box");
                    P.append(M), P.xdsoftScroller(), S.on("afterOpen.xdsoft", function() {
                        P.xdsoftScroller()
                    }), S.append(T).append(O), _.withoutCopyright !== !0 && S.append(C), T.append(D).append(E), e("body").append(S);
                    var F = new function() {
                        var e = this;
                        e.now = function() {
                            var e = new Date;
                            return _.yearOffset && e.setFullYear(e.getFullYear() + _.yearOffset), e
                        }, e.currentTime = this.now(), e.isValidDate = function(e) {
                            return "[object Date]" !== Object.prototype.toString.call(e) ? !1 : !isNaN(e.getTime())
                        }, e.setCurrentTime = function(t) {
                            e.currentTime = "string" == typeof t ? e.strToDateTime(t) : e.isValidDate(t) ? t : e.now(), S.trigger("xchange.xdsoft")
                        }, e.empty = function() {
                            e.currentTime = null
                        }, e.getCurrentTime = function() {
                            return e.currentTime
                        }, e.nextMonth = function() {
                            var t = e.currentTime.getMonth() + 1;
                            return 12 == t && (e.currentTime.setFullYear(e.currentTime.getFullYear() + 1), t = 0), e.currentTime.setDate(Math.min(Date.daysInMonth[t], e.currentTime.getDate())), e.currentTime.setMonth(t), _.onChangeMonth && _.onChangeMonth.call && _.onChangeMonth.call(S, F.currentTime, S.data("input")), S.trigger("xchange.xdsoft"), t
                        }, e.prevMonth = function() {
                            var t = e.currentTime.getMonth() - 1;
                            return -1 == t && (e.currentTime.setFullYear(e.currentTime.getFullYear() - 1), t = 11), e.currentTime.setDate(Math.min(Date.daysInMonth[t], e.currentTime.getDate())), e.currentTime.setMonth(t), _.onChangeMonth && _.onChangeMonth.call && _.onChangeMonth.call(S, F.currentTime, S.data("input")), S.trigger("xchange.xdsoft"), t
                        }, e.strToDateTime = function(t) {
                            var i, n, s = [];
                            return (s = /^(\+|\-)(.*)$/.exec(t)) && (s[2] = Date.parseDate(s[2], _.formatDate)) ? (i = s[2].getTime() - 1 * s[2].getTimezoneOffset() * 6e4, n = new Date(F.now().getTime() + parseInt(s[1] + "1") * i)) : n = t ? Date.parseDate(t, _.format) : e.now(), e.isValidDate(n) || (n = e.now()), n
                        }, e.strtodate = function(t) {
                            var i = t ? Date.parseDate(t, _.formatDate) : e.now();
                            return e.isValidDate(i) || (i = e.now()), i
                        }, e.strtotime = function(t) {
                            var i = t ? Date.parseDate(t, _.formatTime) : e.now();
                            return e.isValidDate(i) || (i = e.now()), i
                        }, e.str = function() {
                            return e.currentTime.dateFormat(_.format)
                        }
                    };
                    D.find(".xdsoft_today_button").on("mousedown.xdsoft", function() {
                        S.data("changed", !0), F.setCurrentTime(0), S.trigger("afterOpen.xdsoft")
                    }).on("dblclick.xdsoft", function() {
                        t.val(F.str()), S.trigger("close.xdsoft")
                    }), D.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft", function() {
                        var t = e(this),
                            i = 0,
                            n = !1;
                        ! function s(e) {
                            F.currentTime.getMonth();
                            t.hasClass(_.next) ? F.nextMonth() : t.hasClass(_.prev) && F.prevMonth(), !n && (i = setTimeout(s, e ? e : 100))
                        }(500), e([document.body, window]).on("mouseup.xdsoft", function o() {
                            clearTimeout(i), n = !0, e([document.body, window]).off("mouseup.xdsoft", o)
                        })
                    }), O.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft", function() {
                        var t = e(this),
                            i = 0,
                            n = !1,
                            s = 110;
                        ! function o(e) {
                            var r = A[0].clientHeight,
                                a = M[0].offsetHeight,
                                l = Math.abs(parseInt(M.css("marginTop")));
                            t.hasClass(_.next) && a - r - _.timeHeightInTimePicker >= l ? M.css("marginTop", "-" + (l + _.timeHeightInTimePicker) + "px") : t.hasClass(_.prev) && l - _.timeHeightInTimePicker >= 0 && M.css("marginTop", "-" + (l - _.timeHeightInTimePicker) + "px"), A.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(M.css("marginTop")) / (a - r))]), s = s > 10 ? 10 : s - 10, !n && (i = setTimeout(o, e ? e : s))
                        }(500), e([document.body, window]).on("mouseup.xdsoft", function r() {
                            clearTimeout(i), n = !0, e([document.body, window]).off("mouseup.xdsoft", r)
                        })
                    });
                    var H = 0;
                    S.on("xchange.xdsoft", function(t) {
                        clearTimeout(H), H = setTimeout(function() {
                            for (var t = "", i = new Date(F.currentTime.getFullYear(), F.currentTime.getMonth(), 1, 12, 0, 0), n = 0, s = F.now(); i.getDay() != _.dayOfWeekStart;) i.setDate(i.getDate() - 1);
                            t += "<table><thead><tr>";
                            for (var o = 0; 7 > o; o++) t += "<th>" + _.i18n[_.lang].dayOfWeek[o + _.dayOfWeekStart > 6 ? 0 : o + _.dayOfWeekStart] + "</th>";
                            t += "</tr></thead>", t += "<tbody><tr>";
                            var r = !1,
                                a = !1;
                            _.maxDate !== !1 && (r = F.strtodate(_.maxDate), r = new Date(r.getFullYear(), r.getMonth(), r.getDate(), 23, 59, 59, 999)), _.minDate !== !1 && (a = F.strtodate(_.minDate), a = new Date(a.getFullYear(), a.getMonth(), a.getDate()));
                            for (var l, c, d, u = []; n < F.currentTime.getDaysInMonth() || i.getDay() != _.dayOfWeekStart || F.currentTime.getMonth() == i.getMonth();) u = [], n++, l = i.getDate(), c = i.getFullYear(), d = i.getMonth(), u.push("xdsoft_date"), (r !== !1 && i > r || a !== !1 && a > i) && u.push("xdsoft_disabled"), F.currentTime.getMonth() != d && u.push("xdsoft_other_month"), (_.defaultSelect || S.data("changed")) && F.currentTime.dateFormat("d.m.Y") == i.dateFormat("d.m.Y") && u.push("xdsoft_current"), s.dateFormat("d.m.Y") == i.dateFormat("d.m.Y") && u.push("xdsoft_today"), (0 == i.getDay() || 6 == i.getDay() || ~_.weekends.indexOf(i.dateFormat("d.m.Y"))) && u.push("xdsoft_weekend"), _.beforeShowDay && "function" == typeof _.beforeShowDay && u.push(_.beforeShowDay(i)), t += '<td data-date="' + l + '" data-month="' + d + '" data-year="' + c + '" class="xdsoft_date xdsoft_day_of_week' + i.getDay() + " " + u.join(" ") + '"><div>' + l + "</div></td>", i.getDay() == _.dayOfWeekStartPrev && (t += "</tr>"), i.setDate(l + 1);
                            t += "</tbody></table>", E.html(t), D.find(".xdsoft_label span").eq(0).text(_.i18n[_.lang].months[F.currentTime.getMonth()]), D.find(".xdsoft_label span").eq(1).text(F.currentTime.getFullYear());
                            var h = "",
                                p = "",
                                d = "",
                                f = function(e, t) {
                                    var i = F.now();
                                    i.setHours(e), e = parseInt(i.getHours()), i.setMinutes(t), t = parseInt(i.getMinutes()), u = [], (_.maxTime !== !1 && F.strtotime(_.maxTime).getTime() < i.getTime() || _.minTime !== !1 && F.strtotime(_.minTime).getTime() > i.getTime()) && u.push("xdsoft_disabled"), (_.initTime || _.defaultSelect || S.data("changed")) && parseInt(F.currentTime.getHours()) == parseInt(e) && (_.step > 59 || Math[_.roundTime](F.currentTime.getMinutes() / _.step) * _.step == parseInt(t)) && (_.defaultSelect || S.data("changed") ? u.push("xdsoft_current") : _.initTime && u.push("xdsoft_init_time")), parseInt(s.getHours()) == parseInt(e) && parseInt(s.getMinutes()) == parseInt(t) && u.push("xdsoft_today"), h += '<div class="xdsoft_time ' + u.join(" ") + '" data-hour="' + e + '" data-minute="' + t + '">' + i.dateFormat(_.formatTime) + "</div>"
                                };
                            if (_.allowTimes && e.isArray(_.allowTimes) && _.allowTimes.length)
                                for (var n = 0; n < _.allowTimes.length; n++) p = F.strtotime(_.allowTimes[n]).getHours(), d = F.strtotime(_.allowTimes[n]).getMinutes(), f(p, d);
                            else
                                for (var n = 0, o = 0; n < (_.hours12 ? 12 : 24); n++)
                                    for (o = 0; 60 > o; o += _.step) p = (10 > n ? "0" : "") + n, d = (10 > o ? "0" : "") + o, f(p, d);
                            M.html(h);
                            var m = "",
                                n = 0;
                            for (n = parseInt(_.yearStart, 10) + _.yearOffset; n <= parseInt(_.yearEnd, 10) + _.yearOffset; n++) m += '<div class="xdsoft_option ' + (F.currentTime.getFullYear() == n ? "xdsoft_current" : "") + '" data-value="' + n + '">' + n + "</div>";
                            for (j.children().eq(0).html(m), n = 0, m = ""; 11 >= n; n++) m += '<div class="xdsoft_option ' + (F.currentTime.getMonth() == n ? "xdsoft_current" : "") + '" data-value="' + n + '">' + _.i18n[_.lang].months[n] + "</div>";
                            N.children().eq(0).html(m), e(this).trigger("generate.xdsoft")
                        }, 10), t.stopPropagation()
                    }).on("afterOpen.xdsoft", function() {
                        if (_.timepicker) {
                            var e;
                            if (M.find(".xdsoft_current").length ? e = ".xdsoft_current" : M.find(".xdsoft_init_time").length && (e = ".xdsoft_init_time"), e) {
                                var t = A[0].clientHeight,
                                    i = M[0].offsetHeight,
                                    n = M.find(e).index() * _.timeHeightInTimePicker + 1;
                                n > i - t && (n = i - t), M.css("marginTop", "-" + parseInt(n) + "px"), A.trigger("scroll_element.xdsoft_scroller", [parseInt(n) / (i - t)])
                            }
                        }
                    });
                    var L = 0;
                    E.on("click.xdsoft", "td", function(i) {
                        i.stopPropagation(), L++;
                        var n = e(this),
                            s = F.currentTime;
                        return n.hasClass("xdsoft_disabled") ? !1 : (s.setDate(n.data("date")), s.setMonth(n.data("month")), s.setFullYear(n.data("year")), S.trigger("select.xdsoft", [s]), t.val(F.str()), (L > 1 || _.closeOnDateSelect === !0 || 0 === _.closeOnDateSelect && !_.timepicker) && !_.inline && S.trigger("close.xdsoft"), _.onSelectDate && _.onSelectDate.call && _.onSelectDate.call(S, F.currentTime, S.data("input")), S.data("changed", !0), S.trigger("xchange.xdsoft"), S.trigger("changedatetime.xdsoft"), void setTimeout(function() {
                            L = 0
                        }, 200))
                    }), M.on("click.xdsoft", "div", function(t) {
                        t.stopPropagation();
                        var i = e(this),
                            n = F.currentTime;
                        return i.hasClass("xdsoft_disabled") ? !1 : (n.setHours(i.data("hour")), n.setMinutes(i.data("minute")), S.trigger("select.xdsoft", [n]), S.data("input").val(F.str()), !_.inline && S.trigger("close.xdsoft"), _.onSelectTime && _.onSelectTime.call && _.onSelectTime.call(S, F.currentTime, S.data("input")), S.data("changed", !0), S.trigger("xchange.xdsoft"), void S.trigger("changedatetime.xdsoft"))
                    }), S.mousewheel && T.mousewheel(function(e, t) {
                        return _.scrollMonth ? (0 > t ? F.nextMonth() : F.prevMonth(), !1) : !0
                    }), S.mousewheel && A.unmousewheel().mousewheel(function(e, t) {
                        if (!_.scrollTime) return !0;
                        var i = A[0].clientHeight,
                            n = M[0].offsetHeight,
                            s = Math.abs(parseInt(M.css("marginTop"))),
                            o = !0;
                        return 0 > t && n - i - _.timeHeightInTimePicker >= s ? (M.css("marginTop", "-" + (s + _.timeHeightInTimePicker) + "px"), o = !1) : t > 0 && s - _.timeHeightInTimePicker >= 0 && (M.css("marginTop", "-" + (s - _.timeHeightInTimePicker) + "px"), o = !1), A.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(M.css("marginTop")) / (n - i))]), e.stopPropagation(), o
                    }), S.on("changedatetime.xdsoft", function() {
                        if (_.onChangeDateTime && _.onChangeDateTime.call) {
                            var e = S.data("input");
                            _.onChangeDateTime.call(S, F.currentTime, e), e.trigger("change")
                        }
                    }).on("generate.xdsoft", function() {
                        _.onGenerate && _.onGenerate.call && _.onGenerate.call(S, F.currentTime, S.data("input"))
                    });
                    var z = 0;
                    t.mousewheel && t.mousewheel(function(e, i, n, s) {
                        return _.scrollInput ? !_.datepicker && _.timepicker ? (z = M.find(".xdsoft_current").length ? M.find(".xdsoft_current").eq(0).index() : 0, z + i >= 0 && z + i < M.children().length && (z += i), M.children().eq(z).length && M.children().eq(z).trigger("mousedown"), !1) : _.datepicker && !_.timepicker ? (T.trigger(e, [i, n, s]), t.val && t.val(F.str()), S.trigger("changedatetime.xdsoft"), !1) : void 0 : !0
                    });
                    var q = function() {
                        var t = S.data("input").offset(),
                            i = t.top + S.data("input")[0].offsetHeight - 1,
                            n = t.left;
                        i + S[0].offsetHeight > e(window).height() + e(window).scrollTop() && (i = t.top - S[0].offsetHeight + 1), 0 > i && (i = 0), n + S[0].offsetWidth > e(window).width() && (n = t.left - S[0].offsetWidth + S.data("input")[0].offsetWidth), S.css({
                            left: n,
                            top: i
                        })
                    };
                    S.on("open.xdsoft", function() {
                        var t = !0;
                        _.onShow && _.onShow.call && (t = _.onShow.call(S, F.currentTime, S.data("input"))), t !== !1 && (S.show(), S.trigger("afterOpen.xdsoft"), q(), e(window).off("resize.xdsoft", q).on("resize.xdsoft", q), _.closeOnWithoutClick && e([document.body, window]).on("mousedown.xdsoft", function i() {
                            S.trigger("close.xdsoft"), e([document.body, window]).off("mousedown.xdsoft", i)
                        }))
                    }).on("close.xdsoft", function(e) {
                        var t = !0;
                        _.onClose && _.onClose.call && (t = _.onClose.call(S, F.currentTime, S.data("input"))), t === !1 || _.opened || _.inline || S.hide(), e.stopPropagation()
                    }).data("input", t);
                    var W = 0;
                    S.data("xdsoft_datetime", F), S.setOptions(_), F.setCurrentTime(i()), S.trigger("afterOpen.xdsoft"), t.data("xdsoft_datetimepicker", S).on("open.xdsoft focusin.xdsoft mousedown.xdsoft", function() {
                        t.is(":disabled") || t.is(":hidden") || !t.is(":visible") || (clearTimeout(W), W = setTimeout(function() {
                            t.is(":disabled") || t.is(":hidden") || !t.is(":visible") || (F.setCurrentTime(i()), S.trigger("open.xdsoft"))
                        }, 100))
                    }).on("keydown.xdsoft", function(t) {
                        var i = (this.value, t.which);
                        switch (!0) {
                            case !!~[c].indexOf(i):
                                var n = e("input:visible,textarea:visible");
                                return S.trigger("close.xdsoft"), n.eq(n.index(this) + 1).focus(), !1;
                            case !!~[g].indexOf(i):
                                return S.trigger("close.xdsoft"), !0
                        }
                    })
                },
                D = function(t) {
                    var i = t.data("xdsoft_datetimepicker");
                    i && (i.data("xdsoft_datetime", null), i.remove(), t.data("xdsoft_datetimepicker", null).off("open.xdsoft focusin.xdsoft focusout.xdsoft mousedown.xdsoft blur.xdsoft keydown.xdsoft"), e(window).off("resize.xdsoft"), e([window, document.body]).off("mousedown.xdsoft"), t.unmousewheel && t.unmousewheel())
                };
            return e(document).off("keydown.xdsoftctrl keyup.xdsoftctrl").on("keydown.xdsoftctrl", function(e) {
                e.keyCode == a && (k = !0)
            }).on("keyup.xdsoftctrl", function(e) {
                e.keyCode == a && (k = !1)
            }), this.each(function() {
                var t;
                if (t = e(this).data("xdsoft_datetimepicker")) {
                    if ("string" === e.type(i)) switch (i) {
                        case "show":
                            e(this).select().focus(), t.trigger("open.xdsoft");
                            break;
                        case "hide":
                            t.trigger("close.xdsoft");
                            break;
                        case "destroy":
                            D(e(this));
                            break;
                        case "reset":
                            this.value = this.defaultValue, this.value && t.data("xdsoft_datetime").isValidDate(Date.parseDate(this.value, _.format)) || t.data("changed", !1), t.data("xdsoft_datetime").setCurrentTime(this.value)
                    } else t.setOptions(i);
                    return 0
                }
                "string" !== e.type(i) && (!_.lazyInit || _.open || _.inline ? T(e(this)) : C(e(this)))
            })
        }
    }(jQuery), Date.parseFunctions = {
        count: 0
    }, Date.parseRegexes = [], Date.formatFunctions = {
        count: 0
    }, Date.prototype.dateFormat = function(e) {
        if ("unixtime" == e) return parseInt(this.getTime() / 1e3);
        null == Date.formatFunctions[e] && Date.createNewFormat(e);
        var t = Date.formatFunctions[e];
        return this[t]()
    }, Date.createNewFormat = function(format) {
        var funcName = "format" + Date.formatFunctions.count++;
        Date.formatFunctions[format] = funcName;
        for (var code = "Date.prototype." + funcName + " = function() {return ", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, code += "'" + String.escape(ch) + "' + ") : code += Date.getFormatCode(ch) : special = !0;
        eval(code.substring(0, code.length - 3) + ";}")
    }, Date.getFormatCode = function(e) {
        switch (e) {
            case "d":
                return "String.leftPad(this.getDate(), 2, '0') + ";
            case "D":
                return "Date.dayNames[this.getDay()].substring(0, 3) + ";
            case "j":
                return "this.getDate() + ";
            case "l":
                return "Date.dayNames[this.getDay()] + ";
            case "S":
                return "this.getSuffix() + ";
            case "w":
                return "this.getDay() + ";
            case "z":
                return "this.getDayOfYear() + ";
            case "W":
                return "this.getWeekOfYear() + ";
            case "F":
                return "Date.monthNames[this.getMonth()] + ";
            case "m":
                return "String.leftPad(this.getMonth() + 1, 2, '0') + ";
            case "M":
                return "Date.monthNames[this.getMonth()].substring(0, 3) + ";
            case "n":
                return "(this.getMonth() + 1) + ";
            case "t":
                return "this.getDaysInMonth() + ";
            case "L":
                return "(this.isLeapYear() ? 1 : 0) + ";
            case "Y":
                return "this.getFullYear() + ";
            case "y":
                return "('' + this.getFullYear()).substring(2, 4) + ";
            case "a":
                return "(this.getHours() < 12 ? 'am' : 'pm') + ";
            case "A":
                return "(this.getHours() < 12 ? 'AM' : 'PM') + ";
            case "g":
                return "((this.getHours() %12) ? this.getHours() % 12 : 12) + ";
            case "G":
                return "this.getHours() + ";
            case "h":
                return "String.leftPad((this.getHours() %12) ? this.getHours() % 12 : 12, 2, '0') + ";
            case "H":
                return "String.leftPad(this.getHours(), 2, '0') + ";
            case "i":
                return "String.leftPad(this.getMinutes(), 2, '0') + ";
            case "s":
                return "String.leftPad(this.getSeconds(), 2, '0') + ";
            case "O":
                return "this.getGMTOffset() + ";
            case "T":
                return "this.getTimezone() + ";
            case "Z":
                return "(this.getTimezoneOffset() * -60) + ";
            default:
                return "'" + String.escape(e) + "' + "
        }
    }, Date.parseDate = function(e, t) {
        if ("unixtime" == t) return new Date(isNaN(parseInt(e)) ? 0 : 1e3 * parseInt(e));
        null == Date.parseFunctions[t] && Date.createParser(t);
        var i = Date.parseFunctions[t];
        return Date[i](e)
    }, Date.createParser = function(format) {
        var funcName = "parse" + Date.parseFunctions.count++,
            regexNum = Date.parseRegexes.length,
            currentGroup = 1;
        Date.parseFunctions[format] = funcName;
        for (var code = "Date." + funcName + " = function(input) {\nvar y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, z = -1;\nvar d = new Date();\ny = d.getFullYear();\nm = d.getMonth();\nd = d.getDate();\nvar results = input.match(Date.parseRegexes[" + regexNum + "]);\nif (results && results.length > 0) {", regex = "", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, regex += String.escape(ch)) : (obj = Date.formatCodeToRegex(ch, currentGroup), currentGroup += obj.g, regex += obj.s, obj.g && obj.c && (code += obj.c)) : special = !0;
        code += "if (y > 0 && z > 0){\nvar doyDate = new Date(y,0);\ndoyDate.setDate(z);\nm = doyDate.getMonth();\nd = doyDate.getDate();\n}", code += "if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0)\n{return new Date(y, m, d, h, i, s);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0)\n{return new Date(y, m, d, h, i);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0)\n{return new Date(y, m, d, h);}\nelse if (y > 0 && m >= 0 && d > 0)\n{return new Date(y, m, d);}\nelse if (y > 0 && m >= 0)\n{return new Date(y, m);}\nelse if (y > 0)\n{return new Date(y);}\n}return null;}", Date.parseRegexes[regexNum] = new RegExp("^" + regex + "$"), eval(code)
    }, Date.formatCodeToRegex = function(e, t) {
        switch (e) {
            case "D":
                return {
                    g: 0,
                    c: null,
                    s: "(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)"
                };
            case "j":
            case "d":
                return {
                    g: 1,
                    c: "d = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,2})"
                };
            case "l":
                return {
                    g: 0,
                    c: null,
                    s: "(?:" + Date.dayNames.join("|") + ")"
                };
            case "S":
                return {
                    g: 0,
                    c: null,
                    s: "(?:st|nd|rd|th)"
                };
            case "w":
                return {
                    g: 0,
                    c: null,
                    s: "\\d"
                };
            case "z":
                return {
                    g: 1,
                    c: "z = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,3})"
                };
            case "W":
                return {
                    g: 0,
                    c: null,
                    s: "(?:\\d{2})"
                };
            case "F":
                return {
                    g: 1,
                    c: "m = parseInt(Date.monthNumbers[results[" + t + "].substring(0, 3)], 10);\n",
                    s: "(" + Date.monthNames.join("|") + ")"
                };
            case "M":
                return {
                    g: 1,
                    c: "m = parseInt(Date.monthNumbers[results[" + t + "]], 10);\n",
                    s: "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"
                };
            case "n":
            case "m":
                return {
                    g: 1,
                    c: "m = parseInt(results[" + t + "], 10) - 1;\n",
                    s: "(\\d{1,2})"
                };
            case "t":
                return {
                    g: 0,
                    c: null,
                    s: "\\d{1,2}"
                };
            case "L":
                return {
                    g: 0,
                    c: null,
                    s: "(?:1|0)"
                };
            case "Y":
                return {
                    g: 1,
                    c: "y = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{4})"
                };
            case "y":
                return {
                    g: 1,
                    c: "var ty = parseInt(results[" + t + "], 10);\ny = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n",
                    s: "(\\d{1,2})"
                };
            case "a":
                return {
                    g: 1,
                    c: "if (results[" + t + "] == 'am') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
                    s: "(am|pm)"
                };
            case "A":
                return {
                    g: 1,
                    c: "if (results[" + t + "] == 'AM') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}",
                    s: "(AM|PM)"
                };
            case "g":
            case "G":
            case "h":
            case "H":
                return {
                    g: 1,
                    c: "h = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{1,2})"
                };
            case "i":
                return {
                    g: 1,
                    c: "i = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{2})"
                };
            case "s":
                return {
                    g: 1,
                    c: "s = parseInt(results[" + t + "], 10);\n",
                    s: "(\\d{2})"
                };
            case "O":
                return {
                    g: 0,
                    c: null,
                    s: "[+-]\\d{4}"
                };
            case "T":
                return {
                    g: 0,
                    c: null,
                    s: "[A-Z]{3}"
                };
            case "Z":
                return {
                    g: 0,
                    c: null,
                    s: "[+-]\\d{1,5}"
                };
            default:
                return {
                    g: 0,
                    c: null,
                    s: String.escape(e)
                }
        }
    }, Date.prototype.getTimezone = function() {
        return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3")
    }, Date.prototype.getGMTOffset = function() {
        return (this.getTimezoneOffset() > 0 ? "-" : "+") + String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset()) / 60), 2, "0") + String.leftPad(Math.abs(this.getTimezoneOffset()) % 60, 2, "0")
    }, Date.prototype.getDayOfYear = function() {
        var e = 0;
        Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
        for (var t = 0; t < this.getMonth(); ++t) e += Date.daysInMonth[t];
        return e + this.getDate()
    }, Date.prototype.getWeekOfYear = function() {
        var e = this.getDayOfYear() + (4 - this.getDay()),
            t = new Date(this.getFullYear(), 0, 1),
            i = 7 - t.getDay() + 4;
        return String.leftPad(Math.ceil((e - i) / 7) + 1, 2, "0")
    }, Date.prototype.isLeapYear = function() {
        var e = this.getFullYear();
        return 0 == (3 & e) && (e % 100 || e % 400 == 0 && e)
    }, Date.prototype.getFirstDayOfMonth = function() {
        var e = (this.getDay() - (this.getDate() - 1)) % 7;
        return 0 > e ? e + 7 : e
    }, Date.prototype.getLastDayOfMonth = function() {
        var e = (this.getDay() + (Date.daysInMonth[this.getMonth()] - this.getDate())) % 7;
        return 0 > e ? e + 7 : e
    }, Date.prototype.getDaysInMonth = function() {
        return Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28, Date.daysInMonth[this.getMonth()]
    }, Date.prototype.getSuffix = function() {
        switch (this.getDate()) {
            case 1:
            case 21:
            case 31:
                return "st";
            case 2:
            case 22:
                return "nd";
            case 3:
            case 23:
                return "rd";
            default:
                return "th"
        }
    }, String.escape = function(e) {
        return e.replace(/('|\\)/g, "\\$1")
    }, String.leftPad = function(e, t, i) {
        var n = new String(e);
        for (null == i && (i = " "); n.length < t;) n = i + n;
        return n
    }, Date.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], Date.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], Date.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], Date.y2kYear = 50, Date.monthNumbers = {
        Jan: 0,
        Feb: 1,
        Mar: 2,
        Apr: 3,
        May: 4,
        Jun: 5,
        Jul: 6,
        Aug: 7,
        Sep: 8,
        Oct: 9,
        Nov: 10,
        Dec: 11
    }, Date.patterns = {
        ISO8601LongPattern: "Y-m-d H:i:s",
        ISO8601ShortPattern: "Y-m-d",
        ShortDatePattern: "n/j/Y",
        LongDatePattern: "l, F d, Y",
        FullDateTimePattern: "l, F d, Y g:i:s A",
        MonthDayPattern: "F d",
        ShortTimePattern: "g:i A",
        LongTimePattern: "g:i:s A",
        SortableDateTimePattern: "Y-m-d\\TH:i:s",
        UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
        YearMonthPattern: "F, Y"
    },
    function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }(function(e) {
        function t(t) {
            var s, o = t || window.event,
                r = [].slice.call(arguments, 1),
                a = 0,
                l = 0,
                c = 0,
                d = 0,
                u = 0;
            return t = e.event.fix(o), t.type = "mousewheel", o.wheelDelta && (a = o.wheelDelta), o.detail && (a = -1 * o.detail), o.deltaY && (c = -1 * o.deltaY, a = c), o.deltaX && (l = o.deltaX, a = -1 * l), void 0 !== o.wheelDeltaY && (c = o.wheelDeltaY), void 0 !== o.wheelDeltaX && (l = -1 * o.wheelDeltaX), d = Math.abs(a), (!i || i > d) && (i = d), u = Math.max(Math.abs(c), Math.abs(l)), (!n || n > u) && (n = u), s = a > 0 ? "floor" : "ceil", a = Math[s](a / i), l = Math[s](l / n), c = Math[s](c / n), r.unshift(t, a, l, c), (e.event.dispatch || e.event.handle).apply(this, r)
        }
        var i, n, s = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            o = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"];
        if (e.event.fixHooks)
            for (var r = s.length; r;) e.event.fixHooks[s[--r]] = e.event.mouseHooks;
        e.event.special.mousewheel = {
            setup: function() {
                if (this.addEventListener)
                    for (var e = o.length; e;) this.addEventListener(o[--e], t, !1);
                else this.onmousewheel = t
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var e = o.length; e;) this.removeEventListener(o[--e], t, !1);
                else this.onmousewheel = null
            }
        }, e.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }),
    function(e) {
        "use strict";

        function t(t, i) {
            var n = this,
                s = t.attr("name") || i.name || "";
            t.parent().hide();
            var o = t.css("width");
            t.parent().show(), "0px" == o && (o = t.outerWidth() + 20), this.$el = t.hide(), this.options = i, this.$parent = e("<div" + e.map(["class", "title"], function(e) {
                var t = n.$el.attr(e) || "";
                return t = ("class" === e ? "ms-parent" + (t ? " " : "") : "") + t, t ? " " + e + '="' + t + '"' : ""
            }).join("") + " />"), this.$choice = e('<button type="button" class="ms-choice"><span class="placeholder">' + i.placeholder + "</span><div></div></button>"), this.$drop = e('<div class="ms-drop ' + i.position + '"></div>'), this.$el.after(this.$parent), this.$parent.append(this.$choice), this.$parent.append(this.$drop), this.$el.prop("disabled") && this.$choice.addClass("disabled"), this.$parent.css("width", i.width || o), this.options.keepOpen || e("body").click(function(t) {
                e(t.target)[0] !== n.$choice[0] && e(t.target).parents(".ms-choice")[0] !== n.$choice[0] && (e(t.target)[0] !== n.$drop[0] && e(t.target).parents(".ms-drop")[0] === n.$drop[0] || !n.options.isOpen || n.close())
            }), this.selectAllName = 'name="selectAll' + s + '"', this.selectGroupName = 'name="selectGroup' + s + '"', this.selectItemName = 'name="selectItem' + s + '"'
        }
        t.prototype = {
            constructor: t,
            init: function() {
                var t = this,
                    i = [];
                this.options.filter && i.push('<div class="ms-search">', '<input type="text" autocomplete="off" autocorrect="off" autocapitilize="off" spellcheck="false">', "</div>"), i.push("<ul>"), this.options.selectAll && !this.options.single && i.push('<li class="ms-select-all">', "<label>", '<input type="checkbox" ' + this.selectAllName + " /> ", this.options.selectAllDelimiter[0] + this.options.selectAllText + this.options.selectAllDelimiter[1], "</label>", "</li>"), e.each(this.$el.children(), function(e, n) {
                    i.push(t.optionToHtml(e, n))
                }), i.push('<li class="ms-no-results">' + this.options.noMatchesFound + "</li>"), i.push("</ul>"), this.$drop.html(i.join("")), this.$drop.find("ul").css("max-height", this.options.maxHeight + "px"), this.$drop.find(".multiple").css("width", this.options.multipleWidth + "px"), this.$searchInput = this.$drop.find(".ms-search input"), this.$selectAll = this.$drop.find("input[" + this.selectAllName + "]"), this.$selectGroups = this.$drop.find("input[" + this.selectGroupName + "]"), this.$selectItems = this.$drop.find("input[" + this.selectItemName + "]:enabled"), this.$disableItems = this.$drop.find("input[" + this.selectItemName + "]:disabled"), this.$noResults = this.$drop.find(".ms-no-results"), this.events(), this.updateSelectAll(!0), this.update(!0), this.options.isOpen && this.open()
            },
            optionToHtml: function(t, i, n, s) {
                var o, r = this,
                    a = e(i),
                    l = [],
                    c = this.options.multiple,
                    d = ["class", "title"],
                    u = e.map(d, function(e) {
                        var t = "class" === e && c,
                            i = a.attr(e) || "";
                        return t || i ? " " + e + '="' + (t ? "multiple" + (i ? " " : "") : "") + i + '"' : ""
                    }).join(""),
                    h = this.options.single ? "radio" : "checkbox";
                if (a.is("option")) {
                    var p = a.val(),
                        f = r.options.textTemplate(a),
                        m = void 0 != r.$el.attr("multiple") ? a.prop("selected") : "selected" == a.attr("selected"),
                        g = this.options.styler(p) ? ' style="' + this.options.styler(p) + '"' : "";
                    o = s || a.prop("disabled"), this.options.blockSeparator > "" && this.options.blockSeparator == a.val() ? l.push("<li" + u + g + ">", '<label class="' + this.options.blockSeparator + (o ? "disabled" : "") + '">', f, "</label>", "</li>") : l.push("<li" + u + g + ">", "<label" + (o ? ' class="disabled"' : "") + ">", '<input type="' + h + '" ' + this.selectItemName + ' value="' + p + '"' + (m ? ' checked="checked"' : "") + (o ? ' disabled="disabled"' : "") + (n ? ' data-group="' + n + '"' : "") + "/> ", f, "</label>", "</li>")
                } else if (!n && a.is("optgroup")) {
                    var v = "group_" + t,
                        y = a.attr("label");
                    o = a.prop("disabled"), l.push('<li class="group">', '<label class="optgroup' + (o ? " disabled" : "") + '" data-group="' + v + '">', this.options.hideOptgroupCheckboxes ? "" : '<input type="checkbox" ' + this.selectGroupName + (o ? ' disabled="disabled"' : "") + " /> ", y, "</label>", "</li>"), e.each(a.children(), function(e, t) {
                        l.push(r.optionToHtml(e, t, v, o))
                    })
                }
                return l.join("")
            },
            events: function() {
                function t(e) {
                    e.preventDefault(), i[i.options.isOpen ? "close" : "open"]()
                }
                var i = this,
                    n = this.$el.parent().closest("label")[0] || e("label[for=" + this.$el.attr("id") + "]")[0];
                n && e(n).off("click").on("click", function(e) {
                    "label" === e.target.nodeName.toLowerCase() && e.target === this && (t(e), i.options.filter && i.options.isOpen || i.focus(), e.stopPropagation())
                }), this.$choice.off("click").on("click", t).off("focus").on("focus", this.options.onFocus).off("blur").on("blur", this.options.onBlur), this.$parent.off("keydown").on("keydown", function(e) {
                    switch (e.which) {
                        case 27:
                            i.close(), i.$choice.focus()
                    }
                }), this.$searchInput.off("keydown").on("keydown", function(e) {
                    9 === e.keyCode && e.shiftKey && i.close()
                }).off("keyup").on("keyup", function(e) {
                    return i.options.filterAcceptOnEnter && (13 === e.which || 32 == e.which) && i.$searchInput.val() ? (i.$selectAll.click(), i.close(), void i.focus()) : void i.filter()
                }), this.$selectAll.off("click").on("click", function() {
                    var t = e(this).prop("checked"),
                        n = i.$selectItems.filter(":visible");
                    n.length === i.$selectItems.length ? i[t ? "checkAll" : "uncheckAll"]() : (i.$selectGroups.prop("checked", t), n.prop("checked", t), i.options[t ? "onCheckAll" : "onUncheckAll"](), i.update())
                }), this.$selectGroups.off("click").on("click", function() {
                    var t = e(this).parent().attr("data-group"),
                        n = i.$selectItems.filter(":visible"),
                        s = n.filter('[data-group="' + t + '"]'),
                        o = s.length !== s.filter(":checked").length;
                    s.prop("checked", o), i.updateSelectAll(), i.update(), i.options.onOptgroupClick({
                        label: e(this).parent().text(),
                        checked: o,
                        children: s.get()
                    })
                }), this.$selectItems.off("click").on("click", function() {
                    i.updateSelectAll(), i.update(), i.updateOptGroupSelect(), i.options.onClick({
                        label: e(this).parent().text(),
                        value: e(this).val(),
                        checked: e(this).prop("checked")
                    }), i.options.single && i.options.isOpen && !i.options.keepOpen && i.close()
                })
            },
            open: function() {
                if (!this.$choice.hasClass("disabled")) {
                    if (this.options.isOpen = !0, this.$choice.find(">div").addClass("open"), this.$drop.show(), this.$selectAll.parent().show(), this.$noResults.hide(), 0 === this.$el.children().length && (this.$selectAll.parent().hide(), this.$noResults.show()), this.options.container) {
                        var t = this.$drop.offset();
                        this.$drop.appendTo(e(this.options.container)), this.$drop.offset({
                            top: t.top,
                            left: t.left
                        })
                    }
                    this.options.filter && (this.$searchInput.val(""), this.$searchInput.focus(), this.filter()), this.options.onOpen()
                }
            },
            close: function() {
                this.options.isOpen = !1, this.$choice.find(">div").removeClass("open"), this.$drop.hide(), this.options.container && (this.$parent.append(this.$drop), this.$drop.css({
                    top: "auto",
                    left: "auto"
                })), this.options.onClose()
            },
            update: function(t) {
                var i = this.getSelects(),
                    n = this.$choice.find(">span");
                0 === i.length ? n.addClass("placeholder").html(this.options.placeholder) : n.removeClass("placeholder").html(this.options.countSelected && i.length < this.options.minimumCountSelected ? (this.options.displayValues ? i : this.getSelects("text")).join(this.options.delimiter) : this.options.allSelected && i.length === this.$selectItems.length + this.$disableItems.length ? this.options.allSelected : (this.options.countSelected || this.options.etcaetera) && i.length > this.options.minimumCountSelected ? this.options.etcaetera ? (this.options.displayValues ? i : this.getSelects("text").slice(0, this.options.minimumCountSelected)).join(this.options.delimiter) + "..." : this.options.countSelected.replace("#", i.length).replace("%", this.$selectItems.length + this.$disableItems.length) : (this.options.displayValues ? i : this.getSelects("text")).join(this.options.delimiter)), this.$el.val(this.getSelects()), this.$drop.find("li").removeClass("selected"), this.$drop.find("input[" + this.selectItemName + "]:checked").each(function() {
                    e(this).parents("li").first().addClass("selected")
                }), t || this.$el.trigger("change")
            },
            updateSelectAll: function(e) {
                var t = this.$selectItems;
                e || (t = t.filter(":visible")), this.$selectAll.prop("checked", t.length && t.length === t.filter(":checked").length), this.$selectAll.prop("checked") && this.options.onCheckAll()
            },
            updateOptGroupSelect: function() {
                var t = this.$selectItems.filter(":visible");
                e.each(this.$selectGroups, function(i, n) {
                    var s = e(n).parent().attr("data-group"),
                        o = t.filter('[data-group="' + s + '"]');
                    e(n).prop("checked", o.length && o.length === o.filter(":checked").length)
                })
            },
            getSelects: function(t) {
                var i = this,
                    n = [],
                    s = [];
                return this.$drop.find("input[" + this.selectItemName + "]:checked").each(function() {
                    n.push(e(this).parents("li").first().text()), s.push(e(this).val())
                }), "text" === t && this.$selectGroups.length && (n = [], this.$selectGroups.each(function() {
                    var t = [],
                        s = e.trim(e(this).parent().text()),
                        o = e(this).parent().data("group"),
                        r = i.$drop.find("[" + i.selectItemName + '][data-group="' + o + '"]'),
                        a = r.filter(":checked");
                    if (0 !== a.length) {
                        if (t.push("["), t.push(s), r.length > a.length) {
                            var l = [];
                            a.each(function() {
                                l.push(e(this).parent().text())
                            }), t.push(": " + l.join(", "))
                        }
                        t.push("]"), n.push(t.join(""))
                    }
                })), "text" === t ? n : s
            },
            setSelects: function(t) {
                var i = this;
                this.$selectItems.prop("checked", !1), e.each(t, function(e, t) {
                    i.$selectItems.filter('[value="' + t + '"]').prop("checked", !0)
                }), this.$selectAll.prop("checked", this.$selectItems.length === this.$selectItems.filter(":checked").length), this.update()
            },
            enable: function() {
                this.$choice.removeClass("disabled")
            },
            disable: function() {
                this.$choice.addClass("disabled")
            },
            checkAll: function() {
                this.$selectItems.prop("checked", !0), this.$selectGroups.prop("checked", !0), this.$selectAll.prop("checked", !0), this.update(), this.options.onCheckAll()
            },
            uncheckAll: function() {
                this.$selectItems.prop("checked", !1), this.$selectGroups.prop("checked", !1), this.$selectAll.prop("checked", !1), this.update(), this.options.onUncheckAll()
            },
            focus: function() {
                this.$choice.focus(), this.options.onFocus()
            },
            blur: function() {
                this.$choice.blur(), this.options.onBlur()
            },
            refresh: function() {
                this.init()
            },
            filter: function() {
                var t = this,
                    i = e.trim(this.$searchInput.val()).toLowerCase();
                0 === i.length ? (this.$selectItems.parent().show(), this.$disableItems.parent().show(), this.$selectGroups.parent().show()) : (this.$selectItems.each(function() {
                    var t = e(this).parent();
                    t[t.text().toLowerCase().indexOf(i) < 0 ? "hide" : "show"]()
                }), this.$disableItems.parent().hide(), this.$selectGroups.each(function() {
                    var i = e(this).parent(),
                        n = i.attr("data-group"),
                        s = t.$selectItems.filter(":visible");
                    i[0 === s.filter('[data-group="' + n + '"]').length ? "hide" : "show"]()
                }), this.$selectItems.filter(":visible").length ? (this.$selectAll.parent().show(), this.$noResults.hide()) : (this.$selectAll.parent().hide(), this.$noResults.show())), this.updateOptGroupSelect(), this.updateSelectAll()
            }
        }, e.fn.multipleSelect = function() {
            var i, n = arguments[0],
                s = arguments,
                o = ["getSelects", "setSelects", "enable", "disable", "checkAll", "uncheckAll", "focus", "blur", "refresh"];
            return this.each(function() {
                var r = e(this),
                    a = r.data("multipleSelect"),
                    l = e.extend({}, e.fn.multipleSelect.defaults, r.data(), "object" == typeof n && n);
                if (a || (a = new t(r, l), r.data("multipleSelect", a)), "string" == typeof n) {
                    if (e.inArray(n, o) < 0) throw "Unknown method: " + n;
                    i = a[n](s[1])
                } else a.init(), s[1] && (i = a[s[1]].apply(a, [].slice.call(s, 2)))
            }), i ? i : this
        }, e.fn.multipleSelect.defaults = {
            name: "",
            isOpen: !1,
            placeholder: "",
            selectAll: !0,
            selectAllText: "Select all",
            selectAllDelimiter: ["[", "]"],
            allSelected: "All selected",
            minimumCountSelected: 3,
            countSelected: "# of % selected",
            noMatchesFound: "No matches found",
            multiple: !1,
            multipleWidth: 80,
            single: !1,
            filter: !1,
            width: void 0,
            maxHeight: 250,
            container: null,
            position: "bottom",
            keepOpen: !1,
            blockSeparator: "",
            displayValues: !1,
            delimiter: ", ",
            styler: function() {
                return !1
            },
            textTemplate: function(e) {
                return e.text()
            },
            onOpen: function() {
                return !1
            },
            onClose: function() {
                return !1
            },
            onCheckAll: function() {
                return !1
            },
            onUncheckAll: function() {
                return !1
            },
            onFocus: function() {
                return !1
            },
            onBlur: function() {
                return !1
            },
            onOptgroupClick: function() {
                return !1
            },
            onClick: function() {
                return !1
            }
        }
    }(jQuery),
    function(e, t) {
        if ("function" == typeof define && define.amd) define(["exports", "jquery"], function(e, i) {
            return t(e, i)
        });
        else if ("undefined" != typeof exports) {
            var i = require("jquery");
            t(exports, i)
        } else t(e, e.jQuery || e.Zepto || e.ender || e.$)
    }(this, function(e, t) {
        function i(e, i) {
            function s(e, t, i) {
                return e[t] = i, e
            }

            function o(e, t) {
                for (var i, o = e.match(n.key); void 0 !== (i = o.pop());)
                    if (n.push.test(i)) {
                        var a = r(e.replace(/\[\]$/, ""));
                        t = s([], a, t)
                    } else n.fixed.test(i) ? t = s([], i, t) : n.named.test(i) && (t = s({}, i, t));
                return t
            }

            function r(e) {
                return void 0 === p[e] && (p[e] = 0), p[e]++
            }

            function a(e) {
                switch (t('[name="' + e.name + '"]', i).attr("type")) {
                    case "checkbox":
                        return "on" === e.value ? !0 : e.value;
                    default:
                        return e.value
                }
            }

            function l(t) {
                if (!n.validate.test(t.name)) return this;
                var i = o(t.name, a(t));
                return h = e.extend(!0, h, i), this
            }

            function c(t) {
                if (!e.isArray(t)) throw new Error("formSerializer.addPairs expects an Array");
                for (var i = 0, n = t.length; n > i; i++) this.addPair(t[i]);
                return this
            }

            function d() {
                return h
            }

            function u() {
                return JSON.stringify(d())
            }
            var h = {},
                p = {};
            this.addPair = l, this.addPairs = c, this.serialize = d, this.serializeJSON = u
        }
        var n = {
            validate: /^[a-z_][a-z0-9_]*(?:\[(?:\d*|[a-z0-9_]+)\])*$/i,
            key: /[a-z0-9_]+|(?=\[\])/gi,
            push: /^$/,
            fixed: /^\d+$/,
            named: /^[a-z0-9_]+$/i
        };
        return i.patterns = n, i.serializeObject = function() {
            return this.length > 1 ? new Error("jquery-serialize-object can only serialize one form at a time") : new i(t, this).addPairs(this.serializeArray()).serialize()
        }, i.serializeJSON = function() {
            return this.length > 1 ? new Error("jquery-serialize-object can only serialize one form at a time") : new i(t, this).addPairs(this.serializeArray()).serializeJSON()
        }, "undefined" != typeof t.fn && (t.fn.serializeObject = i.serializeObject, t.fn.serializeJSON = i.serializeJSON), e.FormSerializer = i, i
    }),
    function(e) {
        "use strict";
        e.fn.lightGallery = function(t) {
            var i, n, s, o, r, a, l, c, d, u, h, p = {
                    mode: "slide",
                    useCSS: !0,
                    cssEasing: "ease",
                    easing: "linear",
                    speed: 600,
                    addClass: "",
                    closable: !0,
                    loop: !1,
                    auto: !1,
                    pause: 4e3,
                    escKey: !0,
                    controls: !0,
                    hideControlOnEnd: !1,
                    preload: 1,
                    showAfterLoad: !0,
                    selector: null,
                    index: !1,
                    html: !1,
                    lang: {
                        allPhotos: "All photos"
                    },
                    counter: !1,
                    exThumbImage: !1,
                    thumbnail: !0,
                    animateThumb: !0,
                    currentPagerPosition: "middle",
                    thumbWidth: 100,
                    thumbMargin: 5,
                    mobileSrc: !1,
                    mobileSrcMaxWidth: 640,
                    swipeThreshold: 50,
                    vimeoColor: "CCCCCC",
                    videoAutoplay: !0,
                    videoMaxWidth: 855,
                    dynamic: !1,
                    dynamicEl: [],
                    onOpen: function() {},
                    onSlideBefore: function() {},
                    onSlideAfter: function() {},
                    onSlideNext: function() {},
                    onSlidePrev: function() {},
                    onBeforeClose: function() {},
                    onCloseAfter: function() {}
                },
                f = e(this),
                m = this,
                g = null,
                v = 0,
                y = !1,
                b = !1,
                w = !1,
                x = void 0 !== document.createTouch || "ontouchstart" in window || "onmsgesturechange" in window || navigator.msMaxTouchPoints,
                $ = !1,
                k = !1,
                _ = !1,
                S = e.extend(!0, {}, p, t),
                C = {
                    init: function() {
                        f.each(function() {
                            var t = e(this);
                            1 == S.dynamic ? (g = S.dynamicEl, v = 0, l = v, T.init(v)) : (g = null !== S.selector ? e(S.selector) : t.children(), g.on("click", function(i) {
                                g = null !== S.selector ? e(S.selector) : t.children(), i.preventDefault(), i.stopPropagation(), v = g.index(this), l = v, T.init(v)
                            }))
                        })
                    }
                },
                T = {
                    init: function() {
                        b = !0, this.structure(), this.getWidth(), this.closeSlide(), this.autoStart(), this.counter(), this.slideTo(), this.buildThumbnail(), this.keyPress(), S.index ? (this.slide(S.index), this.animateThumb(S.index)) : (this.slide(v), this.animateThumb(v)), 0 == y && this.touch(), this.enableTouch(), setTimeout(function() {
                            i.addClass("opacity")
                        }, 50)
                    },
                    structure: function() {
                        e("body").append('<div id="lightGallery-outer" class="' + S.addClass + '"><div id="lightGallery-Gallery"><div id="lightGallery-slider"></div><a id="lightGallery-close" class="close"></a></div></div>').addClass("lightGallery"), n = e("#lightGallery-outer"), i = e("#lightGallery-Gallery"), S.showAfterLoad === !0 && i.addClass("showAfterLoad"), s = i.find("#lightGallery-slider");
                        var t = "";
                        if (1 == S.dynamic)
                            for (var r = 0; r < S.dynamicEl.length; r++) t += '<div class="lightGallery-slide"></div>';
                        else g.each(function() {
                            t += '<div class="lightGallery-slide"></div>'
                        });
                        s.append(t), o = i.find(".lightGallery-slide")
                    },
                    closeSlide: function() {
                        S.closable && e("#lightGallery-outer").on("click", function(t) {
                            y ? (e(t.target).is("#lightGallery-outer") || e(t.target).is(".prevSlide") || e(t.target).is(".nextSlide") || e(t.target).is(".prevSlide *") || e(t.target).is(".nextSlide *")) && m.destroy(!1) : e(t.target).is(".lightGallery-slide") && m.destroy(!1)
                        }), e("#lightGallery-close").bind("click touchend", function() {
                            m.destroy(!1)
                        })
                    },
                    getWidth: function() {
                        var t = function() {
                            u = e(window).width()
                        };
                        e(window).bind("resize.lightGallery", t())
                    },
                    doCss: function() {
                        var e = function() {
                            for (var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"], t = document.documentElement, i = 0; i < e.length; i++)
                                if (e[i] in t.style) return !0
                        };
                        return S.useCSS && e() ? !0 : !1
                    },
                    enableTouch: function() {
                        var t = this;
                        if (x) {
                            var i = {},
                                n = {};
                            e("body").on("touchstart.lightGallery", function(e) {
                                n = e.originalEvent.targetTouches[0], i.pageX = e.originalEvent.targetTouches[0].pageX, i.pageY = e.originalEvent.targetTouches[0].pageY
                            }), e("body").on("touchmove.lightGallery", function(e) {
                                var t = e.originalEvent;
                                n = t.targetTouches[0], e.preventDefault()
                            }), e("body").on("touchend.lightGallery", function() {
                                var e = n.pageX - i.pageX,
                                    s = S.swipeThreshold;
                                e >= s ? (t.prevSlide(), clearInterval(h)) : -s >= e && (t.nextSlide(), clearInterval(h))
                            })
                        }
                    },
                    touch: function() {
                        var t, i, n = this;
                        e(".lightGallery").bind("mousedown", function(e) {
                            e.stopPropagation(), e.preventDefault(), t = e.pageX
                        }), e(".lightGallery").bind("mouseup", function(e) {
                            e.stopPropagation(), e.preventDefault(), i = e.pageX, i - t > 20 ? n.prevSlide() : t - i > 20 && n.nextSlide()
                        })
                    },
                    isVideo: function(e, t) {
                        var i = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9_\-]+)/i),
                            n = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
                            s = !1;
                        return 1 == S.dynamic ? "true" == S.dynamicEl[t].iframe && (s = !0) : "true" === g.eq(t).attr("data-iframe") && (s = !0), i || n || s ? !0 : void 0
                    },
                    loadVideo: function(e, t) {
                        var i = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9_\-]+)/i),
                            n = e.match(/start=(\d+)/),
                            s = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
                            o = "",
                            r = "";
                        return i ? (r = n ? "?start= " + n[1] + "&" : "?", r += S.videoAutoplay === !0 && w === !1 ? "autoplay=1&rel=0&wmode=opaque&enablejsapi=1" : "wmode=opaque&enablejsapi=1", o = '<iframe class="object" width="560" height="315" src="//www.youtube.com/embed/' + i[1] + r + '" frameborder="0" allowfullscreen></iframe>') : s ? (r = S.videoAutoplay === !0 && w === !1 ? "autoplay=1&amp;&api=1" : "?api=1", o = '<iframe class="object" id="video' + t + '" width="560" height="315"  src="http://player.vimeo.com/video/' + s[1] + "?" + r + "byline=0&amp;portrait=0&amp;color=" + S.vimeoColor + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>') : o = '<iframe class="object" frameborder="0" src="' + e + '"  allowfullscreen="true"></iframe>', '<div class="video_cont" style="max-width:' + S.videoMaxWidth + 'px !important;"><div class="video">' + o + "</div></div>"
                    },
                    addHtml: function(t) {
                        if (S.html === !0) {
                            var i = !1,
                                n = null,
                                s = null,
                                r = null;
                            if (1 == S.dynamic ? (s = S.dynamicEl[t].html, r = S.dynamicEl[t].url) : (s = g.eq(t).attr("data-html"), r = g.eq(t).attr("data-url")), "undefined" != typeof s && null !== s) {
                                var a = s.substring(0, 1);
                                n = "." == a || "#" == a ? e(s).html() : s
                            } else "undefined" != typeof r && null !== r && (n = r, i = !0);
                            "undefined" == typeof n || null === n || o.eq(t).hasClass("loaded") || (i ? (o.eq(t).append('<div class="ext info group"></div>'), o.eq(t).find(".ext").load(n)) : o.eq(t).append(n))
                        }
                    },
                    preload: function(e) {
                        for (var t = e, i = 0; i <= S.preload && !(i >= g.length - e); i++) this.loadContent(t + i, !0);
                        for (var n = 0; n <= S.preload && !(0 > t - n); n++) this.loadContent(t - n, !0)
                    },
                    loadObj: function(e, t) {
                        var i = this;
                        o.eq(t).find(".object").on("load error", function() {
                            o.eq(t).addClass("complete")
                        }), e === !1 && (o.eq(t).hasClass("complete") ? i.preload(t) : o.eq(t).find(".object").on("load error", function() {
                            i.preload(t)
                        }))
                    },
                    loadContent: function(e, t) {
                        {
                            var i, s = this;
                            g.length - e
                        }
                        S.preload > g.length && (S.preload = g.length), i = S.mobileSrc === !0 && u <= S.mobileSrcMaxWidth ? 1 == S.dynamic ? S.dynamicEl[e].mobileSrc : g.eq(e).attr("data-responsive-src") : 1 == S.dynamic ? S.dynamicEl[e].src : g.eq(e).attr("data-src");
                        var r = 0;
                        t === !0 && (r = S.speed + 400), "undefined" != typeof i && null !== i ? (y = !1, s.isVideo(i, e) ? setTimeout(function() {
                            o.eq(e).hasClass("loaded") || (o.eq(e).prepend(s.loadVideo(i, e)), s.addHtml(e), o.eq(e).addClass("loaded"), S.auto && S.videoAutoplay === !0 && clearInterval(h)), s.loadObj(t, e)
                        }, r) : setTimeout(function() {
                            o.eq(e).hasClass("loaded") || (o.eq(e).prepend('<img class="object" src="' + i + '" />'), s.addHtml(e), o.eq(e).addClass("loaded")), s.loadObj(t, e)
                        }, r)) : (y = !0, s.addHtml(e), n.addClass("external"), o.eq(e).addClass("loaded complete"))
                    },
                    counter: function() {
                        if (S.counter === !0) {
                            var t = e("#lightGallery-slider > div").length;
                            i.append("<div id='lightGallery_counter'><span id='lightGallery_counter_current'></span> / <span id='lightGallery_counter_all'>" + t + "</span></div>")
                        }
                    },
                    buildThumbnail: function() {
                        if (S.thumbnail === !0 && g.length > 1) {
                            var t = this;
                            i.append('<div class="thumb_cont"><div class="thumb_info"><span class="close ib"><i class="bUi-iCn-rMv-16" aria-hidden="true"></i></span></div><div class="thumb_inner"></div></div>'), c = i.find(".thumb_cont"), r.after('<a class="cLthumb"></a>'), r.parent().addClass("hasThumb"), i.find(".cLthumb").bind("click touchend", function() {
                                c.addClass("open"), t.doCss() && "slide" === S.mode && (o.eq(v).prevAll().removeClass("nextSlide").addClass("prevSlide"), o.eq(v).nextAll().removeClass("prevSlide").addClass("nextSlide"))
                            }), i.find(".close").bind("click touchend", function() {
                                c.removeClass("open")
                            });
                            var n, s = i.find(".thumb_info"),
                                a = i.find(".thumb_inner"),
                                l = "";
                            if (1 == S.dynamic)
                                for (var u = 0; u < S.dynamicEl.length; u++) n = S.dynamicEl[u].thumb, l += '<div class="thumb"><img src="' + n + '" /></div>';
                            else g.each(function() {
                                n = S.exThumbImage === !1 || "undefined" == typeof e(this).attr(S.exThumbImage) || null == e(this).attr(S.exThumbImage) ? e(this).find("img").attr("src") : e(this).attr(S.exThumbImage), l += '<div class="thumb"><img src="' + n + '" /></div>'
                            });
                            if (a.append(l), d = a.find(".thumb"), d.css({
                                    "margin-right": S.thumbMargin + "px",
                                    width: S.thumbWidth + "px"
                                }), S.animateThumb === !0) {
                                var p = g.length * (S.thumbWidth + S.thumbMargin);
                                i.find(".thumb_inner").css({
                                    width: p + "px",
                                    position: "relative",
                                    "transition-duration": S.speed + "ms"
                                })
                            }
                            d.bind("click touchend", function() {
                                $ = !0;
                                var i = e(this).index();
                                d.removeClass("active"), e(this).addClass("active"), t.slide(i), t.animateThumb(i), clearInterval(h)
                            }), s.prepend('<span class="ib count">' + S.lang.allPhotos + " (" + d.length + ")</span>")
                        }
                    },
                    animateThumb: function(e) {
                        if (S.animateThumb === !0) {
                            var t, n = i.find(".thumb_cont").width();
                            switch (S.currentPagerPosition) {
                                case "left":
                                    t = 0;
                                    break;
                                case "middle":
                                    t = n / 2 - S.thumbWidth / 2;
                                    break;
                                case "right":
                                    t = n - S.thumbWidth
                            }
                            var s = (S.thumbWidth + S.thumbMargin) * e - 1 - t,
                                o = g.length * (S.thumbWidth + S.thumbMargin);
                            s > o - n && (s = o - n), 0 > s && (s = 0), this.doCss() ? i.find(".thumb_inner").css("transform", "translate3d(-" + s + "px, 0px, 0px)") : i.find(".thumb_inner").animate({
                                left: -s + "px"
                            }, S.speed)
                        }
                    },
                    slideTo: function() {
                        var e = this;
                        S.controls === !0 && g.length > 1 && (i.append('<div id="lightGallery-action"><a id="lightGallery-prev"></a><a id="lightGallery-next"></a></div>'), r = i.find("#lightGallery-prev"), a = i.find("#lightGallery-next"), r.bind("click", function() {
                            e.prevSlide(), clearInterval(h)
                        }), a.bind("click", function() {
                            e.nextSlide(), clearInterval(h)
                        }))
                    },
                    autoStart: function() {
                        var e = this;
                        S.auto === !0 && (h = setInterval(function() {
                            v = v + 1 < g.length ? v : -1, v++, e.slide(v)
                        }, S.pause))
                    },
                    keyPress: function() {
                        var t = this;
                        e(window).bind("keyup.lightGallery", function(e) {
                            e.preventDefault(), e.stopPropagation(), 37 === e.keyCode && (t.prevSlide(), clearInterval(h)), 38 === e.keyCode && S.thumbnail === !0 ? c.hasClass("open") || (t.doCss() && "slide" === S.mode && (o.eq(v).prevAll().removeClass("nextSlide").addClass("prevSlide"), o.eq(v).nextAll().removeClass("prevSlide").addClass("nextSlide")), c.addClass("open")) : 39 === e.keyCode && (t.nextSlide(), clearInterval(h)), 40 === e.keyCode && S.thumbnail === !0 ? c.hasClass("open") && c.removeClass("open") : S.escKey === !0 && 27 === e.keyCode && (S.thumbnail === !0 && c.hasClass("open") ? c.removeClass("open") : m.destroy(!1))
                        })
                    },
                    nextSlide: function() {
                        var e = this;
                        v = o.index(o.eq(l)), v + 1 < g.length ? (v++, e.slide(v)) : S.loop ? (v = 0, e.slide(v)) : S.thumbnail === !0 && g.length > 1 ? c.addClass("open") : (o.eq(v).find(".object").addClass("rightEnd"), setTimeout(function() {
                            o.find(".object").removeClass("rightEnd")
                        }, 400)), e.animateThumb(v), S.onSlideNext.call(this, m)
                    },
                    prevSlide: function() {
                        var e = this;
                        v = o.index(o.eq(l)), v > 0 ? (v--, e.slide(v)) : S.loop ? (v = g.length - 1, e.slide(v)) : S.thumbnail === !0 && g.length > 1 ? c.addClass("open") : (o.eq(v).find(".object").addClass("leftEnd"), setTimeout(function() {
                            o.find(".object").removeClass("leftEnd")
                        }, 400)), e.animateThumb(v), S.onSlidePrev.call(this, m)
                    },
                    slide: function(t) {
                        var i = this;
                        if (w ? (setTimeout(function() {
                                i.loadContent(t, !1)
                            }, S.speed + 400), s.hasClass("on") || s.addClass("on"), this.doCss() && "" !== S.speed && (s.hasClass("speed") || s.addClass("speed"), _ === !1 && (s.css("transition-duration", S.speed + "ms"), _ = !0)), this.doCss() && "" !== S.cssEasing && (s.hasClass("timing") || s.addClass("timing"), k === !1 && (s.css("transition-timing-function", S.cssEasing), k = !0)), S.onSlideBefore.call(this, m)) : i.loadContent(t, !1), "slide" === S.mode) {
                            var n = null != navigator.userAgent.match(/iPad/i);
                            !this.doCss() || s.hasClass("slide") || n ? this.doCss() && !s.hasClass("useLeft") && n && s.addClass("useLeft") : s.addClass("slide"), this.doCss() || w ? !this.doCss() && w && s.animate({
                                left: 100 * -t + "%"
                            }, S.speed, S.easing) : s.css({
                                left: 100 * -t + "%"
                            })
                        } else "fade" === S.mode && (this.doCss() && !s.hasClass("fadeM") ? s.addClass("fadeM") : this.doCss() || s.hasClass("animate") || s.addClass("animate"), this.doCss() || w ? !this.doCss() && w && (o.eq(l).fadeOut(S.speed, S.easing), o.eq(t).fadeIn(S.speed, S.easing)) : (o.fadeOut(100), o.eq(t).fadeIn(100)));
                        if (t + 1 >= g.length && S.auto && S.loop === !1 && clearInterval(h), o.eq(l).removeClass("current"), o.eq(t).addClass("current"), this.doCss() && "slide" === S.mode && ($ === !1 ? (e(".prevSlide").removeClass("prevSlide"), e(".nextSlide").removeClass("nextSlide"), o.eq(t - 1).addClass("prevSlide"), o.eq(t + 1).addClass("nextSlide")) : (o.eq(t).prevAll().removeClass("nextSlide").addClass("prevSlide"), o.eq(t).nextAll().removeClass("prevSlide").addClass("nextSlide"))), S.thumbnail === !0 && g.length > 1 && (d.removeClass("active"), d.eq(t).addClass("active")), S.controls && S.hideControlOnEnd && S.loop === !1 && g.length > 1) {
                            var c = g.length;
                            c = parseInt(c) - 1, 0 === t ? (r.addClass("disabled"), a.removeClass("disabled")) : t === c ? (r.removeClass("disabled"), a.addClass("disabled")) : r.add(a).removeClass("disabled")
                        }
                        l = t, w === !1 ? S.onOpen.call(this, m) : S.onSlideAfter.call(this, m), setTimeout(function() {
                            w = !0
                        }), $ = !1, S.counter && e("#lightGallery_counter_current").text(t + 1), y && (o.css("height", e(window).height()), e(window).on("resize.lightGallery", function() {
                            o.css("height", e(window).height())
                        })), e(window).bind("resize.lightGallery", function() {
                            setTimeout(function() {
                                i.animateThumb(t)
                            }, 200)
                        })
                    }
                };
            return m.isActive = function() {
                return b === !0 ? !0 : !1
            }, m.destroy = function(t) {
                b = !1, t = "undefined" != typeof t ? !1 : !0, S.onBeforeClose.call(this, m);
                var s = w;
                w = !1, k = !1, _ = !1, $ = !1, clearInterval(h), t === !0 && g.off("click touch touchstart"), e(".lightGallery").off("mousedown mouseup"), e("body").off("touchstart.lightGallery touchmove.lightGallery touchend.lightGallery"), e(window).off("resize.lightGallery keyup.lightGallery"), s === !0 && (i.addClass("fadeM"), setTimeout(function() {
                    n.remove(), e("body").removeClass("lightGallery")
                }, 500)), S.onCloseAfter.call(this, m)
            }, C.init(), this
        }
    }(jQuery),
    function(e, t) {
        "function" == typeof define && define.amd ? define("sifter", t) : "object" == typeof exports ? module.exports = t() : e.Sifter = t()
    }(this, function() {
        var e = function(e, t) {
            this.items = e, this.settings = t || {
                diacritics: !0
            }
        };
        e.prototype.tokenize = function(e) {
            if (e = n(String(e || "").toLowerCase()), !e || !e.length) return [];
            var t, i, o, a, l = [],
                c = e.split(/ +/);
            for (t = 0, i = c.length; i > t; t++) {
                if (o = s(c[t]), this.settings.diacritics)
                    for (a in r) r.hasOwnProperty(a) && (o = o.replace(new RegExp(a, "g"), r[a]));
                l.push({
                    string: c[t],
                    regex: new RegExp(o, "i")
                })
            }
            return l
        }, e.prototype.iterator = function(e, t) {
            var i;
            i = o(e) ? Array.prototype.forEach || function(e) {
                for (var t = 0, i = this.length; i > t; t++) e(this[t], t, this)
            } : function(e) {
                for (var t in this) this.hasOwnProperty(t) && e(this[t], t, this)
            }, i.apply(e, [t])
        }, e.prototype.getScoreFunction = function(e, t) {
            var i, n, s, o;
            i = this, e = i.prepareSearch(e, t), s = e.tokens, n = e.options.fields, o = s.length;
            var r = function(e, t) {
                    var i, n;
                    return e ? (e = String(e || ""), n = e.search(t.regex), -1 === n ? 0 : (i = t.string.length / e.length, 0 === n && (i += .5), i)) : 0
                },
                a = function() {
                    var e = n.length;
                    return e ? 1 === e ? function(e, t) {
                        return r(t[n[0]], e)
                    } : function(t, i) {
                        for (var s = 0, o = 0; e > s; s++) o += r(i[n[s]], t);
                        return o / e
                    } : function() {
                        return 0
                    }
                }();
            return o ? 1 === o ? function(e) {
                return a(s[0], e)
            } : "and" === e.options.conjunction ? function(e) {
                for (var t, i = 0, n = 0; o > i; i++) {
                    if (t = a(s[i], e), 0 >= t) return 0;
                    n += t
                }
                return n / o
            } : function(e) {
                for (var t = 0, i = 0; o > t; t++) i += a(s[t], e);
                return i / o
            } : function() {
                return 0
            }
        }, e.prototype.getSortFunction = function(e, i) {
            var n, s, o, r, a, l, c, d, u, h, p;
            if (o = this, e = o.prepareSearch(e, i), p = !e.query && i.sort_empty || i.sort, u = function(e, t) {
                    return "$score" === e ? t.score : o.items[t.id][e]
                }, a = [], p)
                for (n = 0, s = p.length; s > n; n++)(e.query || "$score" !== p[n].field) && a.push(p[n]);
            if (e.query) {
                for (h = !0, n = 0, s = a.length; s > n; n++)
                    if ("$score" === a[n].field) {
                        h = !1;
                        break
                    }
                h && a.unshift({
                    field: "$score",
                    direction: "desc"
                })
            } else
                for (n = 0, s = a.length; s > n; n++)
                    if ("$score" === a[n].field) {
                        a.splice(n, 1);
                        break
                    } for (d = [], n = 0, s = a.length; s > n; n++) d.push("desc" === a[n].direction ? -1 : 1);
            return l = a.length, l ? 1 === l ? (r = a[0].field, c = d[0], function(e, i) {
                return c * t(u(r, e), u(r, i))
            }) : function(e, i) {
                var n, s, o;
                for (n = 0; l > n; n++)
                    if (o = a[n].field, s = d[n] * t(u(o, e), u(o, i))) return s;
                return 0
            } : null
        }, e.prototype.prepareSearch = function(e, t) {
            if ("object" == typeof e) return e;
            t = i({}, t);
            var n = t.fields,
                s = t.sort,
                r = t.sort_empty;
            return n && !o(n) && (t.fields = [n]), s && !o(s) && (t.sort = [s]), r && !o(r) && (t.sort_empty = [r]), {
                options: t,
                query: String(e || "").toLowerCase(),
                tokens: this.tokenize(e),
                total: 0,
                items: []
            }
        }, e.prototype.search = function(e, t) {
            var i, n, s, o, r = this;
            return n = this.prepareSearch(e, t), t = n.options, e = n.query, o = t.score || r.getScoreFunction(n), e.length ? r.iterator(r.items, function(e, s) {
                i = o(e), (t.filter === !1 || i > 0) && n.items.push({
                    score: i,
                    id: s
                })
            }) : r.iterator(r.items, function(e, t) {
                n.items.push({
                    score: 1,
                    id: t
                })
            }), s = r.getSortFunction(n, t), s && n.items.sort(s), n.total = n.items.length, "number" == typeof t.limit && (n.items = n.items.slice(0, t.limit)), n
        };
        var t = function(e, t) {
                return "number" == typeof e && "number" == typeof t ? e > t ? 1 : t > e ? -1 : 0 : (e = String(e || "").toLowerCase(), t = String(t || "").toLowerCase(), e > t ? 1 : t > e ? -1 : 0)
            },
            i = function(e) {
                var t, i, n, s;
                for (t = 1, i = arguments.length; i > t; t++)
                    if (s = arguments[t])
                        for (n in s) s.hasOwnProperty(n) && (e[n] = s[n]);
                return e
            },
            n = function(e) {
                return (e + "").replace(/^\s+|\s+$|/g, "")
            },
            s = function(e) {
                return (e + "").replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
            },
            o = Array.isArray || $ && $.isArray || function(e) {
                return "[object Array]" === Object.prototype.toString.call(e)
            },
            r = {
                a: "[a\xc0\xc1\xc2\xc3\xc4\xc5\xe0\xe1\xe2\xe3\xe4\xe5\u0100\u0101]",
                c: "[c\xc7\xe7\u0107\u0106\u010d\u010c]",
                d: "[d\u0111\u0110\u010f\u010e]",
                e: "[e\xc8\xc9\xca\xcb\xe8\xe9\xea\xeb\u011b\u011a\u0112\u0113]",
                i: "[i\xcc\xcd\xce\xcf\xec\xed\xee\xef\u012a\u012b]",
                n: "[n\xd1\xf1\u0148\u0147]",
                o: "[o\xd2\xd3\xd4\xd5\xd5\xd6\xd8\xf2\xf3\xf4\xf5\xf6\xf8\u014c\u014d]",
                r: "[r\u0159\u0158]",
                s: "[s\u0160\u0161]",
                t: "[t\u0165\u0164]",
                u: "[u\xd9\xda\xdb\xdc\xf9\xfa\xfb\xfc\u016f\u016e\u016a\u016b]",
                y: "[y\u0178\xff\xfd\xdd]",
                z: "[z\u017d\u017e]"
            };
        return e
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("microplugin", t) : "object" == typeof exports ? module.exports = t() : e.MicroPlugin = t()
    }(this, function() {
        var e = {};
        e.mixin = function(e) {
            e.plugins = {}, e.prototype.initializePlugins = function(e) {
                var i, n, s, o = this,
                    r = [];
                if (o.plugins = {
                        names: [],
                        settings: {},
                        requested: {},
                        loaded: {}
                    }, t.isArray(e))
                    for (i = 0, n = e.length; n > i; i++) "string" == typeof e[i] ? r.push(e[i]) : (o.plugins.settings[e[i].name] = e[i].options, r.push(e[i].name));
                else if (e)
                    for (s in e) e.hasOwnProperty(s) && (o.plugins.settings[s] = e[s], r.push(s));
                for (; r.length;) o.require(r.shift())
            }, e.prototype.loadPlugin = function(t) {
                var i = this,
                    n = i.plugins,
                    s = e.plugins[t];
                if (!e.plugins.hasOwnProperty(t)) throw new Error('Unable to find "' + t + '" plugin');
                n.requested[t] = !0, n.loaded[t] = s.fn.apply(i, [i.plugins.settings[t] || {}]), n.names.push(t)
            }, e.prototype.require = function(e) {
                var t = this,
                    i = t.plugins;
                if (!t.plugins.loaded.hasOwnProperty(e)) {
                    if (i.requested[e]) throw new Error('Plugin has circular dependency ("' + e + '")');
                    t.loadPlugin(e)
                }
                return i.loaded[e]
            }, e.define = function(t, i) {
                e.plugins[t] = {
                    name: t,
                    fn: i
                }
            }
        };
        var t = {
            isArray: Array.isArray || function(e) {
                return "[object Array]" === Object.prototype.toString.call(e)
            }
        };
        return e
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define("selectize", ["jquery", "sifter", "microplugin"], t) : "object" == typeof exports ? module.exports = t(require("jquery"), require("sifter"), require("microplugin")) : e.Selectize = t(e.jQuery, e.Sifter, e.MicroPlugin)
    }(this, function(e, t, i) {
        "use strict";
        var n = function(e, t) {
                if ("string" != typeof t || t.length) {
                    var i = "string" == typeof t ? new RegExp(t, "i") : t,
                        n = function(e) {
                            var t = 0;
                            if (3 === e.nodeType) {
                                var s = e.data.search(i);
                                if (s >= 0 && e.data.length > 0) {
                                    var o = e.data.match(i),
                                        r = document.createElement("span");
                                    r.className = "highlight";
                                    var a = e.splitText(s),
                                        l = (a.splitText(o[0].length), a.cloneNode(!0));
                                    r.appendChild(l), a.parentNode.replaceChild(r, a), t = 1
                                }
                            } else if (1 === e.nodeType && e.childNodes && !/(script|style)/i.test(e.tagName))
                                for (var c = 0; c < e.childNodes.length; ++c) c += n(e.childNodes[c]);
                            return t
                        };
                    return e.each(function() {
                        n(this)
                    })
                }
            },
            s = function() {};
        s.prototype = {
            on: function(e, t) {
                this._events = this._events || {}, this._events[e] = this._events[e] || [], this._events[e].push(t)
            },
            off: function(e, t) {
                var i = arguments.length;
                return 0 === i ? delete this._events : 1 === i ? delete this._events[e] : (this._events = this._events || {}, void(e in this._events != !1 && this._events[e].splice(this._events[e].indexOf(t), 1)))
            },
            trigger: function(e) {
                if (this._events = this._events || {}, e in this._events != !1)
                    for (var t = 0; t < this._events[e].length; t++) this._events[e][t].apply(this, Array.prototype.slice.call(arguments, 1))
            }
        }, s.mixin = function(e) {
            for (var t = ["on", "off", "trigger"], i = 0; i < t.length; i++) e.prototype[t[i]] = s.prototype[t[i]]
        };
        var o = /Mac/.test(navigator.userAgent),
            r = 65,
            a = 13,
            l = 27,
            c = 37,
            d = 38,
            u = 80,
            h = 39,
            p = 40,
            f = 78,
            m = 8,
            g = 46,
            v = 16,
            y = o ? 91 : 17,
            b = o ? 18 : 17,
            w = 9,
            x = 1,
            $ = 2,
            k = function(e) {
                return "undefined" != typeof e
            },
            _ = function(e) {
                return "undefined" == typeof e || null === e ? null : "boolean" == typeof e ? e ? "1" : "0" : e + ""
            },
            S = function(e) {
                return (e + "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")
            },
            C = function(e) {
                return (e + "").replace(/\$/g, "$$$$")
            },
            T = {};
        T.before = function(e, t, i) {
            var n = e[t];
            e[t] = function() {
                return i.apply(e, arguments), n.apply(e, arguments)
            }
        }, T.after = function(e, t, i) {
            var n = e[t];
            e[t] = function() {
                var t = n.apply(e, arguments);
                return i.apply(e, arguments), t
            }
        };
        var D = function(t, i) {
                if (!e.isArray(i)) return i;
                var n, s, o = {};
                for (n = 0, s = i.length; s > n; n++) i[n].hasOwnProperty(t) && (o[i[n][t]] = i[n]);
                return o
            },
            E = function(e) {
                var t = !1;
                return function() {
                    t || (t = !0, e.apply(this, arguments))
                }
            },
            O = function(e, t) {
                var i;
                return function() {
                    var n = this,
                        s = arguments;
                    window.clearTimeout(i), i = window.setTimeout(function() {
                        e.apply(n, s)
                    }, t)
                }
            },
            A = function(e, t, i) {
                var n, s = e.trigger,
                    o = {};
                e.trigger = function() {
                    var i = arguments[0];
                    return -1 === t.indexOf(i) ? s.apply(e, arguments) : void(o[i] = arguments)
                }, i.apply(e, []), e.trigger = s;
                for (n in o) o.hasOwnProperty(n) && s.apply(e, o[n])
            },
            M = function(e, t, i, n) {
                e.on(t, i, function(t) {
                    for (var i = t.target; i && i.parentNode !== e[0];) i = i.parentNode;
                    return t.currentTarget = i, n.apply(this, [t])
                })
            },
            I = function(e) {
                var t = {};
                if ("selectionStart" in e) t.start = e.selectionStart, t.length = e.selectionEnd - t.start;
                else if (document.selection) {
                    e.focus();
                    var i = document.selection.createRange(),
                        n = document.selection.createRange().text.length;
                    i.moveStart("character", -e.value.length), t.start = i.text.length - n, t.length = n
                }
                return t
            },
            N = function(e, t, i) {
                var n, s, o = {};
                if (i)
                    for (n = 0, s = i.length; s > n; n++) o[i[n]] = e.css(i[n]);
                else o = e.css();
                t.css(o)
            },
            j = function(t, i) {
                if (!t) return 0;
                var n = e("<test>").css({
                    position: "absolute",
                    top: -99999,
                    left: -99999,
                    width: "auto",
                    padding: 0,
                    whiteSpace: "pre"
                }).text(t).appendTo("body");
                N(i, n, ["letterSpacing", "fontSize", "fontFamily", "fontWeight", "textTransform"]);
                var s = n.width();
                return n.remove(), s
            },
            P = function(e) {
                var t = null,
                    i = function(i, n) {
                        var s, o, r, a, l, c, d, u;
                        i = i || window.event || {}, n = n || {}, i.metaKey || i.altKey || (n.force || e.data("grow") !== !1) && (s = e.val(), i.type && "keydown" === i.type.toLowerCase() && (o = i.keyCode, r = o >= 97 && 122 >= o || o >= 65 && 90 >= o || o >= 48 && 57 >= o || 32 === o, o === g || o === m ? (u = I(e[0]), u.length ? s = s.substring(0, u.start) + s.substring(u.start + u.length) : o === m && u.start ? s = s.substring(0, u.start - 1) + s.substring(u.start + 1) : o === g && "undefined" != typeof u.start && (s = s.substring(0, u.start) + s.substring(u.start + 1))) : r && (c = i.shiftKey, d = String.fromCharCode(i.keyCode), d = c ? d.toUpperCase() : d.toLowerCase(), s += d)), a = e.attr("placeholder"), !s && a && (s = a), l = j(s, e) + 4, l !== t && (t = l, e.width(l), e.triggerHandler("resize")))
                    };
                e.on("keydown keyup update blur", i), i()
            },
            F = function(i, n) {
                var s, o, r = this;
                o = i[0], o.selectize = r, s = window.getComputedStyle ? window.getComputedStyle(o, null).getPropertyValue("direction") : o.currentStyle && o.currentStyle.direction, s = s || i.parents("[dir]:first").attr("dir") || "", e.extend(r, {
                    settings: n,
                    $input: i,
                    tagType: "select" === o.tagName.toLowerCase() ? x : $,
                    rtl: /rtl/i.test(s),
                    eventNS: ".selectize" + ++F.count,
                    highlightedValue: null,
                    isOpen: !1,
                    isDisabled: !1,
                    isRequired: i.is("[required]"),
                    isInvalid: !1,
                    isLocked: !1,
                    isFocused: !1,
                    isInputHidden: !1,
                    isSetup: !1,
                    isShiftDown: !1,
                    isCmdDown: !1,
                    isCtrlDown: !1,
                    ignoreFocus: !1,
                    ignoreBlur: !1,
                    ignoreHover: !1,
                    hasOptions: !1,
                    currentResults: null,
                    lastValue: "",
                    caretPos: 0,
                    loading: 0,
                    loadedSearches: {},
                    $activeOption: null,
                    $activeItems: [],
                    optgroups: {},
                    options: {},
                    userOptions: {},
                    items: [],
                    renderCache: {},
                    onSearchChange: null === n.loadThrottle ? r.onSearchChange : O(r.onSearchChange, n.loadThrottle)
                }), r.sifter = new t(this.options, {
                    diacritics: n.diacritics
                }), e.extend(r.options, D(n.valueField, n.options)), delete r.settings.options, e.extend(r.optgroups, D(n.optgroupValueField, n.optgroups)), delete r.settings.optgroups, r.settings.mode = r.settings.mode || (1 === r.settings.maxItems ? "single" : "multi"), "boolean" != typeof r.settings.hideSelected && (r.settings.hideSelected = "multi" === r.settings.mode), r.settings.create && (r.canCreate = function(e) {
                    var t = r.settings.createFilter;
                    return !(!e.length || "function" == typeof t && !t.apply(r, [e]) || "string" == typeof t && !new RegExp(t).test(e) || t instanceof RegExp && !t.test(e))
                }), r.initializePlugins(r.settings.plugins), r.setupCallbacks(), r.setupTemplates(), r.setup()
            };
        return s.mixin(F), i.mixin(F), e.extend(F.prototype, {
            setup: function() {
                var t, i, n, s, r, a, l, c, d, u, h = this,
                    p = h.settings,
                    f = h.eventNS,
                    m = e(window),
                    g = e(document),
                    w = h.$input;
                l = h.settings.mode, c = w.attr("tabindex") || "", d = w.attr("class") || "", t = e("<div>").addClass(p.wrapperClass).addClass(d).addClass(l), i = e("<div>").addClass(p.inputClass).addClass("items").appendTo(t), n = e('<input type="text" autocomplete="off" />').appendTo(i).attr("tabindex", c), a = e(p.dropdownParent || t), s = e("<div>").addClass(p.dropdownClass).addClass(d).addClass(l).hide().appendTo(a), r = e("<div>").addClass(p.dropdownContentClass).appendTo(s), t.css({
                    width: w[0].style.width
                }), h.plugins.names.length && (u = "plugin-" + h.plugins.names.join(" plugin-"), t.addClass(u), s.addClass(u)), (null === p.maxItems || p.maxItems > 1) && h.tagType === x && w.attr("multiple", "multiple"), h.settings.placeholder && n.attr("placeholder", p.placeholder), w.attr("autocorrect") && n.attr("autocorrect", w.attr("autocorrect")), w.attr("autocapitalize") && n.attr("autocapitalize", w.attr("autocapitalize")), h.$wrapper = t, h.$control = i, h.$control_input = n, h.$dropdown = s, h.$dropdown_content = r, s.on("mouseenter", "[data-selectable]", function() {
                    return h.onOptionHover.apply(h, arguments)
                }), s.on("mousedown", "[data-selectable]", function() {
                    return h.onOptionSelect.apply(h, arguments)
                }), M(i, "mousedown", "*:not(input)", function() {
                    return h.onItemSelect.apply(h, arguments)
                }), P(n), i.on({
                    mousedown: function() {
                        return h.onMouseDown.apply(h, arguments)
                    },
                    click: function() {
                        return h.onClick.apply(h, arguments)
                    }
                }), n.on({
                    mousedown: function(e) {
                        e.stopPropagation()
                    },
                    keydown: function() {
                        return h.onKeyDown.apply(h, arguments)
                    },
                    keyup: function() {
                        return h.onKeyUp.apply(h, arguments)
                    },
                    keypress: function() {
                        return h.onKeyPress.apply(h, arguments)
                    },
                    resize: function() {
                        h.positionDropdown.apply(h, [])
                    },
                    blur: function() {
                        return h.onBlur.apply(h, arguments)
                    },
                    focus: function() {
                        return h.ignoreBlur = !1, h.onFocus.apply(h, arguments)
                    },
                    paste: function() {
                        return h.onPaste.apply(h, arguments)
                    }
                }), g.on("keydown" + f, function(e) {
                    h.isCmdDown = e[o ? "metaKey" : "ctrlKey"], h.isCtrlDown = e[o ? "altKey" : "ctrlKey"], h.isShiftDown = e.shiftKey
                }), g.on("keyup" + f, function(e) {
                    e.keyCode === b && (h.isCtrlDown = !1), e.keyCode === v && (h.isShiftDown = !1), e.keyCode === y && (h.isCmdDown = !1)
                }), g.on("mousedown" + f, function(e) {
                    if (h.isFocused) {
                        if (e.target === h.$dropdown[0] || e.target.parentNode === h.$dropdown[0]) return !1;
                        h.$control.has(e.target).length || e.target === h.$control[0] || h.blur()
                    }
                }), m.on(["scroll" + f, "resize" + f].join(" "), function() {
                    h.isOpen && h.positionDropdown.apply(h, arguments)
                }), m.on("mousemove" + f, function() {
                    h.ignoreHover = !1
                }), this.revertSettings = {
                    $children: w.children().detach(),
                    tabindex: w.attr("tabindex")
                }, w.attr("tabindex", -1).hide().after(h.$wrapper), e.isArray(p.items) && (h.setValue(p.items), delete p.items), w[0].validity && w.on("invalid" + f, function(e) {
                    e.preventDefault(), h.isInvalid = !0, h.refreshState()
                }), h.updateOriginalInput(), h.refreshItems(), h.refreshState(), h.updatePlaceholder(), h.isSetup = !0, w.is(":disabled") && h.disable(), h.on("change", this.onChange), w.data("selectize", h), w.addClass("selectized"), h.trigger("initialize"), p.preload === !0 && h.onSearchChange("")
            },
            setupTemplates: function() {
                var t = this,
                    i = t.settings.labelField,
                    n = t.settings.optgroupLabelField,
                    s = {
                        optgroup: function(e) {
                            return '<div class="optgroup">' + e.html + "</div>"
                        },
                        optgroup_header: function(e, t) {
                            return '<div class="optgroup-header">' + t(e[n]) + "</div>"
                        },
                        option: function(e, t) {
                            return '<div class="option">' + t(e[i]) + "</div>"
                        },
                        item: function(e, t) {
                            return '<div class="item">' + t(e[i]) + "</div>"
                        },
                        option_create: function(e, t) {
                            return '<div class="create">Add <strong>' + t(e.input) + "</strong>&hellip;</div>"
                        }
                    };
                t.settings.render = e.extend({}, s, t.settings.render)
            },
            setupCallbacks: function() {
                var e, t, i = {
                    initialize: "onInitialize",
                    change: "onChange",
                    item_add: "onItemAdd",
                    item_remove: "onItemRemove",
                    clear: "onClear",
                    option_add: "onOptionAdd",
                    option_remove: "onOptionRemove",
                    option_clear: "onOptionClear",
                    dropdown_open: "onDropdownOpen",
                    dropdown_close: "onDropdownClose",
                    type: "onType"
                };
                for (e in i) i.hasOwnProperty(e) && (t = this.settings[i[e]], t && this.on(e, t))
            },
            onClick: function(e) {
                var t = this;
                t.isFocused || (t.focus(), e.preventDefault())
            },
            onMouseDown: function(t) {
                {
                    var i = this,
                        n = t.isDefaultPrevented();
                    e(t.target)
                }
                if (i.isFocused) {
                    if (t.target !== i.$control_input[0]) return "single" === i.settings.mode ? i.isOpen ? i.close() : i.open() : n || i.setActiveItem(null), !1
                } else n || window.setTimeout(function() {
                    i.focus()
                }, 0)
            },
            onChange: function() {
                this.$input.trigger("change")
            },
            onPaste: function(e) {
                var t = this;
                (t.isFull() || t.isInputHidden || t.isLocked) && e.preventDefault()
            },
            onKeyPress: function(e) {
                if (this.isLocked) return e && e.preventDefault();
                var t = String.fromCharCode(e.keyCode || e.which);
                return this.settings.create && t === this.settings.delimiter ? (this.createItem(), e.preventDefault(), !1) : void 0
            },
            onKeyDown: function(e) {
                var t = (e.target === this.$control_input[0], this);
                if (t.isLocked) return void(e.keyCode !== w && e.preventDefault());
                switch (e.keyCode) {
                    case r:
                        if (t.isCmdDown) return void t.selectAll();
                        break;
                    case l:
                        return void t.close();
                    case f:
                        if (!e.ctrlKey || e.altKey) break;
                    case p:
                        if (!t.isOpen && t.hasOptions) t.open();
                        else if (t.$activeOption) {
                            t.ignoreHover = !0;
                            var i = t.getAdjacentOption(t.$activeOption, 1);
                            i.length && t.setActiveOption(i, !0, !0)
                        }
                        return void e.preventDefault();
                    case u:
                        if (!e.ctrlKey || e.altKey) break;
                    case d:
                        if (t.$activeOption) {
                            t.ignoreHover = !0;
                            var n = t.getAdjacentOption(t.$activeOption, -1);
                            n.length && t.setActiveOption(n, !0, !0)
                        }
                        return void e.preventDefault();
                    case a:
                        return t.isOpen && t.$activeOption && t.onOptionSelect({
                            currentTarget: t.$activeOption
                        }), void e.preventDefault();
                    case c:
                        return void t.advanceSelection(-1, e);
                    case h:
                        return void t.advanceSelection(1, e);
                    case w:
                        return t.settings.selectOnTab && t.isOpen && t.$activeOption && (t.onOptionSelect({
                            currentTarget: t.$activeOption
                        }), e.preventDefault()), void(t.settings.create && t.createItem() && e.preventDefault());
                    case m:
                    case g:
                        return void t.deleteSelection(e)
                }
                return !t.isFull() && !t.isInputHidden || (o ? e.metaKey : e.ctrlKey) ? void 0 : void e.preventDefault()
            },
            onKeyUp: function(e) {
                var t = this;
                if (t.isLocked) return e && e.preventDefault();
                var i = t.$control_input.val() || "";
                t.lastValue !== i && (t.lastValue = i, t.onSearchChange(i), t.refreshOptions(), t.trigger("type", i))
            },
            onSearchChange: function(e) {
                var t = this,
                    i = t.settings.load;
                i && (t.loadedSearches.hasOwnProperty(e) || (t.loadedSearches[e] = !0, t.load(function(n) {
                    i.apply(t, [e, n])
                })))
            },
            onFocus: function(e) {
                var t = this;
                return t.isFocused = !0, t.isDisabled ? (t.blur(), e && e.preventDefault(), !1) : void(t.ignoreFocus || ("focus" === t.settings.preload && t.onSearchChange(""), t.$activeItems.length || (t.showInput(), t.setActiveItem(null), t.refreshOptions(!!t.settings.openOnFocus)), t.refreshState()))
            },
            onBlur: function(e) {
                var t = this;
                if (t.isFocused = !1, !t.ignoreFocus) {
                    if (!t.ignoreBlur && document.activeElement === t.$dropdown_content[0]) return t.ignoreBlur = !0, void t.onFocus(e);
                    t.settings.create && t.settings.createOnBlur && t.createItem(!1), t.close(), t.setTextboxValue(""), t.setActiveItem(null), t.setActiveOption(null), t.setCaret(t.items.length), t.refreshState()
                }
            },
            onOptionHover: function(e) {
                this.ignoreHover || this.setActiveOption(e.currentTarget, !1)
            },
            onOptionSelect: function(t) {
                var i, n, s = this;
                t.preventDefault && (t.preventDefault(), t.stopPropagation()), n = e(t.currentTarget), n.hasClass("create") ? s.createItem() : (i = n.attr("data-value"), "undefined" != typeof i && (s.lastQuery = null, s.setTextboxValue(""), s.addItem(i), !s.settings.hideSelected && t.type && /mouse/.test(t.type) && s.setActiveOption(s.getOption(i))))
            },
            onItemSelect: function(e) {
                var t = this;
                t.isLocked || "multi" === t.settings.mode && (e.preventDefault(), t.setActiveItem(e.currentTarget, e))
            },
            load: function(e) {
                var t = this,
                    i = t.$wrapper.addClass("loading");
                t.loading++, e.apply(t, [function(e) {
                    t.loading = Math.max(t.loading - 1, 0), e && e.length && (t.addOption(e), t.refreshOptions(t.isFocused && !t.isInputHidden)), t.loading || i.removeClass("loading"), t.trigger("load", e)
                }])
            },
            setTextboxValue: function(e) {
                var t = this.$control_input,
                    i = t.val() !== e;
                i && (t.val(e).triggerHandler("update"), this.lastValue = e)
            },
            getValue: function() {
                return this.tagType === x && this.$input.attr("multiple") ? this.items : this.items.join(this.settings.delimiter)
            },
            setValue: function(e) {
                A(this, ["change"], function() {
                    this.clear(), this.addItems(e)
                })
            },
            setActiveItem: function(t, i) {
                var n, s, o, r, a, l, c, d, u = this;
                if ("single" !== u.settings.mode) {
                    if (t = e(t), !t.length) return e(u.$activeItems).removeClass("active"), u.$activeItems = [], void(u.isFocused && u.showInput());
                    if (n = i && i.type.toLowerCase(), "mousedown" === n && u.isShiftDown && u.$activeItems.length) {
                        for (d = u.$control.children(".active:last"), r = Array.prototype.indexOf.apply(u.$control[0].childNodes, [d[0]]), a = Array.prototype.indexOf.apply(u.$control[0].childNodes, [t[0]]), r > a && (c = r, r = a, a = c), s = r; a >= s; s++) l = u.$control[0].childNodes[s], -1 === u.$activeItems.indexOf(l) && (e(l).addClass("active"), u.$activeItems.push(l));
                        i.preventDefault()
                    } else "mousedown" === n && u.isCtrlDown || "keydown" === n && this.isShiftDown ? t.hasClass("active") ? (o = u.$activeItems.indexOf(t[0]), u.$activeItems.splice(o, 1), t.removeClass("active")) : u.$activeItems.push(t.addClass("active")[0]) : (e(u.$activeItems).removeClass("active"), u.$activeItems = [t.addClass("active")[0]]);
                    u.hideInput(), this.isFocused || u.focus()
                }
            },
            setActiveOption: function(t, i, n) {
                var s, o, r, a, l, c = this;
                c.$activeOption && c.$activeOption.removeClass("active"), c.$activeOption = null, t = e(t), t.length && (c.$activeOption = t.addClass("active"), (i || !k(i)) && (s = c.$dropdown_content.height(), o = c.$activeOption.outerHeight(!0), i = c.$dropdown_content.scrollTop() || 0, r = c.$activeOption.offset().top - c.$dropdown_content.offset().top + i, a = r, l = r - s + o, r + o > s + i ? c.$dropdown_content.stop().animate({
                    scrollTop: l
                }, n ? c.settings.scrollDuration : 0) : i > r && c.$dropdown_content.stop().animate({
                    scrollTop: a
                }, n ? c.settings.scrollDuration : 0)))
            },
            selectAll: function() {
                var e = this;
                "single" !== e.settings.mode && (e.$activeItems = Array.prototype.slice.apply(e.$control.children(":not(input)").addClass("active")), e.$activeItems.length && (e.hideInput(), e.close()), e.focus())
            },
            hideInput: function() {
                var e = this;
                e.setTextboxValue(""), e.$control_input.css({
                    opacity: 0,
                    position: "absolute",
                    left: e.rtl ? 1e4 : -1e4
                }), e.isInputHidden = !0
            },
            showInput: function() {
                this.$control_input.css({
                    opacity: 1,
                    position: "relative",
                    left: 0
                }), this.isInputHidden = !1
            },
            focus: function() {
                var e = this;
                e.isDisabled || (e.ignoreFocus = !0, e.$control_input[0].focus(), window.setTimeout(function() {
                    e.ignoreFocus = !1, e.onFocus()
                }, 0))
            },
            blur: function() {
                this.$control_input.trigger("blur")
            },
            getScoreFunction: function(e) {
                return this.sifter.getScoreFunction(e, this.getSearchOptions())
            },
            getSearchOptions: function() {
                var e = this.settings,
                    t = e.sortField;
                return "string" == typeof t && (t = {
                    field: t
                }), {
                    fields: e.searchField,
                    conjunction: e.searchConjunction,
                    sort: t
                }
            },
            search: function(t) {
                var i, n, s, o = this,
                    r = o.settings,
                    a = this.getSearchOptions();
                if (r.score && (s = o.settings.score.apply(this, [t]), "function" != typeof s)) throw new Error('Selectize "score" setting must be a function that returns a function');
                if (t !== o.lastQuery ? (o.lastQuery = t, n = o.sifter.search(t, e.extend(a, {
                        score: s
                    })), o.currentResults = n) : n = e.extend(!0, {}, o.currentResults), r.hideSelected)
                    for (i = n.items.length - 1; i >= 0; i--) - 1 !== o.items.indexOf(_(n.items[i].id)) && n.items.splice(i, 1);
                return n
            },
            refreshOptions: function(t) {
                var i, s, o, r, a, l, c, d, u, h, p, f, m, g, v, y;
                "undefined" == typeof t && (t = !0);
                var b = this,
                    w = b.$control_input.val(),
                    x = b.search(w),
                    $ = b.$dropdown_content,
                    k = b.$activeOption && _(b.$activeOption.attr("data-value"));
                if (r = x.items.length, "number" == typeof b.settings.maxOptions && (r = Math.min(r, b.settings.maxOptions)), a = {}, b.settings.optgroupOrder)
                    for (l = b.settings.optgroupOrder, i = 0; i < l.length; i++) a[l[i]] = [];
                else l = [];
                for (i = 0; r > i; i++)
                    for (c = b.options[x.items[i].id], d = b.render("option", c), u = c[b.settings.optgroupField] || "", h = e.isArray(u) ? u : [u], s = 0, o = h && h.length; o > s; s++) u = h[s], b.optgroups.hasOwnProperty(u) || (u = ""), a.hasOwnProperty(u) || (a[u] = [], l.push(u)), a[u].push(d);
                for (p = [], i = 0, r = l.length; r > i; i++) u = l[i], b.optgroups.hasOwnProperty(u) && a[u].length ? (f = b.render("optgroup_header", b.optgroups[u]) || "", f += a[u].join(""), p.push(b.render("optgroup", e.extend({}, b.optgroups[u], {
                    html: f
                })))) : p.push(a[u].join(""));
                if ($.html(p.join("")), b.settings.highlight && x.query.length && x.tokens.length)
                    for (i = 0, r = x.tokens.length; r > i; i++) n($, x.tokens[i].regex);
                if (!b.settings.hideSelected)
                    for (i = 0, r = b.items.length; r > i; i++) b.getOption(b.items[i]).addClass("selected");
                m = b.settings.create && b.canCreate(x.query), m && ($.prepend(b.render("option_create", {
                    input: w
                })), y = e($[0].childNodes[0])), b.hasOptions = x.items.length > 0 || m, b.hasOptions ? (x.items.length > 0 ? (v = k && b.getOption(k), v && v.length ? g = v : "single" === b.settings.mode && b.items.length && (g = b.getOption(b.items[0])), g && g.length || (g = y && !b.settings.addPrecedence ? b.getAdjacentOption(y, 1) : $.find("[data-selectable]:first"))) : g = y, b.setActiveOption(g), t && !b.isOpen && b.open()) : (b.setActiveOption(null), t && b.isOpen && b.close())
            },
            addOption: function(t) {
                var i, n, s, o = this;
                if (e.isArray(t))
                    for (i = 0, n = t.length; n > i; i++) o.addOption(t[i]);
                else s = _(t[o.settings.valueField]), "string" != typeof s || o.options.hasOwnProperty(s) || (o.userOptions[s] = !0, o.options[s] = t, o.lastQuery = null, o.trigger("option_add", s, t))
            },
            addOptionGroup: function(e, t) {
                this.optgroups[e] = t, this.trigger("optgroup_add", e, t)
            },
            updateOption: function(t, i) {
                var n, s, o, r, a, l, c = this;
                if (t = _(t), o = _(i[c.settings.valueField]), null !== t && c.options.hasOwnProperty(t)) {
                    if ("string" != typeof o) throw new Error("Value must be set in option data");
                    o !== t && (delete c.options[t], r = c.items.indexOf(t), -1 !== r && c.items.splice(r, 1, o)), c.options[o] = i, a = c.renderCache.item, l = c.renderCache.option, a && (delete a[t], delete a[o]), l && (delete l[t], delete l[o]), -1 !== c.items.indexOf(o) && (n = c.getItem(t), s = e(c.render("item", i)), n.hasClass("active") && s.addClass("active"), n.replaceWith(s)), c.isOpen && c.refreshOptions(!1)
                }
            },
            removeOption: function(e) {
                var t = this;
                e = _(e);
                var i = t.renderCache.item,
                    n = t.renderCache.option;
                i && delete i[e], n && delete n[e], delete t.userOptions[e], delete t.options[e], t.lastQuery = null, t.trigger("option_remove", e), t.removeItem(e)
            },
            clearOptions: function() {
                var e = this;
                e.loadedSearches = {}, e.userOptions = {}, e.renderCache = {}, e.options = e.sifter.items = {}, e.lastQuery = null, e.trigger("option_clear"), e.clear()
            },
            getOption: function(e) {
                return this.getElementWithValue(e, this.$dropdown_content.find("[data-selectable]"))
            },
            getAdjacentOption: function(t, i) {
                var n = this.$dropdown.find("[data-selectable]"),
                    s = n.index(t) + i;
                return s >= 0 && s < n.length ? n.eq(s) : e()
            },
            getElementWithValue: function(t, i) {
                if (t = _(t), "undefined" != typeof t && null !== t)
                    for (var n = 0, s = i.length; s > n; n++)
                        if (i[n].getAttribute("data-value") === t) return e(i[n]);
                return e()
            },
            getItem: function(e) {
                return this.getElementWithValue(e, this.$control.children())
            },
            addItems: function(t) {
                for (var i = e.isArray(t) ? t : [t], n = 0, s = i.length; s > n; n++) this.isPending = s - 1 > n, this.addItem(i[n])
            },
            addItem: function(t) {
                A(this, ["change"], function() {
                    var i, n, s, o, r, a = this,
                        l = a.settings.mode;
                    return t = _(t), -1 !== a.items.indexOf(t) ? void("single" === l && a.close()) : void(a.options.hasOwnProperty(t) && ("single" === l && a.clear(), "multi" === l && a.isFull() || (i = e(a.render("item", a.options[t])), r = a.isFull(), a.items.splice(a.caretPos, 0, t), a.insertAtCaret(i), (!a.isPending || !r && a.isFull()) && a.refreshState(), a.isSetup && (s = a.$dropdown_content.find("[data-selectable]"), a.isPending || (n = a.getOption(t), o = a.getAdjacentOption(n, 1).attr("data-value"), a.refreshOptions(a.isFocused && "single" !== l), o && a.setActiveOption(a.getOption(o))), !s.length || a.isFull() ? a.close() : a.positionDropdown(), a.updatePlaceholder(), a.trigger("item_add", t, i), a.updateOriginalInput()))))
                })
            },
            removeItem: function(e) {
                var t, i, n, s = this;
                t = "object" == typeof e ? e : s.getItem(e), e = _(t.attr("data-value")), i = s.items.indexOf(e), -1 !== i && (t.remove(), t.hasClass("active") && (n = s.$activeItems.indexOf(t[0]), s.$activeItems.splice(n, 1)), s.items.splice(i, 1), s.lastQuery = null, !s.settings.persist && s.userOptions.hasOwnProperty(e) && s.removeOption(e), i < s.caretPos && s.setCaret(s.caretPos - 1), s.refreshState(), s.updatePlaceholder(), s.updateOriginalInput(), s.positionDropdown(), s.trigger("item_remove", e))
            },
            createItem: function(t) {
                var i = this,
                    n = e.trim(i.$control_input.val() || ""),
                    s = i.caretPos;
                if (!i.canCreate(n)) return !1;
                i.lock(), "undefined" == typeof t && (t = !0);
                var o = "function" == typeof i.settings.create ? this.settings.create : function(e) {
                        var t = {};
                        return t[i.settings.labelField] = e, t[i.settings.valueField] = e, t
                    },
                    r = E(function(e) {
                        if (i.unlock(), e && "object" == typeof e) {
                            var n = _(e[i.settings.valueField]);
                            "string" == typeof n && (i.setTextboxValue(""), i.addOption(e), i.setCaret(s), i.addItem(n), i.refreshOptions(t && "single" !== i.settings.mode))
                        }
                    }),
                    a = o.apply(this, [n, r]);
                return "undefined" != typeof a && r(a), !0
            },
            refreshItems: function() {
                if (this.lastQuery = null, this.isSetup)
                    for (var e = 0; e < this.items.length; e++) this.addItem(this.items);
                this.refreshState(), this.updateOriginalInput()
            },
            refreshState: function() {
                var e, t = this;
                t.isRequired && (t.items.length && (t.isInvalid = !1), t.$control_input.prop("required", e)), t.refreshClasses()
            },
            refreshClasses: function() {
                var t = this,
                    i = t.isFull(),
                    n = t.isLocked;
                t.$wrapper.toggleClass("rtl", t.rtl), t.$control.toggleClass("focus", t.isFocused).toggleClass("disabled", t.isDisabled).toggleClass("required", t.isRequired).toggleClass("invalid", t.isInvalid).toggleClass("locked", n).toggleClass("full", i).toggleClass("not-full", !i).toggleClass("input-active", t.isFocused && !t.isInputHidden).toggleClass("dropdown-active", t.isOpen).toggleClass("has-options", !e.isEmptyObject(t.options)).toggleClass("has-items", t.items.length > 0), t.$control_input.data("grow", !i && !n)
            },
            isFull: function() {
                return null !== this.settings.maxItems && this.items.length >= this.settings.maxItems
            },
            updateOriginalInput: function() {
                var e, t, i, n = this;
                if (n.tagType === x) {
                    for (i = [], e = 0, t = n.items.length; t > e; e++) i.push('<option value="' + S(n.items[e]) + '" selected="selected"></option>');
                    i.length || this.$input.attr("multiple") || i.push('<option value="" selected="selected"></option>'), n.$input.html(i.join(""))
                } else n.$input.val(n.getValue()), n.$input.attr("value", n.$input.val());
                n.isSetup && n.trigger("change", n.$input.val())
            },
            updatePlaceholder: function() {
                if (this.settings.placeholder) {
                    var e = this.$control_input;
                    this.items.length ? e.removeAttr("placeholder") : e.attr("placeholder", this.settings.placeholder), e.triggerHandler("update", {
                        force: !0
                    })
                }
            },
            open: function() {
                var e = this;
                e.isLocked || e.isOpen || "multi" === e.settings.mode && e.isFull() || (e.focus(), e.isOpen = !0, e.refreshState(), e.$dropdown.css({
                    visibility: "hidden",
                    display: "block"
                }), e.positionDropdown(), e.$dropdown.css({
                    visibility: "visible"
                }), e.trigger("dropdown_open", e.$dropdown))
            },
            close: function() {
                var e = this,
                    t = e.isOpen;
                "single" === e.settings.mode && e.items.length && e.hideInput(), e.isOpen = !1, e.$dropdown.hide(), e.setActiveOption(null), e.refreshState(), t && e.trigger("dropdown_close", e.$dropdown)
            },
            positionDropdown: function() {
                var e = this.$control,
                    t = "body" === this.settings.dropdownParent ? e.offset() : e.position();
                t.top += e.outerHeight(!0), this.$dropdown.css({
                    width: e.outerWidth(),
                    top: t.top,
                    left: t.left
                })
            },
            clear: function() {
                var e = this;
                e.items.length && (e.$control.children(":not(input)").remove(), e.items = [], e.lastQuery = null, e.setCaret(0), e.setActiveItem(null), e.updatePlaceholder(), e.updateOriginalInput(), e.refreshState(), e.showInput(), e.trigger("clear"))
            },
            insertAtCaret: function(t) {
                var i = Math.min(this.caretPos, this.items.length);
                0 === i ? this.$control.prepend(t) : e(this.$control[0].childNodes[i]).before(t), this.setCaret(i + 1)
            },
            deleteSelection: function(t) {
                var i, n, s, o, r, a, l, c, d, u = this;
                if (s = t && t.keyCode === m ? -1 : 1, o = I(u.$control_input[0]), u.$activeOption && !u.settings.hideSelected && (l = u.getAdjacentOption(u.$activeOption, -1).attr("data-value")), r = [], u.$activeItems.length) {
                    for (d = u.$control.children(".active:" + (s > 0 ? "last" : "first")), a = u.$control.children(":not(input)").index(d), s > 0 && a++, i = 0, n = u.$activeItems.length; n > i; i++) r.push(e(u.$activeItems[i]).attr("data-value"));
                    t && (t.preventDefault(), t.stopPropagation())
                } else(u.isFocused || "single" === u.settings.mode) && u.items.length && (0 > s && 0 === o.start && 0 === o.length ? r.push(u.items[u.caretPos - 1]) : s > 0 && o.start === u.$control_input.val().length && r.push(u.items[u.caretPos]));
                if (!r.length || "function" == typeof u.settings.onDelete && u.settings.onDelete.apply(u, [r]) === !1) return !1;
                for ("undefined" != typeof a && u.setCaret(a); r.length;) u.removeItem(r.pop());
                return u.showInput(), u.positionDropdown(), u.refreshOptions(!0), l && (c = u.getOption(l), c.length && u.setActiveOption(c)), !0
            },
            advanceSelection: function(e, t) {
                var i, n, s, o, r, a, l = this;
                0 !== e && (l.rtl && (e *= -1), i = e > 0 ? "last" : "first", n = I(l.$control_input[0]), l.isFocused && !l.isInputHidden ? (o = l.$control_input.val().length, r = 0 > e ? 0 === n.start && 0 === n.length : n.start === o, r && !o && l.advanceCaret(e, t)) : (a = l.$control.children(".active:" + i), a.length && (s = l.$control.children(":not(input)").index(a), l.setActiveItem(null), l.setCaret(e > 0 ? s + 1 : s))))
            },
            advanceCaret: function(e, t) {
                var i, n, s = this;
                0 !== e && (i = e > 0 ? "next" : "prev", s.isShiftDown ? (n = s.$control_input[i](), n.length && (s.hideInput(), s.setActiveItem(n), t && t.preventDefault())) : s.setCaret(s.caretPos + e))
            },
            setCaret: function(t) {
                var i = this;
                if (t = "single" === i.settings.mode ? i.items.length : Math.max(0, Math.min(i.items.length, t)), !i.isPending) {
                    var n, s, o, r;
                    for (o = i.$control.children(":not(input)"), n = 0, s = o.length; s > n; n++) r = e(o[n]).detach(), t > n ? i.$control_input.before(r) : i.$control.append(r)
                }
                i.caretPos = t
            },
            lock: function() {
                this.close(), this.isLocked = !0, this.refreshState()
            },
            unlock: function() {
                this.isLocked = !1, this.refreshState()
            },
            disable: function() {
                var e = this;
                e.$input.prop("disabled", !0), e.isDisabled = !0, e.lock()
            },
            enable: function() {
                var e = this;
                e.$input.prop("disabled", !1), e.isDisabled = !1, e.unlock()
            },
            destroy: function() {
                var t = this,
                    i = t.eventNS,
                    n = t.revertSettings;
                t.trigger("destroy"), t.off(), t.$wrapper.remove(), t.$dropdown.remove(), t.$input.html("").append(n.$children).removeAttr("tabindex").removeClass("selectized").attr({
                    tabindex: n.tabindex
                }).show(), t.$control_input.removeData("grow"), t.$input.removeData("selectize"), e(window).off(i), e(document).off(i), e(document.body).off(i), delete t.$input[0].selectize
            },
            render: function(e, t) {
                var i, n, s = "",
                    o = !1,
                    r = this,
                    a = /^[\t ]*<([a-z][a-z0-9\-_]*(?:\:[a-z][a-z0-9\-_]*)?)/i;
                return ("option" === e || "item" === e) && (i = _(t[r.settings.valueField]), o = !!i), o && (k(r.renderCache[e]) || (r.renderCache[e] = {}), r.renderCache[e].hasOwnProperty(i)) ? r.renderCache[e][i] : (s = r.settings.render[e].apply(this, [t, S]), ("option" === e || "option_create" === e) && (s = s.replace(a, "<$1 data-selectable")), "optgroup" === e && (n = t[r.settings.optgroupValueField] || "", s = s.replace(a, '<$1 data-group="' + C(S(n)) + '"')), ("option" === e || "item" === e) && (s = s.replace(a, '<$1 data-value="' + C(S(i || "")) + '"')), o && (r.renderCache[e][i] = s), s)
            },
            clearCache: function(e) {
                var t = this;
                "undefined" == typeof e ? t.renderCache = {} : delete t.renderCache[e]
            }
        }), F.count = 0, F.defaults = {
            plugins: [],
            delimiter: ",",
            persist: !0,
            diacritics: !0,
            create: !1,
            createOnBlur: !1,
            createFilter: null,
            highlight: !0,
            openOnFocus: !0,
            maxOptions: 1e3,
            maxItems: null,
            hideSelected: null,
            addPrecedence: !1,
            selectOnTab: !1,
            preload: !1,
            allowEmptyOption: !1,
            scrollDuration: 60,
            loadThrottle: 300,
            dataAttr: "data-data",
            optgroupField: "optgroup",
            valueField: "value",
            labelField: "text",
            optgroupLabelField: "label",
            optgroupValueField: "value",
            optgroupOrder: null,
            sortField: "$order",
            searchField: ["text"],
            searchConjunction: "and",
            mode: null,
            wrapperClass: "selectize-control",
            inputClass: "selectize-input",
            dropdownClass: "selectize-dropdown",
            dropdownContentClass: "selectize-dropdown-content",
            dropdownParent: null,
            render: {}
        }, e.fn.selectize = function(t) {
            var i = e.fn.selectize.defaults,
                n = e.extend({}, i, t),
                s = n.dataAttr,
                o = n.labelField,
                r = n.valueField,
                a = n.optgroupField,
                l = n.optgroupLabelField,
                c = n.optgroupValueField,
                d = function(t, i) {
                    var s, a, l, c, d = e.trim(t.val() || "");
                    if (n.allowEmptyOption || d.length) {
                        for (l = d.split(n.delimiter), s = 0, a = l.length; a > s; s++) c = {}, c[o] = l[s], c[r] = l[s], i.options[l[s]] = c;
                        i.items = l
                    }
                },
                u = function(t, i) {
                    var d, u, h, p, f = 0,
                        m = i.options,
                        g = function(e) {
                            var t = s && e.attr(s);
                            return "string" == typeof t && t.length ? JSON.parse(t) : null
                        },
                        v = function(t, s) {
                            var l, c;
                            if (t = e(t), l = t.attr("value") || "", l.length || n.allowEmptyOption) {
                                if (m.hasOwnProperty(l)) return void(s && (m[l].optgroup ? e.isArray(m[l].optgroup) ? m[l].optgroup.push(s) : m[l].optgroup = [m[l].optgroup, s] : m[l].optgroup = s));
                                c = g(t) || {}, c[o] = c[o] || t.text(), c[r] = c[r] || l, c[a] = c[a] || s, c.$order = ++f, m[l] = c, t.is(":selected") && i.items.push(l)
                            }
                        },
                        y = function(t) {
                            var n, s, o, r, a;
                            for (t = e(t), o = t.attr("label"), o && (r = g(t) || {}, r[l] = o, r[c] = o, i.optgroups[o] = r), a = e("option", t), n = 0, s = a.length; s > n; n++) v(a[n], o)
                        };
                    for (i.maxItems = t.attr("multiple") ? null : 1, p = t.children(), d = 0, u = p.length; u > d; d++) h = p[d].tagName.toLowerCase(), "optgroup" === h ? y(p[d]) : "option" === h && v(p[d])
                };
            return this.each(function() {
                if (!this.selectize) {
                    var s, o = e(this),
                        r = this.tagName.toLowerCase(),
                        a = o.attr("placeholder") || o.attr("data-placeholder");
                    a || n.allowEmptyOption || (a = o.children('option[value=""]').text());
                    var l = {
                        placeholder: a,
                        options: {},
                        optgroups: {},
                        items: []
                    };
                    "select" === r ? u(o, l) : d(o, l), s = new F(o, e.extend(!0, {}, i, l, t))
                }
            })
        }, e.fn.selectize.defaults = F.defaults, F.define("drag_drop", function() {
            if (!e.fn.sortable) throw new Error('The "drag_drop" plugin requires jQuery UI "sortable".');
            if ("multi" === this.settings.mode) {
                var t = this;
                t.lock = function() {
                    var e = t.lock;
                    return function() {
                        var i = t.$control.data("sortable");
                        return i && i.disable(), e.apply(t, arguments)
                    }
                }(), t.unlock = function() {
                    var e = t.unlock;
                    return function() {
                        var i = t.$control.data("sortable");
                        return i && i.enable(), e.apply(t, arguments)
                    }
                }(), t.setup = function() {
                    var i = t.setup;
                    return function() {
                        i.apply(this, arguments);
                        var n = t.$control.sortable({
                            items: "[data-value]",
                            forcePlaceholderSize: !0,
                            disabled: t.isLocked,
                            start: function(e, t) {
                                t.placeholder.css("width", t.helper.css("width")), n.css({
                                    overflow: "visible"
                                })
                            },
                            stop: function() {
                                n.css({
                                    overflow: "hidden"
                                });
                                var i = t.$activeItems ? t.$activeItems.slice() : null,
                                    s = [];
                                n.children("[data-value]").each(function() {
                                    s.push(e(this).attr("data-value"))
                                }), t.setValue(s), t.setActiveItem(i)
                            }
                        })
                    }
                }()
            }
        }), F.define("dropdown_header", function(t) {
            var i = this;
            t = e.extend({
                title: "Untitled",
                headerClass: "selectize-dropdown-header",
                titleRowClass: "selectize-dropdown-header-title",
                labelClass: "selectize-dropdown-header-label",
                closeClass: "selectize-dropdown-header-close",
                html: function(e) {
                    return '<div class="' + e.headerClass + '"><div class="' + e.titleRowClass + '"><span class="' + e.labelClass + '">' + e.title + '</span><a href="javascript:void(0)" class="' + e.closeClass + '">&times;</a></div></div>'
                }
            }, t), i.setup = function() {
                var n = i.setup;
                return function() {
                    n.apply(i, arguments), i.$dropdown_header = e(t.html(t)), i.$dropdown.prepend(i.$dropdown_header)
                }
            }()
        }), F.define("optgroup_columns", function(t) {
            var i = this;
            t = e.extend({
                equalizeWidth: !0,
                equalizeHeight: !0
            }, t), this.getAdjacentOption = function(t, i) {
                var n = t.closest("[data-group]").find("[data-selectable]"),
                    s = n.index(t) + i;
                return s >= 0 && s < n.length ? n.eq(s) : e()
            }, this.onKeyDown = function() {
                var e = i.onKeyDown;
                return function(t) {
                    var n, s, o, r;
                    return !this.isOpen || t.keyCode !== c && t.keyCode !== h ? e.apply(this, arguments) : (i.ignoreHover = !0, r = this.$activeOption.closest("[data-group]"), n = r.find("[data-selectable]").index(this.$activeOption), r = t.keyCode === c ? r.prev("[data-group]") : r.next("[data-group]"), o = r.find("[data-selectable]"), s = o.eq(Math.min(o.length - 1, n)), void(s.length && this.setActiveOption(s)))
                }
            }();
            var n = function() {
                    var e, t = n.width,
                        i = document;
                    return "undefined" == typeof t && (e = i.createElement("div"), e.innerHTML = '<div style="width:50px;height:50px;position:absolute;left:-50px;top:-50px;overflow:auto;"><div style="width:1px;height:100px;"></div></div>', e = e.firstChild, i.body.appendChild(e), t = n.width = e.offsetWidth - e.clientWidth, i.body.removeChild(e)), t
                },
                s = function() {
                    var s, o, r, a, l, c, d;
                    if (d = e("[data-group]", i.$dropdown_content), o = d.length, o && i.$dropdown_content.width()) {
                        if (t.equalizeHeight) {
                            for (r = 0, s = 0; o > s; s++) r = Math.max(r, d.eq(s).height());
                            d.css({
                                height: r
                            })
                        }
                        t.equalizeWidth && (c = i.$dropdown_content.innerWidth() - n(), a = Math.round(c / o), d.css({
                            width: a
                        }), o > 1 && (l = c - a * (o - 1), d.eq(o - 1).css({
                            width: l
                        })))
                    }
                };
            (t.equalizeHeight || t.equalizeWidth) && (T.after(this, "positionDropdown", s), T.after(this, "refreshOptions", s))
        }), F.define("remove_button", function(t) {
            if ("single" !== this.settings.mode) {
                t = e.extend({
                    label: "&times;",
                    title: "Remove",
                    className: "remove",
                    append: !0
                }, t);
                var i = this,
                    n = '<a href="javascript:void(0)" class="' + t.className + '" tabindex="-1" title="' + S(t.title) + '">' + t.label + "</a>",
                    s = function(e, t) {
                        var i = e.search(/(<\/[^>]+>\s*)$/);
                        return e.substring(0, i) + t + e.substring(i)
                    };
                this.setup = function() {
                    var o = i.setup;
                    return function() {
                        if (t.append) {
                            var r = i.settings.render.item;
                            i.settings.render.item = function() {
                                return s(r.apply(this, arguments), n)
                            }
                        }
                        o.apply(this, arguments), this.$control.on("click", "." + t.className, function(t) {
                            if (t.preventDefault(), !i.isLocked) {
                                var n = e(t.currentTarget).parent();
                                i.setActiveItem(n), i.deleteSelection() && i.setCaret(i.items.length)
                            }
                        })
                    }
                }()
            }
        }), F.define("restore_on_backspace", function(e) {
            var t = this;
            e.text = e.text || function(e) {
                return e[this.settings.labelField]
            }, this.onKeyDown = function() {
                var i = t.onKeyDown;
                return function(t) {
                    var n, s;
                    return t.keyCode === m && "" === this.$control_input.val() && !this.$activeItems.length && (n = this.caretPos - 1, n >= 0 && n < this.items.length) ? (s = this.options[this.items[n]], this.deleteSelection(t) && (this.setTextboxValue(e.text.apply(this, [s])), this.refreshOptions(!0)), void t.preventDefault()) : i.apply(this, arguments)
                }
            }()
        }), F
    }), ! function(e, t) {
        function i() {
            function i(t) {
                var i = t || e.event,
                    n = i.keyCode || i.which;
                if (-1 !== [9, 13, 32, 27].indexOf(n)) {
                    for (var s = i.target || i.srcElement, o = -1, r = 0; r < _.length; r++)
                        if (s === _[r]) {
                            o = r;
                            break
                        }
                    9 === n ? (s = -1 === o ? $ : o === _.length - 1 ? _[0] : _[o + 1], N(i), s.focus(), a(s, u.confirmButtonColor)) : (s = 13 === n || 32 === n ? -1 === o ? $ : void 0 : 27 !== n || k.hidden || "none" === k.style.display ? void 0 : k, void 0 !== s && I(s, i))
                }
            }

            function r(t) {
                var i = t || e.event,
                    n = i.target || i.srcElement,
                    s = i.relatedTarget,
                    o = x(m, "visible");
                if (o) {
                    var r = -1;
                    if (null !== s) {
                        for (var a = 0; a < _.length; a++)
                            if (s === _[a]) {
                                r = a;
                                break
                            } - 1 === r && n.focus()
                    } else f = n
                }
            }
            if (void 0 === arguments[0]) return e.console.error("sweetAlert expects at least 1 attribute!"), !1;
            var u = o({}, y);
            switch (typeof arguments[0]) {
                case "string":
                    u.title = arguments[0], u.text = arguments[1] || "", u.type = arguments[2] || "";
                    break;
                case "object":
                    if (void 0 === arguments[0].title) return e.console.error('Missing "title" argument!'), !1;
                    u.title = arguments[0].title, u.text = arguments[0].text || y.text, u.type = arguments[0].type || y.type, u.customClass = arguments[0].customClass || u.customClass, u.allowOutsideClick = arguments[0].allowOutsideClick || y.allowOutsideClick, u.showCancelButton = void 0 !== arguments[0].showCancelButton ? arguments[0].showCancelButton : y.showCancelButton, u.closeOnConfirm = void 0 !== arguments[0].closeOnConfirm ? arguments[0].closeOnConfirm : y.closeOnConfirm, u.closeOnCancel = void 0 !== arguments[0].closeOnCancel ? arguments[0].closeOnCancel : y.closeOnCancel, u.timer = arguments[0].timer || y.timer, u.confirmButtonText = y.showCancelButton ? "Confirm" : y.confirmButtonText, u.confirmButtonText = arguments[0].confirmButtonText || y.confirmButtonText, u.confirmButtonColor = arguments[0].confirmButtonColor || y.confirmButtonColor, u.cancelButtonText = arguments[0].cancelButtonText || y.cancelButtonText, u.imageUrl = arguments[0].imageUrl || y.imageUrl, u.imageSize = arguments[0].imageSize || y.imageSize, u.doneFunction = arguments[1] || null;
                    break;
                default:
                    return e.console.error('Unexpected type of argument! Expected "string" or "object", got ' + typeof arguments[0]), !1
            }
            n(u), d(), l();
            for (var m = b(), g = function(t) {
                    var i = t || e.event,
                        n = i.target || i.srcElement,
                        o = "confirm" === n.className,
                        r = x(m, "visible"),
                        a = u.doneFunction && "true" === m.getAttribute("data-has-done-function");
                    switch (i.type) {
                        case "mouseover":
                            o && (n.style.backgroundColor = s(u.confirmButtonColor, -.04));
                            break;
                        case "mouseout":
                            o && (n.style.backgroundColor = u.confirmButtonColor);
                            break;
                        case "mousedown":
                            o && (n.style.backgroundColor = s(u.confirmButtonColor, -.14));
                            break;
                        case "mouseup":
                            o && (n.style.backgroundColor = s(u.confirmButtonColor, -.04));
                            break;
                        case "focus":
                            var l = m.querySelector("button.confirm"),
                                d = m.querySelector("button.cancel");
                            o ? d.style.boxShadow = "none" : l.style.boxShadow = "none";
                            break;
                        case "click":
                            if (o && a && r) u.doneFunction(!0), u.closeOnConfirm && c();
                            else if (a && r) {
                                var h = String(u.doneFunction).replace(/\s/g, ""),
                                    p = "function(" === h.substring(0, 9) && ")" !== h.substring(9, 10);
                                p && u.doneFunction(!1), u.closeOnCancel && c()
                            } else c()
                    }
                }, v = m.querySelectorAll("button"), w = 0; w < v.length; w++) v[w].onclick = g, v[w].onmouseover = g, v[w].onmouseout = g, v[w].onmousedown = g, v[w].onfocus = g;
            h = t.onclick, t.onclick = function(t) {
                var i = t || e.event,
                    n = i.target || i.srcElement,
                    s = m === n,
                    o = E(m, n),
                    r = x(m, "visible"),
                    a = "true" === m.getAttribute("data-allow-ouside-click");
                !s && !o && r && a && c()
            };
            var $ = m.querySelector("button.confirm"),
                k = m.querySelector("button.cancel"),
                _ = m.querySelectorAll("button:not([type=hidden])");
            p = e.onkeydown, e.onkeydown = i, $.onblur = r, k.onblur = r, e.onfocus = function() {
                e.setTimeout(function() {
                    void 0 !== f && (f.focus(), f = void 0)
                }, 0)
            }
        }

        function n(t) {
            var i = b(),
                n = i.querySelector("h2"),
                s = i.querySelector("p"),
                o = i.querySelector("button.cancel"),
                r = i.querySelector("button.confirm");
            if (n.innerHTML = _(t.title).split("\n").join("<br>"), s.innerHTML = _(t.text || "").split("\n").join("<br>"), t.text && C(s), t.customClass && $(i, t.customClass), D(i.querySelectorAll(".icon")), t.type) {
                for (var l = !1, c = 0; c < v.length; c++)
                    if (t.type === v[c]) {
                        l = !0;
                        break
                    }
                if (!l) return e.console.error("Unknown alert type: " + t.type), !1;
                var d = i.querySelector(".icon." + t.type);
                switch (C(d), t.type) {
                    case "success":
                        $(d, "animate"), $(d.querySelector(".tip"), "animateSuccessTip"), $(d.querySelector(".long"), "animateSuccessLong");
                        break;
                    case "error":
                        $(d, "animateErrorIcon"), $(d.querySelector(".x-mark"), "animateXMark");
                        break;
                    case "warning":
                        $(d, "pulseWarning"), $(d.querySelector(".body"), "pulseWarningIns"), $(d.querySelector(".dot"), "pulseWarningIns")
                }
            }
            if (t.imageUrl) {
                var u = i.querySelector(".icon.custom");
                u.style.backgroundImage = "url(" + t.imageUrl + ")", C(u);
                var h = 80,
                    p = 80;
                if (t.imageSize) {
                    var f = t.imageSize.split("x")[0],
                        m = t.imageSize.split("x")[1];
                    f && m ? (h = f, p = m, u.css({
                        width: f + "px",
                        height: m + "px"
                    })) : e.console.error("Parameter imageSize expects value with format WIDTHxHEIGHT, got " + t.imageSize)
                }
                u.setAttribute("style", u.getAttribute("style") + "width:" + h + "px; height:" + p + "px")
            }
            i.setAttribute("data-has-cancel-button", t.showCancelButton), t.showCancelButton ? o.style.display = "inline-block" : D(o), t.cancelButtonText && (o.innerHTML = _(t.cancelButtonText)), t.confirmButtonText && (r.innerHTML = _(t.confirmButtonText)), r.style.backgroundColor = t.confirmButtonColor, a(r, t.confirmButtonColor), i.setAttribute("data-allow-ouside-click", t.allowOutsideClick);
            var g = t.doneFunction ? !0 : !1;
            i.setAttribute("data-has-done-function", g), i.setAttribute("data-timer", t.timer)
        }

        function s(e, t) {
            e = String(e).replace(/[^0-9a-f]/gi, ""), e.length < 6 && (e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]), t = t || 0;
            var i, n, s = "#";
            for (n = 0; 3 > n; n++) i = parseInt(e.substr(2 * n, 2), 16), i = Math.round(Math.min(Math.max(0, i + i * t), 255)).toString(16), s += ("00" + i).substr(i.length);
            return s
        }

        function o(e, t) {
            for (var i in t) t.hasOwnProperty(i) && (e[i] = t[i]);
            return e
        }

        function r(e) {
            var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);
            return t ? parseInt(t[1], 16) + ", " + parseInt(t[2], 16) + ", " + parseInt(t[3], 16) : null
        }

        function a(e, t) {
            var i = r(t);
            e.style.boxShadow = "0 0 2px rgba(" + i + ", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"
        }

        function l() {
            var e = b();
            A(w(), 10), C(e), $(e, "showSweetAlert"), k(e, "hideSweetAlert"), u = t.activeElement;
            var i = e.querySelector("button.confirm");
            i.focus(), setTimeout(function() {
                $(e, "visible")
            }, 500);
            var n = e.getAttribute("data-timer");
            "null" !== n && "" !== n && (e.timeout = setTimeout(function() {
                c()
            }, n))
        }

        function c() {
            var i = b();
            M(w(), 5), M(i, 5), k(i, "showSweetAlert"), $(i, "hideSweetAlert"), k(i, "visible");
            var n = i.querySelector(".icon.success");
            k(n, "animate"), k(n.querySelector(".tip"), "animateSuccessTip"), k(n.querySelector(".long"), "animateSuccessLong");
            var s = i.querySelector(".icon.error");
            k(s, "animateErrorIcon"), k(s.querySelector(".x-mark"), "animateXMark");
            var o = i.querySelector(".icon.warning");
            k(o, "pulseWarning"), k(o.querySelector(".body"), "pulseWarningIns"), k(o.querySelector(".dot"), "pulseWarningIns"), e.onkeydown = p, t.onclick = h, u && u.focus(), f = void 0, clearTimeout(i.timeout)
        }

        function d() {
            var e = b();
            e.style.marginTop = O(b())
        }
        var u, h, p, f, m = ".sweet-alert",
            g = ".sweet-overlay",
            v = ["error", "warning", "info", "success"],
            y = {
                title: "",
                text: "",
                type: null,
                allowOutsideClick: !1,
                showCancelButton: !1,
                closeOnConfirm: !0,
                closeOnCancel: !0,
                confirmButtonText: "OK",
                confirmButtonColor: "#AEDEF4",
                cancelButtonText: "Cancel",
                imageUrl: null,
                imageSize: null,
                timer: null
            },
            b = function() {
                return t.querySelector(m)
            },
            w = function() {
                return t.querySelector(g)
            },
            x = function(e, t) {
                return new RegExp(" " + t + " ").test(" " + e.className + " ")
            },
            $ = function(e, t) {
                x(e, t) || (e.className += " " + t)
            },
            k = function(e, t) {
                var i = " " + e.className.replace(/[\t\r\n]/g, " ") + " ";
                if (x(e, t)) {
                    for (; i.indexOf(" " + t + " ") >= 0;) i = i.replace(" " + t + " ", " ");
                    e.className = i.replace(/^\s+|\s+$/g, "")
                }
            },
            _ = function(e) {
                var i = t.createElement("div");
                return i.appendChild(t.createTextNode(e)), i.innerHTML
            },
            S = function(e) {
                e.style.opacity = "", e.style.display = "block"
            },
            C = function(e) {
                if (e && !e.length) return S(e);
                for (var t = 0; t < e.length; ++t) S(e[t])
            },
            T = function(e) {
                e.style.opacity = "", e.style.display = "none"
            },
            D = function(e) {
                if (e && !e.length) return T(e);
                for (var t = 0; t < e.length; ++t) T(e[t])
            },
            E = function(e, t) {
                for (var i = t.parentNode; null !== i;) {
                    if (i === e) return !0;
                    i = i.parentNode
                }
                return !1
            },
            O = function(e) {
                e.style.left = "-9999px", e.style.display = "block";
                var t, i = e.clientHeight;
                return t = "undefined" != typeof getComputedStyle ? parseInt(getComputedStyle(e).getPropertyValue("padding"), 10) : parseInt(e.currentStyle.padding), e.style.left = "", e.style.display = "none", "-" + parseInt(i / 2 + t) + "px"
            },
            A = function(e, t) {
                if (+e.style.opacity < 1) {
                    t = t || 16, e.style.opacity = 0, e.style.display = "block";
                    var i = +new Date,
                        n = function() {
                            e.style.opacity = +e.style.opacity + (new Date - i) / 100, i = +new Date, +e.style.opacity < 1 && setTimeout(n, t)
                        };
                    n()
                }
                e.style.display = "block"
            },
            M = function(e, t) {
                t = t || 16, e.style.opacity = 1;
                var i = +new Date,
                    n = function() {
                        e.style.opacity = +e.style.opacity - (new Date - i) / 100, i = +new Date, +e.style.opacity > 0 ? setTimeout(n, t) : e.style.display = "none"
                    };
                n()
            },
            I = function(i) {
                if (MouseEvent) {
                    var n = new MouseEvent("click", {
                        view: e,
                        bubbles: !1,
                        cancelable: !0
                    });
                    i.dispatchEvent(n)
                } else if (t.createEvent) {
                    var s = t.createEvent("MouseEvents");
                    s.initEvent("click", !1, !1), i.dispatchEvent(s)
                } else t.createEventObject ? i.fireEvent("onclick") : "function" == typeof i.onclick && i.onclick()
            },
            N = function(t) {
                "function" == typeof t.stopPropagation ? (t.stopPropagation(), t.preventDefault()) : e.event && e.event.hasOwnProperty("cancelBubble") && (e.event.cancelBubble = !0)
            };
        e.sweetAlertInitialize = function() {
                var e = '<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert" tabIndex="-1"><div class="icon error"><span class="x-mark"><span class="line left"></span><span class="line right"></span></span></div><div class="icon warning"> <span class="body"></span> <span class="dot"></span> </div> <div class="icon info"></div> <div class="icon success"> <span class="line tip"></span> <span class="line long"></span> <div class="placeholder"></div> <div class="fix"></div> </div> <div class="icon custom"></div> <h2>Title</h2><p>Text</p><button class="cancel" tabIndex="2">Cancel</button><button class="confirm" tabIndex="1">OK</button></div>',
                    i = t.createElement("div");
                i.innerHTML = e, t.body.appendChild(i)
            }, e.sweetAlert = e.swal = function() {
                var e = arguments;
                if (null !== b()) i.apply(this, e);
                else var t = setInterval(function() {
                    null !== b() && (clearInterval(t), i.apply(this, e))
                }, 100)
            }, e.swal.setDefaults = function(e) {
                if (!e) throw new Error("userParams is required");
                if ("object" != typeof e) throw new Error("userParams has to be a object");
                o(y, e)
            },
            function() {
                "complete" === t.readyState || "interactive" === t.readyState && t.body ? e.sweetAlertInitialize() : t.addEventListener ? t.addEventListener("DOMContentLoaded", function() {
                    t.removeEventListener("DOMContentLoaded", arguments.callee, !1), e.sweetAlertInitialize()
                }, !1) : t.attachEvent && t.attachEvent("onreadystatechange", function() {
                    "complete" === t.readyState && (t.detachEvent("onreadystatechange", arguments.callee), e.sweetAlertInitialize())
                })
            }()
    }(window, document);