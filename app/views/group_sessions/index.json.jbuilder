json.array!(@group_sessions) do |group_session|
  json.extract! group_session, :id, :title, :price, :capacity, :description, :expert_profile_id
  json.url group_session_url(group_session, format: :json)
end
