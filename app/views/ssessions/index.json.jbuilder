json.array!(@ssessions) do |ssession|
  json.extract! ssession, :id, :reservation_id, :date, :time
  json.url ssession_url(ssession, format: :json)
end
