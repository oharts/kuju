json.array!(@expert_profiles) do |expert_profile|
  json.extract! expert_profile, :id, :add1, :add2, :city, :state, :zip, :avatar, :first_name_text, :last_name_text, :display_name, :expert_type, :foo, :availability, :about, :price, :session_length, :experience, :ages_suitable, :skill_levels, :travel, :studio_facility, :credentials, :listed, :user_id
  json.url expert_profile_url(expert_profile, format: :json)
end
