json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :email, :token, :user_id
  json.url reservation_url(reservation, format: :json)
end
