class ExpertProfile < ActiveRecord::Base
    has_many :users, dependent: :destroy
    has_many :group_sessions, dependent: :destroy
   accepts_nested_attributes_for :users
   
    scope :listed, -> { where(listed: 't') }
    scope :unlisted, -> { where(listed: 'f') }
    
     has_many :attachments, dependent: :destroy
    accepts_nested_attributes_for :attachments,  allow_destroy: true
   has_many :reservations
end
