class Reservation < ActiveRecord::Base
    belongs_to :user
    has_many :ssessions
    belongs_to :expert_profile
    accepts_nested_attributes_for :ssessions,  allow_destroy: true 
    
    def self.make_payment(params = {})
        customer = Stripe::Customer.create(
    email: params[:user][:email],
    card:  params[:stripe_card_token]
  )

  charge = Stripe::Charge.create(
    :customer    => customer.id,
    :amount      => 50,
    :description => 'Rails Stripe customer',
    :currency    => 'usd'
  )
  

rescue Stripe::CardError => e
  flash[:error] = e.message
  redirect_to new_registration_path
    end
    
end
