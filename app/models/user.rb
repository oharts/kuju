class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauth_providers => [:facebook, :google_oauth2]
  before_create :create_profile

      
  scope :buyers, -> { where(type: 'Buyer') }
	scope :experts, -> { where(type: 'Expert') }
	scope :admins, -> { where(admin_check: 'true') }

belongs_to :expert_profile
has_many :reservations
accepts_nested_attributes_for :reservations,  allow_destroy: true

def create_profile
    self.expert_profile=ExpertProfile.create
  end
  
 def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.first_name = auth.info.name
      user.email= auth.info.email if auth.info.email
    end
  end
  
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end
  
  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end
  
end
