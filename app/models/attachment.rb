class Attachment < ActiveRecord::Base
    
    belongs_to :expert_profile
 
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/png"] },size: { in: 0..4.megabytes }

    has_attached_file :media, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment :media, content_type: { content_type: ["image/jpeg", "image/png"] },size: { in: 0..4.megabytes }

end
