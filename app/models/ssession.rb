class Ssession < ActiveRecord::Base
    belongs_to :reservation
    
    before_create :set_dateformat
    
    scope :past_sessions, ->  { where('date < ?', Date.today.to_s) }
    scope :future_sessions, ->  { where('date >= ?', Date.today.to_s) }
    
    def set_dateformat
    
    self.date =Date.strptime(self.date, '%m/%d/%Y') 

    end
    
end
