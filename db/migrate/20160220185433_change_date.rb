class ChangeDate < ActiveRecord::Migration
  def change
    change_column :users, :born_at, :string
  end
end
