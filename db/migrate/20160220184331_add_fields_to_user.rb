class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :born_at, :datetime
    add_column :users, :email_preferences, :string
  end
end
