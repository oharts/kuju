class CreateGroupSessions < ActiveRecord::Migration
  def change
    create_table :group_sessions do |t|
      t.string :title
      t.string :price
      t.string :capacity
      t.string :description
      t.integer :expert_profile_id

      t.timestamps null: false
    end
  end
end
