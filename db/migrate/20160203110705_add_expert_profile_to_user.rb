class AddExpertProfileToUser < ActiveRecord::Migration
  def change
    add_column :users, :expert_profile_id, :integer
  end
end
