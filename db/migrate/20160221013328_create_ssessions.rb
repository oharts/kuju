class CreateSsessions < ActiveRecord::Migration
  def change
    create_table :ssessions do |t|
      t.integer :reservation_id
      t.string :date
      t.string :time

      t.timestamps null: false
    end
  end
end
