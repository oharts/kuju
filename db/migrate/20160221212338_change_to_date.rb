class ChangeToDate < ActiveRecord::Migration
  def change
    change_column :ssessions, :date, :date
  end
end
