class CreateExpertProfiles < ActiveRecord::Migration
  def change
    create_table :expert_profiles do |t|
      t.string :add1
      t.string :add2
      t.string :city
      t.string :state
      t.string :zip
      t.string :avatar
      t.string :first_name_text
      t.string :last_name_text
      t.string :display_name
      t.string :expert_type
      t.string :foo
      t.string :availability
      t.string :about
      t.string :price
      t.string :session_length
      t.string :experience
      t.string :ages_suitable
      t.string :skill_levels
      t.boolean :travel
      t.boolean :studio_facility
      t.text :credentials
      t.string :listed
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
