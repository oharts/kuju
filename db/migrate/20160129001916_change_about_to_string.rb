class ChangeAboutToString < ActiveRecord::Migration
  def change
    change_column :profiles, :about, :string
  end
end
