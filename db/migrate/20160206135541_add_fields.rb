class AddFields < ActiveRecord::Migration
  def change
    add_column :users, :add1, :string
    add_column :users, :add2, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :zip, :string
    add_column :users, :phone, :string
    add_column :users, :sex, :string
    add_column :users, :npi, :string
  end
end
