class RemoveEmailFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :email
  end
end
