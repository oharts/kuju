class CanceledSession < ActiveRecord::Migration
  def change
    add_column :ssessions, :canceled, :string
  end
end
