class AddListingToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :listed, :string
  end
end
