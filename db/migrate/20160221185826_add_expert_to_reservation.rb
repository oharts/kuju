class AddExpertToReservation < ActiveRecord::Migration
  def change
    add_column :reservations, :expert_profile_id, :integer
  end
end
